#  Wall Street Journal - Media Kit
This repo contains the redesign and CMS for the WSJ - MediaKit [website](http://www.wsjmediakit.com).
This is a [Laravel Homestead](http://laravel.com/docs/4.2/homestead) + [Laravel 4.2](http://laravel.com/docs/4.2) project. 
The Homestead Tech Stack is listed [here](http://laravel.com/docs/4.2/homestead#included-software) 


Wall Street Journal Media Kit - http://wsjmediakit.com/

* Upgraded Wall Street Journal's media kit website to a modular-content CMS & Website Builder using Laravel 4.2 
* Managed team of developers to meet deadlines and install features.

## Environments
- Production
	- Site: http://www.wsjmediakit.com
	- Admin: http://www.wsjmediakit.com/admin/

## Project Layout

It uses PHP’s Laravel MVC framework (version 4.2), and abides pretty closely by its conventions in terms of structure, dependency management, etc.

### Important controllers
Site content is handled in terms of modules and module locations and the relationship between the two. All page content controllers are located at app/controllers/admin/page-content. All front end controllers are located at app/controllers

Data Models are located at app/models. The Models that handle module data are located at app/models/modules. The Models that handle the module relationship are app/models/ModuleLocation.php and app/models/ModuleLocationRelationship.php 

### Config files
These are handled by both .env files and environment directories under app/config

### Database folder and migrations
Migration scripts and seed scripts are stored at app/database

The ability for environment aware development is required. This readme will assume a "development", "staging" and "production" environment. Basic proficiency with Laravel 4.2 and Homestead 1 or 2 is required. Follow the steps below for setup:

## Installation Instructions

## Step 1: Get the Homestead Environment Up and Running - Development
The Homestead repo is located [here](https://github.com/laravel/homestead). Follow the instructions [here](http://laravel.com/docs/4.2/homestead) to get Homestead up and running.
Keep in mind that Homestead is dependent upon [Vagrant](http://www.vagrantup.com/downloads.html) and [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and basic setup and proficiency in these two are required.

Your Homestead config doc should be similar to this:

    ---
    ip: "192.168.10.10"
    memory: 2048
    cpus: 1

    authorize: /Users/someDevsMacPcOrLinuxBox/.ssh/id_rsa.pub
    
    keys:
        - /Users/someDevsMacPcOrLinuxBox/.ssh/id_rsa
    
    folders:
        - map: /Users/someDevsMacPcOrLinuxBox/PhpstormProjects
          to: /home/vagrant/projects

    sites:
        - map: vm.wsj2.local
          to: /home/vagrant/projects/work/wsj-mediakit2/public
          provisionScript: /home/vagrant/projects/work/wsj-mediakit2/provision/bash/setup.sh
          appHome: /home/vagrant/projects/work/wsj-mediakit2
    
    variables:
        - key: APPLICATION_ENV
          value: homestead

The two variables `provisionScript` and `appHome` are a helpful customization which you can read about [here](http://stackoverflow.com/questions/23841089/run-provisioning-scripts-per-project-in-laravel-homestead/25419459#25419459)


## Step 2: Clone the WSJ Mediakit files - Development
Install the project to your chosen location outside of your Homestead repo for your development environment

    cd /to/your/chosen/project/folder
    git clone git@github.com:hyfn/wsj-mediakit-2.git
    

## Step 3: Use Composer to install project dependencies for Laravel and WSJ-Mediakit project

    cd /to/your/chosen/project/folder
    composer self-update
	composer install
	
	
## Step 4: Dependencies
 - Zizaco Entrust	
 You will need to 'enable' this dependency and publish its package. Follow, the steps to do so [here](https://github.com/Zizaco/entrust)
	

## Step 5: Configure Your Environments
As mentioned before this project is designed to be environment aware and Homestead enables this. These are the supported environments:

    homestead - Consider this your local
    development
    staging
    production
    
To ensure the project and Homestead are properly configured we used both environment specific folders and environment (.env.*.php) files.


#### Item 1: Configure environment specific folders
Now that your project has been cloned by git, there should be three environment folders under app/config:

 - app/config/homestead
 - app/config/development
 - app/config/staging
 
Each has an app.php file with options that need to be edited to match each environment. The file app/config/app.php is the config by default and is used for production. It must be checked as well.


#### Item 2: Use Environment Aware config files
Create each of the following:

    .env.homestead.php
    .env.development.php
    .env.staging.php
    .env.php - this is for production!

All environment specific variables must be placed in these files. Each file is to be placed in the relevant environment location except for homestead. That stays in each devs local environment. 
Here is an example of the .env.homestead.php file (all .env files are placed at root of the project)

To use the variables within, using database credentials as an example,
create the environment file and add the necessary variables tht are specific to that environment:

.env.homestead.php
------------------

    <?php

    return  array
    (
        'APPLICATION_ENV'               =>  'homestead',
        'APP_ENV'                       =>  'homestead',

        /**
         * DB_CONNECTIONS
         */
        'DB_CONNECTION_db1_driver'      =>  'mysql',
        'DB_CONNECTION_db1_host'        =>  '[ENTER HOST HERE]',
        'DB_CONNECTION_db1_database'    =>  '[ENTER DATABASE NAME HERE]',
        'DB_CONNECTION_db1_username'    =>  '[ENTER DATABASE USERNAME HERE]',
        'DB_CONNECTION_db1_password'    =>  '[ENTER DATABASE USER PASSWORD HERE]',
        'DB_CONNECTION_db1_charset'     =>  'utf8',
        'DB_CONNECTION_db1_collation'   =>  'utf8_unicode_ci',
        'DB_CONNECTION_db1_prefix'      =>  '',
    
        /**
         * Mail Settings
         */
        'MAIL_domain'                   =>  '[ENTER MAIL PROVIDER ACCESS DOMIAN HERE]',
        'MAIL_secret'                   =>  '[ENTER MAIL PROVIDER SECRET HERE],
    
        'GA_property'                   =>  '[ENTER GOOGLE ANALYTICS ID HERE]',
        );

You can add all need-to-be-hidden, environment specific variables such as API keys, mail settings, logging settings, etc here and
replace the default entries with the environment variables you create. In addition, this method is the defined way of
identifying the environment. Only one file, which is explicitly placed, should be on the server
at a time. If more than one file is present, the hierarchy is designated in `bootstrap/start.php` under Detect The Application Environment

## Step 6: Setup permissions ###

Your webserver needs to be able to write to the cache storage. If necessary:

    chown -R youruser:www-data .

This code and all provisioning code should be placed in `provision/bash/setup.sh` if you used the custom setup described above


## Actual Development Workflow

    cd /to/your/chosen/HOMESTEAD/folder
    vagrant up
    
Once this runs without error

    vagrant ssh
    cd /to/your/vagrant/based/project/folder
    
This is specified in your Homestead config file; see above. Homestead should have run your provisioning and deployment scripts. Here are our examples:

Provisioning
---------------

    #!/bin/sh
    
    echo "============================================"
    echo "Homestead - PROVISIONING"
    echo "============================================"
    
    echo "Homestead - Security Checks & Updates"
    env VAR='() { :;}; echo Bash is vulnerable!' bash -c "echo Bash is safe"
    sudo apt-get update -y -q
    sudo apt-get install php5-intl -y -q
    
    echo "============================================"
    echo "Homestead - Enabling XDebug."
    echo "============================================"
    echo '\n '                          >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.remote_port=9000'      >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.remote_mode=req'       >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.remote_host=127.0.0.1' >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.remote_handler=dbgp'   >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.remote_connect_back=1' >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.remote_enable=1'       >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.remote_autostart=0'    >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.max_nesting_level=400' >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.idekey=PHPSTORM'       >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.default_enable=1'      >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.cli_color=1'           >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.scream=0'              >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo 'xdebug.show_local_vars=1'     >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo '\n '                          >> /etc/php5/fpm/conf.d/20-xdebug.ini
    echo "------------------------------------------"
    echo "Homestead - Enabled XDebug."
    echo "------------------------------------------"


Deployment
------------
See `provision/bash/setup.sh` for our homestead deployment script. For other environments, development, staging, production, we use DeployHQ. Our DeployHQ deployment sequences for each environment are as follows. 
Note that the sequence is order dependent and the commands use deployHQ variables:

#### Production

    php %path%/artisan down                                             // Enter maintenance mode
    cd %path% && composer install --prefer-dist                         // Install New Composer Dependencies
    cd %path% && composer dump-autoload                                 // Update Composer Autoload Files
    rm -rf %path%/app/storage/views/*                                   // Clear view cache
    php %path%/artisan optimize --force                                 // Optimize the framework
    php %path%/artisan migrate --no-interaction --env=%environment%     // Migrate the DB
    php %path%/artisan up                                               // Exit maintenance mode
    php %path%/artisan search:create  --env=%environment%               // Update Search Index

#### Staging

    php %path%/artisan down                                             // Enter maintenance mode
    cd %path% && composer install --prefer-dist                         // Install New Composer Dependencies
    cd %path% && composer dump-autoload                                 // Update Composer Autoload Files
    rm -rf %path%/app/storage/views/*                                   // Clear view cache
    php %path%/artisan optimize --force                                 // Optimize the framework
    php %path%/artisan migrate:refresh --seed --env=%environment%       // Migrate & Seed the DB
    php %path%/artisan up                                               // Exit maintenance mode
    php %path%/artisan search:create  --env=%environment%               // Update Search Index

#### Development

    php %path%/artisan down                                             // Enter maintenance mode
    cd %path% && composer install                                       // Install New Composer Dependencies
    cd %path% && composer update                                        // Install New Composer Dependencies
    cd %path% && composer dump-autoload                                 // Update Composer Autoload Files
    rm -rf %path%/app/storage/views/*                                   // Clear view cache
    php %path%/artisan optimize --force                                 // Optimize the framework
    php %path%/artisan migrate:refresh --seed --env=%environment%       // Migrate & Seed the DB
    php %path%/artisan up                                               // Exit maintenance mode
    php %path%/artisan search:create  --env=%environment%               // Update Search Index
    
The variables to replace are

    %path%
    %environment%

        
These commands can also be directly run on the command line once the server prompt appears after you've run `vagrant ssh` in your terminal.        
    
## 

## Frontend

This project uses [gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md) for all front-end compilation of css and javascript.  Once you have the application set up, you will need to install Node and Gulp to make any front end changes. 

- install http://www.nodejs.org/   (Node)
- install http://gulpjs.com/ (Gulp)

Once you have installed both, cd to the root of the project and run npm install to pull down any node modules the project requires.  Assuming things worked properly, you are now ready to run gulp to start watching (and compiling/minifying/concatenating files).

To make gulp watch your css and js directories, cd to the root of the project and simply type gulp.  Now any time you make changes to a css or coffeescript file, your browser should update automatically with the new changes.

### Assets - General guidelines
The current /src/ directory structure tries to group scss and js per module, or logical site section.  Because of this, you will find the .coffee and .scss files for a particular module or element in either the /src/coffee/components or /src/coffee/modules folders.   The ‘coffee’ in the path is only meant to distinguish .coffee files from .js compiled files.  Both scss and .coffee can live in any module or component folder.

### Assets - Images
All icons are placed in /src/assets/img/pngs.  Any images in this folder will be converted into a sprite, so make sure anything added is a 24-bit png.  Once you add any new sprite images, you will want to stop your current gulp watch process and run gulp sprite, which will add all images from the /pngs/ directory into a sprite.  Once complete, run gulp again to resume work.

Non-icon images can be placed in their respective folders within the /src/assets/img/ folder. 
Assuming you are running the gulp task, all newly added images will be copied to the destination folder (/public/assets/frontend/img).  All files are minified as they are moved to the destination folder, so you shouldn’t have to do anything beyond adding images to the folder.


### Assets - Javascript
All elements in the site are either modules (Header, footer, slideshow, etc…) or components (search field, analytics, etc…).   Most modules are comprised of components, so if you are creating a new element for the site and it requires javascript, you will most likely create a new module that contains components for individual pieces of functionality.   All current modules and components are currently in /src/coffee.  When you are running the gulp task, files are converted from .coffee to .js, then minified and concatenated, and finally moved into the /public/assets/frontend/js/ folder.  

If you want to look at the compiled .js files for reference, they are stored in /src/assets/js in an unminified state.  These files are useful for debugging coffeescript errors.


### Assets - SCSS/CSS
All css for the site uses scss, which is automatically compiled when running gulp.  Scss files are stored in their respective module folder, for example: scss for the global search element will be found in /src/coffee/modules/global-search/global-search.coffee.  When adding a new css file, you will want to add a reference to it in the css manifest located at /src/styles/main.scss.  When you have a new stylesheet to add to the project, simply find the section of main.scss that closest categorizes your new css (modules, components, vendor plugins, etc…) and import the scss file like this: @import 'vendor/chosen-overrides';.  Note: you may have to stop and start gulp for it to recognize your new file.  At this point, gulp will include, minify and concatenate all .css and move it to the destination directory /public/assets/frontend/styles.

Most dimensions in the site are either fluid (%-based) or rem based.  The base font size for the project in rems is 10, so rem-based values are easy to calculate using our rem-fallback mixin: @include rem-fallback(max-width, 47.5); would compile to max-width: 475px.  Please try to avoid the use of px-based values.

The site was built desktop-first, so assume any general formatting added will be outside of a media query.  When overriding formatting for mobile or tablets, use the following format :

```scss
.item {
color: red;

    @include breakpoint(tablet) {
      color: green;
    }

    @include breakpoint(mobile) {
      color: blue;
    }
}
```

This code will show the .item element in red with green on tablets and blue on mobile.

### Abbreviated Installation Steps

1. Make sure you have node installed (I suggest using NVM, but you can use the standard installer).
2. In terminal, navigate to the root of the project (you should be in the folder with gulpfile.js).
3. Run npm install which should bring down all node modules required for the project.
4. If things worked properly, you should be able to type 'gulp' (without quotes) and your js and css will be minified and concatenated on save.  You will also have support for live reload if you have the browser add on.
5.  When ready to deploy, you can run 'gulp build' which will clean out the front end files and replace with a current build.
