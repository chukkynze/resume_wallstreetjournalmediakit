<?php
    /**
     * filename:   CustomUserValidator.php
     *
     * @author      Chukky J. Nze <chukky.nze@hyfn.com>
     * @since       9/12/14 9:56 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Library;

    use Hash;

    use Zizaco\Confide\UserValidatorInterface;
    use Zizaco\Confide\ConfideUserInterface;

    class CustomUserValidator implements UserValidatorInterface
    {
        public function validate(ConfideUserInterface $user)
        {
            /**
             * This is the problem with using too many "cool" packages
             * Confide validates the user model and Watson validates all models
             * When Confide removes password_confirmation after successful validation,
             * Watson throws a fit and fails the user model...
             * but Watson doesn't remove password_confirmation and thus craps out bad SQL
             * plus...password has to be hashed BEFORE the watson insert
             *
             * @author  Chukky J. Nze <chukky.nze@hyfn.com>
             * @since   9/12/14 9:56 PM
             */
            #unset($user->password_confirmation);
            return true; // If the user valid
        }

    }
 