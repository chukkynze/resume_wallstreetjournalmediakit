<?php
    // System Routes
    App::missing(function ($exception) {
        return Response::make(View::make("errors.404", [
            'cmsName'       => 'WSJ Media Kit CMS',
            'copyrightName' => 'WSJ Media Kit',
            'currentPath'   => 'error-page',
            'sourceText'    => '',
            'siteProductSections' => [],
            'menuProducts'  => [
                0 => [
                    'name'        => "Products",
                    'link'        => "products",
                    'listClasses' => "",
                    'linkClasses' => "",
                    'subLinks'    => [],
                ],
                1 => [
                    'name'        => "WSJ. Insights",
                    'link'        => "wsjinsights",
                    'listClasses' => "",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ],
                2 => [
                    'name'        => "WSJ. Custom Studios",
                    'link'        => "wsjcustomstudios",
                    'listClasses' => "",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ],
                3 => [
                    'name'        => "Rates &amp; Specs",
                    'link'        => "rates_specs",
                    'listClasses' => "",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ]


                ,
                4 => [
                    'name'        => "Gallery",
                    'link'        => "gallery",
                    'listClasses' => "primary-menu-link mobile-hidden",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ],

            ]

        ]));

    });

    /**
     * Maintain URLs from the old version of this site
     */
    Route::group(['domain' => 'wsjcustomstudios.com'], function() {
        Route::get('{all}', function() { return Redirect::to('http://wsjmediakit.com/wsjcustomstudios'); })->where('all', '.*');
    });
    Route::get('classified', function() { return Redirect::to('https://classifieds.wsj.com/'); });
    Route::get('conferences', function() { return Redirect::to('/products/conferences'); });
    Route::get('custom', function() { return Redirect::to('/wsjcustomstudios'); });
    Route::get('digital', function() { return Redirect::to('/products/online'); });
    Route::get('digital_submit', function() { return Redirect::to('/products/online'); });
    Route::get('global', function() { return Redirect::to('/products/global'); });
    Route::get('home', function() { return Redirect::to('/'); });
    Route::get('mobile', function() { return Redirect::to('/products/mobile'); });
    Route::get('mobile_submit', function() { return Redirect::to('/products/mobile'); });
    Route::get('newspaper', function() { return Redirect::to('/products/newspaper'); });
    Route::get('pdf_preparation', function() { return Redirect::to('/rates_specs'); });
    Route::get('video', function() { return Redirect::to('/products/video'); });
    Route::get('wsjmagazine', function() { return Redirect::to('/products/wsjmagazine'); });
    Route::get('journalreport', function() { return Redirect::to('/products/journalreport'); });
    Route::get('wsjweekend', function() { return Redirect::to('/products/newspaper'); });
    Route::get('classified/contact', function() { return Redirect::to('/contact'); });
    Route::get('conferences/ceocouncil', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/cfonetwork', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/cioconference', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/economics', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/compliance', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/heardonstreet', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/privateequityanalysts', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/unleashinginnovation', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/viewpoints', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/wsjdlive', function() { return Redirect::to('/products/conferences'); });
    Route::get('conferences/contact', function() { return Redirect::to('/contact'); });
    Route::get('custom/contact', function() { return Redirect::to('/contact'); });
    Route::get('digital/products', function() { return Redirect::to('/products/online'); });
    Route::get('digital/demographics', function() { return Redirect::to('/products/online'); });
    Route::get('digital/targeting', function() { return Redirect::to('/products/online'); });
    Route::get('digital/showcase', function() { return Redirect::to('/products/online'); });
    Route::get('digital/specs', function() { return Redirect::to('/rates_specs'); });
    Route::get('digital/research', function() { return Redirect::to('/wsjinsights'); });
    Route::get('digital/calendar', function() { return Redirect::to('/products/online'); });
    Route::get('digital/termsandconditions', function() { return Redirect::to('/products/online'); });
    Route::get('digital/contact', function() { return Redirect::to('/contact'); });
    Route::get('global/gfr', function() { return Redirect::to('/products/global'); });
    Route::get('global/asia', function() { return Redirect::to('/products/global'); });
    Route::get('global/europe', function() { return Redirect::to('/products/global'); });
    Route::get('global/americas', function() { return Redirect::to('/products/global'); });
    Route::get('global/contact', function() { return Redirect::to('/contact'); });
    Route::get('global/specs', function() { return Redirect::to('/rates_specs'); });
    Route::get('mobile/contact', function() { return Redirect::to('/contact'); });
    Route::get('mobile/research', function() { return Redirect::to('/wsjinsights'); });
    Route::get('mobile/showcase', function() { return Redirect::to('/mobile'); });
    Route::get('mobile/smartphones', function() { return Redirect::to('/products/mobile'); });
    Route::get('mobile/specs', function() { return Redirect::to('/rates_specs'); });
    Route::get('mobile/tablets', function() { return Redirect::to('/products/mobile'); });
    Route::get('newspaper/contact', function() { return Redirect::to('/contact'); });
    Route::get('newspaper/greaternewyork', function() { return Redirect::to('/newspaper'); });
    Route::get('newspaper/research', function() { return Redirect::to('/wsjinsights'); });
    Route::get('newspaper/specs', function() { return Redirect::to('/rates_specs'); });
    Route::get('newspaper/wsjweekend', function() { return Redirect::to('/newspaper'); });
    Route::get('video/specs', function() { return Redirect::to('/rates_specs'); });
    Route::get('wsjmagazine/contact', function() { return Redirect::to('/contact'); });
    Route::get('wsjmagazine/specs', function() { return Redirect::to('/rates_specs'); });
    Route::get('pj', function() { return Redirect::to('/newspaper'); });
    Route::get('sites/arena', function() { return Redirect::to('/newspaper'); });
    Route::get('sites/mansion', function() { return Redirect::to('/newspaper'); });
    Route::get('sites/offduty', function() { return Redirect::to('/newspaper'); });
    Route::Get('/ceocouncil', function() { return Redirect::to('/sites/ceocouncil/index.html'); });
    Route::Get('/GNY', function() { return Redirect::to('/newspaper/greaternewyork'); });
    Route::Get('/innovatorfujimoto', function() { return Redirect::to('/sites/innovatorfujimoto/index.html'); });
    Route::Get('/powerofprint', function() { return Redirect::to('/sites/powerofprint/index.html'); });
    Route::Get('/propertyportfoliosweeps', function() { return Redirect::to('/download.php?file=2014_Property_Portfolio_Agent_Sweeps_Rules.pdf'); });
    Route::Get('/wsjmagazine/luxuryescapes', function() { return Redirect::to('/sites/luxuryescapes/index.html'); });

    Route::get('products/wsj-magazine', function() { return Redirect::to('/products/wsjmagazine'); });
    Route::get('products/journal-report', function() { return Redirect::to('/products/journalreport'); });
    Route::get('insights', function() { return Redirect::to('/wsjinsights'); });
    Route::get('wsj-customstudios', function() { return Redirect::to('/wsjcustomstudios'); });

    Route::Get('/sitemap', function() { return Redirect::to('/'); });

    Route::get('download.php', function() {
        switch(Input::get('file')) {
            case '2014_Property_Portfolio_Agent_Sweeps_Rules.pdf':
                return Redirect::to('/files/2014_Property_Portfolio_Agent_Sweeps_Rules.pdf');
                break;
            case 'Dow_Jones_2009_CS4.zip':
                return Redirect::to('/files/Dow_Jones_2009_CS4.zip');
                break;
            case 'SNAP_2009_ICC.zip':
                return Redirect::to('/files/SNAP_2009_ICC.zip');
                break;
            default:
                App::abort(404);
                break;
        }
    });

    /**
     * Landing Pages
     */
    Route::group(['prefix' => '/'], function () {
        Route::get('', 'Hyfn\\Controllers\\HomeController@index');

        Route::get('products', 'Hyfn\\Controllers\\HomeController@showProducts');
        Route::get('products/{siteProductSlug}', 'Hyfn\\Controllers\\HomeController@showProductDetails');

        Route::get('wsjinsights', 'Hyfn\\Controllers\\HomeController@showInsights');

        Route::get('wsjcustomstudios', 'Hyfn\\Controllers\\HomeController@showStudios');

        Route::get('gallery', 'Hyfn\\Controllers\\HomeController@showCaseStudies');
        Route::get('gallery/{id}', 'Hyfn\\Controllers\\HomeController@showCaseStudy');

        Route::get('contact', 'Hyfn\\Controllers\\HomeController@showContact');
        Route::get('rates_specs', 'Hyfn\\Controllers\\HomeController@showRate');


        Route::get('search', 'Hyfn\\Controllers\\SearchController@showSearchResult');
        Route::get('gallery-search', 'Hyfn\\Controllers\\SearchController@showGallerySearchResult');

        Route::post('download/{upload_id}', 'Hyfn\\Controllers\\DownloadController@contact');

        Route::group(['prefix' => 'preview'], function () {
            Route::get('', 'Hyfn\\Controllers\\HomeController@previewIndex');
            Route::get('products', 'Hyfn\\Controllers\\HomeController@previewProducts');
            Route::get('products/{siteProductSlug}', 'Hyfn\\Controllers\\HomeController@previewProductDetails');
            Route::get('insights', 'Hyfn\\Controllers\\HomeController@previewInsights');
            Route::get('wsjcustomstudios', 'Hyfn\\Controllers\\HomeController@previewStudios');
            Route::get('gallery', 'Hyfn\\Controllers\\HomeController@previewCaseStudies');
            Route::get('gallery/{id}', 'Hyfn\\Controllers\\HomeController@previewCaseStudy');
            Route::get('rates_specs', 'Hyfn\\Controllers\\HomeController@previewRate');
        });
    });


    /**
     * Admin Routes
     */
    Route::group(array('prefix' => 'admin'), function () {
        // Admin routes with no auth
        Route::get('login', 'Hyfn\\Controllers\\Admin\\UserController@showLogin');
        Route::post('login', 'Hyfn\\Controllers\\Admin\\UserController@postLogin');
        Route::get('logout', 'Hyfn\\Controllers\\Admin\\UserController@logout');

        Route::group(array('prefix' => 'users'), function () {
            Route::get('forgot-password', 'Hyfn\\Controllers\\Admin\\UserController@showForgotPassword');
            Route::post('forgot-password', 'Hyfn\\Controllers\\Admin\\UserController@postForgotPassword');
            Route::get('confirm/{code}', 'Hyfn\\Controllers\\Admin\\UserController@confirm');
            Route::get('reset-password/{token}', 'Hyfn\\Controllers\\Admin\\UserController@showResetPassword');
            Route::post('reset-password', 'Hyfn\\Controllers\\Admin\\UserController@postResetPassword');
        });

        /**
         * Resource Extras
         */
        Route::post('/save-assigned-permission',
            'Hyfn\\Controllers\\Admin\\PermissionController@saveAssignedPermission');
        Route::post('/save-assigned-role', 'Hyfn\\Controllers\\Admin\\RoleController@saveAssignedRole');


        Route::get('rates_specs/get-rate-specs/{rateProductID}',
            'Hyfn\\Controllers\\Admin\\RateProductController@getRateSpecificationByRateProduct');


        Route::get('site-products/get-all', 'Hyfn\\Controllers\\Admin\\SiteProductController@getSiteProducts');
        Route::get('rate-products/get-all', 'Hyfn\\Controllers\\Admin\\RateProductController@getRateProducts');
        Route::get('contact-products/get-all', 'Hyfn\\Controllers\\Admin\\ContactProductController@getContactProducts');


        Route::get('contacts/get-locations/{siteProductID}',
            'Hyfn\\Controllers\\Admin\\ContactController@getContactLocationsByContactProduct');
        Route::get('contacts/{contactID}/re-order',
            'Hyfn\\Controllers\\Admin\\ContactController@reOrderContact');
        Route::get('contacts/get-all/{contactLocationID}/{siteProductID}',
            'Hyfn\\Controllers\\Admin\\ContactController@getContactsByLocation');
        Route::get('gallery-filter-items/filter/{filterID}',
            'Hyfn\\Controllers\\Admin\\CaseStudyFilterItemController@getFilterItemsByFilterID');


        Route::get('gallery/{caseStudyID}/add-modules', 'Hyfn\\Controllers\\Admin\\CaseStudyController@manageModules');

        // Admin routes that require auth
        Route::group(array('before' => 'auth'), function () {
            Route::get('/', 'Hyfn\\Controllers\\AdminController@index');
            /**
             * Resources
             */


            Route::group(array('prefix' => 'tasks'), function () {
                Route::get('get-leads', 'Hyfn\\Controllers\\Admin\\TaskController@getLeadsCsv');
            });

            Route::group(array('prefix' => 'page-content'), function () {
                Route::resource('insight-pages', 'Hyfn\\Controllers\\Admin\\PageContent\\InsightPageController');
                Route::resource('studios-pages', 'Hyfn\\Controllers\\Admin\\PageContent\\StudiosPageController');
                Route::resource('product-pages', 'Hyfn\\Controllers\\Admin\\PageContent\\ProductPageController');
                Route::get('home-pages/preview', 'Hyfn\\Controllers\\Admin\\PageContent\\HomePageController@preview');
                Route::post('home-pages/save-preview-data',
                    'Hyfn\\Controllers\\Admin\\PageContent\\HomePageController@savePreviewData');
                Route::resource('home-pages', 'Hyfn\\Controllers\\Admin\\PageContent\\HomePageController');
                Route::resource('home-page-sliders', 'Hyfn\\Controllers\\Admin\\PageContent\\HomePageSliderController');

                /**
                 * Module Resource
                 */
                Route::post('modules/create', 'Hyfn\\Controllers\\Admin\\PageContent\\ModuleController@createModule');
                Route::delete('modules/{moduleRelationshipID}',
                    'Hyfn\\Controllers\\Admin\\PageContent\\ModuleController@destroyModule');
                Route::post('modules', 'Hyfn\\Controllers\\Admin\\PageContent\\ModuleController@storeModule');
                Route::get('modules/{moduleRelationshipID}/edit',
                    'Hyfn\\Controllers\\Admin\\PageContent\\ModuleController@editModule');
                Route::put('modules/{moduleRelationshipID}',
                    'Hyfn\\Controllers\\Admin\\PageContent\\ModuleController@updateModule');
                Route::post('modules/save-order',
                    'Hyfn\\Controllers\\Admin\\PageContent\\ModuleController@saveModuleOrder');
            });

            Route::post('upload-categories/save-index-order',
                'Hyfn\\Controllers\\Admin\\UploadCategoryController@saveIndexOrder');

            Route::post('site-products/save-index-order',
                'Hyfn\\Controllers\\Admin\\SiteProductController@saveIndexOrder');

            Route::post('site-product-sections/save-index-order',
                'Hyfn\\Controllers\\Admin\\SiteProductSectionController@saveIndexOrder');

            Route::post('rate-products/save-index-order',
                'Hyfn\\Controllers\\Admin\\RateProductController@saveIndexOrder');

            Route::post('contact-products/save-index-order',
                'Hyfn\\Controllers\\Admin\\ContactProductController@saveIndexOrder');

            Route::post('contact-locations/save-index-order',
                'Hyfn\\Controllers\\Admin\\ContactLocationController@saveIndexOrder');

            Route::post('gallery/save-index-order',
                'Hyfn\\Controllers\\Admin\\CaseStudyController@saveIndexOrder');

            Route::post('contacts/save-index-order',
                'Hyfn\\Controllers\\Admin\\ContactController@saveIndexOrder');

            Route::post('gallery/publish', 'Hyfn\\Controllers\\Admin\\CaseStudyController@publishOrHideCaseStudy');
            Route::post('gallery/preview-listing',
                'Hyfn\\Controllers\\Admin\\CaseStudyController@sessionPreviewListing');
            Route::get('gallery/preview-listing', 'Hyfn\\Controllers\\Admin\\CaseStudyController@previewListing');

            Route::post('gallery-filters/save-index-order',
                'Hyfn\\Controllers\\Admin\\CaseStudyFilterController@saveIndexOrder');

            Route::post('gallery-filter-items/save-index-order',
                'Hyfn\\Controllers\\Admin\\CaseStudyFilterItemController@saveIndexOrder');

            Route::resource('clients', 'Hyfn\\Controllers\\Admin\\ClientController');

            Route::resource('gallery', 'Hyfn\\Controllers\\Admin\\CaseStudyController');
            Route::resource('gallery-filters', 'Hyfn\\Controllers\\Admin\\CaseStudyFilterController');
            Route::get('gallery-filter-items/filter/{filterID}/create',
                'Hyfn\\Controllers\\Admin\\CaseStudyFilterItemController@createFilterItem');
            Route::resource('gallery-filter-items', 'Hyfn\\Controllers\\Admin\\CaseStudyFilterItemController');

            Route::resource('contact-locations', 'Hyfn\\Controllers\\Admin\\ContactLocationController');
            Route::resource('contacts', 'Hyfn\\Controllers\\Admin\\ContactController');
            Route::resource('insights', 'Hyfn\\Controllers\\Admin\\InsightController');
            Route::resource('permissions', 'Hyfn\\Controllers\\Admin\\PermissionController');
            Route::resource('products', 'Hyfn\\Controllers\\Admin\\ProductsController');
            Route::resource('roles', 'Hyfn\\Controllers\\Admin\\RoleController');
            Route::resource('rates_specs', 'Hyfn\\Controllers\\Admin\\RateController');

            Route::resource('site-products', 'Hyfn\\Controllers\\Admin\\SiteProductController');
            Route::resource('rate-products', 'Hyfn\\Controllers\\Admin\\RateProductController');
            Route::resource('contact-products', 'Hyfn\\Controllers\\Admin\\ContactProductController');

            Route::get('site-product-sections/product/{productID}/view',
                'Hyfn\\Controllers\\Admin\\SiteProductSectionController@viewSectionIndex');
            Route::get('site-product-sections/product/{productID}/create',
                'Hyfn\\Controllers\\Admin\\SiteProductSectionController@createSection');
            Route::resource('site-product-sections', 'Hyfn\\Controllers\\Admin\\SiteProductSectionController');
            Route::resource('uploads', 'Hyfn\\Controllers\\Admin\\UploadController');
            Route::resource('upload-categories', 'Hyfn\\Controllers\\Admin\\UploadCategoryController');
            Route::resource('users', 'Hyfn\\Controllers\\Admin\\UserController');
        });

    });


    /**
     * API routes
     */
    Route::group(array('prefix' => 'api', 'before' => 'auth.api'), function () {

        Route::group(array('prefix' => 'v1'), function () {

            Route::post('login', 'ApiV1UserController@login');

            // Require Auth Token before access
            Route::group(array('before' => 'auth.token'), function () {

                Route::resource('users', 'ApiV1UserController');
                Route::any('/logout', 'ApiV1UserController@logout');

            });

        });

    });
