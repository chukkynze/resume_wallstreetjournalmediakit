<?php

    use Illuminate\Console\Command;
    use Symfony\Component\Console\Input\InputOption;
    use Symfony\Component\Console\Input\InputArgument;

    class CmsRefreshCommand extends Command
    {

        /**
         * The console command name.
         *
         * @var string
         */
        protected $name = 'cms:refresh';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Will refresh the migrations and re-seed the app';

        /**
         * Create a new command instance.
         *
         *
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return void
         */
        public function fire()
        {
            $this->call('migrate:refresh');
            $this->call('db:seed');
        }

        /**
         * Get the console command arguments.
         *
         * @return array
         */
        protected function getArguments()
        {
            return array(//array('example', InputArgument::REQUIRED, 'An example argument.'),
            );
        }

        /**
         * Get the console command options.
         *
         * @return array
         */
        protected function getOptions()
        {
            return array(//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            );
        }

    }