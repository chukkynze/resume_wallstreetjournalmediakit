<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       IndexSearchDataCommand
     *
     * filename:    IndexSearchDataCommand.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     *
     * Note:
     *   Currently, the search index is updated every time an upload or a module is inserted ot updated. Yes, it takes a few seconds
     *   and here are 2 options to make it faster:
     *
     * 1. Cron Jobs
     * ------------------------------------------------------
     * To create a cron job as root, edit your cron file:
     * [sudo] crontab -e
     *
     * Add a this line at the end:
     * 00 10 * * * /usr/local/bin/php /home/deploy/sites/wsj-mediakit-2-dev.hyfnrsx1.com/artisan es:create
     *
     * Dev
     * 5 * * * * php /home/deploy/sites/wsj-mediakit-2-dev.hyfnrsx1.com/artisan search:create >> /home/deploy/sites/wsj-mediakit-2-dev.hyfnrsx1.com/cronlog.log
     *
     * Stage
     * 5 * * * * php /home/deploy/sites/wsj-mediakit-2-stage.hyfnrsx1.com/artisan search:create >> /home/deploy/sites/wsj-mediakit-2-stage.hyfnrsx1.com/cronlog.log
     *
     * Prod
     * 5 * * * * php /usr/www/users/autopp05/wsjmediakit.com/artisan search:create  >> /usr/www/users-autopp05-wsjmediakit.com-cronlog.log
     *
     * Make sure you keep a blank line after the last one.
     *
     * 2. Queues
     * ------------------------------------------------------
     * Install a queue service such as Beanstalkd or IronMQ
     * Update the method AdminController@updateSearchIndexes to push to a queue instead of firing the search:create command directly
     *
     * To check if the search index actually ran and for how long, grep the logs for any word permutation of "Search Index Creation" after you uncomment the Log lines
     *
     *
     * Also, note that Gallery isn't active yet so case studies and related modules are not currently indexed. To allow them to be indexed
     * search for "Uncomment to allow gallery search indexing of case studies" in this file and do so
     */

    use Hyfn\Models\SearchData;
    use Hyfn\Models\CaseStudy;
    use Hyfn\Models\ModuleLocationRelationship;

    use Illuminate\Console\Command;
    use Symfony\Component\Console\Input\InputOption;
    use Symfony\Component\Console\Input\InputArgument;

    /**
     * Class IndexSearchDataCommand
     */
    class IndexSearchDataCommand extends Command
    {
        /**
         * The console command name.
         *
         * @var string
         */
        protected $name = 'search:create';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Will run the logic to create a fresh search index.';

        /**
         * Create a new command instance.
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function fire()
        {
            // Get Start Time
            $startTime  =   strtotime("now");
            $this->comment("\n");
            $this->comment("Search Index Creation started at " . date("m-d-Y h:i:s", $startTime) . "\n");
            //Log::info("Search Index Creation started at " . date("m-d-Y h:i:s", $startTime) . "");

            DB::table('search_data')->truncate();

            // Run logic to create pages and media documents for (Elastic)search to index (create & update)
            $urlEnv         =   [
                "local" => "http://wsj-mediakit-2-dev.hyfnrsx1.com",
                                    "homestead"     =>  "http://wsj-mediakit-2-dev.hyfnrsx1.com",
                                    "development"   =>  "http://wsj-mediakit-2-dev.hyfnrsx1.com",
                                    "staging"       =>  "http://wsj-mediakit-2-stage.hyfnrsx1.com",
                                    "production"    =>  "http://wsjmediakit.com",
                                ];

            if(isset($_ENV['APP_ENV']) && array_key_exists($_ENV['APP_ENV'], $urlEnv))
            {
                $site           =   $urlEnv[$_ENV['APP_ENV']];
                $currentPages   =   [
                                        /*
                                        [
                                            'key'               =>  'products',
                                            'url'               =>  '/products',
                                            'defaultTitle'      =>  "Products",
                                            'defaultDesc'       =>  "This is the Product Landing page.",
                                            'defaultKeywords'   =>  "This is the Product Landing page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  0,
                                        ],*/
                                        [
                                            'key'               =>  'products-conferences',
                                            'url'               =>  '/products/conferences',
                                            'defaultTitle'      =>  "Conferences",
                                            'defaultDesc'       =>  "This is the conferences page.",
                                            'defaultKeywords'   =>  "This is the conferences page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'products-newspaper',
                                            'url'               =>  '/products/newspaper',
                                            'defaultTitle'      =>  "Newspaper",
                                            'defaultDesc'       =>  "This is the contacts page.",
                                            'defaultKeywords'   =>  "This is the contacts page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'products-online',
                                            'url'               =>  '/products/online',
                                            'defaultTitle'      =>  "Online",
                                            'defaultDesc'       =>  "This is the online page.",
                                            'defaultKeywords'   =>  "This is the online page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'products-global',
                                            'url'               =>  '/products/global',
                                            'defaultTitle'      =>  "Global",
                                            'defaultDesc'       =>  "This is the global page.",
                                            'defaultKeywords'   =>  "This is the global page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'products-mobile',
                                            'url'               =>  '/products/mobile',
                                            'defaultTitle'      =>  "Mobile",
                                            'defaultDesc'       =>  "This is the mobile page.",
                                            'defaultKeywords'   =>  "This is the mobile page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'products-video',
                                            'url'               =>  '/products/video',
                                            'defaultTitle'      =>  "Video",
                                            'defaultDesc'       =>  "This is the video page.",
                                            'defaultKeywords'   =>  "This is the video page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'products-wsjmagazine',
                                            'url'               =>  '/products/wsjmagazine',
                                            'defaultTitle'      =>  "WSJ. Magazine",
                                            'defaultDesc'       =>  "This is the wsjmagazine page.",
                                            'defaultKeywords'   =>  "This is the wsjmagazine page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'products-journalreport',
                                            'url'               =>  '/products/journalreport',
                                            'defaultTitle'      =>  "Journal Report",
                                            'defaultDesc'       =>  "This is the journalreport page.",
                                            'defaultKeywords'   =>  "This is the journalreport page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'products-marketwatch',
                                            'url'               =>  '/products/marketwatch',
                                            'defaultTitle'      =>  "MarketWatch",
                                            'defaultDesc'       =>  "This is the MarketWatch page.",
                                            'defaultKeywords'   =>  "This is the MarketWatch page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'insights',
                                            'url'               =>  '/wsjinsights',
                                            'defaultTitle'      =>  "WSJ. Insights",
                                            'defaultDesc'       =>  "This is the insights page.",
                                            'defaultKeywords'   =>  "This is the insights page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        [
                                            'key'               =>  'studios',
                                            'url'               =>  '/wsjcustomstudios',
                                            'defaultTitle'      =>  "WSJ. Custom Studios",
                                            'defaultDesc'       =>  "This is the studios page.",
                                            'defaultKeywords'   =>  "This is the studios page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  1,
                                        ],
                                        /*
                                        [
                                            'key'               =>  'rates',
                                            'url'               =>  '/rates_specs',
                                            'defaultTitle'      =>  "Rates & Specs",
                                            'defaultDesc'       =>  "This is the rates page.",
                                            'defaultKeywords'   =>  "This is the rates page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  0,
                                        ],
                                        [
                                            'key'               =>  'contacts',
                                            'url'               =>  '/contact',
                                            'defaultTitle'      =>  "Contacts",
                                            'defaultDesc'       =>  "This is the contacts page.",
                                            'defaultKeywords'   =>  "This is the contacts page.",
                                            'resultType'        =>  "page",
                                            'searchDescOnPg'    =>  0,
                                        ],*/
                                    ];

                $pageCount = 0;
                foreach($currentPages as $pageDataArray)
                {
                    //echo "Creating data for " . $pageDataArray['key'] . " page.\n";
                    $docLoopStartTime  =   strtotime("now");
                    //echo "Starting at " . date("m-d-Y h:i:s", $docLoopStartTime) . "\n";
                    $this->comment("Indexing data from " . $site . $pageDataArray['url'] . "");

                    $pageData                   =   $this->get_web_page($site, $pageDataArray);
                    $SearchData                 =   new SearchData();
                    $SearchData->title          =   $pageDataArray['defaultTitle'];
                    $SearchData->type           =   $pageDataArray['resultType'];
                    $SearchData->body           =   $pageData['clean_content'];
                    $SearchData->description    =   $pageData['defaultDesc'];
                    $SearchData->keywords       =   $pageData['defaultDesc'] != "" ? $pageData['defaultDesc'] : $pageDataArray['defaultKeywords'];
                    $SearchData->url            =   $site.$pageDataArray['url'];
                    $SearchData->object_id = 0;

                    if ($SearchData->isModelValid()) {
                        $SearchData->save();
                    } else {
                        $this->error("<pre>" . print_r($SearchData->getErrors(), 1) . "</pre>\n");
                    }

                    //$docLoopEndTime    =   strtotime("now");
                    //echo "Ending loop in " . ($docLoopEndTime - $docLoopStartTime) . " seconds\n";
                    $pageCount++;
                }

                $this->comment("\nIndexed data for " . $pageCount . " pages.\n\n");
            }
            else
            {
                $this->error("Error: Could not determine site for pages. Check APP_ENV variable.\n");
            }


            $uploadObjectsArray   =   \Hyfn\Models\Upload::whereRaw("`uploads`.`is_live` = 1 and  substr(`uploads`.`download_file`, -3) in (?, ?, ?)", ["pdf", "xls", "lsx"])->get();

            $docCount = 0;
            foreach($uploadObjectsArray as $uploadObject)
            {
                $this->comment("Indexing data for " . $uploadObject->download_file . ".");
                $docLoopStartTime  =   strtotime("now");
                //echo "Starting at " . date("m-d-Y h:i:s", $docLoopStartTime) . "\n";
                //echo "Pulling data from " . $uploadObject->download_file . "\n";

                $SearchData                 =   new SearchData();
                $SearchData->title          =   $uploadObject->name;
                $SearchData->type           =   substr($uploadObject->download_file, -3);
                $SearchData->body           =   $uploadObject->description;
                $SearchData->keywords       =   $uploadObject->keywords;
                $SearchData->description    =   $uploadObject->description;
                $SearchData->url            =   $site.$uploadObject->download_folder.$uploadObject->download_file;
                $SearchData->object_id = $uploadObject->id;

                if ($SearchData->isModelValid()) {
                    $SearchData->save();
                } else {
                    $this->comment("<pre>" . print_r($SearchData->getErrors(), 1) . "</pre>\n");
                }

                //$docLoopEndTime    =   strtotime("now");
                //echo "Ending loop in " . ($docLoopEndTime - $docLoopStartTime) . " seconds\n";

                $docCount++;
            }

            $this->comment("\nIndexed data for " . $docCount . " documents.\n");


            /* Uncomment to allow gallery search indexing of case studies
             $allPublishedCaseStudies        = CaseStudy::where('is_live', '=', 1)
                ->orderBy('order', 'asc')
                ->orderBy('headline', 'asc')
                ->get();
            $allPublishedAndLiveCaseStudies = [];
            foreach ($allPublishedCaseStudies as $caseStudy) {
                $caseStudyModules = ModuleLocationRelationship::where("entity_id", "=", $caseStudy->id)->
                where("entity_type", "=", "Hyfn\\Models\\CaseStudy")->
                get(['module_id', 'module_type']);

                foreach ($caseStudyModules as $module) {
                    $ModelName = $module->module_type;
                    $modelID   = $module->module_id;
                    $Model     = $ModelName::find($modelID);
                    if ($Model->is_live == 1) {
                        $allPublishedAndLiveCaseStudies[] = $caseStudy;
                        break;
                    }
                }
            }

            $itemCount = 0;
            foreach ($allPublishedAndLiveCaseStudies as $galleryObject) {
                $this->comment("Indexing data for " . $galleryObject->headline . ".");
                $docLoopStartTime = strtotime("now");
                //echo "Starting at " . date("m-d-Y h:i:s", $docLoopStartTime) . "\n";

                $galleryData             = $this->get_web_page
                (
                    $site,
                    [
                        'url' => '/gallery/' . $galleryObject->id,
                        'searchDescOnPg' => 0,
                        'defaultDesc'    => $galleryObject->description,
                    ]
                );
                $SearchData = new SearchData();
                $SearchData->title = $galleryObject->headline;
                $SearchData->type = 'gallery-listing';
                $SearchData->body = $galleryData['clean_content'];
                $SearchData->keywords = $galleryObject->keywords;
                $SearchData->description = $galleryObject->description;
                $SearchData->url = $site . '/gallery/' . $galleryObject->id;
                $SearchData->object_id = $galleryObject->id;

                if ($SearchData->isModelValid()) {
                    $SearchData->save();
                } else {
                    $this->error("<pre>" . print_r($SearchData->getErrors(), 1) . "</pre>\n");
                }

                //$docLoopEndTime    =   strtotime("now");
                //echo "Ending loop in " . ($docLoopEndTime - $docLoopStartTime) . " seconds\n";

                $itemCount++;
            }

            $this->comment("\nIndexed data for " . $itemCount . " gallery items.\n\n");
             */

            $endTime    =   strtotime("now");
            $this->comment("Search Index Creation finished at " . date("m-d-Y h:i:s", $endTime) . "");
            $this->comment("Duration: " . ($endTime - $startTime) . " seconds");
            $this->comment("\n");
            //Log::info("Search Index Creation finished at " . date("m-d-Y h:i:s", $endTime) . " Duration: " . ($endTime - $startTime) . " seconds.");
        }


        function get_web_page($site, $pageDataArray)
        {
            $options    =   [
                                'http'  =>  [
                                                'user_agent'    =>  'spider',    // who am i
                                                'max_redirects' =>  10,          // stop after 10 redirects
                                                'timeout'       =>  120,         // timeout on response
                                                'header' => 'Authorization: Basic ' . base64_encode("hyfnadmin:hyfn!2014")
                                            ]
                            ];
            $context    =   stream_context_create($options);
            $page       =   @file_get_contents($site.$pageDataArray['url'], false, $context);

            $pageCheck = preg_replace('/\s{2,}/', '--', substr($page,0, 10));
            //echo "page check => <pre>" . print_r($pageCheck, 1). "</pre>\n\n";

            $result     =  [];
            //echo "page check\n\n";
            if ( $page != false )
            {
                //echo "page is not false\n\n";

                $doc = new DOMDocument();
                //echo "doc object check 1 => <pre>" . print_r($doc, 1). "</pre>\n\n";

                libxml_use_internal_errors(true);
                $doc->loadHTML($this->hideHTags($page));
                libxml_use_internal_errors(false);
                //echo "doc object check 2 => <pre>" . print_r($doc, 1). "</pre>\n\n";


            //echo "doc object check 3 => <pre>" . print_r($doc, 1). "</pre>\n\n";

                $xpath = new DOMXpath($doc);

                //echo "Finding searchable-content \n";
                $SearchableContent          =   $doc->getElementById('searchable-content');
                //echo "1SearchableContent<pre>" . print_r($SearchableContent, 1) . "</pre>\n";


                $hiddenHTagSearchContent = (is_object($SearchableContent) ? $SearchableContent->textContent : '');


                $result['clean_content'] = preg_replace('/\s{2,}/', ' ', $hiddenHTagSearchContent);
                //echo "2SearchableContent<pre>" . print_r($result['clean_content'], 1) . "</pre>\n";

                $defaultDesc                =   "";

                if($pageDataArray['searchDescOnPg'] == 1)
                {

                    //echo "Finding search-description \n";

                    $SearchDescription      =   $doc->getElementById('search-description');
                    $allParagraphs          =   $xpath->query('.//p', $SearchDescription);

                    $x=1;
                    foreach($allParagraphs as $paragraph)
                    {
                        //echo "allParagraphs<pre>" . print_r($paragraph, 1) . "</pre>\n";
                        $defaultDesc            =   preg_replace('/\s{2,}/', ' ', $paragraph->textContent);

                        if($x==1)break;
                    }

                    //echo "Search Desc<pre>" . print_r($defaultDesc, 1) . "</pre>\n";
                }

                $result['defaultDesc']  =   $defaultDesc == "" ? $pageDataArray['defaultDesc'] : $defaultDesc;
            } elseif (!isset($http_response_header))
            {
                //echo "page is false\n\n";
                return null;
                //echo "Bad url, timeout\n";
            }    // Bad url, timeout

            //echo "page is else\n\n";

            // Save the header
            $result['header'] = $http_response_header;

            // Get the *last* HTTP status code
            $nLines = count( $http_response_header );
            for ( $i = $nLines-1; $i >= 0; $i-- )
            {
                $line = $http_response_header[$i];
                if ( strncasecmp( "HTTP", $line, 4 ) == 0 )
                {
                    $response = explode( ' ', $line );
                    $result['http_code'] = $response[1];
                    break;
                }
            }

            return $result;
        }

        /**
         * Get the console command arguments.
         *
         * @return array
         */
        protected function getArguments()
        {
            return
                [
                    //['example', InputArgument::REQUIRED, 'An example argument.'],
                ];
        }

        /**
         * Get the console command options.
         *
         * @return array
         */
        protected function getOptions()
        {
            return
                [
                    //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
                ];
        }

        public function hideHTags($text)
        {
            $text = str_replace("<h1>", "[thisUsedToBeH1]", $text);
            $text = str_replace("</h1>", "[/thisUsedToBeH1]", $text);
            $text = str_replace("<h2>", "[thisUsedToBeH2]", $text);
            $text = str_replace("</h2>", "[/thisUsedToBeH2]", $text);
            $text = str_replace("<h3>", "[thisUsedToBeH3]", $text);
            $text = str_replace("</h3>", "[/thisUsedToBeH3]", $text);

            return $text;
        }

    }
