<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       EtlProdDataToStageCommand
     *
     * filename:    EtlProdDataToStageCommand.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     *
     *
     *
     */

    use Hyfn\Models\SearchData;

    use Illuminate\Console\Command;
    use Symfony\Component\Console\Input\InputOption;
    use Symfony\Component\Console\Input\InputArgument;

    class EtlProdDataToStageCommand extends Command
    {
        /**
         * The console command name.
         *
         * @var string
         */
        protected $name = 'etl:run';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Will run an etl of prod to stage.';

        /**
         * Create a new command instance.
         */
        public function __construct()
        {
            parent::__construct();
            $this->connection = 'db1';
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function fire()
        {
            // Get Start Time
            $startTime = strtotime("now");
            echo "\n";
            echo "\n";
            echo "ETL of prod to stage started at " . date("m-d-Y h:i:s", $startTime) . "\n";

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');


            // Here's the work


            //site_product_sections;
            echo "\n\n";
            // pull from prod
            $fromProdSiteProductSectionsTBL = DB::connection('dbLIVE')->table('site_product_sections')->get();

            // truncate stg tbl
            DB::connection($this->connection)->table('site_product_sections')->truncate();

            // extract
            foreach ($fromProdSiteProductSectionsTBL as $site_product_sections) {
                //transform & load
                DB::connection($this->connection)->table('site_product_sections')->insert
                ([
                    'id'                       => $site_product_sections->id,
                    'name'                     => $site_product_sections->name,
                    'heading'                  => $site_product_sections->heading,
                    'name_slug'                => $site_product_sections->name_slug,
                    'site_product_id'          => $site_product_sections->site_product_id,
                    'enable_link'              => $site_product_sections->enable_link,
                    'enable_all_caps'          => $site_product_sections->enable_all_caps,
                    'only_headings_on_landing' => $site_product_sections->only_headings_on_landing,
                    'link_text'                => $site_product_sections->link_text,
                    'order'                    => $site_product_sections->order,
                    'created_at'               => $site_product_sections->created_at,
                    'updated_at'               => $site_product_sections->updated_at,
                    'deleted_at'               => $site_product_sections->deleted_at,
                ]);
            }
            
            
            
            

            //Users
            $fromProdUsersTBL = DB::connection('dbLIVE')->table('users')->get();

            DB::connection($this->connection)->table('users')->truncate();
            foreach ($fromProdUsersTBL as $user) {
                $User                    = new \Hyfn\Models\User();
                $User->id                = $user->id;
                $User->username          = $user->username;
                $User->email             = $user->email;
                $User->password          = $user->password;
                $User->confirmation_code = $user->confirmation_code;
                $User->remember_token    = $user->remember_token;
                $User->first_name        = $user->first_name;
                $User->last_name         = $user->last_name;
                $User->confirmed         = $user->confirmed;
                $User->created_at        = $user->created_at;
                $User->updated_at        = $user->updated_at;
                $User->deleted_at        = $user->deleted_at;

                if ($User->isModelValid()) {
                    $User->save();
                    echo "Created user " . $user->username . " <" . $user->email . ">" . "\n";
                } else {
                    echo "<pre>" . print_r($User->getErrors(), 1) . "</pre>\n";
                }
            }

            //Uploads
            echo "\n\n";
            $fromProdUploadsTBL = DB::connection('dbLIVE')->table('uploads')->get();

            DB::connection($this->connection)->table('uploads')->truncate();
            foreach ($fromProdUploadsTBL as $uploads) {
                $Upload                  = new \Hyfn\Models\Upload();
                $Upload->id              = $uploads->id;
                $Upload->name            = $uploads->name;
                $Upload->description     = $uploads->description;
                $Upload->download_file   = $uploads->download_file;
                $Upload->download_folder = $uploads->download_folder;
                $Upload->status          = $uploads->status;
                $Upload->uploader_id     = $uploads->uploader_id;
                $Upload->keywords        = $uploads->keywords;
                $Upload->category        = $uploads->category;
                $Upload->gated           = $uploads->gated;
                $Upload->is_live         = $uploads->is_live;
                $Upload->created_at      = $uploads->created_at;
                $Upload->updated_at      = $uploads->updated_at;

                if ($Upload->isModelValid()) {
                    $Upload->save();
                    echo "Created Upload " . $Upload->name . " ." . "\n";
                } else {
                    echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                }
            }

            ////Leads
            ///////////////////////////////
            echo "\n\n";
            // pull from prod
            $fromProdLeadsTBL = DB::connection('dbLIVE')->table('leads')->get();

            // truncate stg tbl
            DB::connection($this->connection)->table('leads')->truncate();

            // extract
            foreach ($fromProdLeadsTBL as $lead) {
                // transform
                $Lead             = new \Hyfn\Models\Lead();
                $Lead->id         = $lead->id;
                $Lead->email      = $lead->email;
                $Lead->first_name = $lead->first_name;
                $Lead->last_name  = $lead->last_name;
                $Lead->company    = $lead->company;
                $Lead->title      = $lead->title;
                $Lead->upload_id  = 82;
                $Lead->created_at = $lead->created_at;
                $Lead->updated_at = $lead->updated_at;

                if ($Lead->isValid()) {
                    // load
                    $Lead->save();
                    echo "Created Lead " . $Lead->first_name . " ." . "\n";
                } else {
                    echo "<pre>" . print_r($Lead->getErrors(), 1) . "</pre>\n";
                }
            }
            //////////////////////////////////////


            ////PdfListModule
            ///////////////////////////////
            echo "\n\n";
            // pull from prod
            $fromProdPdfListModuleTBL = DB::connection('dbLIVE')->table('pdf_list_modules')->get();

            // truncate stg tbl
            DB::connection($this->connection)->table('pdf_list_modules')->truncate();

            // extract
            foreach ($fromProdPdfListModuleTBL as $pdf_list_modules) {
                // transform
                $PdfListModule             = new \Hyfn\Models\Modules\PdfListModule();
                $PdfListModule->id         = $pdf_list_modules->id;
                $PdfListModule->upload_id  = $pdf_list_modules->upload_id;
                $PdfListModule->body       = $pdf_list_modules->body;
                $PdfListModule->body_2     = $pdf_list_modules->body_2;
                $PdfListModule->has_border = $pdf_list_modules->has_border;
                $PdfListModule->is_live    = $pdf_list_modules->is_live;
                $PdfListModule->title      = $pdf_list_modules->title;
                $PdfListModule->created_at = $pdf_list_modules->created_at;
                $PdfListModule->updated_at = $pdf_list_modules->updated_at;

                if ($PdfListModule->isValid()) {
                    // load
                    $PdfListModule->save();
                    echo "Created Lead " . $PdfListModule->id . " ." . "\n";
                } else {
                    echo "<pre>" . print_r($PdfListModule->getErrors(), 1) . "</pre>\n";
                }
            }
            //////////////////////////////////////


            ////TextModule
            ///////////////////////////////
            echo "\n\n";
            // pull from prod
            $fromProdTextModuleTBL = DB::connection('dbLIVE')->table('text_modules')->get();

            // truncate stg tbl
            DB::connection($this->connection)->table('text_modules')->truncate();

            // extract
            foreach ($fromProdTextModuleTBL as $text_modules) {
                // transform
                $TextModule             = new \Hyfn\Models\Modules\TextModule();
                $TextModule->id         = $text_modules->id;
                $TextModule->upload_id  = $text_modules->upload_id;
                $TextModule->body       = $text_modules->body;
                $TextModule->has_border = $text_modules->has_border;
                $TextModule->is_live    = $text_modules->is_live;
                $TextModule->title      = $text_modules->title;
                $TextModule->created_at = $text_modules->created_at;
                $TextModule->updated_at = $text_modules->updated_at;

                if ($TextModule->isValid()) {
                    // load
                    $TextModule->save();
                    echo "Created TextModule " . $TextModule->id . " ." . "\n";
                } else {
                    echo "<pre>" . print_r($TextModule->getErrors(), 1) . "</pre>\n";
                }
            }
            //////////////////////////////////////


            /**
             * This goes from stg to prod
             *
             * ////ModuleLocationRelationship
            * ///////////////////////////////
            * echo "\n\n";
            * // pull from prod
            * $fromProdModuleLocationRelationshipsTBL = DB::connection('dbLIVE')->table('module_location_relationships')->get();

            *
* // truncate stg tbl
            * DB::connection($this->connection)->table('module_location_relationships')->truncate();

            *
* // extract
            * foreach ($fromProdModuleLocationRelationshipsTBL as $module) {
                * // transform
                * $ModuleLocationRelationship              = new \Hyfn\Models\ModuleLocationRelationship();
                * $ModuleLocationRelationship->id          = $module->id;
                * $ModuleLocationRelationship->location_id = $module->location_id;
                * $ModuleLocationRelationship->entity_id   = $module->entity_id;
                * $ModuleLocationRelationship->entity_type = $module->entity_type;
                * $ModuleLocationRelationship->module_id   = $module->module_id;
                * $ModuleLocationRelationship->module_type = $module->module_type;
                * $ModuleLocationRelationship->order       = $module->order;
                * $ModuleLocationRelationship->created_at  = $module->created_at;
                * $ModuleLocationRelationship->updated_at  = $module->updated_at;

                *
* if ($ModuleLocationRelationship->isValid()) {
                    * // load
                    * $ModuleLocationRelationship->save();
                    * echo "Created ModuleLocationRelationship " . $ModuleLocationRelationship->id . " ." . "\n";
                * } else {
                    * echo "<pre>" . print_r($ModuleLocationRelationship->getErrors(), 1) . "</pre>\n";
                * }
             * }
             * //////////////////////////////////////

             */


            //permissions;
            echo "\n\n";
            // pull from prod
            $fromProdPermissionsTBL = DB::connection('dbLIVE')->table('permissions')->get();

            // truncate stg tbl
            DB::connection($this->connection)->table('permissions')->truncate();

            // extract
            foreach ($fromProdPermissionsTBL as $permissions) {
                //transform & load
                DB::connection($this->connection)->table('permissions')->insert
                ([
                    'id'           => $permissions->id,
                    'name'         => $permissions->name,
                    'display_name' => $permissions->display_name,
                    'created_at'   => $permissions->created_at,
                    'updated_at'   => $permissions->updated_at,
                ]);
            }


            //assigned_roles;
            echo "\n\n";
            // pull from prod
            $fromProdAssignedRolesTBL = DB::connection('dbLIVE')->table('assigned_roles')->get();
            // truncate stg tbl
            DB::connection($this->connection)->table('assigned_roles')->truncate();
            // extract
            foreach ($fromProdAssignedRolesTBL as $assigned_role) {
                //transform & load
                DB::connection($this->connection)->table('assigned_roles')->insert
                ([
                    'id'      => $assigned_role->id,
                    'user_id' => $assigned_role->user_id,
                    'role_id' => $assigned_role->role_id
                ]);
            }


            //permission_role;
            echo "\n\n";
            // pull from prod
            $fromProdPermissionRolesTBL = DB::connection('dbLIVE')->table('permission_role')->get();

            // truncate stg tbl
            DB::connection($this->connection)->table('permission_role')->truncate();

            // extract
            foreach ($fromProdPermissionRolesTBL as $permission_role) {
                //transform & load
                DB::connection($this->connection)->table('permission_role')->insert
                ([
                    'id'            => $permission_role->id,
                    'permission_id' => $permission_role->permission_id,
                    'role_id'       => $permission_role->role_id
                ]);
            }









            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime = strtotime("now");
            echo "ETL finished at " . date("m-d-Y h:i:s", $endTime) . "\n";
            echo "Duration: " . ($endTime - $startTime) . " seconds\n";
            echo "\n";
            echo "\n";
        }

        /**
         * Get the console command arguments.
         *
         * @return array
         */
        protected function getArguments()
        {
            return
                [
                    //['example', InputArgument::REQUIRED, 'An example argument.'],
                ];
        }

        /**
         * Get the console command options.
         *
         * @return array
         */
        protected function getOptions()
        {
            return
                [
                    //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
                ];
        }

    }
