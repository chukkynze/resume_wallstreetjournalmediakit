<?php
    # fine to remove this comment
    
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class SetupModuleTables extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            /**
             * Model: SplitTextModule
             */
            Schema::connection($this->connection)->dropIfExists('split_text_modules');
            Schema::connection($this->connection)->create('split_text_modules', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('upload_id')->unsigned();
                $table->text('col1');
                $table->text('col2');
                $table->boolean('has_border')->default(false);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: SummaryModule
             */
            Schema::connection($this->connection)->dropIfExists('summary_modules');
            Schema::connection($this->connection)->create('summary_modules', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('upload_id')->unsigned();
                $table->text('body');
                $table->integer('image_id')->unsigned();
                $table->enum('orientation', ['Left', 'Right'])->default('Left');
                $table->boolean('has_border')->default(false);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: TextColumnModule
             */
            Schema::connection($this->connection)->dropIfExists('text_column_modules');
            Schema::connection($this->connection)->create('text_column_modules', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('upload_id')->unsigned()->default(0);
                $table->text('body')->nullable();
                $table->text('column_1')->nullable();
                $table->text('column_2')->nullable();
                $table->text('column_3')->nullable();
                $table->text('column_4')->nullable();
                $table->text('column_5')->nullable();
                $table->text('column_6')->nullable();
                $table->text('column_7')->nullable();
                $table->text('column_8')->nullable();
                $table->text('body_2')->nullable();
                $table->boolean('has_border')->default(false);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: TextColumn
             */
            Schema::connection($this->connection)->dropIfExists('text_columns');
            Schema::connection($this->connection)->create('text_columns', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('text_column_module_id')->unsigned();
                $table->integer('col_id')->unsigned();
                $table->text('line');
                $table->tinyInteger('order')->unsigned()->default(255);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: ImageColumnModule
             */
            Schema::connection($this->connection)->dropIfExists('image_column_modules');
            Schema::connection($this->connection)->create('image_column_modules', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('upload_id')->unsigned()->default(0);
                $table->text('body')->nullable();
                $table->text('body_2')->nullable();
                $table->boolean('has_border')->default(false);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model:
             */
            Schema::connection($this->connection)->dropIfExists('uploadables');
            Schema::connection($this->connection)->create('uploadables', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->integer('upload_id')->unsigned();
                $table->integer('uploadable_id')->unsigned();
                $table->string('uploadable_type');
                $table->tinyInteger('order')->unsigned()->default(255);

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: TextModule
             */
            Schema::connection($this->connection)->dropIfExists('text_modules');
            Schema::connection($this->connection)->create('text_modules', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('upload_id')->unsigned()->default(0);
                $table->text('body');
                $table->boolean('has_border')->default(false);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: PdfListModule
             */
            Schema::connection($this->connection)->dropIfExists('pdf_list_modules');
            Schema::connection($this->connection)->create('pdf_list_modules', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('upload_id')->unsigned()->default(0);
                $table->text('body')->nullable();
                $table->text('body_2')->nullable();
                $table->boolean('has_border')->default(false);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: SlideshowModule
             */
            Schema::connection($this->connection)->dropIfExists('slideshow_modules');
            Schema::connection($this->connection)->create('slideshow_modules', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->boolean('has_border')->default(false);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: SingleImageModule
             */
            Schema::connection($this->connection)->dropIfExists('single_image_modules');
            Schema::connection($this->connection)->create('single_image_modules', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('upload_id')->unsigned();
                $table->boolean('target')->default(false);
                $table->string('link_url');
                $table->string('alt_text');
                $table->boolean('has_border')->default(false);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: VideoModule
             */
            Schema::connection($this->connection)->dropIfExists('video_modules');
            Schema::connection($this->connection)->create('video_modules', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('upload_id')->unsigned();
                $table->string('url');
                $table->boolean('has_border')->default(false);
                $table->boolean('is_live')->default(true);
                $table->string('title');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: ModuleLocation
             */
            Schema::connection($this->connection)->dropIfExists('module_locations');
            Schema::connection($this->connection)->create('module_locations', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');
                $table->string('slug');

                $table->timestamps();

                // Indexes

            });


            /**
             * Model: ModuleLocationRelationship
             */
            Schema::connection($this->connection)->dropIfExists('module_location_relationships');
            Schema::connection($this->connection)->create('module_location_relationships', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('location_id');

                $table->integer('entity_id');
                $table->string('entity_type');

                $table->integer('module_id');
                $table->string('module_type');

                $table->integer('order')->default(255);

                $table->timestamps();

                // Indexes

            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            /**
             * Model: ModuleLocationRelationship
             */
            Schema::connection($this->connection)->dropIfExists('module_location_relationships');

            /**
             * Model: ModuleLocation
             */
            Schema::connection($this->connection)->dropIfExists('module_locations');

            /**
             * Model: VideoModule
             */
            Schema::connection($this->connection)->dropIfExists('video_modules');

            /**
             * Model: SingleImageModule
             */
            Schema::connection($this->connection)->dropIfExists('single_image_modules');

            /**
             * Model: SlideshowModule
             */
            Schema::connection($this->connection)->dropIfExists('slideshow_modules');

            /**
             * Model: PdfListModule
             */
            Schema::connection($this->connection)->dropIfExists('pdf_list_modules');

            /**
             * Model: TextModule
             */
            Schema::connection($this->connection)->dropIfExists('text_modules');

            /**
             * Model:
             */
            Schema::connection($this->connection)->dropIfExists('uploadables');

            /**
             * Model: ImageColumnModule
             */
            Schema::connection($this->connection)->dropIfExists('image_column_modules');

            /**
             * Model: TextColumn
             */
            Schema::connection($this->connection)->dropIfExists('text_columns');

            /**
             * Model: TextColumnModule
             */
            Schema::connection($this->connection)->dropIfExists('text_column_modules');

            /**
             * Model: SummaryModule
             */
            Schema::connection($this->connection)->dropIfExists('summary_modules');

            /**
             * Model: SplitTextModule
             */
            Schema::connection($this->connection)->dropIfExists('split_text_modules');
        }

    }
