<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    class EntrustSetupTables extends Migration
    {
        protected $connection = "db1";

        /**
         * Run the migrations.
         *
         * @return  void
         */
        public function up()
        {
            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            // Creates the roles table
            Schema::connection($this->connection)->create('roles', function ($table) {
                $table->increments('id')->unsigned();
                $table->string('name')->unique();
                $table->timestamps();
            });

            // Creates the assigned_roles (Many-to-Many relation) table
            Schema::connection($this->connection)->create('assigned_roles', function ($table) {
                $table->increments('id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->integer('role_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('role_id')->references('id')->on('roles');
            });

            // Creates the permissions table
            Schema::connection($this->connection)->create('permissions', function ($table) {
                $table->increments('id')->unsigned();
                $table->string('name')->unique();
                $table->string('display_name');
                $table->timestamps();
            });

            // Creates the permission_role (Many-to-Many relation) table
            Schema::connection($this->connection)->create('permission_role', function ($table) {
                $table->increments('id')->unsigned();
                $table->integer('permission_id')->unsigned();
                $table->integer('role_id')->unsigned();
                $table->foreign('permission_id')->references('id')->on('permissions'); // assumes a users table
                $table->foreign('role_id')->references('id')->on('roles');
            });

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');
        }

        /**
         * Reverse the migrations.
         *
         * @return  void
         */
        public function down()
        {
            Schema::connection($this->connection)->table('assigned_roles', function (Blueprint $table) {
                $table->dropForeign('assigned_roles_user_id_foreign');
                $table->dropForeign('assigned_roles_role_id_foreign');
            });

            Schema::connection($this->connection)->table('permission_role', function (Blueprint $table) {
                $table->dropForeign('permission_role_permission_id_foreign');
                $table->dropForeign('permission_role_role_id_foreign');
            });

            Schema::connection($this->connection)->drop('assigned_roles');
            Schema::connection($this->connection)->drop('permission_role');
            Schema::connection($this->connection)->drop('roles');
            Schema::connection($this->connection)->drop('permissions');
        }

    }
