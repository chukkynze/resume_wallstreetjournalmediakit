<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchTables extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        /**
         * Entity: SearchData
         * Table: search_data
         */
		Schema::connection($this->connection)->dropIfExists('search_data');
		Schema::connection($this->connection)->create('search_data', function(Blueprint $table)
        {
            // Parameters
            $table->engine = 'MyISAM';

            // Columns
            $table->increments('id');
            $table->text('title');
            $table->string('type');
            $table->text('description');
            $table->text('keywords');
            $table->text('body');
            $table->string('url')->default('/');
            $table->integer('object_id');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE search_data ADD FULLTEXT search_title(title)');
        DB::statement('ALTER TABLE search_data ADD FULLTEXT search_body(body)');
        DB::statement('ALTER TABLE search_data ADD FULLTEXT search_description(description)');
        DB::statement('ALTER TABLE search_data ADD FULLTEXT search_keywords(keywords)');
        DB::statement('ALTER TABLE search_data ADD FULLTEXT search_all(title, body, description, keywords)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::connection($this->connection)->table('search_data', function($table)
	    {
            $table->dropIndex('search_title');
            $table->dropIndex('search_body');
            $table->dropIndex('search_description');
            $table->dropIndex('search_keywords');
            $table->dropIndex('search_all');
        });

        /**
         * Model: SearchData
         */
        Schema::connection($this->connection)->dropIfExists('search_data');
	}

}
