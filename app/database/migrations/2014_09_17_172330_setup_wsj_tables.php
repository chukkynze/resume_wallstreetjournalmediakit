<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class SetupWsjTables extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            /**
             * Resource:    UploadController
             * Model:       Upload
             */
            Schema::connection($this->connection)->dropIfExists('uploads');
            Schema::connection($this->connection)->create('uploads', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');

                $table->string('name', 120);
                $table->text('description')->nullable();
                $table->text('download_file');
                $table->text('download_folder');
                $table->enum('status', array('Draft', 'Published'))->default('Draft');
                $table->smallInteger('uploader_id');
                $table->text('keywords')->nullable();
                $table->string('category', 30);
                $table->boolean('gated')->default(false);
                $table->boolean('is_live')->default(false);

                $table->timestamps();

                // Indexes

            });


            /**
             * Resource:    UploadCategoryController
             * Model:       UploadCategory
             */
            Schema::connection($this->connection)->dropIfExists('upload_category');
            Schema::connection($this->connection)->create('upload_category', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');
                $table->integer('order')->default(1);

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });


            /**
             * Resource:    TagsController
             * Model:       Tags
             */
            Schema::connection($this->connection)->dropIfExists('tags');
            Schema::connection($this->connection)->create('tags', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');

                $table->timestamps();

                // Indexes

            });


            /**
             * Pivot:   Upload_Tags
             */
            Schema::connection($this->connection)->dropIfExists('tag_upload');
            Schema::connection($this->connection)->create('tag_upload', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('tag_id')->unsigned()->index();
                $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
                $table->integer('upload_id')->unsigned()->index();
                $table->foreign('upload_id')->references('id')->on('uploads')->onDelete('cascade');
                $table->timestamps();
            });
            /**
             * Resource:    InsightPageController
             * Model:       InsightPage
             */
            Schema::connection($this->connection)->dropIfExists('insights_page');
            Schema::connection($this->connection)->create('insights_page', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('title');
                $table->integer('banner_img_id');
                $table->text('source_text');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    StudiosPageController
             * Model:       StudiosPage
             */
            Schema::connection($this->connection)->dropIfExists('studios_page');
            Schema::connection($this->connection)->create('studios_page', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('title');
                $table->integer('banner_img_id');
                $table->text('source_text');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    SiteProductController
             * Model:       SiteProduct
             */
            Schema::connection($this->connection)->dropIfExists('contact_products');
            Schema::connection($this->connection)->create('contact_products', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');
                $table->string('description');
                $table->integer('order');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    SiteProductController
             * Model:       SiteProduct
             */
            Schema::connection($this->connection)->dropIfExists('rate_products');
            Schema::connection($this->connection)->create('rate_products', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');
                $table->string('submit_ad_link');
                $table->integer('order');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    SiteProductController
             * Model:       SiteProduct
             */
            Schema::connection($this->connection)->dropIfExists('site_products');
            Schema::connection($this->connection)->create('site_products', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');
                $table->string('name_slug');
                $table->integer('heading_vs_image');
                $table->integer('on_menu');
                $table->integer('on_product_landing');
                $table->integer('heading_icon_upload_id');
                $table->integer('section_order');
                $table->text('source_text');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    SiteProductSectionController
             * Model:       SiteProductSection
             */
            Schema::connection($this->connection)->dropIfExists('site_product_sections');
            Schema::connection($this->connection)->create('site_product_sections', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');
                $table->string('heading');
                $table->string('name_slug');
                $table->integer('site_product_id');
                $table->integer('enable_link');
                $table->integer('enable_all_caps');
                $table->integer('only_headings_on_landing');
                $table->string('link_text');
                $table->integer('order');

                $table->timestamps();
                $table->softDeletes();

                // Indexes
                $table->unique(array('site_product_id', 'heading', 'name_slug'));

            });

            /**
             * Resource:    ClientController
             * Model:       Client
             */
            Schema::connection($this->connection)->dropIfExists('clients');
            Schema::connection($this->connection)->create('clients', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    ContactLocationController
             * Model:       ContactLocation
             */
            Schema::connection($this->connection)->dropIfExists('contact_locations');
            Schema::connection($this->connection)->create('contact_locations', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');
                $table->integer('order')->default(1);

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    ContactController
             * Model:       Contact
             */
            Schema::connection($this->connection)->dropIfExists('contacts');
            Schema::connection($this->connection)->create('contacts', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('department');
                $table->string('full_name');
                $table->string('job_title');
                $table->string('phone_number');
                $table->string('email');
                $table->integer('location_id');
                $table->integer('product_id');
                $table->integer('order');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    MediaSpecificationController
             * Model:       MediaSpecification
             */
            Schema::connection($this->connection)->dropIfExists('rate_products_media_specs');
            Schema::connection($this->connection)->create('rate_products_media_specs', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('rate_product_id');
                $table->integer('upload_id');
                $table->integer('order');

                $table->timestamps();

                // Indexes

            });

            /**
             * Resource:    HomePageController
             * Model:       HomePage
             */
            Schema::connection($this->connection)->dropIfExists('home_pages');
            Schema::connection($this->connection)->create('home_pages', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('home_page_text');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    HomePageSliderController
             * Model:       HomePageSlider
             */
            Schema::connection($this->connection)->dropIfExists('home_page_sliders');
            Schema::connection($this->connection)->create('home_page_sliders', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->integer('home_page_id');
                $table->text('main_text');
                $table->text('sub_text');
                $table->text('source_text');
                $table->string('upload_id');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });

            /**
             * Resource:    ProductPageController
             * Model:       ProductPage
             */
            Schema::connection($this->connection)->dropIfExists('product_pages');
            Schema::connection($this->connection)->create('product_pages', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('heading');
                $table->integer('banner_image_enabled');
                $table->integer('banner_image_upload_id');
                $table->string('section_order');
                $table->string('module_order');
                $table->string('source_text_enabled');
                $table->text('source_text');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::connection($this->connection)->dropIfExists('product_pages');
            Schema::connection($this->connection)->dropIfExists('home_page_sliders');
            Schema::connection($this->connection)->dropIfExists('home_pages');
            Schema::connection($this->connection)->dropIfExists('rate_products_media_specs');
            Schema::connection($this->connection)->dropIfExists('contacts');
            Schema::connection($this->connection)->dropIfExists('contact_locations');
            Schema::connection($this->connection)->dropIfExists('clients');
            Schema::connection($this->connection)->dropIfExists('contact_products');
            Schema::connection($this->connection)->dropIfExists('rate_products');
            Schema::connection($this->connection)->dropIfExists('site_products');
            Schema::connection($this->connection)->dropIfExists('site_product_sections');
            Schema::connection($this->connection)->dropIfExists('studios_page');
            Schema::connection($this->connection)->dropIfExists('insights_page');
            Schema::connection($this->connection)->dropIfExists('upload_category');
            Schema::connection($this->connection)->dropIfExists('tag_upload');
            Schema::connection($this->connection)->dropIfExists('tags');
            Schema::connection($this->connection)->dropIfExists('uploads');
        }

    }
