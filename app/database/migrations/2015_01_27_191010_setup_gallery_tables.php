<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class SetupGalleryTables extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            /**
             * Resource:    CaseStudyController
             * Model:       CaseStudy
             */
            Schema::connection($this->connection)->dropIfExists('case_studies');
            Schema::connection($this->connection)->create('case_studies', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('headline');
                $table->string('client_name')->default('');
                $table->text('description');
                $table->string('keywords');
                $table->integer('listing_pic_id');
                $table->integer('client_id');
                $table->integer('is_live');
                $table->integer('order');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });


            /**
             * Resource:    CaseStudyFilterController
             * Model:       CaseStudyFilter
             */
            Schema::connection($this->connection)->dropIfExists('case_study_filters');
            Schema::connection($this->connection)->create('case_study_filters', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');
                $table->text('description');
                $table->integer('order');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });


            /**
             * Resource:    CaseStudyFilterItemController
             * Model:       CaseStudyFilterItem
             */
            Schema::connection($this->connection)->dropIfExists('case_study_filter_items');
            Schema::connection($this->connection)->create('case_study_filter_items', function (Blueprint $table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('name');
                $table->text('description');
                $table->integer('filter_id');
                $table->integer('order');

                $table->timestamps();
                $table->softDeletes();

                // Indexes

            });


            /**
             * Resource:    CaseStudyController
             * Model:       CaseStudyCategoryRelationship
             */
            Schema::connection($this->connection)->dropIfExists('case_study_filter_item_relationship');
            Schema::connection($this->connection)->create('case_study_filter_item_relationship',
                function (Blueprint $table) {
                    // Parameters
                    $table->engine = 'InnoDB';

                    // Columns
                    $table->increments('id');
                    $table->integer('case_study_id');
                    $table->integer('filter_id');
                    $table->integer('filter_item_id');

                    $table->timestamps();
                    $table->softDeletes();

                    // Indexes

                });


        }


        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::connection($this->connection)->dropIfExists('case_study_filter_item_relationship');
            Schema::connection($this->connection)->dropIfExists('case_study_filter_items');
            Schema::connection($this->connection)->dropIfExists('case_study_filters');
            Schema::connection($this->connection)->dropIfExists('case_studies');
        }

    }
