<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    class CreateTableApiKeys extends Migration
    {
        protected $connection = "db1";

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::connection($this->connection)->create('api_keys', function (Blueprint $table) {
                $table->increments('id');
                $table->string('key');
                $table->string('device');
                $table->timestamps();
                $table->index('key');
            });
        }


        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::connection($this->connection)->drop('api_keys');
        }

    }
