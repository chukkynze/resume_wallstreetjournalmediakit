<?php

    use Illuminate\Database\Migrations\Migration;

    class ConfideSetupUsersTable extends Migration
    {
        protected $connection = "db1";

        /**
         * Run the migrations.
         */
        public function up()
        {
            // Creates the users table
            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            Schema::connection($this->connection)->dropIfExists('users');
            Schema::connection($this->connection)->create('users', function ($table) {
                // Parameters
                $table->engine = 'InnoDB';

                // Columns
                $table->increments('id');
                $table->string('username')->unique();
                $table->string('email')->unique();
                $table->string('password', 256);
                $table->string('confirmation_code');
                $table->string('remember_token')->nullable();
                $table->string('first_name');
                $table->string('last_name');
                $table->boolean('confirmed')->default(false);

                $table->timestamps();
                $table->softDeletes();
            });

            // Creates password reminders table
            Schema::create('password_reminders', function ($table) {
                $table->string('email');
                $table->string('token');
                $table->timestamp('created_at');
            });

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');
        }

        /**
         * Reverse the migrations.
         */
        public function down()
        {
            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            Schema::connection($this->connection)->drop('password_reminders');
            Schema::connection($this->connection)->drop('users');

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');
        }
    }
