<?php

    class ApiKeysTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding API Key Data.\n";
            $this->table      = 'api_keys';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->table($this->table)->truncate();

            $date = new DateTime();

            $apikeys = array(
                'key'        => md5('LOVEMESOMEKEYS'),
                'device'     => 'iPad',
                'created_at' => $date,
                'updated_at' => $date
            );

            // Uncomment the below to run the seeder
            DB::connection($this->connection)->table($this->table)->insert($apikeys);


            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
