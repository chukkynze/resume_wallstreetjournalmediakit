<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       HomePageSliderTableSeeder
     *
     * filename:    HomePageSliderTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/20/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\PageContent\HomePageSlider;

    class HomePageSliderTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Page Content - Home Page Slider Data.\n";
            $this->table      = 'home_page_sliders';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $allHomePageSliders =
                [

                    [
                        'id'           => 1,
                        'home_page_id' => 1,
                        'main_text' => 'A Platform As Powerful As The People Who Use It.',
                        'sub_text'    => '',
                        'source_text' => '',
                        'upload_id'    => 54,
                    ],
                    [
                        'id'           => 2,
                        'home_page_id' => 1,
                        'main_text'    => 'A Platform As Powerful As The People Who Use It.',
                        'sub_text'    => '<p class="feature-text"><span class="color-light-blue">#1</span> Most Credible</p><p class="feature-text"><span class="color-light-blue">#1</span> Paid Circulation</p>',
                        'source_text' => 'August 2012 Pew Research Media Believability Report; AAM Publisher\'s Statement September 2014',
                        'upload_id'    => 55,
                    ],
                    [
                        'id'           => 3,
                        'home_page_id' => 1,
                        'main_text'    => 'A Platform As Powerful As The People Who Use It.',
                        'sub_text'    => '<p class="feature-text">The average consumer spends <span class="color-light-blue">83 minutes</span> reading The Wall Street Journal newspaper every day.</p>',
                        'source_text' => 'Source: 2013 Thought Leader Study, Ipsos MediaCT.',
                        'upload_id'   => 58,
                    ],
                    [
                        'id'           => 4,
                        'home_page_id' => 1,
                        'main_text'    => 'A Platform As Powerful As The People Who Use It.',
                        'sub_text'    => '<p class="feature-text"><span class="color-light-blue">8 in 10</span> readers take action as a result of advertising in The Wall Street Journal.</p>',
                        'source_text'  => 'Source: 2014 Thought Leader Report, Ipsos MediaCT. Base: WSJ Readers, Net Business/Consumer Ad Actions.',
                        'upload_id'    => 56,
                    ],
                    [
                        'id'           => 5,
                        'home_page_id' => 1,
                        'main_text'    => 'A Platform As Powerful As The People Who Use It.',
                        'sub_text'    => '<p class="feature-text">Consumers spend an average of <span class="color-light-blue">17 million minutes</span> across WSJ digital properties every day.</p>',
                        'source_text'  => 'Source: comScore Key Measures Worldwide Desktop and U.S. Mobile Exclusive Multi-platform report, July-September 2014 3-month average; *comScore Q3 2014 Worldwide Desktop Traffic + U.S. Mobile Exclusive visitors and mobile page views, global Mins/Visitor reflects desktop traffic only, July-September 3-month average, *Traffic by device breakdown is based on visits.',
                        'upload_id'    => 57,
                    ],

                ];

            foreach ($allHomePageSliders as $slider) {
                $HomePageSlider               = new HomePageSlider;
                $HomePageSlider->id           = $slider['id'];
                $HomePageSlider->home_page_id = $slider['home_page_id'];
                $HomePageSlider->main_text    = $slider['main_text'];
                $HomePageSlider->sub_text = $slider['sub_text'];
                $HomePageSlider->source_text  = $slider['source_text'];
                $HomePageSlider->upload_id    = $slider['upload_id'];

                echo "Creating HomePage Slider [" . $HomePageSlider->id . "].\n";

                if ($HomePageSlider->isModelValid()) {
                    $HomePageSlider->save();
                } else {
                    echo "<pre>" . print_r($HomePageSlider->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
