<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       ContactLocationTableSeeder
     *
     * filename:    ContactLocationTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\ContactLocation;

    class ContactLocationTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Contact Location Data.\n";
            $this->table      = 'contact_locations';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 1;
            $contctLocation->name = "Atlanta";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 2;
            $contctLocation->name = "Boston";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 3;
            $contctLocation->name = "Chicago";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 4;
            $contctLocation->name = "Dallas";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 5;
            $contctLocation->name = "Detroit";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 6;
            $contctLocation->name = "Los Angeles";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 7;
            $contctLocation->name = "New York";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 8;
            $contctLocation->name = "San Francisco";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 9;
            $contctLocation->name = "Washington, DC";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 10;
            $contctLocation->name = "Asia";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 11;
            $contctLocation->name = "Europe";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 12;
            $contctLocation->name = "Latin America";
            $contctLocation->save();

            $contctLocation       = new ContactLocation;
            $contctLocation->id   = 13;
            $contctLocation->name = "None";
            $contctLocation->save();

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
