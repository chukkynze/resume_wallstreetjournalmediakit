<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       RateTableSeeder
     *
     * filename:    RateTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Rate;
    use Hyfn\Models\Upload;

    class RateTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Rate & Specifications Data.\n";
            $this->table      = 'rate_specs';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $uploadsString = '{
            "1":["105","106","107","78","4"],
            "2":["101"],
            "3":["102"],
            "4":["101"],
            "5":["88","89"],
            "6":["0"]}';

            $Rate                 = new Rate;
            $Rate->id             = 1;
            $Rate->products_array = '["1","2","3","4","5","6"]';
            $Rate->uploads_array = $uploadsString;

            $Rate->save();

            foreach((array) json_decode($uploadsString, true) as $uploadArray)
            {
                foreach($uploadArray as $uploadID)
                {
                    if($uploadID >= 1)
                    {
                        $Upload             =   Upload::find($uploadID);
                        $Upload->is_live    =   1;

                        if ($Upload->isModelValid())
                        {
                            $Upload->save();
                        }
                        else
                        {
                            echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                        }
                    }
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
