<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       RateProductsTableSeeder
     *
     * filename:    RateProductsTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\RateProduct;

    class RateProductsTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Rate Products Table Data.\n";
            $this->table      = 'rate_products';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    [
                        'id'             => 1,
                        'name'           => "Newspaper",
                        'submit_ad_link' => "https://www.epic.dowjones.com/epicaddrop/controller",
                        'order'          => 1,
                    ],
                    [
                        'id'             => 2,
                        'name'           => "Online",
                        'submit_ad_link' => "",
                        'order'          => 2,
                    ],
                    [
                        'id'             => 3,
                        'name'           => "Mobile",
                        'submit_ad_link' => "",
                        'order'          => 3,
                    ],
                    [
                        'id'             => 4,
                        'name'           => "Video",
                        'submit_ad_link' => "",
                        'order'          => 4,
                    ],
                    [
                        'id'             => 5,
                        'name'           => "WSJ. Magazine",
                        'submit_ad_link' => "https://www.epic.dowjones.com/epicaddrop/controller",
                        'order'          => 5,
                    ],
                    [
                        'id'             => 6,
                        'name'           => "Classified",
                        'submit_ad_link' => "",
                        'order'          => 6,
                    ],
                ];

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new RateProduct();
                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }

                echo "Creating Rate Product - " . $Model->name . " .\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
