<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       ModuleLocationRelationshipTableSeeder
     *
     * filename:    ModuleLocationRelationshipTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\ModuleLocationRelationship;

    class ModuleLocationRelationshipProductDetailsConferencesTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Module Location Relationship Data for Product Details Conferences Page.\n";
            $this->table = 'module_location_relationships';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime('now');

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');
            //DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    //// START Product Details Page 4 - Conferences - SiteProduct 5
                    [
                        'id'          => 0,
                        'location_id' => 4,
                        'entity_id'   => 5,
                        'entity_type' => "Hyfn\\Models\\SiteProduct",
                        'module_id' => 37,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    //// START Product Details Page Section 5 - SiteProductSection - 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
                    // Section 1 - CEO Council
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 19,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 38,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Section 2 - CFO Network
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 20,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 39,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Section 3 - CIO Network
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 21,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 40,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Section 4 - WSJDLive
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 22,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 41,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Section 6 - ECO:nomics
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 23,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 42,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Section 7 - Global Compliance Symposium
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 24,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 43,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Section 8 - Heard on the Street Live
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 25,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 44,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Section 9 - Private Equity Analyst
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 26,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 45,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Section 10 - Viewpoints
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 27,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 46,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    //// END Product Details Page Section 5 - SiteProductSection - 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
                ];

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model              = new ModuleLocationRelationship();
                $Model->id          = $row['id'];
                $Model->location_id = $row['location_id'];

                $Model->entity_id   = $row['entity_id'];
                $Model->entity_type = $row['entity_type'];

                $Model->module_id   = $row['module_id'];
                $Model->module_type = $row['module_type'];

                $Model->order = $row['order'];
                $Model->save();

                echo "Creating Module Location Relationship Type [".$Model->id."].\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>".print_r($Model->getErrors(), 1)."</pre>\n";
                }
            }

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded ".$seedCount." of ".count($Rows)." rows in ".$duration." seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }
    }
