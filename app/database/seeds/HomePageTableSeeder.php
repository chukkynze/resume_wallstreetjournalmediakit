<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       HomePageTableSeeder
     *
     * filename:    HomePageTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/20/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\PageContent\HomePage;

    class HomePageTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Page Content - Home Page Data.\n";
            $this->table      = 'home_pages';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $allHomePages =
                [

                    [
                        'id'             => 1,
                        'home_page_text' => 'text',
                    ],

                ];

            foreach ($allHomePages as $homePage) {
                $HomePage                 = new HomePage;
                $HomePage->id             = $homePage['id'];
                $HomePage->home_page_text = $homePage['home_page_text'];

                echo "Creating HomePage [" . $HomePage->id . "].\n";

                if ($HomePage->isModelValid()) {
                    $HomePage->save();
                } else {
                    echo "<pre>" . print_r($HomePage->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
