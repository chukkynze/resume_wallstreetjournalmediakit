<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       UploadTableSeeder
     *
     * filename:    UploadTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/07/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Upload;

    class UploadTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Upload Data.\n";
            $this->table      = 'uploads';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $allUploads =
                [

                    // Rates & Specs
                    [
                        'id'              => 1,
                        'name'            => "General Rate Card",
                        'description'     => "Description for General Rate Card",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "2014_General_Rate_Card.pdf",
                        'status'          => "Draft",
                        'uploader_id'     => 1,
                        'keywords'        => "General Rate Card",
                        'category'        => 1,
                        'gated' => false,
                    ],
                    [
                        'id'              => 2,
                        'name'            => "Regional Rates",
                        'description'     => "Description for Regional Rates",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "2014_Regional_General_Rates.pdf",
                        'status'          => "Draft",
                        'uploader_id'     => 1,
                        'keywords'        => "Regional Rates",
                        'category'        => 1,
                        'gated' => false,
                    ],
                    [
                        'id'              => 3,
                        'name'            => "Ad Units Guide",
                        'description'     => "Description for Ad Units Guide",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "2014_Ad_Units_Guide.pdf",
                        'status'          => "Draft",
                        'uploader_id'     => 1,
                        'keywords'        => "Ad Units Guide",
                        'category'        => 1,
                        'gated' => false,
                    ],
                    [
                        'id'              => 4,
                        'name' => "Newspaper PDF Preparation Guidelines",
                        'description'     => "Description for PDF Preparation",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file' => "newspaper_preparation.pdf",
                        'status'          => "Draft",
                        'uploader_id'     => 1,
                        'keywords'        => "PDF Preparation",
                        'category'        => 1,
                        'gated' => false,
                    ],
                    [
                        'id'              => 5,
                        'name'            => "Online Specs",
                        'description'     => "Description for Online Specs",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Web_Specs.xls",
                        'status'          => "Draft",
                        'uploader_id'     => 1,
                        'keywords'        => "Online Specs",
                        'category'        => 1,
                        'gated' => false,
                    ],
                    [
                        'id'              => 6,
                        'name'            => "Mobile Specs",
                        'description'     => "Description for Mobile Specs",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Mobile_Specs.xls",
                        'status'          => "Draft",
                        'uploader_id'     => 1,
                        'keywords'        => "Mobile Specs",
                        'category'        => 1,
                        'gated' => false,
                    ],
                    [
                        'id'              => 7,
                        'name'            => "WSJ. Magazine Rates & Specs",
                        'description'     => "Description for WSJ. Magazine Rates & Specs",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "2014_WSJ._Magazine_Rates_Specs.pdf",
                        'status'          => "Draft",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ. Magazine Rates & Specs",
                        'category'        => 1,
                        'gated' => false,
                    ],





                    // Banner Images
                    [
                        'id'              => 8,
                        'name'            => "Insights Banner Image",
                        'description'     => "Description for Insights Banner Image",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-insights-marquee.png",
                        'status'          => "Draft",
                        'uploader_id'     => 1,
                        'keywords'        => "Insights Banner Image",
                        'category'        => 2,
                        'gated' => false,
                    ],



                    // Home Page Banner Images
                    [
                        'id'              => 14,
                        'name'            => "Home Page Slide 1",
                        'description'     => "Description for Home Page Slide 1",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-landing-marquee-1.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Home Page Slide 1",
                        'category'        => 2,
                        'gated' => false,
                    ],
                    [
                        'id'              => 15,
                        'name'            => "Home Page Slide 2",
                        'description'     => "Description for Home Page Slide 2",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-landing-marquee-2.jpg",
                        'status'          => "Draft",
                        'uploader_id'     => 1,
                        'keywords'        => "Home Page Slide 2",
                        'category'        => 2,
                        'gated' => false,
                    ],
                    [
                        'id'              => 16,
                        'name'            => "Home Page Slide 3",
                        'description'     => "Description for Home Page Slide 3",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-landing-marquee-3.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Home Page Slide 3",
                        'category'        => 2,
                        'gated' => false,
                    ],






                    // Product Headings - Title & Icon
                    [
                        'id'              => 25,
                        'name'            => "Newspaper Heading Icon",
                        'description'     => "Description for Newspaper Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "newspaper.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 26,
                        'name'            => "Capabilities Heading Icon",
                        'description'     => "Description for Capabilities Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "capabilities.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Capabilities Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 27,
                        'name'            => "Conferences Heading Icon",
                        'description'     => "Description for Conferences Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "conferences.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Conferences Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 28,
                        'name'            => "Global Heading Icon",
                        'description'     => "Description for Global Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "global.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Global Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 29,
                        'name'            => "Journal Report Heading Icon",
                        'description'     => "Description for Journal Report Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "journal-report.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Journal Report Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 30,
                        'name'            => "MarketWatch Heading Icon",
                        'description'     => "Description for MarketWatch Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "marketwatch.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "MarketWatch Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 31,
                        'name'            => "Mobile Heading Icon",
                        'description'     => "Description for Mobile Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "mobile.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Mobile Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 32,
                        'name'            => "Online Heading Icon",
                        'description'     => "Description for Online Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "online.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Online Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 33,
                        'name'            => "Reports Heading Icon",
                        'description'     => "Description for Reports Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "reports.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Reports Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 34,
                        'name'            => "Video Heading Icon",
                        'description'     => "Description for Video Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "video.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Video Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],
                    [
                        'id'              => 35,
                        'name'            => "WSJ Magazine Heading Icon",
                        'description'     => "Description for WSJ Magazine Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsjmag.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ Magazine Heading Icon",
                        'category'        => 6,
                        'gated' => false,
                    ],









                    // WSJ Magazine Full Width Images
                    [
                        'id'              => 41,
                        'name'            => "WSJ Magazine - Bloque de Color",
                        'description'     => "Description for WSJ Magazine - Bloque de Color",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-magazine-bloque.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ Magazine - Bloque de Color",
                        'category'        => 8,
                        'gated' => false,
                    ],
                    [
                        'id'              => 42,
                        'name'            => "WSJ Magazine - 2015 Important Dates",
                        'description'     => "Description for WSJ Magazine - 2015 Important Dates",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-magazine-dates.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ Magazine - 2015 Important Dates",
                        'category'        => 8,
                        'gated' => false,
                    ],
                    [
                        'id'              => 43,
                        'name'            => "WSJ Magazine - Pharell and friends 2013 Innovation Awards",
                        'description'     => "Description for WSJ Magazine - Pharell and friends 2013 Innovation Awards",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-magazine-models.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ Magazine - Pharell and friends 2013 Innovation Awards",
                        'category'        => 8,
                        'gated' => false,
                    ],







                    // WSJ Image Column Module pictures
                    [
                        'id'              => 44,
                        'name'            => "WSJ Magazine - Cover 1",
                        'description'     => "Description for WSJ Magazine - Cover 1",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-magazine-cover-1.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ Magazine - Cover 1",
                        'category'        => 7,
                        'gated' => false,
                    ],
                    [
                        'id'              => 45,
                        'name'            => "WSJ Magazine - Cover 2",
                        'description'     => "Description for WSJ Magazine - Cover 2",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-magazine-cover-2.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ Magazine - Cover 2",
                        'category'        => 7,
                        'gated' => false,
                    ],
                    [
                        'id'              => 46,
                        'name'            => "WSJ Magazine - Cover 3",
                        'description'     => "Description for WSJ Magazine - Cover 3",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-magazine-cover-4.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ Magazine - Cover 3",
                        'category'        => 7,
                        'gated' => false,
                    ],
                    [
                        'id'              => 47,
                        'name'            => "WSJ Magazine - Cover 4",
                        'description'     => "Description for WSJ Magazine - Cover 4",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsj-magazine-cover-3.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ Magazine - Cover 4",
                        'category'        => 7,
                        'gated' => false,
                    ],





                    // Video Details - Company Logos
                    [
                        'id'              => 48,
                        'name'            => "A-Head Logo",
                        'description'     => "Description for A-Head Logo",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file' => "video-icon-a-hed-icon.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "A-Head Logo",
                        'category'        => 9,
                        'gated' => false,
                    ],
                    [
                        'id'              => 49,
                        'name'            => "Lunch Break Logo",
                        'description'     => "Description for Lunch Break Logo",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file' => "video-icon-lunch-break-icon.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Lunch Break Logo",
                        'category'        => 9,
                        'gated' => false,
                    ],
                    [
                        'id'              => 50,
                        'name'            => "Money Beat Logo",
                        'description'     => "Description for Money Beat Logo",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file' => "video-icon-money-beat-icon.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Money Beat Logo",
                        'category'        => 9,
                        'gated' => false,
                    ],
                    [
                        'id'              => 51,
                        'name'            => "News Hub Logo",
                        'description'     => "Description for News Hub Logo",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file' => "video-icon-news-hub-icon.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "News Hub Logo",
                        'category'        => 9,
                        'gated' => false,
                    ],
                    [
                        'id'              => 52,
                        'name'            => "Opinion Journal Logo",
                        'description'     => "Description for Opinion Journal Logo",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file' => "video-icon-opinion-journal-icon.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Opinion Journal Logo",
                        'category'        => 9,
                        'gated' => false,
                    ],
                    [
                        'id'              => 53,
                        'name'            => "The Short Answer",
                        'description'     => "Description for The Short Answer",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file' => "video-icon-short-answer-icon.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "The Short Answer",
                        'category'        => 9,
                        'gated' => false,
                    ],











                    // New Banner Images
                    [
                        'id'              => 54,
                        'name'            => "Banner Image 01 WSJ Powerful Platform",
                        'description'     => "Description for Banner Image 01 WSJ Powerful Platform",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Banner_Image_01_WSJ_Powerful_Platform.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Banner Image 01 WSJ Powerful Platform",
                        'category'        => 2,
                        'gated' => false,
                    ],
                    [
                        'id'              => 55,
                        'name'            => "Banner Image 02 WSJ Credible",
                        'description'     => "Description for Banner Image 02 WSJ Credible",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Banner_Image_02_WSJ_Credible.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Banner Image 02 WSJ Credible",
                        'category'        => 2,
                        'gated' => false,
                    ],
                    [
                        'id'              => 56,
                        'name'            => "Banner Image 03 WSJ 8 in 10",
                        'description'     => "Description for Banner Image 03 WSJ 8 in 10",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Banner_Image_03_WSJ_8in10.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Banner Image 03 WSJ 8 in 10",
                        'category'        => 2,
                        'gated' => false,
                    ],
                    [
                        'id'              => 57,
                        'name'            => "Banner Image 04 WSJ 17 Million",
                        'description'     => "Description for Banner Image 04 WSJ 17 Million",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Banner_Image_04_WSJ_17_Million.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Banner Image 04 WSJ 17 Million",
                        'category'        => 2,
                        'gated' => false,
                    ],
                    [
                        'id'              => 58,
                        'name'            => "Banner Image 05 WSJ 83 Minutes",
                        'description'     => "Description for Banner Image 05 WSJ 83 Minutes",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Banner_Image_05_WSJ_83MInutes.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Banner Image 05 WSJ 83 Minutes",
                        'category'        => 2,
                        'gated' => false,
                    ],
                    [
                        'id'              => 59,
                        'name'            => "Banner Image WSJ Custom Studios",
                        'description'     => "Description for Banner Image WSJ Custom Studios",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Banner_Image_WSJ_Custom_Studios.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Banner Image WSJ Custom Studios",
                        'category'        => 2,
                        'gated' => false,
                    ],
                    [
                        'id'              => 60,
                        'name'            => "Banner Image WSJ Insights",
                        'description'     => "Description for Banner Image WSJ Insights",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Banner_Image_WSJ_Insights.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Banner Image WSJ Insights",
                        'category'        => 2,
                        'gated' => false,
                    ],
                    [
                        'id'              => 61,
                        'name'            => "Banner Image WSJ Products",
                        'description'     => "Description for Banner Image WSJ Products",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "Banner_Image_WSJ_Products.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Banner Image WSJ Products",
                        'category'        => 2,
                        'gated' => false,
                    ],











                    // Video Details - Company Logos
                    [
                        'id'              => 62,
                        'name'            => "What's News Logo",
                        'description'     => "Description for What's News Logo",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "video-icon-whats-new-icon.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "What's News Logo",
                        'category'        => 9,
                        'gated' => false,
                    ],













                    // PDFs
                    [
                        'id'              => 63,
                        'name'            => "Arena Edit Calendar 2014",
                        'description'     => "Description for Arena Edit Calendar 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_ArenaEditCalendar2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Arena Edit Calendar 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 64,
                        'name'            => "Barrons.com Audience Profile",
                        'description'     => "Description for Barrons.com Audience Profile",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_BarronsAudienceProfile.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Barrons.com Audience Profile",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 65,
                        'name'            => "Journal Report Edit Calendar 2014",
                        'description'     => "Description for Journal Report Edit Calendar 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_JournalReportEditCalendar2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Journal Report Edit Calendar 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 66,
                        'name'            => "Journal Report Edit Calendar 2015",
                        'description'     => "Description for Journal Report Edit Calendar 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_JournalReportEditCalendar2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Journal Report Edit Calendar 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 67,
                        'name'            => "Mansion Edit Calendar 2014",
                        'description'     => "Description for Mansion Edit Calendar 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_MansionEditCalendar2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Mansion Edit Calendar 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 68,
                        'name'            => "Mansion Edit Calendar 2015",
                        'description'     => "Description for Mansion Edit Calendar 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_MansionEditCalendar2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Mansion Edit Calendar 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 69,
                        'name'            => "MarketWatch Edit Calendar 2014",
                        'description'     => "Description for MarketWatch Edit Calendar 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_MarketWatchEditCalendar2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "MarketWatch Edit Calendar 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 70,
                        'name'            => "MarketWatch.com Audience Profile",
                        'description'     => "Description for MarketWatch.com Audience Profile",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_MarketWatchAudienceProfile.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "MarketWatch.com Audience Profile",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 71,
                        'name'            => "Newspaper Content Guide - Asia Edition",
                        'description'     => "Description for Newspaper Content Guide - Asia Edition",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperContentGuideAsiaEdition.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Content Guide - Asia Edition",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 72,
                        'name'            => "Newspaper Content Guide - Europe Edition",
                        'description'     => "Description for Newspaper Content Guide - Europe Edition",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperContentGuideEuropeEdition.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Content Guide - Europe Edition",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 73,
                        'name'            => "Newspaper Content Guide - Weekday",
                        'description'     => "Description for Newspaper Content Guide - Weekday",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperContentGuideWeekday.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Content Guide - Weekday",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 74,
                        'name'            => "Newspaper Content Guide - WSJ Weekend",
                        'description'     => "Description for Newspaper Content Guide - WSJ Weekend",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperContentGuideWSJWeekend.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Content Guide - WSJ Weekend",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 75,
                        'name'            => "Newspaper Global Rates 2014",
                        'description'     => "Description for Newspaper Global Rates 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperGlobalRates2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Global Rates 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 76,
                        'name'            => "Newspaper Non-Publishing Dates 2014",
                        'description'     => "Description for Newspaper Non-Publishing Dates 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperNonPublishingDates2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Non-Publishing Dates 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 77,
                        'name'            => "Newspaper Regional Rates 2014",
                        'description'     => "Description for Newspaper Regional Rates 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperRegionalRates2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Regional Rates 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 78,
                        'name'            => "Newspaper Ad Unit Specs",
                        'description'     => "Description for Newspaper Ad Unit Specs",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperSpecsUS.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Ad Unit Specs",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 79,
                        'name'            => "Off Duty Edit Calendar 2014",
                        'description'     => "Description for Off Duty Edit Calendar 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_OffDutyEditCalendar2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Off Duty Edit Calendar 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 80,
                        'name' => "Review/Books Edit Calendar 2014",
                        'description'     => "Description for Review_Books Edit Calendar 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_ReviewBooksEditCalendar2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Review_Books Edit Calendar 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 81,
                        'name'            => "Weekend Investor Edit Calendar 2014",
                        'description'     => "Description for Weekend Investor Edit Calendar 2014",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WeekendInvestorEditCalendar2014.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Weekend Investor Edit Calendar 2014",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 82,
                        'name'        => "WSJ. Insights BAV Report: The Power of The Wall Street Journal",
                        'description' => "Description for WSJ. Insights BAV Report: The Power of The Wall Street Journal",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJInsightsBAVReportThePowerofTheWallStreetJournal.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ. Insights BAV Report_ The Power of The Wall Street Journal",
                        'category'        => 1,
                        'gated' => true,
                    ],
                    [
                        'id'              => 83,
                        'name'            => "WSJ. Magazine Audience Profile",
                        'description'     => "Description for WSJ. Magazine Audience Profile",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJMagazineAudienceProfile.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ. Magazine Audience Profile",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 84,
                        'name'            => "WSJ. Magazine Edit Calendar 2015",
                        'description'     => "Description for WSJ. Magazine Edit Calendar 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJMagazineEditCalendar2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ. Magazine Edit Calendar 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 85,
                        'name'            => "WSJ. Magazine Events Overview",
                        'description'     => "Description for WSJ. Magazine Events Overview",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJMagazineEventsOverview.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ. Magazine Events Overview",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 86,
                        'name'            => "WSJ. Magazine Latin America",
                        'description'     => "Description for WSJ. Magazine Latin America",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJMagazineLatinAmerica.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ. Magazine Latin America",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 87,
                        'name'            => "WSJ. Magazine Press Recap",
                        'description'     => "Description for WSJ. Magazine Press Recap",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJMagazinePressRecap.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ. Magazine Press Recap",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 88,
                        'name'            => "WSJ. Magazine Rates 2015",
                        'description'     => "Description for WSJ. Magazine Rates 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJMagazineRates2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ. Magazine Rates 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 89,
                        'name' => "WSJ. Magazine Specs",
                        'description'     => "Description for WSJ. Magazine Specs 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJMagazineSpecs2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ. Magazine Specs 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 90,
                        'name'            => "WSJ.com Audience Profile",
                        'description'     => "Description for WSJ.com Audience Profile",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJAudienceProfile.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ.com Audience Profile",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 91,
                        'name'            => "WSJDN Smartphone Audience Profile",
                        'description'     => "Description for WSJDN Smartphone Audience Profile",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJDNSmartphoneAudienceProfile.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJDN Smartphone Audience Profile",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 92,
                        'name'            => "WSJDN Tablet Audience Profile",
                        'description'     => "Description for WSJDN Tablet Audience Profile",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_WSJDNTabletAudienceProfile.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJDN Tablet Audience Profile",
                        'category'        => 1,
                        'gated'           => false,
                    ],








                    // Weird WSJ Logo from nowhere
                    [
                        'id'              => 93,
                        'name'            => "WSJ Mag Logo",
                        'description'     => "Description for WSJ Mag Logo",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "WSJMagLogo.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJ Mag Logo",
                        'category' => 9,
                        'gated'           => false,
                    ],







                    // Award Heading Icon
                    [
                        'id'              => 94,
                        'name'            => "Awards Heading Icon",
                        'description'     => "Description for Awards Heading Icon",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "awards-icon.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Awards Heading Icon",
                        'category'        => 6,
                        'gated'           => false,
                    ],








                    // Custom Studio Sliders
                    [
                        'id'              => 95,
                        'name'            => "Custom Studio Slider1 - magazines",
                        'description'     => "Description for Custom Studio Slider1 - magazines",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "custom-studios-slides1-magazines.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Custom Studio Slider1 - magazines",
                        'category'        => 4,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 96,
                        'name'            => "Custom Studio Slider2 - custom-research",
                        'description'     => "Description for Custom Studio Slider2 - custom-research",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "custom-studios-slides2-custom-research.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Custom Studio Slider2 - custom-research",
                        'category'        => 4,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 97,
                        'name'            => "Custom Studio Slider3 - content-hubs",
                        'description'     => "Description for Custom Studio Slider3 - content-hubs",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "custom-studios-slides3-content-hubs.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Custom Studio Slider3 - content-hubs",
                        'category'        => 4,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 98,
                        'name'            => "Custom Studio Slider4 - infographics",
                        'description'     => "Description for Custom Studio Slider4 - infographics",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "custom-studios-slides4-infographics.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Custom Studio Slider4 - infographics",
                        'category'        => 4,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 99,
                        'name'            => "Custom Studio Slider5 - special-features",
                        'description'     => "Description for Custom Studio Slider5 - special-features",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "custom-studios-slides5-special-features.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Custom Studio Slider5 - special-features",
                        'category'        => 4,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 100,
                        'name'            => "Custom Studio Slider6 - custom-apps",
                        'description'     => "Description for Custom Studio Slider6 - custom-apps",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "custom-studios-slides6-custom-apps.jpg",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Custom Studio Slider6 - custom-apps",
                        'category'        => 4,
                        'gated'           => false,
                    ],











                    // Spreadsheets
                    [
                        'id'              => 101,
                        'name'            => "WSJDN Online and Video Specs",
                        'description'     => "Description for WSJDN Online and Video Specs",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "wsjdn_online_and_video_specs.xlsx",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJDN Online and Video Specs",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 102,
                        'name'            => "WSJDN Mobile Specs",
                        'description'     => "Description for WSJDN Mobile Specs",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file' => "wsjdn_mobile_specs.xlsx",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "WSJDN Mobile Specs",
                        'category'        => 1,
                        'gated'           => false,
                    ],







                    // More PDF's
                    [
                        'id'              => 103,
                        'name'            => "MarketWatch Edit Calendar 2015",
                        'description'     => "Description for MarketWatch Edit Calendar 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_MarketWatchEditCalendar2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "MarketWatch Edit Calendar 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 104,
                        'name'            => "Arena Edit Calendar 2015",
                        'description'     => "Description for Arena Edit Calendar 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_ArenaEditCalendar2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Arena Edit Calendar 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 105,
                        'name'            => "Newspaper Global Rates 2015",
                        'description'     => "Description for Newspaper Global Rates 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperGlobalRates2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Global Rates 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 106,
                        'name'            => "Newspaper Regional Rates 2015",
                        'description'     => "Description for Newspaper Regional Rates 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperRegionalRates2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Regional Rates 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 107,
                        'name'            => "Material Specs",
                        'description'     => "Description for Material Specs",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_MaterialSpecs.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Material Specs",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 108,
                        'name'            => "Off Duty Edit Calendar 2015",
                        'description'     => "Description for Off Duty Edit Calendar 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_OffDutyEditCalendar2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Off Duty Edit Calendar 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 109,
                        'name'            => "Newspaper Non-Publishing Dates 2015",
                        'description'     => "Description for Newspaper Non-Publishing Dates 2015",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "PDF_NewspaperNonPublishingDates2015.pdf",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Newspaper Non-Publishing Dates 2015",
                        'category'        => 1,
                        'gated'           => false,
                    ],
                    // Gallery
                    [
                        'id'              => 110,
                        'name'            => "Case Study FPO 1",
                        'description'     => "",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "CaseStudyFPO.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Case Study FPO",
                        'category'        => 11,
                        'gated'           => false,
                    ],
                    [
                        'id'              => 111,
                        'name'            => "Case Study FPO 2",
                        'description'     => "",
                        'download_folder' => "/files/uploads/201410/",
                        'download_file'   => "CaseStudyFPO.png",
                        'status'          => "Published",
                        'uploader_id'     => 1,
                        'keywords'        => "Case Study FPO",
                        'category'        => 11,
                        'gated'           => false,
                    ],
                ];

            foreach ($allUploads as $upload) {
                $Upload                  = new Upload;
                $Upload->id = $upload['id'];
                $Upload->name            = $upload['name'];
                $Upload->description     = $upload['description'];
                $Upload->download_folder = $upload['download_folder'];
                $Upload->download_file   = $upload['download_file'];
                $Upload->status          = $upload['status'];
                $Upload->uploader_id     = $upload['uploader_id'];
                $Upload->keywords        = $upload['keywords'];
                $Upload->category        = $upload['category'];
                $Upload->gated           = $upload['gated'];
                $Upload->is_live         = FALSE;

                echo "Creating Upload " . $Upload->name . " ." . "\n";

                if ($Upload->isModelValid()) {
                    $Upload->save();
                } else {
                    echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
