<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       ProductPageTableSeeder
     *
     * filename:    ProductPageTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/20/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\PageContent\ProductPage;

    class ProductPageTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Page Content - Product Page Data.\n";
            $this->table      = 'product_pages';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $allPages =
                [

                    [
                        'id'                     => 1,
                        'heading'                => "Products",
                        'banner_image_enabled'   => 1,
                        'banner_image_upload_id' => 61,
                        'section_order' => json_encode(["8", "9", "10", "11", "2", "3", "4", "12", "5"]),
                        'module_order'  => json_encode([]),
                        'source_text'            => "Source text is needed here.",
                        'source_text_enabled'    => 0,
                    ],

                ];

            foreach ($allPages as $page) {
                $Page                         = new ProductPage;
                $Page->id                     = $page['id'];
                $Page->heading                = $page['heading'];
                $Page->banner_image_enabled   = $page['banner_image_enabled'];
                $Page->banner_image_upload_id = $page['banner_image_upload_id'];
                $Page->section_order          = $page['section_order'];
                $Page->module_order = $page['module_order'];
                $Page->source_text            = $page['source_text'];
                $Page->source_text_enabled    = $page['source_text_enabled'];

                echo "Creating ProductPage [" . $Page->id . "].\n";

                if ($Page->isModelValid()) {
                    $Page->save();
                } else {
                    echo "<pre>" . print_r($Page->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
