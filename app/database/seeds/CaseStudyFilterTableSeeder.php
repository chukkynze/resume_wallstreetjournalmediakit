<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       CaseStudyFilterTableSeeder
     *
     * filename:    CaseStudyFilterTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\CaseStudyFilter;

    class CaseStudyFilterTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Case Study Filter Data.\n";
            $this->table      = 'case_study_filters';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    [
                        'id'          => 1,
                        'name'        => "Product",
                        'description' => "Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 2,
                        'name'        => "Type",
                        'description' => "Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.",
                        'order'       => 2,
                    ],
                    [
                        'id'          => 3,
                        'name'        => "Category",
                        'description' => "Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.",
                        'order'       => 3,
                    ],
                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new CaseStudyFilter();
                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }

                echo "Creating Case Study Filter: \"" . $Model->name . "\".\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
