<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       ClientsTableSeeder
     *
     * filename:    ClientsTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Client;

    class ClientsTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Client Data.\n";
            $this->table      = 'clients';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Client       = new Client;
            $Client->id   = 1;
            $Client->name = "Ford";
            $Client->save();

            $Client       = new Client;
            $Client->id   = 2;
            $Client->name = "WSJ";
            $Client->save();

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
