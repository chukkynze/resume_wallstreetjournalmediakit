<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       SplitTextModuleTableSeeder
     *
     * filename:    SplitTextModuleTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Modules\SplitTextModule;

    class SplitTextModuleTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Split Text Module Data.\n";
            $this->table      = 'split_text_modules';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    /*
                     [
                        'id'         => 1,
                        'has_border' => true,
                        'title'   => "",
                        'is_live' => true,
                    ],
                    [
                        'id'         => 2,
                        'has_border' => true,
                        'title'   => "",
                        'is_live' => true,
                    ],
                     */
                ];


            $dir = preg_replace('/\.php$/', '', __FILE__);

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new SplitTextModule();

                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }
                $Model->col1       = file_get_contents($dir . '/' . $row['id'] . '_col1.html');
                $Model->col2       = file_get_contents($dir . '/' . $row['id'] . '_col2.html');

                echo "Creating Split Text Module [" . $Model->id . "].\n";

                if ($Model->isValid()) {

                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
