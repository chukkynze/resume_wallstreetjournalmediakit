<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       ContactTableSeeder
     *
     * filename:    ContactTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Contact;

    class ContactTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Contact Data.\n";
            $this->table      = 'contacts';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            // 'location_id'  => 13, No ID

            $allContacts =
                [
                    [
                        'full_name'    => "Don Seay",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "404.865.4345",
                        'email'        => "don.seay@wsj.com",
                        'location_id'  => 1,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Lynne Montesanto",
                        'department'   => "GENERAL ADVERTISING",
                        'job_title'    => "Sales Director",
                        'phone_number' => "617.654.6753",
                        'email'        => "lynne.montesanto@wsj.com",
                        'location_id'  => 2,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Paul Lydon",
                        'department'   => "REAL ESTATE ADVERTISING",
                        'job_title'    => "Account Executive, Classified",
                        'phone_number' => "617.654.6718",
                        'email'        => "paul.lydon@wsj.com",
                        'location_id'  => 2,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Michael Corwin",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "312.750.4032",
                        'email'        => "michael.corwin@wsj.com",
                        'location_id'  => 3,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Randy Holloway",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "312.750.4015",
                        'email'        => "randy.holloway@wsj.com",
                        'location_id'  => 3,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Jessica Clary",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "214.951.7130",
                        'email'        => "jessica.clary@wsj.com",
                        'location_id'  => 4,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Michael Corwin",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "312.750.4032",
                        'email'        => "michael.corwin@wsj.com",
                        'location_id'  => 5,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Sara Mascall",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "424.204.4838",
                        'email'        => "sara.mascall@wsj.com",
                        'location_id'  => 6,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "John Kennelly",
                        'department'   => "GENERAL ADVERTISING",
                        'job_title'    => "Sales Director",
                        'phone_number' => "212.597.5773",
                        'email'        => "john.kennelly@wsj.com",
                        'location_id'  => 7,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Nancy McDonald",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "212.597.5642",
                        'email'        => "nancy.mcdonald@wsj.com",
                        'location_id'  => 7,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Alberto Apodaca",
                        'department'   => "LUXURY ADVERTISING",
                        'job_title'    => "Sales Director",
                        'phone_number' => "212.597.5612",
                        'email'        => "alberto.apodaca@wsj.com",
                        'location_id'  => 7,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Paul Carlucci",
                        'department' => "GREATER NEW YORK SECTION (including NYC-only retailers)",
                        'job_title'    => "Sales Director",
                        'phone_number' => "212.597.5636",
                        'email'        => "paul.carlucci@wsj.com",
                        'location_id'  => 7,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Marti Gallardo",
                        'department' => "CLASSIFIED ADVERTISING (including Real Estate and Legal Notices)",
                        'job_title'    => "VP, Vertical Markets",
                        'phone_number' => "212.597.5619",
                        'email'        => "marti.gallardo@wsj.com",
                        'location_id'  => 7,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Jon McManus",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "415.765.6142",
                        'email'        => "jon.mcmanus@wsj.com",
                        'location_id'  => 8,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Schuyler Williams",
                        'department'   => "",
                        'job_title'    => "Account Executive",
                        'phone_number' => "202.828.3390",
                        'email'        => "schuyler.williams@wsj.com",
                        'location_id'  => 9,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Mark Rogers",
                        'department'   => "HONG KONG/CHINA/SINGAPORE",
                        'job_title'    => "Executive Sales Director",
                        'phone_number' => "852.2831.2559",
                        'email'        => "mark.rogers@wsj.com",
                        'location_id'  => 10,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Will Wilkinson",
                        'department'   => "LONDON",
                        'job_title'    => "Vice President, EMEA",
                        'phone_number' => "44.207.572.2112",
                        'email'        => "will.wilkinson@dowjones.com",
                        'location_id'  => 11,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Tiana Normand",
                        'department'   => "PARIS",
                        'job_title'    => "Account Executive",
                        'phone_number' => "33.1.4017.1713",
                        'email'        => "tiana.normand@wsj.com",
                        'location_id'  => 11,
                        'product_id'   => 1,
                    ],
                    [
                        'full_name'    => "Carl LeDunff",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "55.11.3544.7078",
                        'email'        => "carl.ledunff@wsj.com",
                        'location_id'  => 12,
                        'product_id'   => 1,
                    ],
                    // Global
                    [
                        'full_name'    => "Mark Rogers",
                        'department'   => "HONG KONG/CHINA/SINGAPORE",
                        'job_title'    => "Executive Sales Director",
                        'phone_number' => "852.2831.2559",
                        'email'        => "mark.rogers@wsj.com",
                        'location_id'  => 10,
                        'product_id'   => 2,
                    ],
                    [
                        'full_name'    => "Hiroshi Kakiyama",
                        'department'   => "JAPAN/KOREA",
                        'job_title'    => "Sales Director",
                        'phone_number' => "81.3.6269.2701",
                        'email'        => "hiroshi.kakiyama@dowjones.com",
                        'location_id'  => 10,
                        'product_id' => 2,
                    ],
                    [
                        'full_name'    => "Hiroshi Kakiyama",
                        'department'   => "JAPAN/KOREA",
                        'job_title'    => "Sales Director",
                        'phone_number' => "81.3.6269.2701",
                        'email'        => "hiroshi.kakiyama@dowjones.com",
                        'location_id'  => 10,
                        'product_id' => 1,
                    ],
                    [
                        'full_name'    => "Will Wilkinson",
                        'department'   => "LONDON",
                        'job_title'    => "Vice President, EMEA",
                        'phone_number' => "44.207.572.2112",
                        'email'        => "will.wilkinson@dowjones.com",
                        'location_id'  => 11,
                        'product_id'   => 2,
                    ],
                    [
                        'full_name'    => "Tiana Normand",
                        'department'   => "PARIS",
                        'job_title'    => "Account Executive",
                        'phone_number' => "33.1.4017.1713",
                        'email'        => "tiana.normand@wsj.com",
                        'location_id'  => 11,
                        'product_id'   => 2,
                    ],
                    [
                        'full_name'    => "Carl LeDunff",
                        'department'   => "",
                        'job_title'    => "Sales Director",
                        'phone_number' => "55.11.3544.7078",
                        'email'        => "carl.ledunff@wsj.com",
                        'location_id'  => 12,
                        'product_id'   => 2,
                    ],
                    // WSJ. Magazine
                    [
                        'full_name'    => "Anthony Cenname",
                        'department'   => "",
                        'job_title'    => "Publisher, WSJ. Magazine",
                        'phone_number' => "212.597.5663",
                        'email'        => "anthony.cenname@wsj.com",
                        'location_id' => 13,
                        'product_id'   => 3,
                    ],
                    [
                        'full_name'    => "Stephanie Arnold",
                        'department'   => "",
                        'job_title'    => "Sales Director, WSJ. Magazine",
                        'phone_number' => "212.659.2183",
                        'email'        => "stephanie.arnold@wsj.com",
                        'location_id' => 13,
                        'product_id'   => 3,
                    ],
                    // MarketWatch
                    [
                        'full_name'    => "Caitlin Supka",
                        'department'   => "",
                        'job_title'    => "Global Sales Manager, MarketWatch",
                        'phone_number' => "",
                        'email'        => "caitlin.supka@dowjones.com",
                        'location_id' => 13,
                        'product_id'   => 4,
                    ],
                    // Conferences
                    [
                        'full_name'    => "Alisha Hathaway",
                        'department'   => "",
                        'job_title'    => "Global Sales Director, Executive Conferences",
                        'phone_number' => "202.862.1389",
                        'email'        => "alisha.hathaway@wsj.com",
                        'location_id' => 13,
                        'product_id'   => 5,
                    ],
                    // WSJ. Custom Studios
                    [
                        'full_name'    => "Jordan Hyman",
                        'department'   => "",
                        'job_title'    => "Executive Director, Content Sales",
                        'phone_number' => "212.659.1919",
                        'email'        => "jordan.hyman@wsj.com",
                        'location_id' => 13,
                        'product_id'   => 6,
                    ],
                    // WSJ. Custom Studios
                    [
                        'full_name'    => "Thomas Grimshaw",
                        'department'   => "",
                        'job_title'    => "Sales Director, Special Features & Mansion",
                        'phone_number' => "212.597.5646",
                        'email'        => "thomas.grimshaw@dowjones.com",
                        'location_id' => 13,
                        'product_id'   => 6,
                    ],
                    // WSJ. Insights
                    [
                        'full_name'    => "Elizabeth Nann",
                        'department'   => "",
                        'job_title'    => "Executive Director, Global Consumer Insights",
                        'phone_number' => "212.597.5752",
                        'email'        => "elizabeth.nann@wsj.com",
                        'location_id' => 13,
                        'product_id'   => 7,
                    ],
                    // New Business Inquiries
                    [
                        'full_name'    => "Kelly Compton",
                        'department'   => "",
                        'job_title'    => "Account Executive",
                        'phone_number' => "312.750.4159",
                        'email'        => "Kelly.Compton@wsj.com",
                        'location_id' => 13,
                        'product_id'   => 8,
                    ],
                    [
                        'full_name'    => "Gloria Hauter",
                        'department'   => "",
                        'job_title'    => "Account Executive",
                        'phone_number' => "312.750.4235",
                        'email'        => "Gloria.Hauter@wsj.com",
                        'location_id' => 13,
                        'product_id'   => 8,
                    ],
                ];

            foreach ($allContacts as $contactPerson) {
                $contact               = new Contact;
                $contact->full_name    = $contactPerson['full_name'];
                $contact->department   = $contactPerson['department'];
                $contact->job_title    = $contactPerson['job_title'];
                $contact->phone_number = $contactPerson['phone_number'];
                $contact->email        = $contactPerson['email'];
                $contact->location_id  = $contactPerson['location_id'];
                $contact->product_id   = $contactPerson['product_id'];
                $contact->order = 1;

                echo "Creating contact " . $contact->full_name . " <" . $contact->location_id . " under " . $contact->product_id . ">" . "\n";

                if ($contact->isModelValid()) {
                    //$contactLocation    =   $contact->location_id > 0 ? \Hyfn\Models\ContactLocation::where("id", "=", $contact->location_id)->first() : 0;
                    //$siteProduct        =   $contact->product_id > 0  ? \Hyfn\Models\SiteProduct::where("id", "=", $contact->product_id)->first(): 0;
                    $contact->save();
                    //echo "Created contact " . $contact->full_name . " <" . $contactLocation->name . " under " . $siteProduct->name . ">";

                } else {
                    echo "<pre>" . print_r($contact->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
