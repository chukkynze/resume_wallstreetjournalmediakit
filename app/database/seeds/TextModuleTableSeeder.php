<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       TextModuleTableSeeder
     *
     * filename:    TextModuleTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Modules\TextModule;

    class TextModuleTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Text Module Data.\n";
            $this->table      = 'text_modules';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    //////////// START LANDING PAGE SNIPPET CONTENT
                    // PRODUCT LANDING SNIPPET: newspaper
                    [
                        'id'         => 1,
                        'upload_id' => 25,
                        'has_border' => true,
                        'title'   => "Newspaper",
                        'is_live' => true,
                    ],
                    // PRODUCT LANDING SNIPPET: online
                    [
                        'id'         => 2,
                        'upload_id' => 32,
                        'has_border'=> true,
                        'title'   => "Online",
                        'is_live' => true,
                    ],
                    //PRODUCT LANDING SNIPPET: mobile
                    [
                        'id'         => 3,
                        'upload_id' => 31,
                        'has_border' => true,
                        'title'   => "Mobile",
                        'is_live' => true,
                    ],
                    //PRODUCT LANDING SNIPPET: video
                    [
                        'id'         => 4,
                        'upload_id'  => 34,
                        'title'   => "Video",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //PRODUCT LANDING SNIPPET: global
                    [
                        'id'         => 5,
                        'upload_id' => 28,
                        'title'   => "Global",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //PRODUCT LANDING SNIPPET: magazine
                    [
                        'id'         => 6,
                        'upload_id' => 35,
                        'title'   => "Magazine",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //PRODUCT LANDING SNIPPET: marketwatch
                    [
                        'id'         => 7,
                        'upload_id' => 30,
                        'title'   => "Marketwatch",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //PRODUCT LANDING SNIPPET: journal report
                    [
                        'id'         => 8,
                        'upload_id' => 29,
                        'title'   => "Journal Report",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //PRODUCT LANDING SNIPPET: conferences
                    [
                        'id'         => 9,
                        'upload_id' => 27,
                        'title'   => "Conferences",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    ////////////END LANDING PAGE SNIPPET CONTENT

                    //////////// START DETAIL PAGES

                    //NEWSPAPER DETAIL module 1
                    [
                        'id'         => 10,
                        'upload_id' => 25,
                        'title'   => "Newspaper Heading",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //NEWSPAPER DETAIL module 5
                    [
                        'id'         => 11,
                        'upload_id' => 0,
                        'title'   => "Global",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //ONLINE DETAIL module 2
                    [
                        'id'         => 12,
                        'upload_id' => 0,
                        'title'   => "WSJ. Magazine",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //ONLINE DETAIL module 3
                    [
                        'id'         => 13,
                        'upload_id' => 0,
                        'title'   => "Capabilities Body",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //ONLINE DETAIL module 4
                    [
                        'id'         => 14,
                        'upload_id' => 0,
                        'title'   => "Awards",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //ONLINE DETAIL module 5
                    [
                        'id'         => 15,
                        'upload_id' => 0,
                        'title'   => "Heading",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //ONLINE DETAIL module 6
                    [
                        'id'         => 16,
                        'upload_id' => 0,
                        'title'   => "Global",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //start mobile detail pages
                    //MOBILE DETAIL module 2
                    [
                        'id'         => 17,
                        'upload_id' => 0,
                        'title'   => "The Wall Street Journal",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //MOBILE DETAIL module 3
                    [
                        'id'         => 18,
                        'upload_id' => 0,
                        'title'   => "WSJ. Magazine",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //MOBILE DETAIL module 4
                    [
                        'id'         => 19,
                        'upload_id' => 0,
                        'title'   => "WSJ Live",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //MOBILE DETAIL module 5
                    [
                        'id'         => 20,
                        'upload_id' => 0,
                        'title'   => "MarketWatch",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //MOBILE DETAIL module 6
                    [
                        'id'         => 21,
                        'upload_id' => 0,
                        'title'   => "Barron's",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //MOBILE DETAIL module 7
                    [
                        'id'         => 22,
                        'upload_id' => 0,
                        'title'   => "MarketWatch",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //start video detail pages
                    //VIDEO DETAIL module 1
                    [
                        'id'         => 23,
                        'upload_id' => 0,
                        'title'   => "Barron's",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //VIDEO DETAIL module 2
                    [
                        'id'         => 24,
                        'upload_id' => 0,
                        'title'   => "Global",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //start global detail pages
                    //GLOBAL DETAIL module 2
                    [
                        'id'         => 25,
                        'upload_id' => 0,
                        'title'   => "Heading",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //GLOBAL DETAIL module 3
                    [
                        'id'         => 26,
                        'upload_id' => 0,
                        'title'   => "The Wall Steet Journal",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //GLOBAL DETAIL module 4
                    [
                        'id'         => 27,
                        'upload_id' => 0,
                        'title'   => "Online",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //GLOBAL DETAIL module 5
                    [
                        'id'         => 28,
                        'upload_id' => 0,
                        'title'   => "Online",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //GLOBAL DETAIL module 6
                    [
                        'id'         => 29,
                        'upload_id' => 0,
                        'title'   => "Empty Title",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //GLOBAL DETAIL module 7
                    [
                        'id'         => 30,
                        'upload_id' => 0,
                        'title'   => "Empty Title",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //GLOBAL DETAIL module 8
                    [
                        'id'         => 31,
                        'upload_id' => 0,
                        'title'   => "Empty Title",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //GLOBAL DETAIL module 9
                    [
                        'id'         => 32,
                        'upload_id' => 0,
                        'title'   => "Empty Title",
                        'is_live' => true,
                        'has_border' => false,
                    ],

                    // WSJ Magazine - connect
                    [
                        'id'         => 33,
                        'upload_id' => 0,
                        'title'   => "Connect",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //start marketwatch detail pages
                    //MARKETWATCH DETAIL module 2
                    [
                        'id'         => 34,
                        'upload_id'  => 34,
                        'title'   => "",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //MARKETWATCH DETAIL module 3
                    [
                        'id'         => 35,
                        'upload_id'  => 0,
                        'title'   => "Online",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //MARKETWATCH DETAIL module 4
                    [
                        'id'         => 36,
                        'upload_id'  => 0,
                        'title'   => "Mobile",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //start conferences detail pages
                    //CONFERENCES DETAIL module 1
                    [
                        'id'         => 37,
                        'upload_id'  => 0,
                        'title'   => "Heading",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //CONFERENCES DETAIL module 2
                    [
                        'id'         => 38,
                        'upload_id'  => 0,
                        'title'   => "CEO Council",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //CONFERENCES DETAIL module 3
                    [
                        'id'         => 39,
                        'upload_id'  => 0,
                        'title'   => "CFO Network",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //CONFERENCES DETAIL module 4
                    [
                        'id'         => 40,
                        'upload_id'  => 0,
                        'title'   => "CIO Network",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //CONFERENCES DETAIL module 5
                    [
                        'id'         => 41,
                        'upload_id'  => 0,
                        'title'   => "WSJDLive",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //CONFERENCES DETAIL module 6
                    [
                        'id'         => 42,
                        'upload_id'  => 0,
                        'title'   => "ECO:nomics",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //CONFERENCES DETAIL module 7
                    [
                        'id'         => 43,
                        'upload_id'  => 0,
                        'title'   => "Global Compliance Symposium",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //CONFERENCES DETAIL module 8
                    [
                        'id'         => 44,
                        'upload_id'  => 0,
                        'title'   => "Heard on the Street Live",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //CONFERENCES DETAIL module 9
                    [
                        'id'         => 45,
                        'upload_id'  => 0,
                        'title'   => "Private Equity Analyst Conference",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //CONFERENCES DETAIL module 10
                    [
                        'id'         => 46,
                        'upload_id'  => 0,
                        'title'   => "Viewpoints",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //INSIGHTS module 1
                    [
                        'id'         => 47,
                        'upload_id'  => 34,
                        'title'   => "",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //Custom Studios module 1
                    [
                        'id'         => 48,
                        'upload_id' => 0,
                        'title'   => "Heading",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //Custom Studios module 3
                    [
                        'id'         => 49,
                        'upload_id' => 0,
                        'title'   => "Capabilities Body",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //Custom Studios module 4
                    [
                        'id'         => 50,
                        'upload_id' => 94,
                        'title'   => "Awards",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //Global Details Page
                    [
                        'id'        => 51,
                        'upload_id' => 35,
                        'title'   => "",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //Custom Std
                    [
                        'id'         => 52,
                        'upload_id'  => 26,
                        'title'   => "Capabilities",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //Insights - Cap
                    [
                        'id'         => 53,
                        'upload_id'  => 26,
                        'title'   => "Capabilities",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //Insights - Upcoming
                    [
                        'id'         => 54,
                        'upload_id'  => 0,
                        'title'   => "Upcoming",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    //global - mobile - asia
                    [
                        'id'         => 55,
                        'upload_id'  => 0,
                        'title'   => "Asia",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //global - mobile - europe
                    [
                        'id'         => 56,
                        'upload_id'  => 0,
                        'title'   => "Europe",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //global - magazine - europe
                    [
                        'id'         => 57,
                        'upload_id'  => 0,
                        'title'   => "Europe",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //global - magazine - latin america
                    [
                        'id'         => 58,
                        'upload_id'  => 0,
                        'title'   => "Latin America",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //mobile - what's news
                    [
                        'id'         => 59,
                        'upload_id'  => 0,
                        'title'   => "What's News",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //journal report - recurring series
                    [
                        'id'         => 60,
                        'upload_id'  => 0,
                        'title'   => "Recurring Series",
                        'is_live' => true,
                        'has_border' => false,
                    ],
                    //video product - distribution
                    [
                        'id'         => 61,
                        'upload_id'  => 0,
                        'title'   => "Distribution",
                        'is_live' => true,
                        'has_border' => true,
                    ],
                    // Case Study 1
                    [
                        'id'         => 62,
                        'upload_id'  => 0,
                        'title'      => "Case Study 1",
                        'is_live' => false,
                        'has_border' => false,
                    ],
                    // Case Study 2
                    [
                        'id'         => 63,
                        'upload_id'  => 0,
                        'title'      => "Case Study 2",
                        'is_live'    => true,
                        'has_border' => false,
                    ],
                    // Case Study 3
                    [
                        'id'         => 64,
                        'upload_id'  => 0,
                        'title'      => "Case Study 3",
                        'is_live'    => true,
                        'has_border' => false,
                    ],
                    // Case Study 4
                    [
                        'id'         => 65,
                        'upload_id'  => 0,
                        'title'      => "Case Study 4",
                        'is_live'    => true,
                        'has_border' => true,
                    ],
                ];


            $dir = preg_replace('/\.php$/', '', __FILE__);

            $seedCount = 0;
            foreach ($Rows as $row) {

                $Model = new TextModule();

                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }
                $Model->body = file_get_contents($dir . '/' . $row['id'] . '.html');
                $Model->body = $Model->body != "" ? $Model->body : "&nbsp;";


                echo "Creating Text Module [" . $Model->id . " - " . $Model->title . " ].\n";

                if ($Model->isValid()) {

                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
