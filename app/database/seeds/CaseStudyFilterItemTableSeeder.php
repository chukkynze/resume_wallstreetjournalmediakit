<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       CaseStudyFilterItemTableSeeder
     *
     * filename:    CaseStudyFilterItemTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\CaseStudyFilterItem;

    class CaseStudyFilterItemTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Case Study Filter Items Data.\n";
            $this->table      = 'case_study_filter_items';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    // Product Filter Items - 1
                    [
                        'id'          => 1,
                        'name'        => "The Wall Street Journal",
                        'filter_id'   => 1,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 2,
                        'name'        => "Conferences",
                        'filter_id'   => 1,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 3,
                        'name'        => "MarketWatch",
                        'filter_id'   => 1,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 4,
                        'name'        => "WSJ. Magazine",
                        'filter_id'   => 1,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 5,
                        'name'        => "WSJ. Custom Studios",
                        'filter_id'   => 1,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 6,
                        'name'        => "WSJ. Insights",
                        'filter_id'   => 1,
                        'description' => "",
                        'order'       => 1,
                    ],
                    // Type Filter Items - 2
                    [
                        'id'          => 7,
                        'name'        => "Custom Research",
                        'filter_id'   => 2,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 8,
                        'name'        => "Digital Advertising",
                        'filter_id'   => 2,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 9,
                        'name'        => "Events",
                        'filter_id'   => 2,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 10,
                        'name'        => "Infographics",
                        'filter_id'   => 2,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 11,
                        'name'        => "Native Advertising",
                        'filter_id'   => 2,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 12,
                        'name'        => "Video",
                        'filter_id'   => 2,
                        'description' => "",
                        'order'       => 1,
                    ],
                    // Category Filter Items - 3
                    [
                        'id'          => 13,
                        'name'        => "Automotive",
                        'filter_id'   => 3,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 14,
                        'name'        => "Energy",
                        'filter_id'   => 3,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 15,
                        'name'        => "Finance",
                        'filter_id'   => 3,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 16,
                        'name'        => "Spirits",
                        'filter_id'   => 3,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 17,
                        'name'        => "Tech",
                        'filter_id'   => 3,
                        'description' => "",
                        'order'       => 1,
                    ],
                    [
                        'id'          => 18,
                        'name'        => "Travel",
                        'filter_id'   => 3,
                        'description' => "",
                        'order'       => 1,
                    ],
                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new CaseStudyFilterItem();
                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }

                echo "Creating Case Study Filter Item: \"" . $Model->filter_id . " - " . $Model->name . "\".\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
