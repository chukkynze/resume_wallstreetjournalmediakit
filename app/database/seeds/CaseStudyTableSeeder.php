<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       CaseStudyTableSeeder
     *
     * filename:    CaseStudyTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\CaseStudy;

    class CaseStudyTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Case Study Data.\n";
            $this->table      = 'case_studies';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    [
                        'id'             => 1,
                        'headline'       => "Case Study 1",
                        'description'    => "Eu explicari interesset reprehendunt pri, in vide possim ius.",
                        'keywords'       => "Eu, explicari, interesset, reprehendunt, pri",
                        'listing_pic_id' => 0,
                        'is_live' => 0,
                        'client_id'      => 1,
                        'order'          => 1,
                    ],
                    [
                        'id'             => 2,
                        'headline'       => "Case Study 2",
                        'description'    => "Eum ea porro forensibus, saepe conceptam liberavisse ne sit. Eum ea porro forensibus, saepe conceptam liberavisse ne sit. Eum ea porro forensibus, saepe conceptam liberavisse ne sit. Eum ea porro forensibus, saepe conceptam liberavisse ne sit. Eum ea porro forensibus, saepe conceptam liberavisse ne sit. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.",
                        'keywords'       => "Eu, explicari, interesset, reprehendunt, pri",
                        'listing_pic_id' => 0,
                        'is_live' => 1,
                        'client_id'      => 1,
                        'order'          => 1,
                    ],
                    [
                        'id'             => 3,
                        'headline'       => "Case Study 3",
                        'description'    => "Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.  Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.",
                        'keywords'       => "Eu, explicari, interesset, reprehendunt, pri",
                        'listing_pic_id' => 0,
                        'is_live' => 1,
                        'client_id'      => 1,
                        'order'          => 1,
                    ],
                    [
                        'id'             => 4,
                        'headline'       => "Case Study 4",
                        'description'    => "Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.  Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.  Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.  Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.  Eu explicari interesset reprehendunt pri, in vide possim ius. Eum ea porro forensibus, saepe conceptam liberavisse ne sit.",
                        'keywords'       => "Eu, explicari, interesset, reprehendunt, pri",
                        'listing_pic_id' => 0,
                        'is_live' => 1,
                        'client_id'      => 1,
                        'order'          => 1,
                    ],
                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model                        = new CaseStudy();
                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }

                echo "Creating Case Study: \"" . $Model->headline . "\".\n";

                if ($Model->isModelValid()) {
                    $Model->client_name = '';
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
