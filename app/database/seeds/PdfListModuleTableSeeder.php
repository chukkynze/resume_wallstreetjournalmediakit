<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       PdfListModuleTableSeeder
     *
     * filename:    PdfListModuleTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Modules\PdfListModule;
    use Hyfn\Models\Upload;

    class PdfListModuleTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Pdf List Module Data.\n";
            $this->table      = 'pdf_list_modules';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    //newspaper detail module 2
                    [
                        'id'         => 1,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'title'   => "Heading",
                        'upload_ids' => [105, 106, 107, 78, 4, 109],
                    ],
                    //newspaper detail module 3
                    [
                        'id'         => 2,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'title'   => "Weekday",
                        'upload_ids' => [73, 104, 68, 66],
                    ],
                    //newspaper detail module 4
                    [
                        'id'         => 3,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'title'   => "Weekend",
                        'upload_ids' => [74, 108],
                    ],
                    //Online detail module 1
                    [
                        'id'         => 4,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'title'   => "Heading",
                        'upload_ids' => [101],
                    ],
                    //Mobile detail module 1
                    [
                        'id'         => 5,
                        'upload_id'  => 0,
                        'has_border' => true,
                        'title'   => "Heading",
                        'upload_ids' => [91, 92, 102],
                    ],
                    //Global detail module 1 Asia - Newspaper
                    [
                        'id'         => 6,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'title'   => "Asia",
                        'upload_ids' => [71],
                    ],
                    //Global detail module 6 Europe - 29 - Newspaper
                    [
                        'id'         => 7,
                        'upload_id'  => 0,
                        'has_border' => true,
                        'title'   => "Europe",
                        'upload_ids' => [72],
                    ],
                    //Global detail module Latin America - 30 - Newspaper
                    [
                        'id'         => 8,
                        'upload_id'  => 0,
                        'has_border' => true,
                        'title'   => "Latin America",
                        'upload_ids' => [],
                    ],
                    //WSJ Magazine detail module 2 Audience
                    [
                        'id'         => 9,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'title'   => "Audience",
                        'upload_ids' => [83],
                    ],
                    //WSJ Magazine detail module 3 Edit Calendar
                    [
                        'id'         => 10,
                        'upload_id'  => 0,
                        'has_border' => true,
                        'title'   => "Edit Calendar",
                        'upload_ids' => [84],
                    ],
                    //WSJ Magazine detail module 5 Rates & Specs
                    [
                        'id'         => 11,
                        'upload_id'  => 0,
                        'has_border' => true,
                        'title'   => "Rates & Specs",
                        'upload_ids' => [88, 89],
                    ],
                    //WSJ Magazine detail module 6 What's New & Next
                    [
                        'id'         => 12,
                        'upload_id'  => 0,
                        'has_border' => true,
                        'upload_ids' => [86],
                        'title'   => "New & Next",
                    ],
                    //WSJ Magazine detail module 8 Awards & Press
                    [
                        'id'         => 13,
                        'upload_id'  => 0,
                        'has_border' => true,
                        'upload_ids' => [87],
                        'title'   => "Awards & Press",
                    ],

                    //WSJ Magazine detail module 10 Events
                    [
                        'id'         => 15,
                        'upload_id'  => 0,
                        'has_border' => true,
                        'upload_ids' => [85],
                        'title'   => "Events",
                    ],
                    //Marketwatch detail module 1
                    [
                        'id'         => 16,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'upload_ids' => [70, 103],
                        'title'   => "Heading",
                    ],
                    //Journal Report detail module 1
                    [
                        'id'         => 17,
                        'upload_id' => 0,
                        'has_border' => true,
                        'upload_ids' => [66],
                        'title'   => "Heading",
                    ],
                    //Insights module 2
                    [
                        'id'         => 18,
                        'upload_id'  => 33,
                        'has_border' => true,
                        'title'   => "Reports",
                        'upload_ids' => [82],
                    ],
                    // New modules for online
                    [
                        'id'         => 19,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'title'   => "The Wall Street Journal",
                        'upload_ids' => [90],
                    ],
                    [
                        'id'         => 20,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'upload_ids' => [70],
                        'title'   => "MarketWatch",
                    ],
                    [
                        'id'         => 21,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'upload_ids' => [64],
                        'title'   => "Barron's",
                    ],
                    [
                        'id'         => 22,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'upload_ids' => [101],
                        'title'   => "Heading",
                    ],
                    [
                        'id'         => 23,
                        'upload_id'  => 0,
                        'has_border' => false,
                        'upload_ids' => [],
                        'title'   => "Heading",
                    ],

                ];


            $dir               = preg_replace('/\.php$/', '', __FILE__);

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model             = new PdfListModule();
                $Model->id         = $row['id'];
                $Model->upload_id  = $row['upload_id'];
                $Model->title  =     $row['title'];
                $Model->body   = file_get_contents($dir . '/' . $row['id'] . '_body.html');
                $Model->body_2 = file_get_contents($dir . '/' . $row['id'] . '_body2.html');
                $Model->has_border = $row['has_border'];

                echo "Creating Pdf List Module [" . $Model->id . "].\n";

                if ($Model->isValid())
                {

                    $Model->save();
                    $Model->files()->sync($row['upload_ids']);

                    foreach($row['upload_ids'] as $uploadID)
                    {
                        $Upload             =   Upload::find($uploadID);
                        $Upload->is_live    =   1;

                        if ($Upload->isModelValid())
                        {
                            $Upload->save();
                        }
                        else
                        {
                            echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                        }
                    }

                    $seedCount++;

                }
                else
                {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
