<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       SiteProductSectionTableSeeder
     *
     * filename:    SiteProductSectionTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/07/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\SiteProductSection;

    class SiteProductSectionTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Site Product Section Data.\n";
            $this->table      = 'site_product_sections';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    // Newspaper
                    [
                        'id'                       => 1,
                        'name'                     => "US Weekday",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("US Weekday"),
                        'site_product_id'          => 8,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Weekday",
                        'order' => 1,
                    ],
                    [
                        'id'                       => 2,
                        'name'                     => "US Weekend",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("US Weekend"),
                        'site_product_id'          => 8,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Weekend",
                        'order' => 2,
                    ],
                    [
                        'id'                       => 3,
                        'name'                     => "Global",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Global"),
                        'site_product_id'          => 8,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Global",
                        'order' => 3,
                    ],
                    // Online
                    [
                        'id'                       => 4,
                        'name'                     => "The Wall Street Journal",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("The Wall Street Journal"),
                        'site_product_id'          => 9,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "THE WALL STREET JOURNAL",
                        'order' => 1,
                    ],
                    [
                        'id'                       => 5,
                        'name'                     => "WSJ. Magazine",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("WSJ. Magazine"),
                        'site_product_id'          => 9,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "WSJ. MAGAZINE",
                        'order' => 2,
                    ],
                    [
                        'id'                       => 6,
                        'name'                     => "MarketWatch",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("MarketWatch"),
                        'site_product_id'          => 9,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "MARKETWATCH",
                        'order' => 3,
                    ],
                    [
                        'id'                       => 7,
                        'name'                     => "Barron’s",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Barron’s"),
                        'site_product_id'          => 9,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "BARRON’S",
                        'order' => 4,
                    ],
                    [
                        'id'                       => 8,
                        'name'                     => "Global",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Global"),
                        'site_product_id'          => 9,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "GLOBAL",
                        'order' => 5,
                    ],
                    // Mobile
                    [
                        'id'                       => 9,
                        'name'                     => "The Wall Street Journal",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("The Wall Street Journal"),
                        'site_product_id'          => 10,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "The Wall Street Journal",
                        'order' => 1,
                    ],
                    [
                        'id'                       => 10,
                        'name'                     => "WSJ. Magazine",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("WSJ. Magazine"),
                        'site_product_id'          => 10,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "WSJ. Magazine",
                        'order' => 2,
                    ],
                    [
                        'id'              => 11,
                        'name'            => "WSJ Live",
                        'heading'         => "",
                        'name_slug'       => Str::slug("WSJ Live"),
                        'site_product_id' => 10,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "WSJ Live",
                        'order'           => 3,
                    ],
                    [
                        'id'              => 12,
                        'name'            => "MarketWatch",
                        'heading'         => "",
                        'name_slug'       => Str::slug("MarketWatch"),
                        'site_product_id' => 10,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "MarketWatch",
                        'order'           => 4,
                    ],
                    [
                        'id'              => 13,
                        'name'            => "Barron’s",
                        'heading'         => "",
                        'name_slug'       => Str::slug("Barron’s"),
                        'site_product_id' => 10,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "Barron’s",
                        'order'           => 5,
                    ],
                    [
                        'id'              => 14,
                        'name'            => "Global",
                        'heading'         => "",
                        'name_slug'       => Str::slug("Global"),
                        'site_product_id' => 10,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "Global",
                        'order'           => 6,
                    ],
                    // Video
                    [
                        'id'              => 15,
                        'name'            => "Programming",
                        'heading'         => "",
                        'name_slug'       => Str::slug("Programming"),
                        'site_product_id' => 11,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "Programming",
                        'order'           => 1,
                    ],
                    [
                        'id'              => 16,
                        'name'            => "Distribution",
                        'heading'         => "",
                        'name_slug'       => Str::slug("Distribution"),
                        'site_product_id' => 11,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "Distribution",
                        'order'           => 2,
                    ],
                    // MarketWatch
                    [
                        'id'              => 17,
                        'name'            => "Online",
                        'heading'         => "",
                        'name_slug'       => Str::slug("Online"),
                        'site_product_id' => 4,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "Online",
                        'order'           => 1,
                    ],
                    [
                        'id'              => 18,
                        'name'            => "Mobile",
                        'heading'         => "",
                        'name_slug'       => Str::slug("Mobile"),
                        'site_product_id' => 4,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "Mobile",
                        'order'           => 2,
                    ],
                    // Conferences
                    [
                        'id'              => 19,
                        'name'            => "CEO Council",
                        'heading'         => "",
                        'name_slug'       => Str::slug("CEO Council"),
                        'site_product_id' => 5,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 0,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "CEO Council",
                        'order'           => 1,
                    ],
                    [
                        'id'              => 20,
                        'name'            => "CFO Network",
                        'heading'         => "",
                        'name_slug'       => Str::slug("CFO Network"),
                        'site_product_id' => 5,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 0,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "CFO Network",
                        'order'           => 2,
                    ],
                    [
                        'id'              => 21,
                        'name'            => "CIO Network",
                        'heading'         => "",
                        'name_slug'       => Str::slug("CIO Network"),
                        'site_product_id' => 5,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 0,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "CIO Network",
                        'order'           => 3,
                    ],
                    [
                        'id'              => 22,
                        'name'            => "WSJDLive",
                        'heading'         => "",
                        'name_slug'       => Str::slug("WSJDLive"),
                        'site_product_id' => 5,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 0,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "WSJDLive",
                        'order'           => 4,
                    ],
                    [
                        'id'              => 23,
                        'name'            => "ECO:nomics",
                        'heading'         => "",
                        'name_slug'       => Str::slug("ECO:nomics"),
                        'site_product_id' => 5,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 0,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "ECO:nomics",
                        'order'           => 5,
                    ],
                    [
                        'id'              => 24,
                        'name'            => "Global Compliance Symposium",
                        'heading'         => "",
                        'name_slug'       => Str::slug("Global Compliance Symposium"),
                        'site_product_id' => 5,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 0,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "Global Compliance Symposium",
                        'order'           => 6,
                    ],
                    [
                        'id'              => 25,
                        'name'            => "Heard on the Street Live",
                        'heading'         => "",
                        'name_slug'       => Str::slug("Heard on the Street Live"),
                        'site_product_id' => 5,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 0,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "Heard on the Street Live",
                        'order'           => 7,
                    ],
                    [
                        'id'              => 26,
                        'name'      => "Private Equity Analyst Conference",
                        'heading'         => "",
                        'name_slug' => Str::slug("Private Equity Analyst Conference"),
                        'site_product_id' => 5,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 0,
                        'only_headings_on_landing' => 0,
                        'link_text' => "Private Equity Analyst Conference",
                        'order'           => 8,
                    ],
                    [
                        'id'              => 27,
                        'name'            => "Viewpoints",
                        'heading'         => "",
                        'name_slug'       => Str::slug("Viewpoints"),
                        'site_product_id' => 5,
                        'enable_link'     => 1,
                        'enable_all_caps'          => 0,
                        'only_headings_on_landing' => 0,
                        'link_text'       => "Viewpoints",
                        'order'           => 9,
                    ],
                    // Global
                    [
                        'id'                       => 28,
                        'name'                     => "Asia",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Asia"),
                        'site_product_id'          => 2,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Asia",
                        'order' => 1,
                    ],
                    [
                        'id'                       => 29,
                        'name'                     => "Europe",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Europe"),
                        'site_product_id'          => 2,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Europe",
                        'order' => 2,
                    ],
                    [
                        'id'                       => 30,
                        'name'                     => "Latin America",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Latin America"),
                        'site_product_id'          => 2,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Latin America",
                        'order' => 3,
                    ],
                    // WSJ Magazine
                    [
                        'id'                       => 31,
                        'name'                     => "Audience",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Audience"),
                        'site_product_id'          => 3,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Audience",
                        'order' => 1,
                    ],
                    [
                        'id'                       => 32,
                        'name'                     => "Edit Calendar",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Edit Calendar"),
                        'site_product_id'          => 3,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Edit Calendar",
                        'order' => 2,
                    ],
                    [
                        'id'                       => 33,
                        'name'                     => "Rates & Specs",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Rates & Specs"),
                        'site_product_id'          => 3,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Rates & Specs",
                        'order' => 3,
                    ],
                    [
                        'id'                       => 34,
                        'name'                     => "What's New & Next",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("What's New & Next"),
                        'site_product_id'          => 3,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "What's New & Next",
                        'order' => 4,
                    ],
                    [
                        'id'                       => 35,
                        'name'                     => "Awards & Press",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Awards & Press"),
                        'site_product_id'          => 3,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Awards & Press",
                        'order' => 5,
                    ],
                    [
                        'id'                       => 36,
                        'name'                     => "Events",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Events"),
                        'site_product_id'          => 3,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Events",
                        'order' => 6,
                    ],
                    [
                        'id'                       => 37,
                        'name'                     => "Connect",
                        'heading'                  => "",
                        'name_slug'                => Str::slug("Connect"),
                        'site_product_id'          => 3,
                        'enable_link'              => 1,
                        'enable_all_caps'          => 1,
                        'only_headings_on_landing' => 0,
                        'link_text'                => "Connect",
                        'order' => 7,
                    ],
                ];

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model                           = new SiteProductSection();
                $Model->id = $row['id'];
                $Model->name                     = $row['name'];
                $Model->heading                  = $row['heading'];
                $Model->name_slug                = $row['name_slug'];
                $Model->site_product_id          = $row['site_product_id'];
                $Model->enable_link              = $row['enable_link'];
                $Model->enable_all_caps          = $row['enable_all_caps'];
                $Model->only_headings_on_landing = $row['only_headings_on_landing'];
                $Model->link_text                = $row['link_text'];
                $Model->order = $row['order'];

                echo "Creating Site Product Section " . $Model->name . " ." . "\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
