<?php

    use Hyfn\Models\User;

    class UsersTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding User Data.\n";
            $this->table      = 'users';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            // Generate a random 12 digit password
            $password = '';
            for ($i = 0; $i < 13; $i++) {
                $ascii_code = rand(50, 122);
                $password .= chr($ascii_code);
            }

            $password = "Wsj!123";

            $user                    = new User;
            $user->username          = 'admin';
            $user->email             = 'chukky.nze@hyfn.com';
            $user->password          = Hash::make($password);
            $user->confirmation_code = md5(uniqid(mt_rand(), true));
            $user->first_name        = 'Chukky';
            $user->last_name         = 'Nze';
            $user->confirmed         = 1;

            if (!$user->forceSave()) {
                echo "Unable to create user\n";
                echo "<pre>" . print_r($user->errors(), 1) . "</pre>" . "\n";
                echo "<pre>" . print_r($user->getErrors(), 1) . "</pre>" . "\n";
            } else {
                echo "Created user " . $user->username . " <" . $user->email . ">" . "\n";
            }

            echo "Admin Password: " . $password . "\n";

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');


            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
