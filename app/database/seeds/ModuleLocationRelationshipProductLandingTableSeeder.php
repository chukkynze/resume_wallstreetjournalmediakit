<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       ModuleLocationRelationshipTableSeeder
     *
     * filename:    ModuleLocationRelationshipTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\ModuleLocationRelationship;

    class ModuleLocationRelationshipProductLandingTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Module Location Relationship Data for Product Landing Page Sections.\n";
            $this->table      = 'module_location_relationships';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            //DB::connection($this->connection)->table($this->table)->truncate();

            // newspaper
            // online
            // mobile
            // video
            // global
            // wsj magazine
            // marketwatch
            // journal report
            // conference

            $Rows =
                [
                    //// START Product Landing Page
                    // Product Landing - Newspaper
                    [
                        'id'          => 1,
                        'location_id' => 3,
                        'entity_id'   => 8,
                        'entity_type' => "Hyfn\\Models\\SiteProduct",
                        'module_id'   => 1,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],

                   // Product Landing - Online
                   [
                       'id'          => 2,
                       'location_id' => 3,
                       'entity_id'   => 9,
                       'entity_type' => "Hyfn\\Models\\SiteProduct",
                       'module_id'   => 2,
                       'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                       'order' => 1,
                   ],


                   // Product Landing - Mobile
                   [
                       'id'          => 3,
                       'location_id' => 3,
                       'entity_id'   => 10,
                       'entity_type' => "Hyfn\\Models\\SiteProduct",
                       'module_id'   => 3,
                       'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                       'order' => 1,
                   ],


                   // Product Landing - Video
                   [
                       'id'          => 4,
                       'location_id' => 3,
                       'entity_id'   => 11,
                       'entity_type' => "Hyfn\\Models\\SiteProduct",
                       'module_id'   => 4,
                       'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                       'order' => 1,
                   ],

                    // Product Landing - Global
                    [
                        'id'          => 5,
                        'location_id' => 3,
                        'entity_id'   => 2,
                        'entity_type' => "Hyfn\\Models\\SiteProduct",
                        'module_id'   => 5,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Product Landing - WSJ. Magazine
                    [
                        'id'          => 6,
                        'location_id' => 3,
                        'entity_id'   => 3,
                        'entity_type' => "Hyfn\\Models\\SiteProduct",
                        'module_id'   => 6,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Product Landing - MarketWatch
                    [
                        'id'          => 7,
                        'location_id' => 3,
                        'entity_id'   => 4,
                        'entity_type' => "Hyfn\\Models\\SiteProduct",
                        'module_id'   => 7,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],

                    // Product Landing - Journal Report
                    [
                        'id'          => 8,
                        'location_id' => 3,
                        'entity_id'   => 12,
                        'entity_type' => "Hyfn\\Models\\SiteProduct",
                        'module_id'   => 8,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],

                    // Product Landing - Conferences
                    [
                        'id'          => 9,
                        'location_id' => 3,
                        'entity_id'   => 5,
                        'entity_type' => "Hyfn\\Models\\SiteProduct",
                        'module_id'   => 9,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],

                    //// END Product Landing Page


                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model              = new ModuleLocationRelationship();
                $Model->id          = $row['id'];
                $Model->location_id = $row['location_id'];

                $Model->entity_id   = $row['entity_id'];
                $Model->entity_type = $row['entity_type'];

                $Model->module_id   = $row['module_id'];
                $Model->module_type = $row['module_type'];

                $Model->order = $row['order'];
                $Model->save();

                echo "Creating Module Location Relationship Type [" . $Model->id . "].\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
