<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       ModuleLocationTableSeeder
     *
     * filename:    ModuleLocationTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\ModuleLocation;

    class ModuleLocationTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Module Location Data.\n";
            $this->table      = 'module_locations';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    [
                        'id'   => 1,
                        'name' => "Case Study Page",
                        'slug' => Str::slug("Case Study Page"),
                    ],
                    [
                        'id'   => 2,
                        'name' => "Insights Page",
                        'slug' => Str::slug("Insights Page"),
                    ],
                    [
                        'id'   => 3,
                        'name' => "Products Landing Page",
                        'slug' => Str::slug("Products Landing Page"),
                    ],
                    [
                        'id'   => 4,
                        'name' => "Products Details Page",
                        'slug' => Str::slug("Products Details Page"),
                    ],
                    [
                        'id'   => 5,
                        'name' => "Product Details Section",
                        'slug' => Str::slug("Product Details Section"),
                    ],
                    // Custom Studios
                    [
                        'id'   => 6,
                        'name' => "Custom Studios Capabilities",
                        'slug' => Str::slug("Custom Studios Capabilities"),
                    ],
                    [
                        'id'   => 7,
                        'name' => "Custom Studios Awards",
                        'slug' => Str::slug("Custom Studios Awards"),
                    ],
                    [
                        'id'   => 8,
                        'name' => "Custom Studios Main",
                        'slug' => Str::slug("Custom Studios Main"),
                    ],
                    [
                        'id'   => 9,
                        'name' => "Case Study Details Page",
                        'slug' => Str::slug("Case Study Details Page"),
                    ],
                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model       = new ModuleLocation();
                $Model->id   = $row['id'];
                $Model->name = $row['name'];
                $Model->slug = $row['slug'];
                $Model->save();

                echo "Creating Module Location \"" . $Model->name . "\".\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
