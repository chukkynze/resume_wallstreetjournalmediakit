<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       SiteProductsTableSeeder
     *
     * filename:    SiteProductsTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\SiteProduct;

    class SiteProductsTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Site Products Table Data.\n";
            $this->table      = 'site_products';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows = [
                [
                    'id'                     => 2,
                    'name'                   => 'Global',
                    'name_slug'              => Str::slug('Global'),
                    'on_menu'                => 1,
                    'on_product_landing'     => 1,
                    'heading_icon_upload_id' => 28,
                    'heading_vs_image'       => 0,
                    'section_order'          => 5,
                    'source_text'            => 'Source: ABC Jan – June 2014; 2.8 readers per copy WSJ Subscriber Study 2013',
                ],
                [
                    'id'                     => 3,
                    'name'                   => 'WSJ. Magazine',
                    'name_slug'              => 'wsjmagazine',
                    'on_menu'                => 1,
                    'on_product_landing'     => 1,
                    'heading_icon_upload_id' => 93,
                    'heading_vs_image'       => 1,
                    'section_order'          => 6,
                    'source_text'            => '',
                ],
                [
                    'id'                     => 4,
                    'name'                   => 'MarketWatch',
                    'name_slug'              => Str::slug('MarketWatch'),
                    'on_menu'                => 1,
                    'on_product_landing'     => 1,
                    'heading_icon_upload_id' => 30,
                    'heading_vs_image'       => 0,
                    'section_order'          => 7,
                    'source_text'            => 'Source: comScore Q3 2014',
                ],
                [
                    'id'                     => 5,
                    'name'                   => 'Conferences',
                    'name_slug'              => Str::slug('Conferences'),
                    'on_menu'                => 1,
                    'on_product_landing'     => 1,
                    'heading_icon_upload_id' => 27,
                    'heading_vs_image'       => 0,
                    'section_order'          => 9,
                    'source_text'            => '',
                ],
                [
                    'id'                     => 8,
                    'name'                   => 'Newspaper',
                    'name_slug'              => Str::slug('Newspaper'),
                    'on_menu'                => 1,
                    'on_product_landing'     => 1,
                    'heading_icon_upload_id' => 25,
                    'heading_vs_image'       => 0,
                    'section_order'          => 1,
                    'source_text'            => 'Source: August 2012 Pew Research Media Believability Report; AAM Publisher\'s Statement September 2014.',
                ],
                [
                    'id'                     => 9,
                    'name'                   => 'Online',
                    'name_slug'              => Str::slug('Online'),
                    'on_menu'                => 1,
                    'on_product_landing'     => 1,
                    'heading_icon_upload_id' => 32,
                    'heading_vs_image'       => 0,
                    'section_order'          => 2,
                    'source_text'            => 'Source: comScore Q3 2014 Worldwide Desktop Traffic + U.S. Mobile Exclusive Visitors.',
                ],
                [
                    'id'                     => 10,
                    'name'                   => 'Mobile',
                    'name_slug'              => Str::slug('Mobile'),
                    'on_menu'                => 1,
                    'on_product_landing'     => 1,
                    'heading_icon_upload_id' => 31,
                    'heading_vs_image'       => 0,
                    'section_order'          => 3,
                    'source_text'            => '',
                ],
                [
                    'id'                     => 11,
                    'name'                   => 'Video',
                    'name_slug'              => Str::slug('Video'),
                    'on_menu'                => 1,
                    'on_product_landing'     => 1,
                    'heading_icon_upload_id' => 28,
                    'heading_vs_image'       => 0,
                    'section_order'          => 4,
                    'source_text'            => 'Source: Internal Records Q2 2014, Omniture Q2 2014; comScore Q2 2014',
                ],
                [
                    'id'                     => 12,
                    'name'                   => 'Journal Report',
                    'name_slug'              => 'journalreport',
                    'on_menu'                => 1,
                    'on_product_landing'     => 1,
                    'heading_icon_upload_id' => 29,
                    'heading_vs_image'       => 0,
                    'section_order'          => 8,
                    'source_text'            => '',
                ],
            ];

            // Hotfix removed
            // Hotfix removed dupled

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new SiteProduct();
                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }

                echo "Creating Site Product " . $Model->name . " .\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
