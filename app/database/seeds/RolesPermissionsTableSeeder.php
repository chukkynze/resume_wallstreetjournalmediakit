<?php

    use Illuminate\Support\Facades\DB;
    use Hyfn\Models\User;
    use Hyfn\Models\Role;
    use Hyfn\Models\Permission;

    class RolesPermissionsTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Roles & Permissions Data.\n";
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table('roles')->truncate();

            $godRole       = new Role();
            $godRole->name = 'god';
            $godRole->save();

            $superadminRole       = new Role();
            $superadminRole->name = 'superadmin';
            $superadminRole->save();

            $adminRole       = new Role();
            $adminRole->name = 'admin';
            $adminRole->save();

            $userRole       = new Role();
            $userRole->name = 'user';
            $userRole->save();


            DB::connection($this->connection)->table('permissions')->truncate();

            $managePermissions               = new Permission;
            $managePermissions->name         = 'manage_permissions';
            $managePermissions->display_name = 'Manage Permissions';
            $managePermissions->save();

            $manageRoles               = new Permission;
            $manageRoles->name         = 'manage_roles';
            $manageRoles->display_name = 'Manage Roles';
            $manageRoles->save();

            $manageGods               = new Permission;
            $manageGods->name         = 'manage_gods';
            $manageGods->display_name = 'Manage Gods';
            $manageGods->save();

            $manageSuperadmins               = new Permission;
            $manageSuperadmins->name         = 'manage_superadmins';
            $manageSuperadmins->display_name = 'Manage Superadmins';
            $manageSuperadmins->save();

            $manageAdmins               = new Permission;
            $manageAdmins->name         = 'manage_admins';
            $manageAdmins->display_name = 'Manage Admins';
            $manageAdmins->save();

            $manageUsers               = new Permission;
            $manageUsers->name         = 'manage_users';
            $manageUsers->display_name = 'Manage Users';
            $manageUsers->save();

            $manageReports               = new Permission;
            $manageReports->name         = 'manage_reports';
            $manageReports->display_name = 'Manage Reports';
            $manageReports->save();

            $manageClients               = new Permission;
            $manageClients->name         = 'manage_clients';
            $manageClients->display_name = 'Manage Clients';
            $manageClients->save();

            $manageCaseStudies               = new Permission;
            $manageCaseStudies->name         = 'manage_case_studies';
            $manageCaseStudies->display_name = 'Manage Case Studies';
            $manageCaseStudies->save();

            $manageReportCategories               = new Permission;
            $manageReportCategories->name         = 'manage_report_categories';
            $manageReportCategories->display_name = 'Manage Report Categories';
            $manageReportCategories->save();

            $manageCaseStudyCategories               = new Permission;
            $manageCaseStudyCategories->name         = 'manage_case_study_categories';
            $manageCaseStudyCategories->display_name = 'Manage Case Study Categories';
            $manageCaseStudyCategories->save();

            $manageCaseStudyPlatforms               = new Permission;
            $manageCaseStudyPlatforms->name         = 'manage_case_study_platforms';
            $manageCaseStudyPlatforms->display_name = 'Manage Case Study Platforms';
            $manageCaseStudyPlatforms->save();

            $manageUploads               = new Permission;
            $manageUploads->name         = 'manage_uploads';
            $manageUploads->display_name = 'Manage Uploads';
            $manageUploads->save();

            $manageUploadCategories               = new Permission;
            $manageUploadCategories->name         = 'manage_upload_categories';
            $manageUploadCategories->display_name = 'Manage Upload Categories';
            $manageUploadCategories->save();

            $manageInsights               = new Permission;
            $manageInsights->name         = 'manage_insights';
            $manageInsights->display_name = 'Manage Insights';
            $manageInsights->save();

            $manageSiteProducts               = new Permission;
            $manageSiteProducts->name         = 'manage_site_products';
            $manageSiteProducts->display_name = 'Manage Site Products';
            $manageSiteProducts->save();

            $manageRateProducts               = new Permission;
            $manageRateProducts->name         = 'manage_rate_products';
            $manageRateProducts->display_name = 'Manage Rate Products';
            $manageRateProducts->save();

            $manageContactProducts               = new Permission;
            $manageContactProducts->name         = 'manage_contact_products';
            $manageContactProducts->display_name = 'Manage Contact Products';
            $manageContactProducts->save();

            $manageContactLocations               = new Permission;
            $manageContactLocations->name         = 'manage_contact_locations';
            $manageContactLocations->display_name = 'Manage Contact Locations';
            $manageContactLocations->save();

            $manageContacts               = new Permission;
            $manageContacts->name         = 'manage_contacts';
            $manageContacts->display_name = 'Manage Contacts';
            $manageContacts->save();

            $manageRates               = new Permission;
            $manageRates->name         = 'manage_rates';
            $manageRates->display_name = 'Manage Rates';
            $manageRates->save();

            $managePageContent               = new Permission;
            $managePageContent->name         = 'manage_page_content';
            $managePageContent->display_name = 'Manage Page Content';
            $managePageContent->save();

            $manageStudios               = new Permission;
            $manageStudios->name         = 'manage_studios';
            $manageStudios->display_name = 'Manage Custom Studios';
            $manageStudios->save();


            /**
             * Assign Permissions to Roles
             */

            $godRole->perms()
                ->sync
                (
                    array
                    (
                        $managePermissions->id,
                        $manageRoles->id,
                        $manageGods->id,
                        $manageSuperadmins->id,
                        $manageAdmins->id,
                        $manageUsers->id,
                        $manageReports->id,
                        $manageReportCategories->id,
                        $manageCaseStudies->id,
                        $manageCaseStudyCategories->id,
                        $manageUploads->id,
                        $manageUploadCategories->id,
                        $manageStudios->id,
                        $manageInsights->id,
                        $manageSiteProducts->id,
                        $manageRateProducts->id,
                        $manageContactProducts->id,
                        $manageContactLocations->id,
                        $manageContacts->id,
                        $manageRates->id,
                        $managePageContent->id,
                        $manageCaseStudyPlatforms->id,
                        $manageClients->id,
                    )
                );
            $superadminRole->perms()
                ->sync
                (
                    array
                    (
                        $manageSuperadmins->id,
                        $manageAdmins->id,
                        $manageUsers->id,
                        $manageReports->id,
                        $manageReportCategories->id,
                        $manageCaseStudies->id,
                        $manageCaseStudyCategories->id,
                        $manageUploads->id,
                        $manageUploadCategories->id,
                        $manageStudios->id,
                        $manageInsights->id,
                        $manageSiteProducts->id,
                        $manageRateProducts->id,
                        $manageContactProducts->id,
                    )
                );
            $adminRole->perms()
                ->sync
                (
                    array
                    (
                        $manageAdmins->id,
                        $manageUsers->id,
                        $manageReports->id,
                        $manageReportCategories->id,
                        $manageCaseStudies->id,
                        $manageCaseStudyCategories->id,
                        $manageUploads->id,
                        $manageUploadCategories->id,
                        $manageInsights->id,
                        $manageStudios->id,
                        $manageSiteProducts->id,
                        $manageRateProducts->id,
                        $manageContactProducts->id,
                    )
                );


            /**
             * Assign Roles to Users
             */
            $god = User::find(1);
            $god->attachRole($godRole);

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');


            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }
    }
