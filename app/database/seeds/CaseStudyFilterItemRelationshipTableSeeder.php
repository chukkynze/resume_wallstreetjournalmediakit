<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       CaseStudyFilterItemRelationshipTableSeeder
     *
     * filename:    CaseStudyFilterItemRelationshipTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\CaseStudyFilterItemRelationship;

    class CaseStudyFilterItemRelationshipTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Case Study Filter Item Relationships Data.\n";
            $this->table      = 'case_study_filter_item_relationship';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    // Case Study 1
                    // Filter 1
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 1,
                        'filter_item_id' => 1,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 1,
                        'filter_item_id' => 2,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 1,
                        'filter_item_id' => 3,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 1,
                        'filter_item_id' => 4,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 1,
                        'filter_item_id' => 5,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 1,
                        'filter_item_id' => 6,
                    ],
                    // Filter 2
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 2,
                        'filter_item_id' => 7,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 2,
                        'filter_item_id' => 8,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 2,
                        'filter_item_id' => 9,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 2,
                        'filter_item_id' => 10,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 2,
                        'filter_item_id' => 11,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 2,
                        'filter_item_id' => 12,
                    ],
                    // Filter 3
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 3,
                        'filter_item_id' => 13,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 3,
                        'filter_item_id' => 14,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 3,
                        'filter_item_id' => 15,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 3,
                        'filter_item_id' => 16,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 3,
                        'filter_item_id' => 17,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 1,
                        'filter_id'      => 3,
                        'filter_item_id' => 18,
                    ],
                    // Case Study 2
                    // Filter 1
                    [
                        'id'             => 0,
                        'case_study_id'  => 2,
                        'filter_id'      => 1,
                        'filter_item_id' => 1,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 2,
                        'filter_id'      => 1,
                        'filter_item_id' => 2,
                    ],
                    // Filter 2
                    [
                        'id'             => 0,
                        'case_study_id'  => 2,
                        'filter_id'      => 2,
                        'filter_item_id' => 7,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 2,
                        'filter_id'      => 2,
                        'filter_item_id' => 8,
                    ],
                    // Filter 3
                    /*
                     [
                        'id'             => 0,
                        'case_study_id'  => 2,
                        'filter_id'      => 3,
                        'filter_item_id' => 13,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 2,
                        'filter_id'      => 3,
                        'filter_item_id' => 14,
                    ],
                     */

                    // Case Study 3
                    // Filter 1
                    [
                        'id'             => 0,
                        'case_study_id'  => 3,
                        'filter_id'      => 1,
                        'filter_item_id' => 1,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 3,
                        'filter_id'      => 1,
                        'filter_item_id' => 2,
                    ],
                    /*
                    // Filter 2
                    [
                        'id'             => 0,
                        'case_study_id'  => 3,
                        'filter_id'      => 2,
                        'filter_item_id' => 7,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 3,
                        'filter_id'      => 2,
                        'filter_item_id' => 8,
                    ],

                    // Filter 3
                    [
                        'id'             => 0,
                        'case_study_id'  => 3,
                        'filter_id'      => 3,
                        'filter_item_id' => 13,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 3,
                        'filter_id'      => 3,
                        'filter_item_id' => 14,
                    ],
                     */

                    // Case Study 4
                    /*
                     // Filter 1
                    [
                        'id'             => 0,
                        'case_study_id'  => 4,
                        'filter_id'      => 1,
                        'filter_item_id' => 1,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 4,
                        'filter_id'      => 1,
                        'filter_item_id' => 2,
                    ],

                    // Filter 2
                    [
                        'id'             => 0,
                        'case_study_id'  => 4,
                        'filter_id'      => 2,
                        'filter_item_id' => 7,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 4,
                        'filter_id'      => 2,
                        'filter_item_id' => 8,
                    ],

                    // Filter 3
                    [
                        'id'             => 0,
                        'case_study_id'  => 4,
                        'filter_id'      => 3,
                        'filter_item_id' => 13,
                    ],
                    [
                        'id'             => 0,
                        'case_study_id'  => 4,
                        'filter_id'      => 3,
                        'filter_item_id' => 14,
                    ],
                     */
                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new CaseStudyFilterItemRelationship();
                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }

                echo "Creating Case Study Filter Item Relationship: \"" . $Model->id . ".\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
