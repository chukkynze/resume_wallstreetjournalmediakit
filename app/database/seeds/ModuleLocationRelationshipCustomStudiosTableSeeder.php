<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       ModuleLocationRelationshipTableSeeder
     *
     * filename:    ModuleLocationRelationshipTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\ModuleLocationRelationship;

    class ModuleLocationRelationshipCustomStudiosTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Module Location Relationship Data for Custom Studios.\n";
            $this->table      = 'module_location_relationships';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            //DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    // Main
                    [
                        'id' => 0,
                        'location_id' => 8 ,
                        'entity_id'   => 1,
                        'entity_type' => "Hyfn\\Models\\PageContent\\StudiosPage",
                        'module_id'   => 48,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    //// Capabilities

                    // Capabilities - text
                    [
                        'id'          => 0,
                        'location_id' => 6,
                        'entity_id'   => 1,
                        'entity_type' => "Hyfn\\Models\\PageContent\\StudiosPage",
                        'module_id'   => 52,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Capabilities - Slideshow
                    [
                        'id'          => 0,
                        'location_id' => 6,
                        'entity_id'   => 1,
                        'entity_type' => "Hyfn\\Models\\PageContent\\StudiosPage",
                        'module_id'   => 2,
                        'module_type' => "Hyfn\\Models\\Modules\\SlideshowModule",
                        'order' => 2,
                    ],
                    // Capabilities - text
                    [
                        'id'          => 0,
                        'location_id' => 6,
                        'entity_id'   => 1,
                        'entity_type' => "Hyfn\\Models\\PageContent\\StudiosPage",
                        'module_id'   => 49,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 3,
                    ],
                    //// Awards

                    // Awards - Text
                    [
                        'id'          => 0,
                        'location_id' => 7,
                        'entity_id'   => 1,
                        'entity_type' => "Hyfn\\Models\\PageContent\\StudiosPage",
                        'module_id'   => 50,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],


                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model              = new ModuleLocationRelationship();
                $Model->location_id = $row['location_id'];

                $Model->entity_id   = $row['entity_id'];
                $Model->entity_type = $row['entity_type'];

                $Model->module_id   = $row['module_id'];
                $Model->module_type = $row['module_type'];

                $Model->order = $row['order'];
                $Model->save();

                echo "Creating Module Location Relationship Type [" . $Model->id . "].\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
