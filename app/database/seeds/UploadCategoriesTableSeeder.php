<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       UploadCategoriesTableSeeder
     *
     * filename:    UploadCategoriesTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\UploadCategory;

    class UploadCategoriesTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Upload Category Data.\n";
            $this->table      = 'upload_category';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();


            $uploadCategory        = new UploadCategory;
            $uploadCategory->id    = 1;
            $uploadCategory->order = 1;
            $uploadCategory->name  = "PDFs + Spreadsheets";
            $uploadCategory->save();
            echo "Created Upload Category - " . $uploadCategory->name . " .\n";


            $uploadCategory        = new UploadCategory;
            $uploadCategory->id    = 2;
            $uploadCategory->order = 1;
            $uploadCategory->name  = "Image: Page Banners";
            $uploadCategory->save();
            echo "Created Upload Category - " . $uploadCategory->name . " .\n";


            $uploadCategory        = new UploadCategory;
            $uploadCategory->id    = 4;
            $uploadCategory->order = 1;
            $uploadCategory->name  = "Image: Text + Image Module";
            $uploadCategory->save();
            echo "Created Upload Category - " . $uploadCategory->name . " .\n";


            $uploadCategory        = new UploadCategory;
            $uploadCategory->id    = 6;
            $uploadCategory->order = 1;
            $uploadCategory->name  = "Image: Header Icon";
            $uploadCategory->save();
            echo "Created Upload Category - " . $uploadCategory->name . " .\n";


            $uploadCategory        = new UploadCategory;
            $uploadCategory->id    = 7;
            $uploadCategory->order = 1;
            $uploadCategory->name  = "Image: Multi Column Module";
            $uploadCategory->save();
            echo "Created Upload Category - " . $uploadCategory->name . " .\n";


            $uploadCategory        = new UploadCategory;
            $uploadCategory->id    = 8;
            $uploadCategory->order = 1;
            $uploadCategory->name  = "Image: Full Width";
            $uploadCategory->save();
            echo "Created Upload Category - " . $uploadCategory->name . " .\n";


            $uploadCategory        = new UploadCategory;
            $uploadCategory->id    = 9;
            $uploadCategory->order = 1;
            $uploadCategory->name  = "Image: Logos";
            $uploadCategory->save();
            echo "Created Upload Category - " . $uploadCategory->name . " .\n";


            $uploadCategory        = new UploadCategory;
            $uploadCategory->id    = 10;
            $uploadCategory->order = 1;
            $uploadCategory->name  = "Image: General";
            $uploadCategory->save();
            echo "Created Upload Category - " . $uploadCategory->name . " .\n";


            $uploadCategory        = new UploadCategory;
            $uploadCategory->id    = 11;
            $uploadCategory->order = 1;
            $uploadCategory->name  = "Image: Gallery Landing";
            $uploadCategory->save();
            echo "Created Upload Category - " . $uploadCategory->name . " .\n";


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
