<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       SiteProductsTableSeeder
     *
     * filename:    SiteProductsTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\ContactProduct;

    class ContactProductsTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Contacts Products Table Data.\n";
            $this->table      = 'contact_products';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    [
                        'id'          => 1,
                        'name'        => "The Wall Street Journal",
                        'description' => "",
                        'order' => 2,
                    ],
                    [
                        'id'          => 2,
                        'name'        => "Global",
                        'description' => "",
                        'order' => 3,
                    ],
                    [
                        'id'          => 3,
                        'name'        => "WSJ. Magazine",
                        'description' => "",
                        'order' => 4,
                    ],
                    [
                        'id'          => 4,
                        'name'        => "MarketWatch",
                        'description' => "",
                        'order' => 5,
                    ],
                    [
                        'id'          => 5,
                        'name'        => "Conferences",
                        'description' => "",
                        'order' => 6,
                    ],
                    [
                        'id'          => 6,
                        'name'        => "WSJ. Custom Studios",
                        'description' => "",
                        'order' => 7,
                    ],
                    [
                        'id'          => 7,
                        'name'        => "WSJ. Insights",
                        'description' => "",
                        'order' => 8,
                    ],
                    [
                        'id'          => 8,
                        'name'        => "New Business Inquiries",
                        'description' => "",
                        'order'       => 1,
                    ],
                ];

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new ContactProduct();
                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }

                echo "Creating Contact Product - " . $Model->name . " .\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
