<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       SlideshowModuleTableSeeder
     *
     * filename:    SlideshowModuleTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Modules\SlideshowModule;
    use Hyfn\Models\Upload;

    class SlideshowModuleTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Slideshow Module Data.\n";
            $this->table      = 'slideshow_modules';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [

                    [
                        'id'         => 2,
                        'has_border' => false,
                        'upload_ids' => [95, 96, 97, 98, 99, 100],
                        'title'   => "Capabilities Slideshow",
                        'is_live' => true,
                    ],
                    // Case Study 1
                    [
                        'id'         => 3,
                        'has_border' => false,
                        'upload_ids' => [95, 96, 97, 98, 99, 100],
                        'title'      => "Case Study 1 Slideshow",
                        'is_live' => false,
                    ],
                    // Case Study 2
                    [
                        'id'         => 4,
                        'has_border' => false,
                        'upload_ids' => [95, 96, 97, 98, 99, 100],
                        'title'      => "Case Study 2 Slideshow",
                        'is_live'    => true,
                    ],
                    // Case Study 3
                    [
                        'id'         => 5,
                        'has_border' => false,
                        'upload_ids' => [95, 96, 97, 98, 99, 100],
                        'title'      => "Case Study 3 Slideshow",
                        'is_live'    => true,
                    ],
                    // Case Study 4
                    [
                        'id'         => 6,
                        'has_border' => false,
                        'upload_ids' => [95, 96, 97, 98, 99, 100],
                        'title'      => "Case Study 4 Slideshow",
                        'is_live' => true,
                    ],
                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model             = new SlideshowModule();
                $Model->id         = $row['id'];
                $Model->has_border = $row['has_border'];
                $Model->title      = $row['title'];
                $Model->is_live    = $row['is_live'];

                echo "Creating Slideshow Module [" . $Model->id . "].\n";

                if ($Model->isValid()) {

                    $Model->save();
                    $Model->slides()->sync($row['upload_ids']);

                    foreach($row['upload_ids'] as $uploadID)
                    {
                        $Upload             =   Upload::find($uploadID);
                        $Upload->is_live    =   1;

                        if ($Upload->isModelValid())
                        {
                            $Upload->save();
                        }
                        else
                        {
                            echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                        }
                    }

                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
