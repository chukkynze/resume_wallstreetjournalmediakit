<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       InsightReportTableSeeder
     *
     * filename:    InsightReportTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/07/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\InsightReport;

    class InsightReportTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Insight Report Data.\n";
            $this->table      = 'insight_reports';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $allInsightReports =
                [
                    [
                        'insights_id' => 1,
                        'upload_id'   => 10,
                        'order'       => 1,
                    ],
                    [
                        'insights_id' => 1,
                        'upload_id'   => 11,
                        'order'       => 2,
                    ],
                    [
                        'insights_id' => 1,
                        'upload_id'   => 12,
                        'order'       => 3,
                    ],

                ];

            foreach ($allInsightReports as $insightReport) {
                $InsightReports              = new InsightReport;
                $InsightReports->insights_id = $insightReport['insights_id'];
                $InsightReports->upload_id   = $insightReport['upload_id'];
                $InsightReports->order       = $insightReport['order'];

                echo "Creating Insight Report for Insight (" . $InsightReports->insights_id . ") ." . "\n";

                if ($InsightReports->isModelValid()) {
                    $InsightReports->save();
                } else {
                    echo "<pre>" . print_r($InsightReports->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
