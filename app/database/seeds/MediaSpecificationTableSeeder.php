<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       MediaSpecificationTableSeeder
     *
     * filename:    MediaSpecificationTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\MediaSpecification;
    use Hyfn\Models\Upload;

    class MediaSpecificationTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Media Specifications Data.\n";
            $this->table = 'rate_products_media_specs';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows = [
                [
                    'rate_product_id' => 1,
                    'upload_id'       => 105,
                    'order'           => 1,
                ],
                [
                    'rate_product_id' => 1,
                    'upload_id'       => 106,
                    'order'           => 2,
                ],
                [
                    'rate_product_id' => 1,
                    'upload_id'       => 107,
                    'order'           => 3,
                ],
                [
                    'rate_product_id' => 1,
                    'upload_id'       => 78,
                    'order'           => 4,
                ],
                [
                    'rate_product_id' => 1,
                    'upload_id'       => 4,
                    'order'           => 5,
                ],
                [
                    'rate_product_id' => 2,
                    'upload_id'       => 101,
                    'order'           => 1,
                ],
                [
                    'rate_product_id' => 3,
                    'upload_id'       => 102,
                    'order'           => 1,
                ],
                [
                    'rate_product_id' => 4,
                    'upload_id'       => 101,
                    'order'           => 1,
                ],
                [
                    'rate_product_id' => 5,
                    'upload_id'       => 88,
                    'order'           => 1,
                ],
                [
                    'rate_product_id' => 5,
                    'upload_id'       => 89,
                    'order'           => 2,
                ],
            ];

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new MediaSpecification();
                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }

                echo "Creating Media Specification for Rate Product [" . $Model->rate_product_id . "] and Upload [" . $Model->upload_id . "].\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;

                    if ($Model->upload_id >= 1) {
                        $Upload = Upload::find($Model->upload_id);
                        $Upload->is_live = 1;

                        if ($Upload->isModelValid()) {
                            $Upload->save();
                        } else {
                            echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                        }
                    }
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded all rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
