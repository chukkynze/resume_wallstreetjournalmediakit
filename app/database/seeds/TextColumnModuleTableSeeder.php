<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       TextColumnModuleTableSeeder
     *
     * filename:    TextColumnModuleTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Modules\TextColumnModule;

    class TextColumnModuleTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Text Column Module Data.\n";
            $this->table      = 'text_column_modules';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    //video product page module 4
                    [
                        'id'         => 1,
                        'upload_id'  => '0',
                        'has_border' => false,
                        'title'      => "Video Plus",
                        'is_live'    => true,
                        'column_1'   => "<p><span style=\"color: #3eb4e4;\">On Network<br /></span>WSJ Video Hub<br />In-article Video<br />WSJ.com homepage<br />WSJ Apps</p>",
                        'column_2'   => "<p><span style=\"color: #3eb4e4;\">Syndication Partners<br /></span>AOL<br />Grab Media<br />Yahoo!<br />YouTube</p>",
                        'column_3'   => "",
                        'column_4'   => "",
                        'column_5'   => "",
                        'column_6'   => "",
                        'column_7'   => "",
                        'column_8'   => "",
                    ],
                    //video product page module 5
                    [
                        'id'         => 2,
                        'upload_id'  => '0',
                        'has_border' => false,
                        'title'   => "Apps",
                        'is_live' => true,
                        'column_1' => "<p>iOS 8<br />Apple TV<br />Boxee<br />BlackBerry<br />iPad<br />Phillips Smart TV<br />Android<br />iPad<br />Xbox One</p>",
                        'column_2' => "<p>Nokia<br />Google TV<br />Roku TV<br />Kindle Fire<br />Vizio<br />Smart VIERA<br />Windows 8<br />Yahoo! Connected TV<br />Samsung Smart TV</p>",
                        'column_3' => "",
                        'column_4' => "",
                        'column_5' => "",
                        'column_6' => "",
                        'column_7' => "",
                        'column_8' => "",
                    ],
                ];


            $dir               = preg_replace('/\.php$/', '', __FILE__);

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model             = new TextColumnModule();
                $Model->id         = $row['id'];
                $Model->title      = $row['title'];
                $Model->upload_id  = $row['upload_id'];
                $Model->body       = file_get_contents($dir . '/' . $row['id'] . '_body.html');
                $Model->body_2     = '';
                $Model->has_border = $row['has_border'];
                $Model->is_live    = $row['is_live'];

                $Model->column_1 = $row['column_1'];
                $Model->column_2 = $row['column_2'];
                $Model->column_3 = $row['column_3'];
                $Model->column_4 = $row['column_4'];
                $Model->column_5 = $row['column_5'];
                $Model->column_6 = $row['column_6'];
                $Model->column_7 = $row['column_7'];
                $Model->column_8 = $row['column_8'];

                echo "Creating Text Column Module [" . $Model->id . "].\n";

                if ($Model->isValid()) {

                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
