<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       ImageColumnModuleTableSeeder
     *
     * filename:    ImageColumnModuleTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Modules\ImageColumnModule;
    use Hyfn\Models\Upload;

    class ImageColumnModuleTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Image Column Module Data.\n";
            $this->table      = 'image_column_modules';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    // WSJ Magazine - Main Section
                    [
                        'id'         => 4,
                        'title'      => "THE WORLD'S LEADING LUXURY MAGAZINE",
                        'upload_id'  => 0,
                        'has_border' => false,
                        'upload_ids' => [44, 45, 46, 47],
                    ],
                ];


            $dir               = preg_replace('/\.php$/', '', __FILE__);

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model             = new ImageColumnModule();
                $Model->id         = $row['id'];
                $Model->title         = $row['title'];
                $Model->upload_id  = $row['upload_id'];
                $Model->body   = file_get_contents($dir . '/' . $row['id'] . '_body.html');
                $Model->body_2 = file_get_contents($dir . '/' . $row['id'] . '_body2.html');
                $Model->has_border = $row['has_border'];

                echo "Creating Image Column Module [" . $Model->id . "].\n";

                if ($Model->isValid()) {

                    $Model->save();
                    $Model->images()->sync($row['upload_ids']);

                    foreach($row['upload_ids'] as $uploadID)
                    {
                        $Upload             =   Upload::find($uploadID);
                        $Upload->is_live    =   1;

                        if ($Upload->isModelValid())
                        {
                            $Upload->save();
                        }
                        else
                        {
                            echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                        }
                    }

                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
