<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       SummaryModuleTableSeeder
     *
     * filename:    SummaryModuleTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Modules\SummaryModule;
    use Hyfn\Models\Upload;

    class SummaryModuleTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Summary Module Data.\n";
            $this->table      = 'summary_modules';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    /*
                    [
                        'id'          => 1,
                        'upload_id'   => 23,
                        'orientation' => "Right",
                        'has_border'  => true,
                        'title'   => "",
                        'is_live' => true,
                    ],
                    [
                        'id'          => 2,
                        'upload_id'   => 24,
                        'orientation' => "Left",
                        'has_border'  => true,
                        'title'   => "",
                        'is_live' => true,
                    ],
                    [
                        'id'          => 3,
                        'upload_id'   => 23,
                        'orientation' => "Right",
                        'has_border'  => true,
                        'title'   => "",
                        'is_live' => true,
                    ],
                    [
                        'id'          => 4,
                        'upload_id'   => 24,
                        'orientation' => "Left",
                        'has_border'  => true,
                        'title'   => "",
                        'is_live' => true,
                    ],
                     */
                ];


            $dir             = preg_replace('/\.php$/', '', __FILE__);

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new SummaryModule();

                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }
                $Model->body = file_get_contents($dir . '/' . $row['id'] . '.html');

                echo "Creating Summary Module [" . $Model->id . "].\n";

                if ($Model->isValid()) {

                    $Model->save();

                    $Upload             =   Upload::find($row['upload_id']);
                    $Upload->is_live    =   1;

                    if ($Upload->isModelValid())
                    {
                        $Upload->save();
                    }
                    else
                    {
                        echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                    }

                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
