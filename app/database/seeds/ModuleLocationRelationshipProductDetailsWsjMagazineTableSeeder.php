<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       ModuleLocationRelationshipTableSeeder
     *
     * filename:    ModuleLocationRelationshipTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\ModuleLocationRelationship;

    class ModuleLocationRelationshipProductDetailsWsjMagazineTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Module Location Relationship Data for Product Details Wsj Magazine Page.\n";
            $this->table      = 'module_location_relationships';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            //DB::connection($this->connection)->table($this->table)->truncate();


            $Rows = [
                //// START Product Details Page 4 - WSJ Magazine - SiteProduct 3
                // Main Section Modules

                // Image Column - The worlds leading luxury col
                [
                    'id'          => 0,
                    'location_id' => 4,
                    'entity_id'   => 3,
                    'entity_type' => "Hyfn\\Models\\SiteProduct",
                    'module_id'   => 4,
                    'module_type' => "Hyfn\\Models\\Modules\\ImageColumnModule",
                    'order' => 1,
                ],
                //// START Product Details Page Section 5 - SiteProductSection - 31,32,33,34,35,36,37
                // Section - Audience 31
                // PdfListModule
                [
                    'id'          => 0,
                    'location_id' => 5,
                    'entity_id'   => 31,
                    'entity_type' => "Hyfn\\Models\\SiteProductSection",
                    'module_id'   => 9,
                    'module_type' => "Hyfn\\Models\\Modules\\PdfListModule",
                    'order' => 1,
                ],
                // Section - Edit Calendar 32
                // PdfListModule
                [
                    'id'          => 0,
                    'location_id' => 5,
                    'entity_id'   => 32,
                    'entity_type' => "Hyfn\\Models\\SiteProductSection",
                    'module_id'   => 10,
                    'module_type' => "Hyfn\\Models\\Modules\\PdfListModule",
                    'order' => 1,
                ],
                // SingleImageModule
                [
                    'id'          => 0,
                    'location_id' => 5,
                    'entity_id'   => 32,
                    'entity_type' => "Hyfn\\Models\\SiteProductSection",
                    'module_id'   => 2,
                    'module_type' => "Hyfn\\Models\\Modules\\SingleImageModule",
                    'order' => 2,
                ],
                // Section - Rates & Specs 33
                // PdfListModule
                [
                    'id'          => 0,
                    'location_id' => 5,
                    'entity_id'   => 33,
                    'entity_type' => "Hyfn\\Models\\SiteProductSection",
                    'module_id'   => 11,
                    'module_type' => "Hyfn\\Models\\Modules\\PdfListModule",
                    'order' => 1,
                ],
                // Section - What's New & Next 34
                // PdfListModule
                [
                    'id'          => 0,
                    'location_id' => 5,
                    'entity_id'   => 34,
                    'entity_type' => "Hyfn\\Models\\SiteProductSection",
                    'module_id'   => 12,
                    'module_type' => "Hyfn\\Models\\Modules\\PdfListModule",
                    'order' => 1,
                ],
                // SingleImageModule
                [
                    'id'          => 0,
                    'location_id' => 5,
                    'entity_id'   => 34,
                    'entity_type' => "Hyfn\\Models\\SiteProductSection",
                    'module_id'   => 3,
                    'module_type' => "Hyfn\\Models\\Modules\\SingleImageModule",
                    'order' => 2,
                ],
                // Section - Awards & Press 35
                // PdfListModule
                [
                    'id'          => 0,
                    'location_id' => 5,
                    'entity_id'   => 35,
                    'entity_type' => "Hyfn\\Models\\SiteProductSection",
                    'module_id'   => 13,
                    'module_type' => "Hyfn\\Models\\Modules\\PdfListModule",
                    'order' => 1,
                ],
                // SingleImageModule - Quote ????
                /*
                [
                    'id'            =>  0,
                    'location_id'   =>  5,
                    'entity_id'     =>  35,
                    'entity_type'   =>  "Hyfn\\Models\\SiteProductSection",
                    'module_id'     =>  XX,
                    'module_type'   =>  "Hyfn\\Models\\Modules\\YYYYYYYYY",
                        'order' => 1,
                ],
                */

                // Section - Events 36
                // PdfListModule
                [
                    'id'          => 0,
                    'location_id' => 5,
                    'entity_id'   => 36,
                    'entity_type' => "Hyfn\\Models\\SiteProductSection",
                    'module_id'   => 15,
                    'module_type' => "Hyfn\\Models\\Modules\\PdfListModule",
                    'order' => 1,
                ],
                // SingleImageModule
                [
                    'id'          => 0,
                    'location_id' => 5,
                    'entity_id'   => 36,
                    'entity_type' => "Hyfn\\Models\\SiteProductSection",
                    'module_id'   => 4,
                    'module_type' => "Hyfn\\Models\\Modules\\SingleImageModule",
                    'order' => 2,
                ],
                // Section - Connect 37
                [
                    'id'            =>  0,
                    'location_id'   =>  5,
                    'entity_id'     =>  37,
                    'entity_type'   =>  "Hyfn\\Models\\SiteProductSection",
                    'module_id' => 33,
                    'module_type'   =>  "Hyfn\\Models\\Modules\\TextModule",
                    'order' => 1,
                ],

                //// END Product Details Page Section 5 - SiteProductSection - 31,32,33,34,35,36,37

                //// END Product Details Page 4 - WSJ Magazine - SiteProduct 3
            ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model              = new ModuleLocationRelationship();
                $Model->id          = $row['id'];
                $Model->location_id = $row['location_id'];

                $Model->entity_id   = $row['entity_id'];
                $Model->entity_type = $row['entity_type'];

                $Model->module_id   = $row['module_id'];
                $Model->module_type = $row['module_type'];

                $Model->order = $row['order'];
                $Model->save();

                echo "Creating Module Location Relationship Type [" . $Model->id . "].\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
