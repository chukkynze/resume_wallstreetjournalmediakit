<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       SingleImageModuleTableSeeder
     *
     * filename:    SingleImageModuleTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\Modules\SingleImageModule;
    use Hyfn\Models\Upload;

    class SingleImageModuleTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Single Image Module Data.\n";
            $this->table      = 'single_image_modules';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    // WSJ Magazine Details Page - Edit Calendar
                    [
                        'id'         => 2,
                        'upload_id'  => 42,
                        'target'     => 1,
                        'link_url'   => '',
                        'alt_text'   => 'WSJ Magazine - 2015 Important Dates',
                        'has_border' => false,
                        'title'   => "Important Dates",
                        'is_live' => true,
                    ],
                    // WSJ Magazine Details Page - What's New & Next
                    [
                        'id'         => 3,
                        'upload_id'  => 41,
                        'target'     => 1,
                        'link_url'   => '',
                        'alt_text'   => 'WSJ Magazine - Bloque de Color',
                        'has_border' => false,
                        'title'   => "Bloque de Color",
                        'is_live' => true,
                    ],
                    // WSJ Magazine Details Page - Events
                    [
                        'id'         => 4,
                        'upload_id'  => 43,
                        'target'     => 1,
                        'link_url'   => '',
                        'alt_text'   => 'WSJ Magazine - Pharell and friends 2013 Innovation Awards',
                        'has_border' => false,
                        'title'   => "Pharell and friends",
                        'is_live' => true,
                    ],
                    //Custom Studios awards image
                    // NEEDS UPLOAD FOR IMAGE FOR AWARDS
                    [
                        'id'         => 5,
                        'upload_id'  => 1,
                        'target'     => 1,
                        'link_url'   => '',
                        'alt_text'   => 'Awards',
                        'has_border' => true,
                        'title'   => "Awards",
                        'is_live' => true,
                    ],
                    // Case Study 1
                    [
                        'id'         => 6,
                        'upload_id'  => 42,
                        'target'     => 1,
                        'link_url'   => '',
                        'alt_text'   => 'Case Study 1',
                        'has_border' => false,
                        'title'      => "Case Study 1",
                        'is_live' => false,
                    ],
                    // Case Study 2
                    [
                        'id'         => 7,
                        'upload_id'  => 41,
                        'target'     => 1,
                        'link_url'   => '',
                        'alt_text'   => 'Case Study 2',
                        'has_border' => false,
                        'title'      => "Case Study 2",
                        'is_live'    => true,
                    ],
                    // Case Study 3
                    [
                        'id'         => 8,
                        'upload_id'  => 43,
                        'target'     => 1,
                        'link_url'   => '',
                        'alt_text'   => 'Case Study 3',
                        'has_border' => false,
                        'title'      => "Case Study 3",
                        'is_live'    => true,
                    ],
                    // Case Study 4
                    [
                        'id'         => 9,
                        'upload_id'  => 1,
                        'target'     => 1,
                        'link_url'   => '',
                        'alt_text'   => 'Case Study 4',
                        'has_border' => true,
                        'title'      => "Case Study 4",
                        'is_live'    => true,
                    ],
                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new SingleImageModule();

                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }

                echo "Creating Single Image Module [" . $Model->id . "].\n";

                if ($Model->isValid()) {
                    $Model->save();

                    $Upload             =   Upload::find($row['upload_id']);
                    $Upload->is_live    =   1;

                    if ($Upload->isModelValid())
                    {
                        $Upload->save();
                    }
                    else
                    {
                        echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                    }

                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
