<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;

    class DatabaseSeeder extends BaseSeeder
    {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            Eloquent::unguard();

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            $this->call('UsersTableSeeder');
            $this->call('ApiKeysTableSeeder');
            $this->call('RolesPermissionsTableSeeder');
            $this->call('UploadCategoriesTableSeeder');
            $this->call('SiteProductsTableSeeder');
            $this->call('RateProductsTableSeeder');
            $this->call('ContactProductsTableSeeder');
            $this->call('ContactLocationTableSeeder');
            $this->call('ContactTableSeeder');
            $this->call('UploadTableSeeder');
            $this->call('StudiosTableSeeder');
            $this->call('InsightTableSeeder');
            $this->call('MediaSpecificationTableSeeder');
            $this->call('HomePageTableSeeder');
            $this->call('HomePageSliderTableSeeder');
            $this->call('ClientsTableSeeder');

            $this->call('CaseStudyTableSeeder');
            $this->call('CaseStudyFilterTableSeeder');
            $this->call('CaseStudyFilterItemTableSeeder');
            $this->call('CaseStudyFilterItemRelationshipTableSeeder');

            $this->call('ProductPageTableSeeder');
            $this->call('SiteProductSectionTableSeeder');

            // Module seeders go here
            $this->call('TextColumnModuleTableSeeder');
            $this->call('TextModuleTableSeeder');
            $this->call('SplitTextModuleTableSeeder');
            $this->call('SummaryModuleTableSeeder');
            $this->call('ImageColumnModuleTableSeeder');
            $this->call('PdfListModuleTableSeeder');
            $this->call('SlideshowModuleTableSeeder');
            $this->call('SingleImageModuleTableSeeder');
            $this->call('VideoModuleTableSeeder');
            $this->call('ModuleLocationTableSeeder');


            DB::connection($this->connection)->table('module_location_relationships')->truncate();

            $this->call('ModuleLocationRelationshipProductLandingTableSeeder');
            $this->call('ModuleLocationRelationshipCustomStudiosTableSeeder');
            $this->call('ModuleLocationRelationshipProductDetailsNewspaperTableSeeder');
            $this->call('ModuleLocationRelationshipProductDetailsOnlineTableSeeder');
            $this->call('ModuleLocationRelationshipProductDetailsMobileTableSeeder');
            $this->call('ModuleLocationRelationshipProductDetailsVideoTableSeeder');
            $this->call('ModuleLocationRelationshipProductDetailsGlobalTableSeeder');
            $this->call('ModuleLocationRelationshipInsightsTableSeeder');
            $this->call('ModuleLocationRelationshipProductDetailsMarketWatchTableSeeder');
            $this->call('ModuleLocationRelationshipProductDetailsJournalReportTableSeeder');
            $this->call('ModuleLocationRelationshipProductDetailsConferencesTableSeeder');
            $this->call('ModuleLocationRelationshipProductDetailsWsjMagazineTableSeeder');
            $this->call('ModuleLocationRelationshipCaseStudiesTableSeeder');

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');
        }

    }
