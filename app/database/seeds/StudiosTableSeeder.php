<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       InsightTableSeeder
     *
     * filename:    InsightTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/07/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\PageContent\StudiosPage;

    class StudiosTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Custom Studios Page Data.\n";
            $this->table = 'studios_page';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    [
                        'title'         => "WSJ. Custom Studios",
                        'banner_img_id' => 59,
                        'source_text'   => "",
                    ],
                ];

            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model = new StudiosPage();
                foreach ($row as $key => $val) {
                    $Model->$key = $val;
                }
                echo "Creating Custom Studios Page " . $Model->title . " .\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }


            DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows [" . number_format((($seedCount / count($Rows)) * 100),
                    2) . "%] in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
