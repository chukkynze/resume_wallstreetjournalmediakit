<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       ModuleLocationRelationshipTableSeeder
     *
     * filename:    ModuleLocationRelationshipTableSeeder.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    use Hyfn\Models\ModuleLocationRelationship;

    class ModuleLocationRelationshipProductDetailsVideoTableSeeder extends BaseSeeder
    {
        public function __construct()
        {
            echo "------------------------------------------------------------------------------------\n";
            echo "Seeding Module Location Relationship Data for Product Details Video Page.\n";
            $this->table      = 'module_location_relationships';
            $this->connection = 'db1';
        }

        public function run()
        {
            #ini_set('memory_limit', '-1');
            $startTime = strtotime("now");

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 0');

            //DB::connection($this->connection)->table($this->table)->truncate();

            $Rows =
                [
                    //// START Product Details Page 4 - Video - SiteProduct 11
                    [
                        'id'          => 0,
                        'location_id' => 4,
                        'entity_id'   => 11,
                        'entity_type' => "Hyfn\\Models\\SiteProduct",
                        'module_id'   => 22,
                        'module_type' => "Hyfn\\Models\\Modules\\PdfListModule",
                        'order' => 1,
                    ],
                    //// START Product Details Page Section 5 - SiteProductSection - 15,16
                    // Section - Programming
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 15,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 24,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                   // Section - Distribution
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 16,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 61,
                        'module_type' => "Hyfn\\Models\\Modules\\TextModule",
                        'order' => 1,
                    ],
                    // Section - Video plus
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 16,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 1,
                        'module_type' => "Hyfn\\Models\\Modules\\TextColumnModule",
                        'order' => 2,
                    ],
                    [
                        'id'          => 0,
                        'location_id' => 5,
                        'entity_id'   => 16,
                        'entity_type' => "Hyfn\\Models\\SiteProductSection",
                        'module_id'   => 2,
                        'module_type' => "Hyfn\\Models\\Modules\\TextColumnModule",
                        'order' => 3,
                    ],
                    //// END Product Details Page Section 5 - SiteProductSection - 15,16

                    //// END Product Details Page 4 - Video - SiteProduct 11


                ];


            $seedCount = 0;
            foreach ($Rows as $row) {
                $Model              = new ModuleLocationRelationship();
                $Model->id          = $row['id'];
                $Model->location_id = $row['location_id'];

                $Model->entity_id   = $row['entity_id'];
                $Model->entity_type = $row['entity_type'];

                $Model->module_id   = $row['module_id'];
                $Model->module_type = $row['module_type'];

                $Model->order = $row['order'];
                $Model->save();

                echo "Creating Module Location Relationship Type [" . $Model->id . "].\n";

                if ($Model->isModelValid()) {
                    $Model->save();
                    $seedCount++;
                } else {
                    echo "<pre>" . print_r($Model->getErrors(), 1) . "</pre>\n";
                }
            }

            //DB::connection($this->connection)->statement('SET FOREIGN_KEY_CHECKS = 1');

            $endTime  = strtotime("now");
            $duration = $endTime - $startTime;
            echo "Seeded " . $seedCount . " of " . count($Rows) . " rows in " . $duration . " seconds.\n";
            echo "------------------------------------------------------------------------------------\n";
        }

    }
