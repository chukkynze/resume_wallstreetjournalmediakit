<?php
    /**
     * User: wwallace
     * Date: 11/19/13
     * Time: 4:35 PM
     * raven-php sentry config settings
     * Set enabled to TRUE if you want to enable sentry
     */

    return [
        'enabled' => $_ENV['SENTRY_enabled'],
        'dsn'     => $_ENV['SENTRY_dsn'],
    ];
