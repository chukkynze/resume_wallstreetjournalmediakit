<?php

    return array(

        'doc'  => [
            'application/msword',
            'application/vnd.ms-office'
        ],
        'docx' => [
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        ],
        'xls'  => [
            'application/vnd.ms-excel',
        ],
        'pdf'  => [
            'application/pdf',
            'application/x-download'
        ],
        'jpeg' => [
            'image/jpeg',
        ],
        'jpg'  => [
            'image/jpeg',
        ],
        'jpe'  => [
            'image/jpeg',
        ],
        'gif'  => [
            'image/gif',
        ],
        'png'  => [
            'image/png',
        ],
    );