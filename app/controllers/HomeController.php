<?php

    namespace Hyfn\Controllers;

    use Hyfn\Models\CaseStudy;
    use Hyfn\Models\CaseStudyFilter;
    use Hyfn\Models\CaseStudyFilterItem;
    use Hyfn\Models\CaseStudyFilterItemRelationship;
    use Hyfn\Models\PageContent\InsightPage;
    use Hyfn\Models\PageContent\StudiosPage;
    use Hyfn\Models\ModuleLocationRelationship;
    use Hyfn\Models\Modules\TextColumn;
    use Hyfn\Models\PageContent\HomePage;
    use Hyfn\Models\PageContent\HomePageSlider;
    use Hyfn\Models\PageContent\ProductPage;
    use Hyfn\Models\Rate;
    use Hyfn\Models\SiteProduct;
    use Hyfn\Models\ContactProduct;
    use Hyfn\Models\RateProduct;
    use Hyfn\Models\SiteProductSection;
    use Hyfn\Models\Upload;

    use Input;
    use Redirect;
    use Session;
    use Str;
    use View;

    class HomeController extends BaseController
    {
        public $menuProducts;

        public function __construct()
        {
            $siteProductSections = [];
            $SiteProducts        = SiteProduct::orderBy("section_order", "asc")->get();
            $ModuledSiteProducts = [];
            foreach ($SiteProducts as $siteProduct) {
                $ModuleRelationships = ModuleLocationRelationship::where('entity_id', '=',
                    $siteProduct->id)->where('entity_type', '=', "Hyfn\\Models\\SiteProduct")->where('location_id', '=',
                    "4")->get();

                foreach ($ModuleRelationships as $relationship) {
                    $moduleClassName = $relationship->module_type;
                    $moduleID        = $relationship->module_id;
                    $Module          = $moduleClassName::find($moduleID);

                    if ($Module->is_live == 1) {
                        $ModuledSiteProducts[] = $siteProduct;
                        break;
                    }
                }
            }


            foreach ($ModuledSiteProducts as $siteProduct) {
                $siteProductSections[] = [
                    'name'        => $siteProduct->name,
                    'link'        => $siteProduct->name_slug,
                    'linkClasses' => '',
                    'listClasses' => '',
                ];
            }

            $this->menuProducts = [
                0 => [
                    'name'        => "Products",
                    'link'        => "products",
                    'listClasses' => "primary-menu-link primary-parent-link",
                    'linkClasses' => "",
                    'subLinks'    => $siteProductSections,
                ],
                1 => [
                    'name'        => "WSJ. Insights",
                    'link'        => "wsjinsights",
                    'listClasses' => "primary-menu-link",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ],
                2 => [
                    'name'        => "WSJ. Custom Studios",
                    'link'        => "wsjcustomstudios",
                    'listClasses' => "primary-menu-link",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ],
                3 => [
                    'name'        => "Rates &amp; Specs",
                    'link' => "rates_specs",
                    'listClasses' => "primary-menu-link",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ]
                ,
                4 => [
                    'name'        => "Gallery",
                    'link'        => "gallery",
                    'listClasses' => "primary-menu-link mobile-hidden",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ],
            ];

            $this->previewMode = Session::pull('previewMode') ? true : false;
            echo($this->previewMode ? "<div class='centered-text' style='background-color: orangered; color: #080808; font-weight: bold;'>Preview is enabled for this session only. Once page is refreshed or another page accessed, Preview mode will be terminated.</div>" : "");

            View::share('menuProducts', $this->menuProducts);
            View::share('sourceText', "");
        }


        public function index()
        {
            $HomePage        = HomePage::orderBy("created_at", "desc")->first();
            $HomePageSliders = HomePageSlider::where("home_page_id", "=", $HomePage->id)->orderBy("id", "asc")->get();

            $getSliders = [];
            foreach ($HomePageSliders as $slider) {
                $sliderImageInfo = Upload::find($slider->upload_id);
                $getSliders[]    = [
                    'download_folder' => $sliderImageInfo->download_folder,
                    'download_file'   => $sliderImageInfo->download_file,
                    'main_text'       => $slider->main_text,
                    'sub_text' => $slider->sub_text,
                    'source_text'     => $slider->source_text,
                ];
            }

            $viewData = [
                'sliders' => $getSliders,
            ];

            View::share('currentPath', "index");
            View::share('marqueeLandingTitle', "Welcome to the WSJ Media Kit");

            Session::forget('previewMode');

            return View::make('home.index', $viewData);
        }


        public function showProducts()
        {
            $Page                 = ProductPage::orderBy('created_at', 'desc')->first();
            $bannerImg            = Upload::find($Page->banner_image_upload_id);
            $productSections      = [];
            $showOnlyHeadingLinks = 0;
            $SiteProducts         = SiteProduct::orderBy("section_order", "ASC")->get();

            foreach ($SiteProducts as $siteProduct) {
                $ModuleRelationships = ModuleLocationRelationship::where('entity_id', '=', $siteProduct->id)
                    ->where('entity_type', '=', "Hyfn\\Models\\SiteProduct")
                    ->where('location_id', '=', "3")
                    ->get();

                if ($this->previewMode) {
                    $allSiteProductSections = SiteProductSection::where("site_product_id", "=",
                        $siteProduct->id)->orderBy('order', 'asc')->get();
                    $siteProductSections    = [];
                    $detailsPageHasLiveMods = false;
                    if (!$detailsPageHasLiveMods) {
                        $SectionModuleRelationships = ModuleLocationRelationship::where('entity_id', '=',
                            $siteProduct->id)
                            ->where('entity_type', '=', "Hyfn\\Models\\SiteProduct")
                            ->whereIn('location_id', [4])
                            ->get();
                        foreach ($SectionModuleRelationships as $relationship) {
                            $moduleClassName = $relationship->module_type;
                            $moduleID        = $relationship->module_id;
                            $Module          = $moduleClassName::find($moduleID);
                            if ($Module->is_live == 1) {
                                $detailsPageHasLiveMods = true;
                                break;
                            }
                        }
                    }

                    foreach ($allSiteProductSections as $siteProductSection) {
                        $showOnlyHeadingLinks                                             = $siteProductSection->only_headings_on_landing;
                        $siteProductSections[($siteProductSection->heading ?: "EMPTY")][] = [
                            'name_slug' => $siteProductSection->name_slug,
                            'name'      => $siteProductSection->link_text,
                        ];
                    }

                    $productSections[] = [
                        'name'                 => $siteProduct->name,
                        'name_slug'            => $siteProduct->name_slug,
                        'siteProductSections'  => $siteProductSections,
                        'showOnlyHeadingLinks' => $showOnlyHeadingLinks,
                        'modulesArray'         => $this->getLocationModules(3, "Hyfn\\Models\\SiteProduct",
                            $siteProduct->id, 1),
                        'detailsPageLiveMods'  => ($detailsPageHasLiveMods ? 1 : 0),
                    ];
                } else {
                    $siteProductHasLiveMods = false;
                    foreach ($ModuleRelationships as $relationship) {
                        $moduleClassName = $relationship->module_type;
                        $moduleID        = $relationship->module_id;
                        $Module          = $moduleClassName::find($moduleID);
                        if ($Module->is_live == 1) {
                            $siteProductHasLiveMods = true;
                            break;
                        }
                    }

                    if ($siteProductHasLiveMods) {
                        $allSiteProductSections = SiteProductSection::where("site_product_id", "=",
                            $siteProduct->id)->orderBy('order', 'asc')->get();
                        $siteProductSections    = [];
                        $detailsPageHasLiveMods = false;
                        if (!$detailsPageHasLiveMods) {
                            $SectionModuleRelationships = ModuleLocationRelationship::where('entity_id', '=',
                                $siteProduct->id)
                                ->where('entity_type', '=', "Hyfn\\Models\\SiteProduct")
                                ->whereIn('location_id', [4])
                                ->get();
                            foreach ($SectionModuleRelationships as $relationship) {
                                $moduleClassName = $relationship->module_type;
                                $moduleID        = $relationship->module_id;
                                $Module          = $moduleClassName::find($moduleID);
                                if ($Module->is_live == 1) {
                                    $detailsPageHasLiveMods = true;
                                    break;
                                }
                            }
                        }

                        foreach ($allSiteProductSections as $siteProductSection) {
                            $showOnlyHeadingLinks                                             = $siteProductSection->only_headings_on_landing;
                            $siteProductSections[($siteProductSection->heading ?: "EMPTY")][] = [
                                'name_slug' => $siteProductSection->name_slug,
                                'name'      => $siteProductSection->link_text,
                            ];
                        }

                        $productSections[] = [
                            'name'                 => $siteProduct->name,
                            'name_slug'            => $siteProduct->name_slug,
                            'siteProductSections'  => $siteProductSections,
                            'showOnlyHeadingLinks' => $showOnlyHeadingLinks,
                            'modulesArray'         => $this->getLocationModules(3, "Hyfn\\Models\\SiteProduct",
                                $siteProduct->id, 0),
                            'detailsPageLiveMods'  => ($detailsPageHasLiveMods ? 1 : 0),
                        ];
                    }
                }
            }

            $Page->productSections = $productSections;

            $menuProducts                   = $this->menuProducts;
            $menuProducts[0]['linkClasses'] = 'active';

            View::share('menuProducts', $menuProducts);
            View::share('currentPath', "products");
            View::share('marqueeLandingTitle', $Page->heading);
            View::share('marqueeLandingImgURL',
                $Page->banner_image_enabled == 1 ? $bannerImg->download_folder . $bannerImg->download_file : "");


            if ($this->previewMode) {
                $sourceText = $Page->source_text;
            } else {
                $sourceText = $Page->source_text_enabled == 1 ? $Page->source_text : "";
            }

            $viewData = [
                'Page'       => $Page,
                'heading'    => $Page->heading,
                'sourceText' => $sourceText,
            ];

            Session::forget('previewMode');

            return View::make('home.products', $viewData);
        }


        public function showProductDetails($siteProductSlug)
        {
            if (is_string($siteProductSlug)) {
                $siteProduct = SiteProduct::where("name_slug", "=", $siteProductSlug)->first();

                if (is_object($siteProduct)) {
                    $headingImage           = Upload::find($siteProduct->heading_icon_upload_id);
                    $allSiteProductSections = SiteProductSection::where("site_product_id", "=",
                        $siteProduct->id)->orderBy("order", "ASC")->get();
                    $siteProductSections    = [];
                    $subSections            = [];

                    foreach ($allSiteProductSections as $siteProductSection) {
                        $modulesArray                                                     = $this->getLocationModules(5,
                            "Hyfn\\Models\\SiteProductSection", $siteProductSection->id);
                        $siteProductSections[($siteProductSection->heading ?: "EMPTY")][] = [
                            'name_slug'    => $siteProductSection->name_slug,
                            'name'         => $siteProductSection->name,
                            'link_text'    => $siteProductSection->link_text,
                            'modulesArray' => $modulesArray,
                        ];
                    }

                    $modulesArray = $this->getLocationModules(4, "Hyfn\\Models\\SiteProduct", $siteProduct->id);
                    $url          = ($siteProduct->heading_icon_upload_id >= 1 ? $headingImage->download_folder . $headingImage->download_file : '');
                    $viewData     = [
                        'siteProductID'       => $siteProduct->id,
                        'ucwordHeading'       => ucwords($siteProduct->name),
                        'imageHeading'        => $siteProduct->name,
                        'lowerCaseHeading'    => strtolower($siteProduct->name),
                        'heading_vs_image'    => $siteProduct->heading_vs_image,
                        'headingIcon'         => $url,
                        'detailsPageText'     => $siteProduct->detail_page_text,
                        'siteProductSections' => $siteProductSections,
                        'modulesArray'        => $modulesArray,
                    ];

                    $menuProducts                   = $this->menuProducts;
                    $menuProducts[0]['linkClasses'] = 'active';

                    foreach ($menuProducts[0]['subLinks'] as $key => $subLink) {
                        if ($subLink['name'] == $siteProduct->name) {
                            $menuProducts[0]['subLinks'][$key]['linkClasses'] = 'active';
                        }
                    }

                    View::share('menuProducts', $menuProducts);
                    View::share('currentPath', $siteProduct->name_slug);
                    View::share('marqueeLandingTitle', "Welcome to the WSJ Media Kit");
                    View::share('sourceText', $siteProduct->source_text);

                    Session::forget('previewMode');

                    return View::make('home.products.site-product-details', $viewData);
                } else {
                    Session::forget('previewMode');
                    return Redirect::to('/');
                }
            } else {
                Session::forget('previewMode');
                return Redirect::to('/');
            }
        }


        public function showInsights()
        {
            $insightPage  = InsightPage::orderBy('created_at', 'desc')->first();
            $bannerImg    = Upload::find($insightPage->banner_img_id);
            $modulesArray = $this->getLocationModules(2, "Hyfn\\Models\\PageContent\\InsightPage", $insightPage->id);

            $viewData = [
                'title'        => $insightPage->title,
                'source_text' => $insightPage->source_text,
                'modulesArray' => $modulesArray,
            ];

            $menuProducts                   = $this->menuProducts;
            $menuProducts[1]['linkClasses'] = 'active';

            View::share('menuProducts', $menuProducts);
            View::share('currentPath', "insights");
            View::share('marqueeLandingImgURL', $bannerImg->download_folder . $bannerImg->download_file);
            View::share('marqueeLandingTitle', $insightPage->title);
            View::share('sourceText', $insightPage->source_text);

            Session::forget('previewMode');

            return View::make('home.insights', $viewData);
        }


        public function showStudios()
        {
            $studiosPage = StudiosPage::orderBy('created_at', 'desc')->first();
            $bannerImg   = Upload::find($studiosPage->banner_img_id);

            $viewData = [
                'ucwordHeading'            => $studiosPage->title,
                'modulesArray'             => $this->getLocationModules(8, "Hyfn\\Models\\PageContent\\StudiosPage",
                    $studiosPage->id),
                'awardModulesArray'        => $this->getLocationModules(7, "Hyfn\\Models\\PageContent\\StudiosPage",
                    $studiosPage->id),
                'capabilitiesModulesArray' => $this->getLocationModules(6, "Hyfn\\Models\\PageContent\\StudiosPage",
                    $studiosPage->id),
            ];

            $menuProducts                   = $this->menuProducts;
            $menuProducts[2]['linkClasses'] = 'active';

            View::share('menuProducts', $menuProducts);

            View::share('currentPath', "wsjcustomstudios");
            View::share('marqueeLandingTitle', $studiosPage->title);
            View::share('marqueeLandingImgURL', $bannerImg->download_folder . $bannerImg->download_file);
            View::share('sourceText', $studiosPage->source_text);

            Session::forget('previewMode');

            return View::make('home.studios', $viewData);
        }


        public function showContact()
        {
            $ContactProducts = ContactProduct::orderBy('order', 'asc')->get();

            $products = [];
            foreach ($ContactProducts as $product) {
                $products[]  = [
                    'id'   => $product->id,
                    'name' => $product->name
                ];
            }
            $viewData = [
                'products' => $products,
            ];

            View::share('currentPath', "contact");
            View::share('marqueeLandingTitle', "Give me your number");

            Session::forget('previewMode');

            return View::make('home.contact', $viewData);
        }


        public function showRate()
        {
            $RateProducts = RateProduct::orderBy("order", "asc")->get();
            $rateProducts = [];
            foreach ($RateProducts as $product) {
                $rateProducts[$product->id] = $product->name;
            }

            $viewData = [
                'rateProducts' => $rateProducts,
            ];

            $menuProducts                   = $this->menuProducts;
            $menuProducts[3]['linkClasses'] = 'active';

            View::share('menuProducts', $menuProducts);

            View::share('currentPath', "rates");
            View::share('marqueeLandingTitle', "Give me your number");

            Session::forget('previewMode');

            return View::make('home.rates', $viewData);
        }


        public function showCaseStudies()
        {
            $tinyMCE        =   SiteProduct::find(1);

            // Get all Live and Published Case Studies
            $allPublishedCaseStudies         = CaseStudy::where('is_live', '=', 1)->orderBy('headline', 'asc')->get();
            $allPublishedAndLiveCaseStudyIDs = [];
            $allPublishedAndLiveCaseStudies  = [];
            foreach ($allPublishedCaseStudies as $caseStudy)
            {
                $caseStudyModules = ModuleLocationRelationship::where("entity_id", "=",
                    $caseStudy->id)->where("entity_type", "=", "Hyfn\\Models\\CaseStudy")->get([
                    'module_id',
                    'module_type'
                ]);

                foreach ($caseStudyModules as $module) {
                    $ModelName = $module->module_type;
                    $modelID   = $module->module_id;
                    $Model     = $ModelName::find($modelID);
                    if (is_object($Model)) {
                        if ($Model->is_live == 1) {
                            $allPublishedAndLiveCaseStudies[]  = $caseStudy;
                            $allPublishedAndLiveCaseStudyIDs[] = $caseStudy->id;
                            break;
                        }
                    } else {
                        continue;
                    }
                }
            }

            // Use the Live and Published Case Studies to generate listings
            $c                  =   0;
            $caseStudyListings  =   [];
            if (count($allPublishedAndLiveCaseStudies) >= 1)
            {
                foreach ($allPublishedAndLiveCaseStudies as $liveCaseStudy)
                {
                    $caseStudyListings[$c]['id']          = $liveCaseStudy->id;
                    $caseStudyListings[$c]['headline']    = $liveCaseStudy->headline;
                    $caseStudyListings[$c]['description'] = $liveCaseStudy->description;


                    if ($liveCaseStudy->listing_pic_id >= 1) {
                        $ListingPic                           = Upload::find($liveCaseStudy->listing_pic_id);
                        $caseStudyListings[$c]['listing_pic'] = $ListingPic->download_folder . $ListingPic->download_file;
                    } else {
                        $caseStudyListings[$c]['listing_pic'] = "/files/uploads/201410/CaseStudyDefault.png";
                    }

                    // Chosen Filters for this Case Study
                    $caseStudyFilters = CaseStudyFilterItemRelationship::where("case_study_id", "=", $liveCaseStudy->id)
                        ->distinct()
                        ->get(['filter_id']);
                    $chosenFilters    = [];
                    foreach ($caseStudyFilters as $filter) {
                        $filterItems = CaseStudyFilterItemRelationship::where("case_study_id", "=",
                            $liveCaseStudy->id)->
                        where("filter_id", "=", $filter->filter_id)->
                        distinct()->
                        get(['filter_item_id']);
                        foreach ($filterItems as $item) {
                            // $chosenFilters[] = "filter-" . $filter->filter_id . "-filter-item-" . $item->filter_item_id;
                            $chosenFilters[] = "filter-item-" . $item->filter_item_id;
                        }
                    }

                    $caseStudyListings[$c]['chosenFilters'] = $chosenFilters;

                    $c++;
                }
            }

            // Use the Live and Published Case Studies to generate filters
            $allFilterIDsWithContent = CaseStudyFilterItemRelationship::whereIn('case_study_id',
                $allPublishedAndLiveCaseStudyIDs)->distinct()->get(['filter_id'])->toArray();
            foreach ($allFilterIDsWithContent as $filterRel) {
                $caseStudyFilterIDs[] = $filterRel['filter_id'];
            }

            $filters     = CaseStudyFilter::whereIn('id', $caseStudyFilterIDs)->orderBy('order', 'asc')->get();
            $filterArray = [];
            foreach ($filters as $filter) {
                $filterArray[$filter->id]['name'] = $filter->name;


                $allFilterItemIDsWithContent = CaseStudyFilterItemRelationship::whereIn('case_study_id',
                    $allPublishedAndLiveCaseStudyIDs)->
                where('filter_id', '=', $filter->id)->
                distinct()->
                get(['filter_item_id'])->
                toArray();
                $caseStudyFilterItemIDs      = [];
                foreach ($allFilterItemIDsWithContent as $filterItemRel) {
                    $caseStudyFilterItemIDs[] = $filterItemRel['filter_item_id'];
                }

                $filterItems      = CaseStudyFilterItem::whereIn('id', $caseStudyFilterItemIDs)
                    ->orderBy('order', 'asc')
                    ->get();
                $filterItemsArray = [];
                foreach ($filterItems as $item) {
                    $filterItemsArray[] = [
                        'htmlID'      => 'filter-item-' . $item->id,
                        'displayName' => $item->name,
                    ];
                }

                $filterArray[$filter->id]['filterItems'] = $filterItemsArray;
            }


            $viewData = [
                'tinyMceOutput'     => '',
                'filterArray'       => $filterArray,
                'caseStudyListings' => $caseStudyListings,
            ];

            $menuProducts                   = $this->menuProducts;
            $menuProducts[4]['linkClasses'] = 'active';

            View::share('menuProducts', $menuProducts);

            View::share('currentPath', "gallery");
            View::share('marqueeLandingTitle', "Gallery");

            Session::forget('previewMode');

            return View::make('home.case-studies', $viewData);
        }


        public function showCaseStudy($id)
        {
            $CaseStudy = CaseStudy::find($id);

	        if( is_object( $CaseStudy ) )
	        {
                if ($CaseStudy->is_live == 0 && $this->previewMode == false) {
                    return Redirect::to('gallery')->with("viewMessage",
                        'The Gallery Entry: ' . $CaseStudy->headline . ' is no longer live.');
                }

                $modulesArray = $this->getLocationModules(9, "Hyfn\\Models\\CaseStudy", $CaseStudy->id);

                $viewData = [
                    'caseStudy'    => $CaseStudy,
                    'modulesArray' => $modulesArray,
                ];

                $menuProducts                   = $this->menuProducts;
                $menuProducts[4]['linkClasses'] = 'active';

                View::share('menuProducts', $menuProducts);

                View::share('currentPath', "gallery");
                View::share('marqueeLandingTitle', "Gallery");

                Session::forget('previewMode');

                return View::make('home.case-study', $viewData);
	        }
	        else
	        {
                return Redirect::to('/gallery')->with("viewMessage", 'Gallery Entry not found. Please choose another.');
            }
        }


        public function previewIndex()
        {
            return Redirect::to('/')->with("previewMode", true);
        }


        public function previewProducts()
        {
            return Redirect::to('products')->with("previewMode", true);
        }


        public function previewProductDetails($siteProductSlug)
        {
            return Redirect::to('products/' . $siteProductSlug)->with("previewMode", true);
        }


        public function previewInsights()
        {
            return Redirect::to('insights')->with("previewMode", true);
        }


        public function previewStudios()
        {
            return Redirect::to('wsjcustomstudios')->with("previewMode", true);
        }


        public function previewCaseStudies()
        {
            return Redirect::to('gallery')->with("previewMode", true);
        }


        public function previewCaseStudy($id)
        {
            return Redirect::to('gallery/' . $id)->with("previewMode", true);
        }


        public function previewRate()
        {
            return Redirect::to('rates_specs')->with("previewMode", true);
        }

    }
