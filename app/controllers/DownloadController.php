<?php namespace Hyfn\Controllers;

use Hyfn\Models\Lead;
use Hyfn\Models\Upload;
use Input;
use Response;

class DownloadController extends BaseController
{
    public function contact($upload_id)
    {
        $upload = Upload::findOrFail($upload_id);

        $lead = new Lead;
        $lead->fill(Input::all());
        $lead->upload_id = $upload_id;

        if ($lead->save()) {
            $response = [
                'success' => true,
                'url' => $upload->download_folder . $upload->download_file,
            ];
        } else {
            $response = [
                'success' => false,
                'errors' => $lead->getErrors(),
            ];
        }

        return Response::json($response);
    }
}
