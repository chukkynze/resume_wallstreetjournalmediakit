<?php

    namespace Hyfn\Controllers;

    use Hyfn\Models\PageContent\ProductPage;
    use Hyfn\Models\SiteProduct;
    use Hyfn\Models\CaseStudy;
    use Hyfn\Models\CaseStudyFilter;
    use Hyfn\Models\CaseStudyFilterItem;
    use Hyfn\Models\CaseStudyFilterItemRelationship;
    use Hyfn\Models\ModuleLocationRelationship;
    use Hyfn\Models\Upload;

    use DB;
    use Input;
    use Session;
    use View;

    class SearchController extends BaseController
    {
        public $menuProducts;

        public function __construct()
        {
            $siteProductSections = [];
            $SiteProducts = SiteProduct::orderBy("section_order", "asc")->get();
            $ModuledSiteProducts = [];
            foreach ($SiteProducts as $siteProduct) {
                $ModuleRelationships = ModuleLocationRelationship::where('entity_id', '=',
                    $siteProduct->id)->where('entity_type', '=', "Hyfn\\Models\\SiteProduct")->where('location_id', '=',
                    "4")->get();

                foreach ($ModuleRelationships as $relationship) {
                    $moduleClassName = $relationship->module_type;
                    $moduleID        = $relationship->module_id;
                    $Module          = $moduleClassName::find($moduleID);

                    if ($Module->is_live == 1) {
                        $ModuledSiteProducts[] = $siteProduct;
                        break;
                    }
                }
            }


            foreach ($ModuledSiteProducts as $siteProduct) {
                $siteProductSections[] = [
                    'name' => $siteProduct->name,
                    'link' => $siteProduct->name_slug,
                    'linkClasses' => '',
                    'listClasses' => '',
                ];
            }

            $this->menuProducts = [
                0 => [
                    'name'        => "Products",
                    'link'        => "products",
                    'listClasses' => "primary-menu-link primary-parent-link",
                    'linkClasses' => "",
                    'subLinks'    => $siteProductSections,
                ],
                1 => [
                    'name'        => "WSJ. Insights",
                    'link'        => "wsjinsights",
                    'listClasses' => "primary-menu-link",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ],
                2 => [
                    'name'        => "WSJ. Custom Studios",
                    'link'        => "wsjcustomstudios",
                    'listClasses' => "primary-menu-link",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ],
                3 => [
                    'name'        => "Rates &amp; Specs",
                    'link' => "rates_specs",
                    'listClasses' => "primary-menu-link",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ]
                ,
                4 => [
                    'name'        => "Gallery",
                    'link'        => "gallery",
                    'listClasses' => "primary-menu-link mobile-hidden",
                    'linkClasses' => "",
                    'subLinks'    => "",
                ],
            ];

            $this->previewMode = Session::pull('previewMode') ? true : false;
            echo($this->previewMode ? "<div class='centered-text' style='background-color: orangered; color: #080808; font-weight: bold;'>Preview is enabled for this session only. Once page is refreshed or another page accessed, Preview mode will be terminated.</div>" : "");

            View::share('menuProducts', $this->menuProducts);
            View::share('sourceText', "");
        }


        public function showSearchResult()
        {
            $searchResults  =   $this->getSearchResults(Input::get('search'));
            $viewData       =   [
                                    'originalSearchTerm'    =>  Input::get('search'),
                                    'searchResults'         =>  $searchResults,
                                ];

            View::share('currentPath'           , "search");
            View::share('marqueeLandingTitle'   , "Search");

            return View::make('home.search', $viewData);
        }


        public function showGallerySearchResult()
        {
            $searchResults              = $this->getGallerySearchResults(Input::get('search'));
            $allFoundAndLiveCaseStudies = [];
            foreach ($searchResults as $foundCaseStudy) {
                $CaseStudy = CaseStudy::find($foundCaseStudy['caseStudyID']);
                $CaseStudy->snippet = $foundCaseStudy['snippet'];

                if ($CaseStudy->is_live == 1) {
                    $caseStudyModules = ModuleLocationRelationship::where("entity_id", "=", $CaseStudy->id)->
                    where("entity_type", "=", "Hyfn\\Models\\CaseStudy")->
                    get(['module_id', 'module_type']);


                    foreach ($caseStudyModules as $module) {
                        $ModelName = $module->module_type;
                        $modelID   = $module->module_id;
                        $Model     = $ModelName::find($modelID);
                        if ($Model->is_live == 1) {
                            $allFoundAndLiveCaseStudies[] = $CaseStudy;
                            break;
                        }
                    }
                } else {
                    continue;
                }
            }

            $c                 = 0;
            $caseStudyListings = [];
            if (count($allFoundAndLiveCaseStudies) >= 1) {
                foreach ($allFoundAndLiveCaseStudies as $liveCaseStudy) {
                    $caseStudyListings[$c]['id']          = $liveCaseStudy->id;
                    $caseStudyListings[$c]['headline']    = $liveCaseStudy->headline;
                    $caseStudyListings[$c]['description'] = $liveCaseStudy->snippet;


                    if ($liveCaseStudy->listing_pic_id >= 1) {
                        $ListingPic                           = Upload::find($liveCaseStudy->listing_pic_id);
                        $caseStudyListings[$c]['listing_pic'] = $ListingPic->download_folder . $ListingPic->download_file;
                    } else {
                        $caseStudyListings[$c]['listing_pic'] = "/files/uploads/201410/CaseStudyDefault.png";
                    }

                    // Chosen Filters for this Case Study
                    $caseStudyFilters = CaseStudyFilterItemRelationship::where("case_study_id", "=", $liveCaseStudy->id)
                        ->distinct()
                        ->get(['filter_id']);
                    $chosenFilters    = [];
                    foreach ($caseStudyFilters as $filter) {
                        $filterItems = CaseStudyFilterItemRelationship::where("case_study_id", "=",
                            $liveCaseStudy->id)->
                        where("filter_id", "=", $filter->filter_id)->
                        distinct()->
                        get(['filter_item_id']);
                        foreach ($filterItems as $item) {
                            $chosenFilters[] = "filter-item-" . $item->filter_item_id;
                        }
                    }

                    $caseStudyListings[$c]['chosenFilters'] = $chosenFilters;

                    $c++;
                }
            }


            $filters     = CaseStudyFilter::orderBy('order', 'asc')->get();
            $filterArray = [];
            foreach ($filters as $filter) {
                $filterArray[$filter->id]['name'] = $filter->name;
                $filterItems                      = CaseStudyFilterItem::where('filter_id', '=', $filter->id)
                    ->orderBy('order', 'asc')
                    ->get();
                $filterItemsArray                 = [];
                foreach ($filterItems as $item) {
                    $filterItemsArray[] = [
                        'htmlID'      => 'filter-item-' . $item->id,
                        'displayName' => $item->name,
                    ];
                }
                $filterArray[$filter->id]['filterItems'] = $filterItemsArray;
            }

            $viewData = [
                'originalSearchTerm' => Input::get('search'),
                'tinyMceOutput'      => '',
                'filterArray'        => $filterArray,
                'caseStudyListings'  => $caseStudyListings,
            ];

            $menuProducts                   = $this->menuProducts;
            $menuProducts[4]['linkClasses'] = 'active';

            View::share('menuProducts', $menuProducts);

            View::share('currentPath', "gallery");
            View::share('marqueeLandingTitle', "Gallery");

            Session::forget('previewMode');

            return View::make('home.case-studies', $viewData);
        }


        public function getGallerySearchResults($searchTerm)
        {
            // Raw Search Results
            $titleWeight       = 1;
            $bodyWeight         =   1;
            $descriptionWeight = 1;
            $keywordsWeight    = 1;
            $results            =   DB::table('search_data')
                                    ->select(DB::raw
                                    (
                                        "
                                            * ,
                                            MATCH(title)        AGAINST(:search1) AS title_score,
                                            MATCH(body)         AGAINST(:search2) AS body_score,
                                            MATCH(description)  AGAINST(:search3) AS description_score,
                                            MATCH(keywords)     AGAINST(:search4) AS keywords_score
                                        "
                                    ))
                                    ->whereRaw
                                    (
                                        "MATCH(title, body, description, keywords) AGAINST(:search5)",
                                        [
                                            'search1'=>$searchTerm,
                                            'search2'=>$searchTerm,
                                            'search3'=>$searchTerm,
                                            'search4'=>$searchTerm,
                                            'search5'=>$searchTerm,
                                        ]
                                    )
                                    ->orderByRaw
                                    (
                                        "
                                            (title_score       * :orderBy1) +
                                            (body_score        * :orderBy2) +
                                            (description_score * :orderBy3) +
                                            (keywords_score    * :orderBy4) desc
                                        ",
                                        [
                                            'orderBy1'=>$titleWeight,
                                            'orderBy2'=>$bodyWeight,
                                            'orderBy3'=>$descriptionWeight,
                                            'orderBy4'=>$keywordsWeight
                                        ]
                                    )
                                    ->get();

            if(count($results) == 0)
            {
                $results    =   DB::table('search_data')
                                ->select(DB::raw
                                (
                                    "
                                        * ,
                                        MATCH(title)        AGAINST(:search1 IN BOOLEAN MODE) AS title_score,
                                        MATCH(body)         AGAINST(:search2 IN BOOLEAN MODE) AS body_score,
                                        MATCH(description)  AGAINST(:search3 IN BOOLEAN MODE) AS description_score,
                                        MATCH(keywords)     AGAINST(:search4 IN BOOLEAN MODE) AS keywords_score
                                    "
                                ))
                                ->whereRaw
                                (
                                    "MATCH(title, body, description, keywords) AGAINST(:search5 IN BOOLEAN MODE)",
                                    [
                                        'search1'=>$searchTerm,
                                        'search2'=>$searchTerm,
                                        'search3'=>$searchTerm,
                                        'search4'=>$searchTerm,
                                        'search5'=>$searchTerm
                                    ]
                                )
                                ->orderByRaw
                                (
                                    "
                                        (title_score       * :titleWeight)       +
                                        (body_score        * :bodyWeight)        +
                                        (description_score * :descriptionWeight) +
                                        (keywords_score    * :keywordsWeight) desc
                                    ",
                                    [
                                        'orderBy1'=>$titleWeight,
                                        'orderBy2'=>$bodyWeight,
                                        'orderBy3'=>$descriptionWeight,
                                        'orderBy4'=>$keywordsWeight
                                    ]
                                )
                                ->get();
            }

            if(count($results) == 0)
            {
                $results    =   DB::table('search_data')
                                ->select(DB::raw
                                (
                                    "
                                        * ,
                                        MATCH(title)        AGAINST(:search1 WITH QUERY EXPANSION) AS title_score,
                                        MATCH(body)         AGAINST(:search2 WITH QUERY EXPANSION) AS body_score,
                                        MATCH(description)  AGAINST(:search3 WITH QUERY EXPANSION) AS description_score,
                                        MATCH(keywords)     AGAINST(:search4 WITH QUERY EXPANSION) AS keywords_score
                                    "
                                ))
                                ->whereRaw
                                (
                                    "MATCH(title, body, description, keywords) AGAINST(:search5 WITH QUERY EXPANSION)",
                                    [
                                        'search1'=>$searchTerm,
                                        'search2'=>$searchTerm,
                                        'search3'=>$searchTerm,
                                        'search4'=>$searchTerm,
                                        'search5'=>$searchTerm
                                    ]
                                )
                                ->orderByRaw
                                (
                                    "
                                        (title_score       * :titleWeight)       +
                                        (body_score        * :bodyWeight)        +
                                        (description_score * :descriptionWeight) +
                                        (keywords_score    * :keywordsWeight) desc
                                    ",
                                    [
                                        'orderBy1'=>$titleWeight,
                                        'orderBy2'=>$bodyWeight,
                                        'orderBy3'=>$descriptionWeight,
                                        'orderBy4'=>$keywordsWeight
                                    ]
                                )
                                ->get();
            }

            if (count($results) == 0) {
                $results = DB::table('search_data')
                    ->select(DB::raw
                    (
                        "
                                        * ,
                                        MATCH(title)        AGAINST(:search1 IN NATURAL LANGUAGE MODE) AS title_score,
                                        MATCH(body)         AGAINST(:search2 IN NATURAL LANGUAGE MODE) AS body_score,
                                        MATCH(description)  AGAINST(:search3 IN NATURAL LANGUAGE MODE) AS description_score,
                                        MATCH(keywords)     AGAINST(:search4 IN NATURAL LANGUAGE MODE) AS keywords_score
                                    "
                    ))
                    ->whereRaw
                    (
                        "MATCH(title, body, description, keywords) AGAINST(:search5 IN NATURAL LANGUAGE MODE)",
                        [
                            'search1' => $searchTerm,
                            'search2' => $searchTerm,
                            'search3' => $searchTerm,
                            'search4' => $searchTerm,
                            'search5' => $searchTerm
                        ]
                    )
                    ->orderByRaw
                    (
                        "
                                        (title_score       * :titleWeight)       +
                                        (body_score        * :bodyWeight)        +
                                        (description_score * :descriptionWeight) +
                                        (keywords_score    * :keywordsWeight) desc
                                    ",
                        [
                            'orderBy1' => $titleWeight,
                            'orderBy2' => $bodyWeight,
                            'orderBy3' => $descriptionWeight,
                            'orderBy4' => $keywordsWeight
                        ]
                    )
                    ->get();
            }
            // dd(DB::getQueryLog());

            // Process Results
            $output=[];
            $maxScore=0;
            foreach ($results as $result)
            {
                $totalScore =   ( ($result->title_score * $titleWeight) + ($result->body_score * $bodyWeight) + ($result->description_score * $descriptionWeight) + ($result->keywords_score * $keywordsWeight) );
                if($totalScore > $maxScore)
                {
                    $maxScore = $totalScore;
                }
                $score = @number_format(($totalScore / $maxScore) * 100, 0);


                $body        = $this->handleHTags($result->body);
                $description = $this->handleHTags($result->description);

                $foundInBody        = (stristr($body, $searchTerm) != false ? 1 : 0);
                $foundInDescription = (stristr($description, $searchTerm) != false ? 1 : 0);

                if ($result->type == 'gallery-listing') {
                    $searchedBody = $description;
                } else {
                    continue;
                }

                $snippet = $this->highlightSearchTerm($searchedBody, $searchTerm,
                        1) . ($searchedBody == "" ? "" : "...");


                $output[]   =   [
                    'foundInBody'        => $foundInBody,
                    'foundInDescription' => $foundInDescription,
                    'title'              => $result->title,
                    'type'               => $result->type,
                    'url'                => $result->url,
                    'caseStudyID'        => $result->object_id,
                    'score'              => $score,
                    'snippet'            => $snippet,
                    'title_score'        => $result->title_score,
                    'body_score'         => $result->body_score,
                    'description_score'  => $result->description_score,
                    'keywords_score'     => $result->keywords_score,
                    'titleWeight'        => $titleWeight,
                    'bodyWeight'         => $bodyWeight,
                    'descriptionWeight'  => $descriptionWeight,
                    'keywordsWeight'     => $keywordsWeight,
                                    'title_scoreWeighted'       =>  $result->title_score        * $titleWeight,
                                    'body_scoreWeighted'        =>  $result->body_score         * $bodyWeight,
                                    'description_scoreWeighted' =>  $result->description_score  * $descriptionWeight,
                                    'keywords_scoreWeighted'    =>  $result->keywords_score     * $keywordsWeight,
                                ];
            }


            // Return Processed Results [?? IDs or Listings ??]
            return $output;
        }


        public function getSearchResults($searchTerm)
        {
            // Raw Search Results
            $titleWeight       = 10;
            $bodyWeight        = 1;
            $descriptionWeight = 4;
            $keywordsWeight    = 4;
            $results           = DB::table('search_data')
                ->select(DB::raw
                (
                    "
                        * ,
                        MATCH(title)        AGAINST(:search1) AS title_score,
                        MATCH(body)         AGAINST(:search2) AS body_score,
                        MATCH(description)  AGAINST(:search3) AS description_score,
                        MATCH(keywords)     AGAINST(:search4) AS keywords_score
                    "
                ))
                ->whereRaw
                (
                    "MATCH(title, body, description, keywords) AGAINST(:search5)",
                    [
                        'search1' => $searchTerm,
                        'search2' => $searchTerm,
                        'search3' => $searchTerm,
                        'search4' => $searchTerm,
                        'search5' => $searchTerm,
                    ]
                )
                ->orderByRaw
                (
                    "
                                            (title_score       * :orderBy1) +
                                            (body_score        * :orderBy2) +
                                            (description_score * :orderBy3) +
                                            (keywords_score    * :orderBy4) desc
                                        ",
                    [
                        'orderBy1' => $titleWeight,
                        'orderBy2' => $bodyWeight,
                        'orderBy3' => $descriptionWeight,
                        'orderBy4' => $keywordsWeight
                    ]
                )
                ->get();

            if (count($results) == 0) {
                $results = DB::table('search_data')
                    ->select(DB::raw
                    (
                        "
                            * ,
                            MATCH(title)        AGAINST(:search1 IN BOOLEAN MODE) AS title_score,
                            MATCH(body)         AGAINST(:search2 IN BOOLEAN MODE) AS body_score,
                            MATCH(description)  AGAINST(:search3 IN BOOLEAN MODE) AS description_score,
                            MATCH(keywords)     AGAINST(:search4 IN BOOLEAN MODE) AS keywords_score
                        "
                    ))
                    ->whereRaw
                    (
                        "MATCH(title, body, description, keywords) AGAINST(:search5 IN BOOLEAN MODE)",
                        [
                            'search1' => $searchTerm,
                            'search2' => $searchTerm,
                            'search3' => $searchTerm,
                            'search4' => $searchTerm,
                            'search5' => $searchTerm
                        ]
                    )
                    ->orderByRaw
                    (
                        "
                                        (title_score       * :titleWeight)       +
                                        (body_score        * :bodyWeight)        +
                                        (description_score * :descriptionWeight) +
                                        (keywords_score    * :keywordsWeight) desc
                                    ",
                        [
                            'orderBy1' => $titleWeight,
                            'orderBy2' => $bodyWeight,
                            'orderBy3' => $descriptionWeight,
                            'orderBy4' => $keywordsWeight
                        ]
                    )
                    ->get();
            }

            if (count($results) == 0) {
                $results = DB::table('search_data')
                    ->select(DB::raw
                    (
                        "
                            * ,
                            MATCH(title)        AGAINST(:search1 WITH QUERY EXPANSION) AS title_score,
                            MATCH(body)         AGAINST(:search2 WITH QUERY EXPANSION) AS body_score,
                            MATCH(description)  AGAINST(:search3 WITH QUERY EXPANSION) AS description_score,
                            MATCH(keywords)     AGAINST(:search4 WITH QUERY EXPANSION) AS keywords_score
                        "
                    ))
                    ->whereRaw
                    (
                        "MATCH(title, body, description, keywords) AGAINST(:search5 WITH QUERY EXPANSION)",
                        [
                            'search1' => $searchTerm,
                            'search2' => $searchTerm,
                            'search3' => $searchTerm,
                            'search4' => $searchTerm,
                            'search5' => $searchTerm
                        ]
                    )
                    ->orderByRaw
                    (
                        "
                                        (title_score       * :titleWeight)       +
                                        (body_score        * :bodyWeight)        +
                                        (description_score * :descriptionWeight) +
                                        (keywords_score    * :keywordsWeight) desc
                                    ",
                        [
                            'orderBy1' => $titleWeight,
                            'orderBy2' => $bodyWeight,
                            'orderBy3' => $descriptionWeight,
                            'orderBy4' => $keywordsWeight
                        ]
                    )
                    ->get();
            }

            if (count($results) == 0) {
                $results = DB::table('search_data')
                    ->select(DB::raw
                    (
                        "
                            * ,
                            MATCH(title)        AGAINST(:search1 IN NATURAL LANGUAGE MODE) AS title_score,
                            MATCH(body)         AGAINST(:search2 IN NATURAL LANGUAGE MODE) AS body_score,
                            MATCH(description)  AGAINST(:search3 IN NATURAL LANGUAGE MODE) AS description_score,
                            MATCH(keywords)     AGAINST(:search4 IN NATURAL LANGUAGE MODE) AS keywords_score
                        "
                    ))
                    ->whereRaw
                    (
                        "MATCH(title, body, description, keywords) AGAINST(:search5 IN NATURAL LANGUAGE MODE)",
                        [
                            'search1' => $searchTerm,
                            'search2' => $searchTerm,
                            'search3' => $searchTerm,
                            'search4' => $searchTerm,
                            'search5' => $searchTerm
                        ]
                    )
                    ->orderByRaw
                    (
                        "
                                        (title_score       * :titleWeight)       +
                                        (body_score        * :bodyWeight)        +
                                        (description_score * :descriptionWeight) +
                                        (keywords_score    * :keywordsWeight) desc
                                    ",
                        [
                            'orderBy1' => $titleWeight,
                            'orderBy2' => $bodyWeight,
                            'orderBy3' => $descriptionWeight,
                            'orderBy4' => $keywordsWeight
                        ]
                    )
                    ->get();
            }
            // dd(DB::getQueryLog());

            // Process Results
            $output   = [];
            $maxScore = 0;
            foreach ($results as $result) {
                $totalScore = (($result->title_score * $titleWeight) + ($result->body_score * $bodyWeight) + ($result->description_score * $descriptionWeight) + ($result->keywords_score * $keywordsWeight));
                if ($totalScore > $maxScore) {
                    $maxScore = $totalScore;
                }
                $score = @number_format(($totalScore / $maxScore) * 100, 0);


                $body        = $this->handleHTags($result->body);
                $description = $this->handleHTags($result->description);

                $foundInBody        = (stristr($body, $searchTerm) != false ? 1 : 0);
                $foundInDescription = (stristr($description, $searchTerm) != false ? 1 : 0);
                $mediaTypes = ['pdf', 'lsx', 'xls'];

                if ($result->type == 'page') {
                    if (stristr($description, $searchTerm) != false) {
                        $searchedBody = $description;
                    } elseif (stristr($body, $searchTerm) != false) {
                        // Find the first occurence of the search term
                        $strPosOfFirstOccurrence = strpos($body, $searchTerm);

                        // The body is now split by this position into not found and found
                        $foundBody                = substr($body, $strPosOfFirstOccurrence, 250);
                        $notFoundBody             = substr($body, $strPosOfFirstOccurrence - 150,
                            $strPosOfFirstOccurrence - 1);
                        $everythingAfterLastSpace = substr(strchr(trim($notFoundBody), " "), 0, 250);
                        $subtract20Chars          = ($strPosOfFirstOccurrence - 20 > 0 ? $strPosOfFirstOccurrence - 20 : 0);
                        $searchedBody             = "..." . $everythingAfterLastSpace;
                    } else {
                        $searchedBody = $description;
                    }
                } elseif ($result->type == 'gallery-listing') {
                    $searchedBody = $description;
                } elseif (in_array($result->type, $mediaTypes)) {
                    $searchedBody = $description;
                } elseif (stristr($description, $searchTerm) != false) {
                    $searchedBody = $description;
                } else {
                    $searchedBody = "";
                }

                $snippet = $this->highlightSearchTerm($searchedBody, $searchTerm) . ($searchedBody == "" ? "" : "...");


                $output[] = [
                    'foundInBody'               => $foundInBody,
                    'foundInDescription'        => $foundInDescription,
                    'title'                     => $result->title,
                    'type'                      => $result->type,
                    'url'                       => $result->url,
                    'score'                     => $score,
                    'snippet'                   => $snippet,
                    'title_score'               => $result->title_score,
                    'body_score'                => $result->body_score,
                    'description_score'         => $result->description_score,
                    'keywords_score'            => $result->keywords_score,
                    'titleWeight'               => $titleWeight,
                    'bodyWeight'                => $bodyWeight,
                    'descriptionWeight'         => $descriptionWeight,
                    'keywordsWeight'            => $keywordsWeight,
                    'title_scoreWeighted'       => $result->title_score * $titleWeight,
                    'body_scoreWeighted'        => $result->body_score * $bodyWeight,
                    'description_scoreWeighted' => $result->description_score * $descriptionWeight,
                    'keywords_scoreWeighted'    => $result->keywords_score * $keywordsWeight,
                ];
            }


            // Return Processed Results
            return $output;
        }


        public function highlightSearchTerm($str, $searchTerm, $styleMode = 0)
        {
            if ($styleMode == 1) {
                $style = "font-style: italic; font-weight: bolder; background-color: #000000; color: yellow;";
            } else {
                $style = "font-style: italic; font-weight: bolder; background-color: #FFFF00;";
            }

            $occurrences = substr_count(strtolower($str), strtolower($searchTerm));
            $newString   = $str;
            $match       = [];

            for ($i = 0; $i < $occurrences; $i++) {
                $match[$i] = stripos($str, $searchTerm, $i);
                $match[$i] = substr($str, $match[$i], strlen($searchTerm));
                $newString = str_replace($match[$i], '[#]' . $match[$i] . '[@]', strip_tags($newString));
            }

            $newString = str_replace('[#]', '<span style="' . $style . '">', $newString);
            $newString = str_replace('[@]', '</span>', $newString);


            return $newString;
        }


        public function handleHTags($text)
        {
            $text = str_replace("[thisUsedToBeH1]", "<strong>", $text);
            $text = str_replace("[/thisUsedToBeH1]", "</strong> - ", $text);
            $text = str_replace("[thisUsedToBeH2]", "<strong>", $text);
            $text = str_replace("[/thisUsedToBeH2]", "</strong> - ", $text);
            $text = str_replace("[thisUsedToBeH3]", "<strong>", $text);
            $text = str_replace("[/thisUsedToBeH3]", "</strong> - ", $text);

            return $text;
        }

    }
