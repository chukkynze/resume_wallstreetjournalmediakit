<?php

    namespace Hyfn\Controllers;

    use \Illuminate\Routing\Controller;

    use Hyfn\Models\Upload;
    use Hyfn\Models\ModuleLocationRelationship;
    use Hyfn\Models\Modules\TextColumn;

    use Str;
    use View;
    use Validator;
    use Hyfn\Library\MimeReader;

    class BaseController extends Controller
    {
        public $previewMode = false;

        public function __construct()
        {
            Validator::extend('wsj_report_mimes', function ($attribute, $value, $parameters) {
                $allowed = [
                    'application/pdf',
                    'application/x-download'
                ];
                $mime    = new MimeReader($value->getRealPath());
                return in_array($mime->get_type(), $allowed);
            });
        }

        /**
         * Setup the layout used by the controller.
         *
         * @return void
         */
        protected function setupLayout()
        {
            if (!is_null($this->layout)) {
                $this->layout = View::make($this->layout);
            }
        }


        public function getModuleTypesForSelect()
        {
            return [
                0                   => 'Choose A Module Type',
                'TextModule'      => 'Text Module',
                'PdfListModule'   => 'PDF List Module',
                'SlideshowModule' => 'Slide Show Module',
                'SingleImageModule' => 'Single Image Module',
                'ImageColumnModule' => 'Image Column Module',
                'SummaryModule' => 'Summary Module',
                'SplitTextModule' => 'Split Text Module',
                'VideoModule' => 'Video Module',
                'TextColumnModule' => 'Text Column Module',
            ];
        }


        public function getLocationModules($locationID, $entityType, $entityID, $forceNonLive = 0)
        {
            $moduleLocationRelationships = ModuleLocationRelationship::where("location_id", "=", $locationID)->
            where("entity_type", "=", $entityType)->
            where("entity_id", "=", $entityID)->orderBy("order", "ASC")->get();
            $modulesArray                = [];
            foreach ($moduleLocationRelationships as $moduleLocationRelationship) {
                $module = $moduleLocationRelationship->module;

                if (is_object($module)) {
                    if (!$this->previewMode && !$module->is_live && $forceNonLive == 0) {
                        continue;
                    }

                    $module->relationshipID         = $moduleLocationRelationship->id;
                    $module->relationshipLocationID = $moduleLocationRelationship->location_id;
                    $module->relationshipEntityID   = $moduleLocationRelationship->entity_id;
                    $module->relationshipEntityType = $moduleLocationRelationship->entity_type;
                    $module->relationshipModuleID   = $moduleLocationRelationship->module_id;
                    $module->relationshipModuleType = $moduleLocationRelationship->module_type;
                    $module->relationshipOrder      = $moduleLocationRelationship->order;

                    // VideoModule
                    if ($moduleLocationRelationship->module_type == "Hyfn\\Models\\Modules\\VideoModule") {
                        // Source
                        if (strpos(strtolower($module->url), "youtube.com")) {
                            $module->source = 'youtube';
                        } elseif (strpos(strtolower($module->url), "vimeo.com")) {
                            $module->source = 'vimeo';
                        } else {
                            $module->source = "";
                        }

                        // Code
                        $urlSource    = $module->source == 'vimeo' ? "vimeo.com/" : "www.youtube.com/watch?v=";
                        $module->code = str_replace("https://" . $urlSource, "",
                            str_replace("http://" . $urlSource, "", $module->url));

                    } // SingleImageModule
                    elseif ($moduleLocationRelationship->module_type == "Hyfn\\Models\\Modules\\SingleImageModule") {
                        // Target
                        $module->targetName = ($module->target == 0 ? "_self" : "_blank");

                        // Upload File
                        $upload      = Upload::find($module->upload_id);
                        $module->url = $upload->download_folder . $upload->download_file;
                    } // SlideshowModule
                    elseif ($moduleLocationRelationship->module_type == "Hyfn\\Models\\Modules\\SlideshowModule") {

                    } // PdfListModule
                    elseif ($moduleLocationRelationship->module_type == "Hyfn\\Models\\Modules\\PdfListModule") {
                        // Upload File - Icon Heading Image
                        $module->iconHeading = Upload::find($module->upload_id);
                        $module->uploadLoop  = $module->files;
                    } // ImageColumnModule
                    elseif ($moduleLocationRelationship->module_type == "Hyfn\\Models\\Modules\\ImageColumnModule") {
                        // Upload File - Icon Heading Image
                        $module->iconHeading = Upload::find($module->upload_id);
                        $module->uploadLoop  = $module->images;
                    } // SummaryModule
                    elseif ($moduleLocationRelationship->module_type == "Hyfn\\Models\\Modules\\SummaryModule") {
                        // Upload File - Icon Heading Image
                        $module->iconHeading = Upload::find($module->upload_id);
                        $module->uploadLoop  = $module->images;
                    } // SplitTextModule
                    elseif ($moduleLocationRelationship->module_type == "Hyfn\\Models\\Modules\\SplitTextModule") {

                    }// TextModule
                    elseif ($moduleLocationRelationship->module_type == "Hyfn\\Models\\Modules\\TextModule") {
                        // Upload File - Icon Heading Image
                        $module->iconHeading = Upload::find($module->upload_id);
                    }// TextColumnModule
                    elseif ($moduleLocationRelationship->module_type == "Hyfn\\Models\\Modules\\TextColumnModule") {
                        // Upload File - Icon Heading Image
                        $module->iconHeading = Upload::find($module->upload_id);
                        $textColumnUnits     = TextColumn::where("text_column_module_id", "=", $module->id)->get();
                        $textBoxes           = [];
                        foreach ($textColumnUnits as $key => $row) {
                            $textBoxes[$row->col_id][] = $row->line;
                        }
                        $module->textBoxes = $textBoxes;
                    }

                    $modulesArray[] = [
                        'partial_name' => Str::slug(str_replace("Module", "",
                            str_replace("Hyfn\\Models\\Modules\\", "", $moduleLocationRelationship->module_type))),
                        'moduleObject' => $module,
                    ];
                } else {
                    continue;
                }

            }

            return $modulesArray;
        }

    }
