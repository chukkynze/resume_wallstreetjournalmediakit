<?php
    namespace Hyfn\Controllers;

    use Artisan;
    use View;
    use Auth;
    use Config;
    use Redirect;

    class AdminController extends BaseController
    {

        protected $loggedInUser;
        protected $success_message = false;

        public function __construct()
        {
            $this->cmsName       = Config::get('app.cmsName');
            $this->copyrightName = Config::get('app.copyrightName');

            // Fetch the User object, or set it to false if not logged in
            if (Auth::check()) {
                $this->loggedInUser = Auth::user();
            } else {
                $this->loggedInUser = false;
                return Redirect::to('/admin/login');
            }

            View::share('activeSection', "Home");
            View::share('loggedInUser', $this->loggedInUser);
            View::share('success_message', $this->success_message);
            View::share('success_message', $this->success_message);
            View::share('success_message', $this->success_message);
        }


        public function index()
        {
            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
            ];
            return View::make('admin.index', $viewData);
        }


        public function updateSearchIndexes()
        {
            Artisan::call('search:create');
        }
    }