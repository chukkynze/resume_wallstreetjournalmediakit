<?php
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\UploadCategory;

    class UploadCategoryController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Uploads");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allUploadCategories = UploadCategory::orderBy('order', 'asc')->get();

            $initIndexOrder = '';
            foreach ($allUploadCategories as $category) {
                $initIndexOrder .= "&index-row[]=" . $category->id;
            }


            $viewData = [
                'cmsName'                => $this->cmsName,
                'copyrightName'          => $this->copyrightName,
                'allowUploadCategoryMgt' => ($this->loggedInUser->can("manage_upload_categories") ? 1 : 0),
                'uploadCategories'       => $allUploadCategories,
                'initIndexOrder'         => $initIndexOrder,
            ];


            return View::make('admin.upload-categories.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $viewData = [
                'cmsName'        => $this->cmsName,
                'copyrightName'  => $this->copyrightName,
                'uploadCategory' => new UploadCategory(),
            ];

            return View::make('admin.upload-categories.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $uploadCategoryFormData = Input::all();
            $uploadCategory         = new UploadCategory();
            $uploadCategory->fill($uploadCategoryFormData);

            if ($uploadCategory->isModelValid()) {
                $uploadCategory->save();

                return Redirect::to('admin/upload-categories')
                    ->with('success_message', trans('admin.uploadCategory.create_success'));
            } else {
                return Redirect::to('admin/upload-categories/create')
                    ->withInput(Input::all())
                    ->withErrors($uploadCategory->getErrors());
            }
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function edit($id)
        {
            $viewData = [
                'cmsName'        => $this->cmsName,
                'copyrightName'  => $this->copyrightName,
                'uploadCategory' => UploadCategory::find($id),
            ];

            return View::make('admin.upload-categories.edit', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function update($id)
        {
            $uploadCategoryFormData = Input::all();
            $uploadCategory         = UploadCategory::find($id);
            $uploadCategory->fill($uploadCategoryFormData);

            if ($uploadCategory->isModelValid()) {
                $uploadCategory->save();

                return Redirect::to('admin/upload-categories')
                    ->with('success_message', trans('admin.uploadCategory.update_success') . $uploadCategory->name);
            } else {
                return Redirect::to('admin/upload-categories/' . $id . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($uploadCategory->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $UploadCategory     = UploadCategory::find($id);
            $UploadCategoryName = $UploadCategory->name;

            if ($UploadCategory->id >= 1) {
                UploadCategory::destroy($id);

                return Redirect::to('admin/upload-categories')
                    ->with('success_message',
                        "Upload Category: " . $UploadCategoryName . " has been deleted successfully.");
                #->with('success_message',trans('admin.delete_success'));
            } else {
                return Redirect::to('admin/upload-categories')
                    ->with('error_message', trans('admin.uploadCategory.delete_failure'));
            }
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource        = UploadCategory::find($id);
                $Resource->order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }


    }
