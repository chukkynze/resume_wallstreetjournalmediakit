<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       SiteProductController
     *
     * filename:    SiteProductController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:07 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use Str;
    use View;

    use Hyfn\Models\SiteProduct;
    use Hyfn\Models\SiteProductSection;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;


    class SiteProductController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Products");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allSiteProducts = SiteProduct::orderBy('section_order', 'asc')->get();

            $initIndexOrder = '';
            foreach ($allSiteProducts as $product) {
                $initIndexOrder .= "&index-row[]=" . $product->id;
            }
            $viewData       = [
                'cmsName'             => $this->cmsName,
                'copyrightName'       => $this->copyrightName,
                'allowSiteProductMgt' => ($this->loggedInUser->can("manage_site_products") ? 1 : 0),
                'siteProducts'   => $allSiteProducts,
                'initIndexOrder' => $initIndexOrder,
            ];


            return View::make('admin.site-products.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $headingImages = Upload::whereIn('category', [6, 9])->get();

            foreach ($headingImages as $headingImage) {
                $uploadCategory             = UploadCategory::find($headingImage->category);
                $headingImage->categoryName = $uploadCategory->name;
            }
            $initSubSectionOrder    = "";
            $siteProductSubSections = [];

            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'siteProduct'   => new SiteProduct(),
                'headingImages' => $headingImages,
                'initSubSectionOrder'    => $initSubSectionOrder,
                'siteProductSubSections' => $siteProductSubSections,
            ];

            return View::make('admin.site-products.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $formData = Input::all();
            $resource = new SiteProduct();
            $resource->fill($formData);
            $resource->name_slug = Input::get("name_slug");
            $resource->on_menu = 1;
            $resource->on_product_landing = 1;
            $resource->on_product_landing = 1;
            $resource->section_order = SiteProduct::count() + 1;

            if ($resource->isModelValid()) {
                $resource->save();

                return Redirect::to('admin/page-content/product-pages')
                    ->with('success_message', trans('admin.site-products.create_success'));
            } else {
                return Redirect::to('admin/site-products/create')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $headingImages = Upload::whereIn('category', [6, 9])->get();

            foreach ($headingImages as $headingImage) {
                $uploadCategory             = UploadCategory::find($headingImage->category);
                $headingImage->categoryName = $uploadCategory->name;
            }
            $SiteProduct = SiteProduct::find($id);

            $SiteProductSubSections = SiteProductSection::where("site_product_id", "=",
                $SiteProduct->id)->orderBy("order", "asc")->get();
            $siteProductSubSections = [];
            $initSubSectionOrder    = "";
            foreach ($SiteProductSubSections as $subSection) {
                $siteProductSubSections[] = [
                    'name' => $subSection->name,
                    'id'   => $subSection->id,
                ];
                $initSubSectionOrder .= "&sub-section[]=" . $subSection->id;
            }


            $viewData = [
                'cmsName'                => $this->cmsName,
                'copyrightName'          => $this->copyrightName,
                'siteProduct'            => $SiteProduct,
                'headingImages'          => $headingImages,
                'initSubSectionOrder'    => $initSubSectionOrder,
                'siteProductSubSections' => $siteProductSubSections,
            ];

            return View::make('admin.site-products.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $formData = Input::all();
            $resource = SiteProduct::find($id);
            $resource->fill($formData);

            if ($resource->isModelValid()) {
                $resource->save();

                return Redirect::to('admin/page-content/product-pages')
                    ->with('success_message', trans('admin.site-products.update_success'));
            } else {
                return Redirect::to('admin/site-products/' . $id . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resource     = SiteProduct::find($id);
            $resourceName = $resource->name;

            if ($resource->id >= 1) {
                SiteProduct::destroy($id);

                return Redirect::to('admin/site-products')
                    ->with('success_message', "Site Product: " . $resourceName . " has been deleted successfully.");
            } else {
                return Redirect::to('admin/site-products')
                    ->with('error_message', trans('admin.site-products.delete_failure'));
            }
        }


        public function getSiteProducts()
        {
            $allSiteProducts = SiteProduct::all();

            $output = [];
            foreach ($allSiteProducts as $siteProduct) {
                $output[$siteProduct->id] = $siteProduct->name;
            }

            return json_encode($output);
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource                = SiteProduct::find($id);
                $Resource->section_order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }


    }
