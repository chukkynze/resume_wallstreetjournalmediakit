<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       UploadController
     *
     * filename:    UploadController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:07 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use View;
    use Validator;

    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;
    use Hyfn\Models\User;


    class UploadController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Uploads");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allUploads = Upload::all();

            foreach ($allUploads as $upload) {
                $uploader             = User::find($upload->uploader_id);
                $upload->uploader     = $uploader->first_name . " " . $uploader->last_name;
                $uploadCategory       = UploadCategory::find($upload->category);
                $upload->categoryName = $uploadCategory->name;
            }


            $viewData = [
                'cmsName'        => $this->cmsName,
                'copyrightName'  => $this->copyrightName,
                'allowUploadMgt' => ($this->loggedInUser->can("manage_uploads") ? 1 : 0),
                'activeSection'  => "Uploads",
                'uploads'        => $allUploads,
            ];


            return View::make('admin.uploads.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $Users      = User::all();
            $usersArray = [];
            foreach ($Users as $user) {
                $usersArray[$user->id] = $user->first_name . " " . $user->last_name;
            }

            $uploadCategories       = UploadCategory::all();
            $uploadCategoryArray    = [];
            $uploadCategoryArray[0] = "Choose a category for your upload.";
            $sizedCategories = [
                1  => '',
                2  => ' (Size: 1686 x 923)',
                4  => ' (Size: 1104 x 624)',
                6  => '',
                7  => ' (Size: 157 x 180)',
                8  => ' (Size: 1104 x 509)',
                9  => '',
                10 => '',
                11 => ' (Size: 460 x 460)',
            ];


            foreach ($uploadCategories as $uploadCategory) {
                $uploadCategoryArray[$uploadCategory->id] = $uploadCategory->name . $sizedCategories[$uploadCategory->id];
            }

            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'upload'        => new Upload(),
                'categories'    => $uploadCategoryArray,
                'usersArray'    => $usersArray,
                'mode'          => "create",
            ];

            return View::make('admin.uploads.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $resourceModel              = new Upload();
            $resourceModel->name        = Input::get('name');
            $resourceModel->description = Input::get('description');
            $resourceModel->uploader_id = (int)Input::get('uploader_id');
            $resourceModel->category    = Input::get('category');
            $resourceModel->keywords    = Input::get('keywords');
            // $resourceModel->status      = Input::get('status') ?: 'Draft';
            $resourceModel->status      = Input::get('status') ?: 'Published';
            $resourceModel->gated = Input::get('gated') ?: false;
            $resourceModel->is_live = false;

            // Process Images
            $uploadDownloadFile = Input::file('download_file');
            if (isset($uploadDownloadFile)) {
                $resourceModel->download_file   = $uploadDownloadFile->getClientOriginalName();
                $resourceModel->download_folder = "/files/uploads/201410/";

                /**
                 * It is unnecessary to store the actual file in the database. We only need the file location. Thus, the actual
                 * file validation will be done here, while the file location validation will be done in the model.
                 */
                $validator = Validator::make
                (
                    [
                        'download_file' => $uploadDownloadFile
                    ],
                    [
                        'download_file' => [
                            'required',
                            //'size:10000',
                            'mimes:pdf,doc,docx,xls,xlsx,png,jpg,jpeg',
                        ],
                    ],
                    [
                        'download_file.required' => 'A download file is required..',
                        'download_file.size'     => 'The download file is too large.',
                        'download_file.mimes'    => 'The file you are trying to upload is not of an approved format (.pdf, .doc, .docx, .xls, .png, jpg). ',
                    ]
                );

                $imagesAreValid = false;
                if ($validator->passes()) {
                    $imagesAreValid = true;
                } else {
                    $imageErrors = $validator->messages()->toArray();
                }

                if ($resourceModel->isModelValid('saving') && $imagesAreValid) {
                    $resourceModel->save();

                    $this->updateSearchIndexes();

                    if ($uploadDownloadFile->move(public_path() . $resourceModel->download_folder . "/",
                        $resourceModel->download_file)
                    ) {
                        return Redirect::to('admin/uploads')
                            ->with('success_message',
                                trans('admin.uploads.create_success') . " " . $resourceModel->name);
                    } else {
                        $warning = " Upload could not be moved to - " . __DIR__ . "../../../public/" . $resourceModel->download_folder . "/" . $resourceModel->download_file . ".";
                        return Redirect::to('admin/uploads/' . $resourceModel->id . '/edit')
                            ->with('warning_message', $warning)
                            ->with('success_message',
                                trans('admin.uploads.create_success') . " " . $resourceModel->name);
                    }
                } else {
                    if (isset($imageErrors) && count($imageErrors) > 0) {
                        foreach ($imageErrors as $key => $errorArray) {
                            foreach ($errorArray as $errorMsg) {
                                $resourceModel->getErrors()->add($key, $errorMsg);
                            }
                        }
                    }

                    $allErrors = $resourceModel->getErrors();

                    return Redirect::to('admin/uploads/create')
                        ->withInput(Input::except('download_file'))
                        ->withErrors($allErrors);
                }
            } else {
                return Redirect::to('admin/uploads/create')
                    ->withInput(Input::except('download_file'))
                    ->with('error_message', 'Please, remember to select a file before you save.');
            }
        }


        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function show($id)
        {
            //
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $Users      = User::all();
            $usersArray = [];
            foreach ($Users as $user) {
                $usersArray[$user->id] = $user->first_name . " " . $user->last_name;
            }

            $uploadCategories       = UploadCategory::all();
            $uploadCategoryArray    = [];
            $uploadCategoryArray[0] = "Choose a category for your upload.";
            foreach ($uploadCategories as $uploadCategory) {
                $uploadCategoryArray[$uploadCategory->id] = $uploadCategory->name;
            }

            $currentFile = Upload::find($id);

            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'upload'        => Upload::find($id),
                'categories'    => $uploadCategoryArray,
                'usersArray'    => $usersArray,
                'mode' => "edit",
            ];

            return View::make('admin.uploads.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $formData                   = Input::all();
            $resourceModel              = Upload::find($id);
            $resourceModel->name        = Input::get('name');
            $resourceModel->description = Input::get('description');
            $resourceModel->uploader_id = (int)Input::get('uploader_id');
            $resourceModel->category    = Input::get('category');
            $resourceModel->keywords    = Input::get('keywords');
            $resourceModel->status      = 'Published';
            $resourceModel->gated       = Input::get('gated') ?: false;

            // Process Images
            $uploadDownloadFile = Input::file('download_file');
            if (isset($uploadDownloadFile)) {
                $resourceModel->download_file   = $uploadDownloadFile->getClientOriginalName();
                $resourceModel->download_folder = "/files/uploads/201410/";

                /**
                 * It is unnecessary to store the actual file in the database. We only need the file location. Thus, the actual
                 * file validation will be done here, while the file location validation will be done in the model.
                 */
                $validator = Validator::make
                (
                    [
                        'download_file' => $uploadDownloadFile
                    ],
                    [
                        'download_file' => [
                            'required',
                            //'size:10000',
                            'mimes:pdf,doc,docx,xls,xlsx,png,jpg,jpeg',
                        ],
                    ],
                    [
                        'download_file.required' => 'A download file is required..',
                        'download_file.size'     => 'The download file is too large.',
                        'download_file.mimes'    => 'The file you are trying to upload is not of an approved format (.pdf, .doc, .docx, .xls, .png, jpg). ',
                    ]
                );

                $imagesAreValid = false;
                if ($validator->passes()) {
                    $imagesAreValid = true;
                } else {
                    $imageErrors = $validator->messages()->toArray();
                }

                if ($resourceModel->isModelValid('saving') && $imagesAreValid) {
                    $resourceModel->save();

                    $this->updateSearchIndexes();

                    if ($uploadDownloadFile->move(public_path() . $resourceModel->download_folder . "/",
                        $resourceModel->download_file)
                    ) {
                        return Redirect::to('admin/uploads')
                            ->with('success_message',
                                trans('admin.uploads.edit_success') . " " . $resourceModel->name);
                    } else {
                        $warning = " Upload could not be moved to - " . __DIR__ . "../../../public/" . $resourceModel->download_folder . "/" . $resourceModel->download_file . ".";
                        return Redirect::to('admin/uploads/' . $resourceModel->id . '/edit')
                            ->with('warning_message', $warning)
                            ->with('success_message',
                                trans('admin.uploads.edit_success') . " " . $resourceModel->name);
                    }
                } else {
                    if (isset($imageErrors) && count($imageErrors) > 0) {
                        foreach ($imageErrors as $key => $errorArray) {
                            foreach ($errorArray as $errorMsg) {
                                $resourceModel->getErrors()->add($key, $errorMsg);
                            }
                        }
                    }

                    $allErrors = $resourceModel->getErrors();

                    return Redirect::to('admin/uploads/edit')
                        ->withInput(Input::except('download_file'))
                        ->withErrors($allErrors);
                }
            } else {
                if ($resourceModel->isModelValid('saving')) {
                    $resourceModel->save();

                    $this->updateSearchIndexes();

                    return Redirect::to('admin/uploads')
                        ->with('success_message',
                            trans('admin.uploads.edit_success'));
                } else {
                    return Redirect::to('admin/uploads/' . $id . '/edit')
                        ->withInput(Input::except('download_file'))
                        ->withErrors($resourceModel->getErrors());
                }
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resourceModel = Upload::find($id);
            $resourceName  = $resourceModel->name;

            if ($resourceModel->id >= 1) {
                $resourceModel::destroy($id);

                return Redirect::to('admin/uploads')
                    ->with('success_message', "Upload: " . $resourceName . " has been successfully deleted.");
            } else {
                return Redirect::to('admin/uploads')
                    ->with('error_message', trans('admin.uploads.delete_failure'));
            }
        }


    }
