<?php
    namespace Hyfn\Controllers\Admin;

    use Illuminate\Support\MessageBag;
    use Hyfn\Controllers\AdminController;
    use Hyfn\Models\User;
    use Hyfn\Models\Role;
    use Hyfn\Models\Permission;
    use Hyfn\Library\Hyfn;
    use Auth;
    use Hash;
    use View;
    use Input;
    use Redirect;
    use Session;
    use Lang;
    use Config;
    use Zizaco\Confide\ConfideUser;

    class UserController extends AdminController
    {

        use ConfideUser;

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Users");


        }

        public function index()
        {
            $Users = User::all();

            foreach ($Users as $user)
            {
                $roles    = $user->roles;
                $roleList = '';
                foreach ($roles as $role) {
                    $roleList .= $role->name . ", ";
                }

                $user->user_roles = substr($roleList, 0, -2);
            }


            $viewData = [
                'users' => $Users,
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'allowUserMgt'  => ($this->loggedInUser->can("manage_users") ? 1 : 0),
            ];

            return View::make('admin.users.index', $viewData);
        }


        public function create()
        {
            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
            ];

            return View::make('admin.users.create', $viewData)->with('user', new User());
        }


        /**
         * Stores a New User
         *
         * @author  Will Wallace
         * @since
         * @author  Chukky Nze <chukky.nze@hyfn.com>
         * @since   Sep 12 2014
         *
         *
         */
        public function store()
        {
            // Get form data, create new user
            $user_data                          = Input::all();
            $user                               = new User();
            $user_data['password_confirmation'] = Input::get('password_confirmation');
            $user->fill($user_data);
            $user->confirmed = true;
            /**
             * Password confirmation is not part of the model...technically. But, it's needed for validation. So, we
             * add it in for that purpose and then take it out just before saving to the database.
             */
            $user->password_confirmation = Input::get('password_confirmation');

            if ($user->isModelValid('creating')) {
                $user->password = Hash::make($user_data['password']);
                unset($user->password_confirmation);
                $user->save();

                $userRole = Role::where('id', '=', 4)->first();
                $user->attachRole($userRole);

                return Redirect::to('admin/users/' . $user->id . '/edit')
                    ->with('success_message', trans('admin.create_success'));
            } else {
                return Redirect::to('admin/users/create')
                    ->withInput(Input::except('password'))
                    ->withErrors($user->getErrors());
            }
        }


        public function show($user_id)
        {

        }


        public function edit($user_id)
        {
            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
            ];

            return View::make('admin.users.edit', $viewData)->with('user', User::find($user_id));
        }


        public function update($user_id)
        {
            // Get form data, find user and update
            $user_data = Input::all();
            $user      = User::find($user_id);

            if (empty($user_data['password'])) {
                unset($user_data['password']);
                unset($user_data['password_confirmation']);
                $user->password_confirmation = $user->password;
            }

            $user->fill($user_data);

            if ($user->isModelValid('updating')) {
                $user->save();
                return Redirect::to('admin/users/' . $user_id . '/edit')
                    ->with('success_message', trans('admin.update_success'));
            } else {
                return Redirect::to('admin/users/' . $user_id . '/edit')
                    ->withInput(Input::except('password'))
                    ->withErrors($user->getErrors());
            }
        }


        public function destroy($user_id)
        {
            $User       = User::find($user_id);
            $first_name = $User->first_name;
            $last_name  = $User->last_name;
            $email      = $User->email;

            if ($User->id > 1) {
                User::destroy($user_id);

                return Redirect::to('admin/users')
                    ->with('success_message',
                        "User: " . $first_name . " " . $last_name . "<" . $email . ">  - Deleted Successfully.");
                #->with('success_message',trans('admin.delete_success'));
            } else {
                return Redirect::to('admin/users')
                    ->with('error_message', "User was not deleted.");
            }
        }


        /**
         * Displays the login form
         *
         */
        public function showLogin()
        {
            if (Auth::user()) {
                // If user is logged, redirect to internal
                // page, change it to '/admin', '/dashboard' or something
                return Redirect::to('/admin');
            } else {
                $viewData = [
                    'cmsName'       => $this->cmsName,
                    'copyrightName' => $this->copyrightName,
                ];
                return View::make('admin.login', $viewData);
            }
        }


        /**
         * Log the user in and redirect to index
         *
         * @author  Will Wallace
         * @since
         * @author  Chukky Nze <chukky.nze@hyfn.com>
         * @since   Sep 12 2014
         *
         * @return $this|\Illuminate\Http\RedirectResponse
         */
        public function postLogin()
        {
            $rules = [
                'username' => 'required',
                'password' => 'required'
            ];

            $validate = Hyfn::validate($rules);
            if ($validate !== true) {
                return Redirect::to('login')
                    ->withInput(Input::except('password'))
                    ->withErrors($validate->errors());
            }

            $input = array(
                'email'    => Input::get('email'), // May be the username too
                'username' => Input::get('username'), // so we have to pass both
                'password' => Input::get('password'),
                'remember' => Input::get('remember'),
            );

            // If you wish to only allow login from confirmed users, call logAttempt
            // with the second parameter as true.
            // logAttempt will check if the 'email' perhaps is the username.
            if (\Confide::logAttempt($input, true)) {
                // If the session 'loginRedirect' is set, then redirect
                // to that route. Otherwise redirect to '/'
                $r = Session::get('loginRedirect');
                if (!empty($r)) {
                    Session::forget('loginRedirect');
                    return Redirect::to($r);
                }

                return Redirect::to('/admin'); // change it to '/admin', '/dashboard' or something
            } else {
                $user = new User;

                // Check if there was too many login attempts
                if (\Confide::isThrottled($input)) {
                    $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
                } else {
                    $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
                }

                if (is_null($user->validationErrors)) {
                    $user->validationErrors = new MessageBag();
                }

                $user->validationErrors->add('login_error', $err_msg);

                return Redirect::to('admin/login')
                    ->withInput(Input::except('password'))
                    ->with('error', true)
                    ->withErrors($user->validationErrors);
            }
        }


        /**
         * Attempt to confirm account with code
         *
         * @param  string $code
         */
        public function confirm($code)
        {
            if (\Confide::confirm($code)) {
                $notice_msg = Lang::get('confide::confide.alerts.confirmation');
                return Redirect::to('login')
                    ->with('notice', $notice_msg);
            } else {
                $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
                return Redirect::to('login')
                    ->with('error', $error_msg);
            }
        }


        /**
         * Displays the forgot password form
         *
         */
        public function showForgotPassword()
        {
            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
            ];
            return View::make(Config::get('confide::forgot_password_form'), $viewData);
        }


        /**
         * Attempt to send change password link to the given email
         *
         */
        public function postForgotPassword()
        {
            if (\Confide::forgotPassword(Input::get('email'))) {
                $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
                return Redirect::to('admin/login')
                    ->with('notice', $notice_msg);
            } else {
                $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
                return Redirect::action('Hyfn\\Controllers\\Admin\\UserController@showForgotPassword')
                    ->withInput()
                    ->with('error', $error_msg);
            }
        }


        /**
         * Shows the change password form with the given token
         *
         */
        public function showResetPassword($token)
        {
            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
            ];
            return View::make(Config::get('confide::reset_password_form'), $viewData)
                ->with('token', $token);
        }


        /**
         * Attempt change password of the user
         *
         */
        public function postResetPassword()
        {
            $input = array(
                'token'                 => Input::get('token'),
                'password'              => Input::get('password'),
                'password_confirmation' => Input::get('password_confirmation'),
            );

            // Get User from token
            $User = \Confide::userByResetPasswordToken(Input::get('token'));

            // By passing an array with the token, password and confirmation
            if ($User->resetPassword($input)) {
                $notice_msg = Lang::get('confide::confide.alerts.password_reset');
                return Redirect::to('admin/login')
                    ->with('notice', $notice_msg);
            } else {
                $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
                return Redirect::action('Hyfn\\Controllers\\Admin\\UserController@showResetPassword',
                    array('token' => $input['token']))
                    ->withInput()
                    ->with('error', $error_msg);
            }
        }


        /**
         * Log the user out of the application and return to login page.
         *
         * @author  Will Wallace
         * @since
         * @author  Chukky Nze <chukky.nze@hyfn.com>
         * @since   Sep 12 2014
         *
         * @return \Illuminate\Http\RedirectResponse
         */
        public function logout()
        {
            Auth::logout();

            return Redirect::to('/admin/login');
        }

    }
