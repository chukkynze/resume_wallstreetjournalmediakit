<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       RateProductController
     *
     * filename:    RateProductController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:07 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Hyfn\Models\MediaSpecification;
    use Input;
    use Redirect;
    use Response;
    use Str;
    use View;

    use Hyfn\Models\RateProduct;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;


    class RateProductController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Rates");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allProducts = RateProduct::orderBy("order", "asc")->get();

            $initIndexOrder = '';
            foreach ($allProducts as $product) {
                $initIndexOrder .= "&index-row[]=" . $product->id;
            }
            $viewData    = [
                'cmsName'             => $this->cmsName,
                'copyrightName'       => $this->copyrightName,
                'allowRateProductMgt' => ($this->loggedInUser->can("manage_rate_products") ? 1 : 0),
                'allProducts'         => $allProducts,
                'initIndexOrder' => $initIndexOrder,
            ];


            return View::make('admin.rate-products.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $RateProduct = new RateProduct();
            $uploadFiles = Upload::whereIn('category', [1])->get();
            $init_order = '';

            $chosenUploads        = [];
            $chosenUploadsIDArray = [];

            foreach ($uploadFiles as $uploadFile) {
                $uploadCategory           = UploadCategory::find($uploadFile->category);
                $uploadFile->categoryName = $uploadCategory->name;

            }

            $viewData = [
                'cmsName'              => $this->cmsName,
                'copyrightName'        => $this->copyrightName,
                'rateProduct'          => $RateProduct,
                'uploadFiles'          => $uploadFiles,
                'init_order'           => $init_order,
                'chosenUploads'        => $chosenUploads,
                'chosenUploadsIDArray' => $chosenUploadsIDArray,
            ];

            return View::make('admin.rate-products.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $resource = new RateProduct();
            $resource->fill(Input::all());

            if ($resource->isModelValid()) {
                $resource->save();

                $rateMedia = explode("&rate[]=", substr(Input::get('rate-media-order'), 8));
                //$affectedRows   =   MediaSpecification::where('rate_product_id', '=', $resource->id)->delete();

                $x = 1;
                foreach ($rateMedia as $uploadID) {
                    $MediaSpecification                  = new MediaSpecification();
                    $MediaSpecification->rate_product_id = $resource->id;
                    $MediaSpecification->upload_id       = $uploadID;
                    $MediaSpecification->order           = $x;
                    $MediaSpecification->save();
                    $x++;
                }

                return Redirect::to('admin/rate-products')
                    ->with('success_message', trans('admin.rate-products.create_success'));
            } else {
                return Redirect::to('admin/rate-products/create')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $RateProduct = RateProduct::find($id);
            $uploadFiles = Upload::whereIn('category', [1])->get();
            $init_order  = '';
            $MediaSpecs  = MediaSpecification::where('rate_product_id', '=', $id)->orderBy("order", "asc")->get();

            $chosenUploads        = [];
            $chosenUploadsIDArray = [];
            foreach ($MediaSpecs as $media) {
                $upload = Upload::find($media->upload_id);
                $init_order .= "&rate[]=" . $upload->id;
                $chosenUploadsIDArray[] = $upload->id;
                $chosenUploads[]        = [
                    'uploadID'   => $upload->id,
                    'uploadName' => $upload->name,
                ];

            }

            foreach ($uploadFiles as $uploadFile) {
                $uploadCategory           = UploadCategory::find($uploadFile->category);
                $uploadFile->categoryName = $uploadCategory->name;

            }

            $viewData = [
                'cmsName'              => $this->cmsName,
                'copyrightName'        => $this->copyrightName,
                'rateProduct'          => $RateProduct,
                'uploadFiles'          => $uploadFiles,
                'init_order'           => $init_order,
                'chosenUploads'        => $chosenUploads,
                'chosenUploadsIDArray' => $chosenUploadsIDArray,
            ];

            return View::make('admin.rate-products.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $resource = RateProduct::find($id);
            $resource->fill(Input::all());

            if ($resource->isModelValid()) {
                $resource->save();

                $rateMedia    = explode("&rate[]=", substr(Input::get('rate-media-order'), 8));
                $affectedRows = MediaSpecification::where('rate_product_id', '=', $id)->delete();

                $x = 1;
                foreach ($rateMedia as $uploadID) {
                    $MediaSpecification                  = new MediaSpecification();
                    $MediaSpecification->rate_product_id = $resource->id;
                    $MediaSpecification->upload_id       = $uploadID;
                    $MediaSpecification->order           = $x;
                    $MediaSpecification->save();
                    $x++;
                }

                return Redirect::to('admin/rate-products')
                    ->with('success_message', trans('admin.rate-products.update_success'));
            } else {
                return Redirect::to('admin/rate-products/' . $id . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resource     = RateProduct::find($id);
            $resourceName = $resource->name;

            if ($resource->id >= 1) {
                RateProduct::destroy($id);

                return Redirect::to('admin/rate-products')
                    ->with('success_message', "Rate Product: " . $resourceName . " has been deleted successfully.");
            } else {
                return Redirect::to('admin/site-products')
                    ->with('error_message', trans('admin.rate-products.delete_failure'));
            }
        }


        public function getRateProducts()
        {
            $allProducts = RateProduct::orderBy("order", "asc")->get();

            $output = [];
            foreach ($allProducts as $product) {
                $output[$product->id] = $product->name;
            }

            return json_encode($output);
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource        = RateProduct::find($id);
                $Resource->order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }


        /**
         * @param $rateProductID
         *
         * @return string
         */
        public function getRateSpecificationByRateProduct($rateProductID)
        {
            $RateProduct = RateProduct::find($rateProductID);

            if ($RateProduct->id >= 1) {
                $MediaSpecs = MediaSpecification::where("rate_product_id", "=", $rateProductID)->orderBy("order",
                    "asc")->get();
                $RateSpecs  = [];

                foreach ($MediaSpecs as $specs) {
                    $Upload              = Upload::find($specs->upload_id);
                    $RateSpecs['data'][] = [
                        'name'      => $Upload->name,
                        'file'      => $Upload->download_file,
                        'iconClass' => substr($Upload->download_file, -4) == ".xls" || substr($Upload->download_file,
                            -5) == ".xlsx" ? "excel-link" : "pdf-link",
                        'location'  => $Upload->download_folder,
                    ];
                }

                $RateSpecs['submit_ad_link'] = ($RateProduct->submit_ad_link != ""
                    ? $RateProduct->submit_ad_link
                    : 0);

                // Weird Edge Cases
                switch ($RateProduct->name) {
                    case 'Classified'   :
                        $RateSpecs['data'][0]         = [
                            'name'      => 0,
                            'file'      => 0,
                            'iconClass' => 0,
                            'location'  => 0,
                        ];
                        $RateSpecs['classified_text'] = "Please visit <a href='http://classifieds.wsj.com' target='_blank'>classifieds.wsj.com</a> for rates, specs and to submit ad materials. ";
                }

            } else {
                $RateSpecs = [];
            }

            return json_encode($RateSpecs);
        }


    }
