<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       ContactController
     *
     * filename:    ContactController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/29/14 11:25 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\Contact;
    use Hyfn\Models\ContactLocation;
    use Hyfn\Models\ContactProduct;


    class ContactController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Contact");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allContacts = Contact::all();

            foreach ($allContacts as $contact) {
                if ($contact->location_id >= 1) {
                    $location          = ContactLocation::find($contact->location_id);
                    $contact->location = $location->name;
                } else {
                    $contact->location = "Unassigned";
                }

                if ($contact->product_id >= 1) {
                    $location = ContactProduct::find($contact->product_id);
                    $contact->product = $location->name;
                } else {
                    $contact->product = "Unassigned";
                }
            }

            $viewData = [
                'cmsName'         => $this->cmsName,
                'copyrightName'   => $this->copyrightName,
                'allowContactMgt' => ($this->loggedInUser->can("manage_contacts") ? 1 : 0),
                'contacts'        => $allContacts,
            ];


            return View::make('admin.contacts.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $contactLocations = ContactLocation::all();
            $contactProducts = ContactProduct::all();

            $locations = [];
            foreach ($contactLocations as $contactLocation) {
                $ContactLocation                 = ContactLocation::find($contactLocation->id);
                $locations[$ContactLocation->id] = $ContactLocation->name;
            }

            $products = [];
            foreach ($contactProducts as $contactProduct) {
                $ContactProduct                = ContactProduct::find($contactProduct->id);
                $products[$ContactProduct->id] = $ContactProduct->name;
            }

            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'contact'       => new Contact(),
                'locations'     => $locations,
                'products'      => $products,
            ];

            return View::make('admin.contacts.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $contactModel               = new Contact();
            $contactModel->department   = Input::get('department');
            $contactModel->full_name    = Input::get('full_name');
            $contactModel->job_title    = Input::get('job_title');
            $contactModel->phone_number = Input::get('phone_number');
            $contactModel->email        = Input::get('email');
            $contactModel->location_id  = Input::get('location_id');
            $contactModel->product_id   = Input::get('product_id');

            if ($contactModel->isModelValid('saving')) {
                $contactModel->save();

                return Redirect::to('admin/contacts/')
                    ->with('success_message', trans('admin.contacts.create_success'));
            } else {
                return Redirect::to('admin/contacts/create')
                    ->withInput(Input::all())
                    ->withErrors($contactModel->getErrors());
            }
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $contactLocations = ContactLocation::all();
            $contactProducts = ContactProduct::all();

            $locations = [];
            foreach ($contactLocations as $contactLocation) {
                $ContactLocation                 = ContactLocation::find($contactLocation->id);
                $locations[$ContactLocation->id] = $ContactLocation->name;
            }

            $products = [];
            foreach ($contactProducts as $contactProduct) {
                $ContactProduct                = ContactProduct::find($contactProduct->id);
                $products[$ContactProduct->id] = $ContactProduct->name;
            }

            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'contact'       => Contact::find($id),
                'locations'     => $locations,
                'products'      => $products,
            ];

            return View::make('admin.contacts.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $contactModel               = Contact::find($id);
            $contactModel->department   = Input::get('department');
            $contactModel->full_name    = Input::get('full_name');
            $contactModel->job_title    = Input::get('job_title');
            $contactModel->phone_number = Input::get('phone_number');
            $contactModel->email        = Input::get('email');
            $contactModel->location_id  = Input::get('location_id');
            $contactModel->product_id   = Input::get('product_id');

            if ($contactModel->isModelValid('saving')) {
                $contactModel->save();

                return Redirect::to('admin/contacts/')
                    ->with('success_message', trans('admin.contacts.update_success'));
            } else {
                return Redirect::to('admin/contacts/' . $id . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($contactModel->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resourceModel = Contact::find($id);
            $ContactName   = $resourceModel->full_name;

            if ($resourceModel->id >= 1) {
                $resourceModel::destroy($id);

                return Redirect::to('admin/contacts')
                    ->with('success_message', "Contact: " . $ContactName . " has been deleted successfully.");
            } else {
                return Redirect::to('admin/contacts')
                    ->with('error_message', trans('admin.contacts.delete_failure'));
            }
        }

        /**
         * Getting Contacts via ajax from the frontend
         *
         * There are three urls you'll need. All 3 will return a JSON object
         *
         * 1.   To get all the "contact products" for the first drop down you'll use :
         *      http://vm.wsj.local:8000/admin/contact-products/get-all
         *      That should return something like this:
         *      {
         * "1": "The Wall Street Journal",
         * "2": "Global",
         * "3": "WSJ. Magazine",
         * "4": "MarketWatch",
         * "5": "Conferences",
         * "6": "WSJ. Custom Studios",
         * "7": "WSJ. Insights"
         * }
         *
         * 2.   These ids are contactProductIDs
         *      You can use the contactProductIDs in the second url to get all the locations (of existing contacts)
         *      http://vm.wsj.local:8000/admin/contacts/get-locations/2
         *      That should return something like this:
         *      {
         * "2-10": "Asia",
         * "2-11": "Europe",
         * "2-12": "Latin America"
         * }
         *      Notice that the contactProductID is also included with the new contactLocationID
         *      "2-10"
         *      10 is the contactLocationID
         *
         * 3.   You'll use both to get the actual array of contacts. So to get all the contacts under Global product in Asia...
         *      http://vm.wsj.local:8000/admin/contacts/get-all/2/10
         *      This will return:
         *      [
         * {
         * "department": "HONG KONG/CHINA/SINGAPORE",
         * "full_name": "Mark Rogers",
         * "job_title": "Executive Sales Director",
         * "phone_number": "852.2831.2559",
         * "email": "mark.rogers@wsj.com",
         * "location_id": 10,
         * "product_id": 2,
         * "created_at": "2014-10-01 21:31:45",
         * "updated_at": "2014-10-01 21:31:45",
         * "deleted_at": null
         * },
         * {
         * "department": "JAPAN/KOREA",
         * "full_name": "Hiroshi Kakiyama",
         * "job_title": "Sales Director",
         * "phone_number": "81.3.6269.2701",
         * "email": "hiroshi.kakiyama@dowjones.com",
         * "location_id": 10,
         * "product_id": 2,
         * "created_at": "2014-10-01 21:31:45",
         * "updated_at": "2014-10-01 21:31:45",
         * "deleted_at": null
         * }
         * ]
         */


        /**
         * @param $contactProductID
         *
         * @return string
         */
        public function getContactLocationsByContactProduct($contactProductID)
        {
            $allContactsUnderContactProduct = Contact::where("product_id", "=", $contactProductID)->get();

            $output = [];
            foreach ($allContactsUnderContactProduct as $contact) {
                $ContactLocation                                      = ContactLocation::find($contact->location_id);
                $output[$contactProductID . "-" . $contact->location_id] = $ContactLocation->name;
            }

            asort($output);
            return json_encode($output);
        }

        /**
         * @param $contactLocationID
         * @param $contactProductID
         *
         * @return string
         */
        public function getContactsByLocation($contactLocationID, $contactProductID)
        {
            $allContactsUnderContactProduct = Contact::where("product_id", "=",
                $contactLocationID)->where("location_id", "=", $contactProductID)->orderBy('order', 'asc')->get();

            return json_encode($allContactsUnderContactProduct);
        }


        public function reOrderContact($contactID)
        {
            $Contact  = Contact::find($contactID);
            $Product  = ContactProduct::find($Contact->product_id);
            $Location = ContactLocation::find($Contact->location_id);
            $Contacts = Contact::where('product_id', '=', $Contact->product_id)->
            where('location_id', '=', $Contact->location_id)->
            orderBy('order', 'asc')->
            get();

            $initIndexOrder = '';
            foreach ($Contacts as $emp) {
                $initIndexOrder .= "&index-row[]=" . $emp->id;
            }

            $viewData = [
                'cmsName'        => $this->cmsName,
                'copyrightName'  => $this->copyrightName,
                'contacts'       => $Contacts,
                'Location'       => $Location,
                'Product'        => $Product,
                'initIndexOrder' => $initIndexOrder,
            ];

            return View::make('admin.contacts.re-order', $viewData);
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource        = Contact::find($id);
                $Resource->order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }
    }
