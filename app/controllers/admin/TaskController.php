<?php
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Hyfn\Models\Lead;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;

    use Input;
    use Redirect;
    use Response;
    use Session;
    use View;
    use Validator;

    class TaskController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Tasks");
        }


        public function getLeadsCsv()
        {
            header("Content-type: text/csv");
            header("Content-Transfer-Encoding: UTF-8");
            header("Content-Disposition: attachment; filename=leads.csv");
            header("Pragma: no-cache");
            header("Expires: 0");

            $Leads   = Lead::all();
            $output  = fopen("php://output", "w");
            $headers = [
                '#',
                'Email',
                'First Name',
                'Last Name',
                'Company',
                'Title',
                'Document Title',
                'Document Category',
                'Document URL',
                'Created At',
                'Updated At',
            ];
            fputcsv($output, $headers, ',', '"');
            foreach ($Leads as $lead) {
                $Upload         = Upload::find($lead->upload_id);
                $UploadCategory = UploadCategory::find($Upload->category);
                $row            = [
                    '#',
                    $lead->email,
                    $lead->first_name,
                    $lead->last_name,
                    $lead->company,
                    $lead->title,
                    $Upload->name,
                    $UploadCategory->name,
                    $Upload->download_folder . $Upload->download_file,
                    $lead->created_at,
                    $lead->updated_at,
                ];

                fputcsv($output, $row, ',', '"');
            }
            fclose($output);
            exit;
        }


    }
