<?php
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Hyfn\Models\User;
    use Input;
    use Redirect;
    use Response;
    use Session;
    use View;
    use Validator;

    use Hyfn\Models\Role;
    use Hyfn\Models\Permission;

    class PermissionController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Users");
        }


        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allRoles       = Role::all();
            $allPermissions = Permission::all();

            foreach ($allRoles as $role) {
                $role->value        = $role->id;
                $role->displayValue = $role->name;
            }


            $viewData = [
                'cmsName'            => $this->cmsName,
                'copyrightName'      => $this->copyrightName,
                'allowPermissionMgt' => ($this->loggedInUser->can("manage_permissions") ? 1 : 0),
                'activeSection'      => "Users",
                'permissions'        => $allPermissions,
                'roles'              => $allRoles,
            ];


            return View::make('admin.permissions.index', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $newPermission               = new Permission;
            $newPermission->name         = Input::get('permissionName');
            $newPermission->display_name = Input::get('permissionDisplayName');
            $newPermission->save();

            $headerArray = array
            (
                'Content-Type'  => 'application/json',
                'Cache-Control' => 'no-cache, must-revalidate',
            );

            $contentArray = array
            (
                "status"  => 'success1',
                "message" => "The permission, " . $newPermission->display_name . ", was successfully created.",
            );

            Session::put('success_message',
                "The permission, " . $newPermission->display_name . ", was successfully created.");

            $response = Response::make($contentArray, 200, $headerArray);

            return $response;
        }


        public function saveAssignedPermission()
        {
            $permission             = Permission::find((int)Input::get("permissionID"));
            $role                   = Role::find((int)Input::get("permissionRole"));
            $roleCurrentPermissions = $role->perms;

            $permissionsArray = [];
            foreach ($roleCurrentPermissions as $oldPermission) {
                $permissionsArray[] = $oldPermission->id;
            }
            $permissionsArray[] = $permission->id;
            $permissionsArray   = array_unique($permissionsArray);

            $role->perms()->sync($permissionsArray);

            $headerArray = array
            (
                'Content-Type'  => 'application/json',
                'Cache-Control' => 'no-cache, must-revalidate',
            );

            $contentArray = array
            (
                "status"  => 'success',
                "message" => "The permission, " . $permission->display_name . ", was successfully assigned to the role, " . $role->name . ".",
            );

            Session::put('success_message',
                "The permission, " . $permission->display_name . ", was successfully assigned to the role, " . $role->name . ".");

            $response = Response::make($contentArray, 200, $headerArray);

            return $response;
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $Permission     = Permission::find($id);
            $permissionName = $Permission->display_name;

            if ($Permission->id > 1) {
                Permission::destroy($id);

                return Redirect::to('admin/permissions')
                    ->with('success_message', "Permission: " . $permissionName . " successfully deleted.");
            } else {
                return Redirect::to('admin/permissions')
                    ->with('error_message', "Permission was not deleted.");
            }
        }


    }
