<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       StudiosPageController
     *
     * filename:    StudiosPageController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:07 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin\PageContent;

    use Hyfn\Controllers\AdminController;

    use Hyfn\Models\ModuleLocation;
    use Input;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\PageContent\StudiosPage;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;


    class StudiosPageController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Custom Studios");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $StudiosPage        = StudiosPage::find(1);
            $modLocIndex = [8, 6, 7];
            $initialModuleOrder = ['', '', ''];
            $moduleMatrix       = [];

            foreach ($modLocIndex as $key => $locationID) {
                $ModuleLocation     = ModuleLocation::find($locationID);
                $allModules         = $this->getLocationModules($ModuleLocation->id,
                    "Hyfn\\Models\\PageContent\\StudiosPage", $StudiosPage->id, 1);
                $initialModuleOrder = "";

                foreach ($allModules as $module) {
                    $moduleObject = $module['moduleObject'];
                    $initialModuleOrder .= "&location-" . $ModuleLocation->id . "-module-relationship[]=" . $moduleObject->relationshipOrder;
                }

                $moduleMatrix[] = [
                    'initialModuleOrder' => $initialModuleOrder,
                    'allModules'         => $allModules,
                    'locationID'         => $ModuleLocation->id,
                    'moduleTypes'        => $this->getModuleTypesForSelect(),
                    'entityType'         => 'Hyfn\\Models\\PageContent\\StudiosPage',
                    'entityID'           => 1,
                    'moduleLocationName' => $ModuleLocation->name,
                ];
            }


            $bannerImage                  = Upload::find($StudiosPage->banner_img_id);
            $StudiosPage->bannerImgName   = $bannerImage->name;
            $StudiosPage->bannerImgFolder = $bannerImage->download_folder;
            $StudiosPage->bannerImgFile   = $bannerImage->download_file;

            $viewData = [
                'cmsName'         => $this->cmsName,
                'copyrightName'   => $this->copyrightName,
                'allowStudiosMgt' => ($this->loggedInUser->can("manage_studios") ? 1 : 0),
                'activeSection'   => "Custom Studios",
                'studiosPages'    => $StudiosPage,
                'moduleMatrix'    => $moduleMatrix,
            ];


            return View::make('admin.page-content.studios-page.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $studiosModel = StudiosPage::orderBy("created_at", "DESC")->first();
            $bannerImages = Upload::where('category', '=', 2)->get();

            foreach ($bannerImages as $bannerImage) {
                $uploadCategory            = UploadCategory::find($bannerImage->category);
                $bannerImage->categoryName = $uploadCategory->name;
            }

            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'studiosPage' => $studiosModel,
                'bannerImages'  => $bannerImages,
            ];

            return View::make('admin.page-content.studios-page.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $studiosModel = StudiosPage::find(1);
            $studiosModel->banner_img_id = Input::get('banner_img_id');
            $studiosModel->title         = Input::get('title');
            $studiosModel->source_text   = Input::get('source_text');

            if ($studiosModel->isModelValid('saving')) {
                $studiosModel->save();

                // Get all existing associated modules and add them to this new page as new modules

                return Redirect::to('admin/page-content/studios-pages/')
                    ->with('success_message', trans('admin.studios.update_success'));
            } else {
                return Redirect::to('admin/page-content/studios-pages/create')
                    ->withInput(Input::all())
                    ->withErrors($studiosModel->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resourceCount = StudiosPage::all()->count();

            if ($resourceCount > 1) {
                $resourceModel = StudiosPage::find($id);
                $resourceDate  = date("F jS Y", strtotime($resourceModel->created_at));
                $resourceModel::destroy($id);

                return Redirect::to('admin/page-content/studios-pages')
                    ->with('success_message',
                        "The Studios Page Content version, created at: " . $resourceDate . ", has been successfully deleted.");
            } else {
                return Redirect::to('admin/page-content/studios-pages')
                    ->with('error_message', trans('admin.studios.delete_min'));
            }
        }

    }