<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       ModuleController
     *
     * filename:    ModuleController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/13/14 11:07 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin\PageContent;

    use Hyfn\Controllers\AdminController;

    use Hyfn\Models\ModuleLocationRelationship;
    use Hyfn\Models\SiteProduct;
    use Hyfn\Models\SiteProductSection;
    use Input;
    use Illuminate\Support\MessageBag;
    use Redirect;
    use Response;
    use Session;
    use View;

    use Hyfn\Models\PageContent\HomePage;
    use Hyfn\Models\PageContent\HomePageSlider;
    use Hyfn\Models\CaseStudy;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;
    use Hyfn\Models\Modules\TextModule;
    use Hyfn\Models\Modules\PdfListModule;
    use Hyfn\Models\Modules\SlideshowModule;
    use Hyfn\Models\Modules\SingleImageModule;
    use Hyfn\Models\Modules\ImageColumnModule;
    use Hyfn\Models\Modules\SummaryModule;
    use Hyfn\Models\Modules\SplitTextModule;
    use Hyfn\Models\Modules\VideoModule;
    use Hyfn\Models\Modules\TextColumnModule;

    class ModuleController extends AdminController
    {

        public $entityTypes;
        public $partialTypes;
        public $redirects;
        public $moduleDisplayNames;

        public function __construct()
        {
            parent::__construct();
            $this->entityTypes = [
                'Hyfn\Models\PageContent\InsightPage - 2' => 'Insight Page',
                'Hyfn\Models\PageContent\StudiosPage - 6' => 'Custom Studios Page: Capabilities Section',
                'Hyfn\Models\PageContent\StudiosPage - 7' => 'Custom Studios Page: Award Section',
                'Hyfn\Models\PageContent\StudiosPage - 8' => 'Custom Studios Page: Main Section',
                'Hyfn\Models\SiteProduct - 3'             => 'Product Landing Page: ',
                'Hyfn\Models\SiteProduct - 4'             => 'Product Details Page (Main): ',
                'Hyfn\Models\SiteProductSection - 5'      => 'Product Details Sub Section: ',
                'Hyfn\Models\CaseStudy - 9'               => 'Gallery Entry Details Page: ',
            ];
            $this->redirects   = [
                'Hyfn\Models\PageContent\InsightPage' => 'page-content/insight-pages',
                'Hyfn\Models\PageContent\StudiosPage' => 'page-content/studios-pages',
                'Hyfn\Models\SiteProduct'             => 'page-content/product-pages',
                'Hyfn\Models\SiteProductSection'      => 'page-content/product-pages',
                'Hyfn\Models\CaseStudy' => 'gallery',
            ];

            $this->partialTypes = [
                'TextModule'       => 'text',
                'PdfListModule'    => 'pdf-list',
                'SlideshowModule'  => 'slideshow',
                'SingleImageModule' => 'single-image',
                'ImageColumnModule' => 'image-column',
                'SummaryModule'    => 'summary',
                'SplitTextModule'  => 'split-text',
                'VideoModule'      => 'video',
                'TextColumnModule' => 'text-column',
            ];

            $this->moduleDisplayNames = [
                'TextModule'       => 'Text Module',
                'PdfListModule'    => 'PDF List Module',
                'SlideshowModule'  => 'Slide Show Module',
                'SingleImageModule' => 'Single Image Module',
                'ImageColumnModule' => 'Image Column Module',
                'SummaryModule'    => 'Summary Module',
                'SplitTextModule'  => 'Split Text Module',
                'VideoModule'      => 'Video Module',
                'TextColumnModule' => 'Text Column Module',
            ];
        }


        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allHomePages = HomePage::orderBy('created_at', 'desc')->whereNull('deleted_at')->get();

            foreach ($allHomePages as $homePage) {
                $HomePageSliders = HomePageSlider::where("home_page_id", "=",
                    $homePage->id)->whereNull('deleted_at')->get();

                $getSliders = [];
                foreach ($HomePageSliders as $slider) {
                    $sliderImageInfo = Upload::find($slider->upload_id);
                    $getSliders[]    = [
                        'download_folder' => $sliderImageInfo->download_folder,
                        'download_file'   => $sliderImageInfo->download_file,
                        'main_text'       => $slider->main_text,
                        'sub_text1'       => $slider->sub_text1,
                        'sub_text2'       => $slider->sub_text2,
                        'source_text'     => $slider->source_text,
                    ];
                }

                $homePage->sliders = $getSliders;
            }

            $viewData = [
                'cmsName'                 => $this->cmsName,
                'copyrightName'           => $this->copyrightName,
                'allowHomePageContentMgt' => ($this->loggedInUser->can("manage_page_content") ? 1 : 0),
                'activeSection'           => "Page Content",
                'allHomePages'            => $allHomePages,
            ];


            return View::make('admin.page-content.home-page.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         * @return \Illuminate\View\View
         * @throws \Exception
         */
        public function createModule()
        {
            $moduleType = Input::get("moduleType");

            if ($moduleType == 'TextModule') {
                $formModule = new TextModule();

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } elseif ($moduleType == 'PdfListModule') {
                $formModule = new PdfListModule();

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $pdfImages = Upload::whereIn('category', [1])->get();
                foreach ($pdfImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }


                $formModule->chosenPDFs = [];

                $moduleData = [
                    'headingImages' => $headingImages,
                    'pdfImages' => $pdfImages,
                ];
            } elseif ($moduleType == 'SlideshowModule') {
                $formModule = new SlideshowModule();

                $mediaImages = Upload::whereIn('category', [4])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }


                $formModule->chosenMedia = [];

                $moduleData = [
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleType == 'SingleImageModule') {
                $formModule = new SingleImageModule();

                $mediaImages = Upload::whereIn('category', [8])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }


                $formModule->chosenMedia = [];

                $moduleData = [
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleType == 'ImageColumnModule') {
                $formModule = new ImageColumnModule();

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $mediaImages = Upload::whereIn('category', [7])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }

                $formModule->chosenMedia = [];

                $moduleData = [
                    'headingImages' => $headingImages,
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleType == 'SummaryModule') {
                $formModule = new SummaryModule();

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $mediaImages = Upload::whereIn('category', [4])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }

                $formModule->chosenMedia = [];

                $moduleData = [
                    'headingImages' => $headingImages,
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleType == 'SplitTextModule') {
                $formModule = new SplitTextModule();

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } elseif ($moduleType == 'VideoModule') {
                $formModule = new VideoModule();

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } elseif ($moduleType == 'TextColumnModule') {
                $formModule = new TextColumnModule();

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $initColDivID = 2;

                if (trim($formModule->column_3) != "") {
                    $initColDivID = 3;
                }
                if (trim($formModule->column_4) != "") {
                    $initColDivID = 4;
                }
                if (trim($formModule->column_5) != "") {
                    $initColDivID = 5;
                }
                if (trim($formModule->column_6) != "") {
                    $initColDivID = 6;
                }
                if (trim($formModule->column_7) != "") {
                    $initColDivID = 7;
                }
                if (trim($formModule->column_8) != "") {
                    $initColDivID = 8;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                    'initColDivID' => $initColDivID,
                ];
            } else {
                throw new \Exception("Invalid Module");
            }


            switch (Input::get("locationID")) {
                case 2 :
                    $addendum = "";
                    View::share('activeSection', "Insights");
                    break;

                case 3 :
                case 4 :
                case 5 :
                    if (Input::get("entityType") === 'Hyfn\Models\SiteProductSection') {
                        $SiteProduct = SiteProductSection::find(Input::get("entityID"));
                    } else {
                        $SiteProduct = SiteProduct::find(Input::get("entityID"));
                    }
                    $addendum = " " . $SiteProduct->name . " Section";
                View::share('activeSection', "Products");
                    break;

                case 6 :
                case 7 :
                case 8 :
                    $addendum = "";
                    View::share('activeSection', "Custom Studios");
                    break;

                case 1 :
                case 9 :
                    $addendum = "";
                    if (Input::get("entityType") === 'Hyfn\Models\CaseStudy') {
                        $CaseStudy = CaseStudy::find(Input::get("entityID"));
                        $addendum  = " " . $CaseStudy->headline . ".";
                    } else {
                        $addendum = "";
                    }
                    View::share('activeSection', "Case Study");
                    break;

                default :
                    $addendum = "";
            }

            if (Input::get("entityType") == 'Hyfn\Models\CaseStudy') {
                $entityRedirect = "gallery/" . Input::get("entityID") . "/add-modules";
            } else {
                $entityRedirect = $this->redirects[Input::get("entityType")];
            }

            $viewData = [
                    'cmsName'               => $this->cmsName,
                    'copyrightName'         => $this->copyrightName,
                    'formModule'            => $formModule,
                    'locationID'            => Input::get("locationID"),
                    'moduleType'            => $moduleType,
                    'moduleTypeDisplayName' => $this->moduleDisplayNames[$moduleType],
                    'entityType'            => Input::get("entityType"),
                    'entityRedirect' => $entityRedirect,
                    'entityTypeName'        => $this->entityTypes[Input::get("entityType") . " - " . Input::get("locationID")] . $addendum,
                    'entityID'              => Input::get("entityID"),
                    'modulePartial'         => $this->partialTypes[Input::get("moduleType")],
                    'images'                => $this->uploadsJson(),
                    'categories'            => $this->categoriesJson(),
                ] + $moduleData;

            return View::make('admin.page-content.modules.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         * @return $this
         * @throws \Exception
         */
        public function storeModule()
        {
            $moduleType     = Input::get("moduleType");
            $locationID     = Input::get("locationID");
            $entityID       = Input::get("entityID");
            $entityType     = Input::get("entityType");
            $entityTypeName = $this->entityTypes[$entityType . " - " . $locationID];

            $isMediaAdded = true;

            if ($moduleType == 'TextModule') {
                $toFormModule = new TextModule();
                $toDataModule = new TextModule();

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } elseif ($moduleType == 'PdfListModule') {
                $toFormModule = new PdfListModule();
                $toDataModule = new PdfListModule();

                $ids          = Input::get("upload_ids");
                $isMediaAdded = (isset($ids) && count($ids) >= 1 ? true : false);

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $pdfImages = Upload::whereIn('category', [1])->get();
                foreach ($pdfImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }

                $toFormModule->chosenPDFs = [];

                $moduleData = [
                    'headingImages' => $headingImages,
                    'pdfImages'     => $pdfImages,
                ];
            } elseif ($moduleType == 'SlideshowModule') {
                $toFormModule             = new SlideshowModule();
                $toDataModule             = new SlideshowModule();

                $ids          = Input::get("upload_ids");
                $isMediaAdded = (isset($ids) && count($ids) >= 1 ? true : false);

                $mediaImages = Upload::whereIn('category', [4])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }


                $toFormModule->chosenMedia = [];

                $moduleData = [
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleType == 'SingleImageModule') {
                $toFormModule             = new SingleImageModule();
                $toDataModule             = new SingleImageModule();
                $toDataModule->upload_id = Input::get('upload_id') ? Input::get('upload_id') : 0;

                $mediaImages = Upload::whereIn('category', [8])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }


                $toFormModule->chosenMedia = [];

                $moduleData = [
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleType == 'ImageColumnModule') {
                $toFormModule             = new ImageColumnModule();
                $toDataModule             = new ImageColumnModule();
                $toDataModule->upload_id = Input::get('upload_id') ? Input::get('upload_id') : 0;

                $ids          = Input::get("upload_ids");
                $isMediaAdded = (isset($ids) && count($ids) >= 1 ? true : false);

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $mediaImages = Upload::whereIn('category', [7])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }

                $toFormModule->chosenMedia = [];

                $moduleData = [
                    'headingImages' => $headingImages,
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleType == 'SummaryModule') {
                $toFormModule             = new SummaryModule();
                $toDataModule             = new SummaryModule();
                $toDataModule->upload_id = Input::get('upload_id') ? Input::get('upload_id') : 0;

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $mediaImages = Upload::whereIn('category', [4])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }

                $toFormModule->chosenMedia = [];

                $moduleData = [
                    'headingImages' => $headingImages,
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleType == 'SplitTextModule') {
                $toFormModule             = new SplitTextModule();
                $toDataModule             = new SplitTextModule();
                $toDataModule->upload_id = Input::get('upload_id') ? Input::get('upload_id') : 0;

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } elseif ($moduleType == 'VideoModule') {
                $toFormModule             = new VideoModule();
                $toDataModule             = new VideoModule();
                $toDataModule->upload_id = Input::get('upload_id') ? Input::get('upload_id') : 0;

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } elseif ($moduleType == 'TextColumnModule') {
                $toFormModule             = new TextColumnModule();
                $toDataModule             = new TextColumnModule();
                $toDataModule->upload_id = Input::get('upload_id') ? Input::get('upload_id') : 0;

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } else {
                throw new \Exception("Invalid Module");
            }

            $toFormModule->fill(Input::all());
            $toDataModule->fill(Input::all());

            if ($toDataModule->isValid('saving') && $isMediaAdded) {
                $toDataModule->save();

                if ($moduleType == 'PdfListModule') {
                    $toDataModule->files()->sync(Input::get("upload_ids"));
                }

                if ($moduleType == 'SlideshowModule') {
                    $toDataModule->slides()->sync(Input::get("upload_ids"));
                }

                if ($moduleType == 'ImageColumnModule') {
                    $toDataModule->images()->sync(Input::get("upload_ids"));
                }

                $countModuleRelationshipType = ModuleLocationRelationship::
                where("location_id", "=", $locationID)->
                where("entity_id", "=", $entityID)->
                where("entity_type", "=", $entityType)->
                count();

                $moduleRelationship              = new ModuleLocationRelationship();
                $moduleRelationship->location_id = $locationID;
                $moduleRelationship->entity_id   = Input::get("entityID");
                $moduleRelationship->entity_type = Input::get("entityType");
                $moduleRelationship->module_id   = $toDataModule->id;
                $moduleRelationship->module_type = 'Hyfn\\Models\\Modules\\' . $moduleType;
                $moduleRelationship->order       = $countModuleRelationshipType + 1;
                $moduleRelationship->save();

                $this->updateSearchIndexes();

                if (Input::get("entityType") == 'Hyfn\Models\CaseStudy') {
                    $entityRedirect = "gallery/" . Input::get("entityID") . "/add-modules";
                } else {
                    $entityRedirect = $this->redirects[Input::get("entityType")];
                }

                switch ($locationID) {
                    case 3  :
                    case 4  :
                    case 5  :
                        $tab = $locationID;
                        break;

                    default :
                        $tab = 0;
                }

                return Redirect::to('admin/' . $entityRedirect)
                    ->with('success_message',
                        'Successfully added a ' . $this->moduleDisplayNames[Input::get("moduleType")] . ' to the ' . $this->entityTypes[Input::get("entityType") . " - " . $locationID])
                    ->with('tab', $tab)
                    ->with('entityID', Input::get("entityID"));
            } else {
                switch (Input::get("locationID")) {
                    case 3 :
                    case 4 :
                    case 5 :
                        $SiteProduct = SiteProduct::find(Input::get("entityID"));
                        $addendum    = " " . $SiteProduct->name . " Section";
                        break;

                    case 9 :
                        $CaseStudy = CaseStudy::find(Input::get("entityID"));
                        $addendum  = " " . $CaseStudy->headline . ".";
                        break;

                    default :
                        $addendum = "";
                }

                if (!$isMediaAdded) {
                    $toDataModule->getErrors()->add('upload_ids', "Please pick your media for this module.");
                }

                if (Input::get("entityType") == 'Hyfn\Models\CaseStudy') {
                    $entityRedirect = "gallery/" . Input::get("entityID") . "/add-modules";
                } else {
                    $entityRedirect = $this->redirects[Input::get("entityType")];
                }

                $viewData = [
                        'cmsName'               => $this->cmsName,
                        'copyrightName'         => $this->copyrightName,
                        'formModule'            => $toFormModule,
                        'locationID'            => Input::get("locationID"),
                        'moduleType'            => $moduleType,
                        'moduleTypeDisplayName' => $this->moduleDisplayNames[$moduleType],
                        'entityType'            => Input::get("entityType"),
                        'entityRedirect' => $entityRedirect,
                        'entityTypeName'        => $this->entityTypes[Input::get("entityType") . " - " . $locationID] . $addendum,
                        'entityID'              => Input::get("entityID"),
                        'modulePartial'         => $this->partialTypes[Input::get("moduleType")],
                        'images'                => $this->uploadsJson(),
                        'categories'            => $this->categoriesJson(),
                    ] + $moduleData;

                return View::make('admin.page-content.modules.create', $viewData)
                    ->withInput(Input::all())
                    ->withErrors($toDataModule->getErrors());
            }
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         * @return \Illuminate\View\View
         * @throws \Exception
         */
        public function editModule($moduleRelationshipID)
        {
            $ModuleLocationRelationship = ModuleLocationRelationship::find($moduleRelationshipID);

            $mlrID          = $ModuleLocationRelationship->id;
            $moduleType     = $ModuleLocationRelationship->module_type;
            $moduleTypeName = str_replace("Hyfn\\Models\\Modules\\", "", $ModuleLocationRelationship->module_type);
            $moduleID       = $ModuleLocationRelationship->module_id;
            $locationID     = $ModuleLocationRelationship->location_id;
            $entityType     = $ModuleLocationRelationship->entity_type;
            $entityID       = $ModuleLocationRelationship->entity_id;

            if ($moduleTypeName == 'TextModule') {
                $formModule = TextModule::find($moduleID);

                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } elseif ($moduleTypeName == 'PdfListModule') {
                $formModule    = PdfListModule::find($moduleID);
                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $pdfImages = Upload::whereIn('category', [1])->get();
                foreach ($pdfImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }


                $chosenPDFs      = $formModule->files;
                $chosenPDFsArray = [];
                foreach ($chosenPDFs as $pdfContent) {
                    $chosenPDFsArray[] = $pdfContent->id;
                }
                $formModule->chosenPDFs = $chosenPDFsArray;


                $moduleData = [
                    'headingImages' => $headingImages,
                    'pdfImages'     => $pdfImages,
                ];
            } elseif ($moduleTypeName == 'SlideshowModule') {
                $formModule = SlideshowModule::find($moduleID);

                $mediaImages = Upload::whereIn('category', [4])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }

                $chosenMedia      = $formModule->slides;
                $chosenMediaArray = [];
                foreach ($chosenMedia as $mediaContent) {
                    $chosenMediaArray[] = $mediaContent->id;
                }
                $formModule->chosenMedia = $chosenMediaArray;

                $moduleData = [
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleTypeName == 'SingleImageModule') {
                $formModule = SingleImageModule::find($moduleID);

                $mediaImages = Upload::whereIn('category', [8])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }

                $chosenMedia      = $formModule->image->id;
                $chosenMediaArray = [$chosenMedia];
                $formModule->chosenMedia = $chosenMediaArray;

                $moduleData = [
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleTypeName == 'ImageColumnModule') {
                $formModule = ImageColumnModule::find($moduleID);
                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $mediaImages = Upload::whereIn('category', [7])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }

                $chosenMedia      = $formModule->images;
                $chosenMediaArray = [];
                foreach ($chosenMedia as $mediaContent) {
                    $chosenMediaArray[] = $mediaContent->id;
                }
                $formModule->chosenMedia = $chosenMediaArray;

                $moduleData = [
                    'headingImages' => $headingImages,
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleTypeName == 'SummaryModule') {
                $formModule = SummaryModule::find($moduleID);
                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $mediaImages = Upload::whereIn('category', [4])->get();
                foreach ($mediaImages as $pdfImage) {
                    $uploadCategory         = UploadCategory::find($pdfImage->category);
                    $pdfImage->categoryName = $uploadCategory->name;
                }

                $chosenMedia      = $formModule->image->id;
                $chosenMediaArray = [$chosenMedia];
                $formModule->chosenMedia = $chosenMediaArray;

                $moduleData = [
                    'headingImages' => $headingImages,
                    'mediaImages' => $mediaImages,
                ];
            } elseif ($moduleTypeName == 'SplitTextModule') {
                $formModule = SplitTextModule::find($moduleID);
                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } elseif ($moduleTypeName == 'VideoModule') {
                $formModule = VideoModule::find($moduleID);
                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                ];
            } elseif ($moduleTypeName == 'TextColumnModule') {
                $formModule = TextColumnModule::find($moduleID);
                $headingImages = Upload::where('category', '=', 6)->get();
                foreach ($headingImages as $headingImage) {
                    $uploadCategory             = UploadCategory::find($headingImage->category);
                    $headingImage->categoryName = $uploadCategory->name;
                }

                $initColDivID = 2;

                if (trim($formModule->column_3) != "") {
                    $initColDivID = 3;
                }
                if (trim($formModule->column_4) != "") {
                    $initColDivID = 4;
                }
                if (trim($formModule->column_5) != "") {
                    $initColDivID = 5;
                }
                if (trim($formModule->column_6) != "") {
                    $initColDivID = 6;
                }
                if (trim($formModule->column_7) != "") {
                    $initColDivID = 7;
                }
                if (trim($formModule->column_8) != "") {
                    $initColDivID = 8;
                }

                $moduleData = [
                    'headingImages' => $headingImages,
                    'initColDivID' => $initColDivID,
                ];
            } else {
                throw new \Exception("Invalid Module");
            }

            switch ($locationID) {
                case 2  :
                    View::share('activeSection', "Insights");
                    $addendum = "";
                    break;
                case 3  :
                case 4  :
                case 5  :
                    if ($entityType === 'Hyfn\Models\SiteProductSection') {
                        $SiteProduct = SiteProductSection::find($entityID);
                    } else {
                        $SiteProduct = SiteProduct::find($entityID);
                    }
                    $addendum = " " . $SiteProduct->name . " Section";
                    View::share('activeSection', "Products");
                    break;

                case 6 :
                case 7 :
                case 8 :
                    View::share('activeSection', "Custom Studios");
                    $addendum = "";
                    break;

                case 1 :
                case 9 :
                    if ($entityType === 'Hyfn\Models\CaseStudy') {
                        $CaseStudy = CaseStudy::find($entityID);
                        $addendum  = " " . $CaseStudy->headline . ".";
                    } else {
                        $addendum = "";
                    }
                    View::share('activeSection', "Case Study");
                    break;

                default :
                    $addendum = "";
            }

            if (Input::get("entityType") == 'Hyfn\Models\CaseStudy') {
                $entityRedirect = "gallery/" . Input::get("entityID") . "/add-modules";
            } else {
                $entityRedirect = $this->redirects[$entityType];
            }

            $viewData = [
                    'cmsName'               => $this->cmsName,
                    'copyrightName'         => $this->copyrightName,
                    'formModule'            => $formModule,
                    'locationID'            => $locationID,
                    'moduleType'            => $moduleTypeName,
                    'moduleTypeDisplayName' => $this->moduleDisplayNames[$moduleTypeName],
                    'entityType'            => $entityType,
                    'entityRedirect' => $entityRedirect,
                    'entityTypeName' => $this->entityTypes[$entityType . " - " . $locationID] . $addendum,
                    'entityID'              => $entityID,
                    'modulePartial'         => $this->partialTypes[$moduleTypeName],
                    'mlrID'                 => $mlrID,
                    'images'                => $this->uploadsJson(),
                    'categories'            => $this->categoriesJson(),
                ] + $moduleData;

            return View::make('admin.page-content.modules.edit', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         * @return $this
         * @throws \Exception
         */
        public function updateModule($moduleRelationshipID)
        {
            $ModuleLocationRelationship = ModuleLocationRelationship::find($moduleRelationshipID);

            $mlrID          = $ModuleLocationRelationship->id;
            $moduleType     = $ModuleLocationRelationship->module_type;
            $moduleTypeName = str_replace("Hyfn\\Models\\Modules\\", "", $ModuleLocationRelationship->module_type);
            $moduleID       = $ModuleLocationRelationship->module_id;
            $locationID     = $ModuleLocationRelationship->location_id;
            $entityType     = $ModuleLocationRelationship->entity_type;
            $entityID       = $ModuleLocationRelationship->entity_id;
            $isMediaAdded = true;

            $formModule = new $moduleType;
            $formModule = $formModule->find($moduleID);
            $formModule->fill(Input::all());

            if ($moduleTypeName == 'PdfListModule' || $moduleTypeName == 'SlideshowModule' || $moduleTypeName == 'ImageColumnModule') {
                $ids          = Input::get("upload_ids");
                $isMediaAdded = (isset($ids) && count($ids) >= 1 ? true : false);
            }

            if ($formModule->isValid('saving') && $isMediaAdded) {
                $formModule->save();

                if ($moduleTypeName == 'PdfListModule') {
                    $formModule->files()->sync(Input::get("upload_ids"));
                }

                if ($moduleTypeName == 'SlideshowModule') {
                    $formModule->slides()->sync(Input::get("upload_ids"));
                }

                if ($moduleTypeName == 'ImageColumnModule') {
                    $formModule->images()->sync(Input::get("upload_ids"));
                }

                $ModuleLocationRelationship->location_id = $locationID;
                $ModuleLocationRelationship->entity_id   = Input::get("entityID");
                $ModuleLocationRelationship->entity_type = Input::get("entityType");
                $ModuleLocationRelationship->module_id   = $formModule->id;
                $ModuleLocationRelationship->module_type = $moduleType;
                $ModuleLocationRelationship->save();

                $this->updateSearchIndexes();

                switch ($locationID) {
                    case 3 :
                    case 4 :
                    case 5 :
                        if ($entityType === 'Hyfn\Models\SiteProductSection') {
                            $SiteProduct = SiteProductSection::find($entityID);
                        } else {
                            $SiteProduct = SiteProduct::find($entityID);
                        }
                        $addendum = " " . $SiteProduct->name . " Section";
                        break;

                    case 9 :
                        $CaseStudy = CaseStudy::find($entityID);
                        $addendum  = " " . $CaseStudy->headline . "";
                        break;

                    default :
                        $addendum = "";
                }

                if ($entityType == 'Hyfn\Models\CaseStudy') {
                    $entityRedirect = "gallery/" . $entityID . "/add-modules";
                } else {
                    $entityRedirect = $this->redirects[$entityType];
                }

                switch ($locationID) {
                    case 3  :
                    case 4  :
                    case 5  :
                        $tab = $locationID;
                        break;

                    default :
                        $tab = 0;
                }

                return Redirect::to('admin/' . $entityRedirect)
                    ->with('success_message',
                        'Successfully updated a ' . $this->moduleDisplayNames[Input::get("moduleType")] . ' to the ' . $this->entityTypes[Input::get("entityType") . " - " . $locationID] . $addendum)
                    ->with('tab', $tab)
                    ->with('entityID', $entityID);
            } else {

                if (!$isMediaAdded) {
                    $formModule->getErrors()->add('upload_ids', "Please pick your media for this module.");
                }
                return Redirect::to('admin/page-content/modules/' . $mlrID . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($formModule->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $moduleRelationshipID
         *
         * @return Response
         */
        public function destroyModule($moduleRelationshipID)
        {
            $moduleRelationship = ModuleLocationRelationship::find($moduleRelationshipID);
            $moduleID           = $moduleRelationship->module_id;
            $moduleType         = $moduleRelationship->module_type;
            $module             = $moduleType::find($moduleID);
            $moduleTitle        = $module->title;
            $moduleLocationID   = $moduleRelationship->location_id;
            $moduleEntityID = $moduleRelationship->entity_id;

            $moduleLocations = [
                2 => 'admin/page-content/insight-pages',
                3 => 'admin/page-content/product-pages',
                4 => 'admin/page-content/product-pages',
                5 => 'admin/page-content/product-pages',
                6 => 'admin/page-content/studios-pages',
                7 => 'admin/page-content/studios-pages',
                8 => 'admin/page-content/studios-pages',
                9 => 'admin/gallery/' . $moduleEntityID . '/add-modules',
            ];

            switch ($moduleLocationID) {
                case 3  :
                case 4  :
                case 5  :
                    $tab = $moduleLocationID;
                    break;

                default :
                    $tab = 0;
            }

            if (ModuleLocationRelationship::destroy($moduleRelationshipID) && $module::destroy($moduleID)) {
                return Redirect::to($moduleLocations[$moduleLocationID])
                    ->with('success_message',
                        "The " . $this->moduleDisplayNames[str_replace("Hyfn\\Models\\Modules\\", "",
                            $moduleType)] . ": " . $moduleTitle . ", has been successfully deleted.")
                    ->with('tab', $tab)
                    ->with('entityID', $moduleEntityID);
            } else {
                return Redirect::to($moduleLocations[$moduleLocationID])
                    ->with('error_message',
                        "The " . $this->moduleDisplayNames[str_replace("Hyfn\\Models\\Modules\\", "",
                            $moduleType)] . ": " . $moduleTitle . ", was not deleted.");
            }
        }


        /**
         * Saves the order and live status of each module as well as updates any potentially used uploads to live as well
         *
         *
         * @return \Illuminate\Http\Response
         */
        public function saveModuleOrder()
        {
            $isLiveArray = (is_array(Input::get("isLive")) ? Input::get("isLive") : []);
            $locationID     =   Input::get("locationID");
            $rowsArray      =   explode("&", substr(str_replace("location-" . $locationID . "-module-relationship[]=", "", Input::get("order")), 1));
            $pageName       =   Input::get("pageName");
            $entityID       =   Input::get("entityID");
            $entityType     =   Input::get("entityType");
            $headerArray    =   [
                                    'Content-Type'  => 'application/json',
                                    'Cache-Control' => 'no-cache, must-revalidate',
                                ];
            $contentArray   =   [
                                    "status"  => 'success',
                                    "message" => "process complete.",
                                ];

            $i = 1;
            foreach ($rowsArray as $key => $moduleRelationshipID)
            {
                $ModuleRelationship        = ModuleLocationRelationship::find($moduleRelationshipID);
                $ModuleClassName           = $ModuleRelationship->module_type;
                $Module                    = $ModuleClassName::find($ModuleRelationship->module_id);
                $Module->is_live           = 0;
                $ModuleRelationship->order = $i;
                $this->setModuleUploadsLiveStatus($ModuleClassName, $Module, 0);

                $i++;

                if (in_array($ModuleRelationship->id, $isLiveArray))
                {
                    $this->setModuleUploadsLiveStatus($ModuleClassName, $Module, 1);
                    $Module->is_live = 1;
                }

                $ModuleRelationship->save();
                $Module->save();
            }

            switch ($locationID) {
                case 3  :
                case 4  :
                case 5  :
                    $tab = $locationID;
                    break;

                default :
                    $tab = 0;
            }

            Session::put('tab', $tab);
            Session::put('entityID', $entityID);
            Session::put('success_message', "Successfully saved new Module Order.");

            $this->updateSearchIndexes();

            return Response::make($contentArray, 200, $headerArray);
        }


        /**
         * Sets all the associated uploads to the specified live status for
         * specific modules
         *
         * @param string    $ModuleClassName
         * @param object    $Module
         * @param int       $liveStatus
         */
        public function setModuleUploadsLiveStatus($ModuleClassName, $Module, $liveStatus=0)
        {
            // At this point, we need to check for the type of module and
            if($ModuleClassName == "Hyfn\\Models\\Modules\\PdfListModule")
            {
                $uploadArray    =   $Module->files;
            }

            if($ModuleClassName == "Hyfn\\Models\\Modules\\ImageColumnModule")
            {
                $uploadArray    =   $Module->images;
            }

            if($ModuleClassName == "Hyfn\\Models\\Modules\\SingleImageModule")
            {
                $uploadArray    =   [$Module->upload_id];
            }

            if($ModuleClassName == "Hyfn\\Models\\Modules\\SlideshowModule")
            {
                $uploadArray    =   $Module->slides;
            }

            if($ModuleClassName == "Hyfn\\Models\\Modules\\SummaryModule")
            {
                $uploadArray    =   [$Module->upload_id];
            }

            // check if it has uploads from Uploads
            if(isset($uploadArray))
            {
                foreach ($uploadArray as $uploadID)
                {
                    if(is_object($uploadID) || (is_int($uploadID) && $uploadID >= 1))
                    {
                        $Upload             =   (is_object($uploadID) ? $uploadID : Upload::find($uploadID) );
                        $Upload->is_live    =   $liveStatus;
                        $Upload->save();
                    }
                }
            }

            $this->updateSearchIndexes();
        }

        private function uploadsJson()
        {
            return Upload::orderBy('name')->get()->map(function($image) {
                return [
                    'title' => $image->name,
                    'value' => $image->download_folder . $image->download_file,
                    'category' => $image->category,
                ];
            });
        }

        private function categoriesJson()
        {
            return UploadCategory::select('id', 'name')
                ->whereNotIn('name', UploadCategory::$wysiwygBlacklist)
                ->get()
                ->map(function($cat) { 
                        return [ 'id' => $cat->id, 'name' => $cat->name ]; 
                });
        }

    }
