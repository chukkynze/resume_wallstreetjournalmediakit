<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       HomePageController
     *
     * filename:    HomePageController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/13/14 11:07 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin\PageContent;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Illuminate\Support\MessageBag;
    use Redirect;
    use Response;
    use Session;
    use View;

    use Hyfn\Models\PageContent\HomePage;
    use Hyfn\Models\PageContent\HomePageSlider;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;


    class HomePageController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Page Content");
        }


        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allHomePages = HomePage::orderBy('created_at', 'desc')->whereNull('deleted_at')->get();

            foreach ($allHomePages as $homePage) {
                $HomePageSliders = HomePageSlider::where("home_page_id", "=",
                    $homePage->id)->whereNull('deleted_at')->get();

                $getSliders = [];
                foreach ($HomePageSliders as $slider) {
                    $sliderImageInfo = Upload::find($slider->upload_id);
                    $getSliders[]    = [
                        'download_folder' => $sliderImageInfo->download_folder,
                        'download_file'   => $sliderImageInfo->download_file,
                        'main_text'       => $slider->main_text,
                        'sub_text' => $slider->sub_text,
                        'source_text'     => $slider->source_text,
                    ];
                }

                $homePage->sliders = $getSliders;
            }

            $viewData = [
                'cmsName'                 => $this->cmsName,
                'copyrightName'           => $this->copyrightName,
                'allowHomePageContentMgt' => ($this->loggedInUser->can("manage_page_content") ? 1 : 0),
                'activeSection'           => "Page Content",
                'allHomePages'            => $allHomePages,
            ];


            return View::make('admin.page-content.home-page.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $homePage      = HomePage::find(1);
            $chosenSliders = HomePageSlider::where('home_page_id', "=", $homePage->id)->get();

            $chosenSliderSet     = [];
            $chosenSlidersArray  = [];
            $homePageSliderOrder = "";
            foreach ($chosenSliders as $chosenSlider) {
                $slider               = Upload::find($chosenSlider->upload_id);
                $chosenSliderSet[]    = $chosenSlider->upload_id;
                $chosenSlidersArray[] = [
                    'id'   => $chosenSlider->upload_id,
                    'name' => $slider->name,
                ];
                $homePageSliderOrder .= "&slider[]=" . $chosenSlider->upload_id;
            }
            $homePage->chosenSliders = $chosenSliderSet;

            $allSliderIDs='';
            $sliders = Upload::where('category', '=', 2)->get();
            foreach ($sliders as $slider)
            {
                $allSliderIDs           .=   $slider->id . "-";
                $uploadCategory       = UploadCategory::find($slider->category);
                $slider->categoryName = $uploadCategory->name;

                $HomePageSliderCount = HomePageSlider::where('home_page_id', "=", $homePage->id)->where('upload_id',
                    "=", $slider->id)->count();
                if ($HomePageSliderCount > 0) {
                    $HomePageSlider      = HomePageSlider::where('home_page_id', "=", $homePage->id)->where('upload_id',
                        "=", $slider->id)->first();
                    $slider->main_text   = $HomePageSlider->main_text;
                    $slider->sub_text    = $HomePageSlider->sub_text;
                    $slider->source_text = $HomePageSlider->source_text;

                }
            }
            $allSliderIDs = substr($allSliderIDs,0,-1);

            $viewData = [
                'cmsName'            => $this->cmsName,
                'copyrightName'      => $this->copyrightName,
                'homePage'           => $homePage,
                'allSliderIDs'       => $allSliderIDs,
                'sliders'            => $sliders,
                'initSliderOrder'    => $homePageSliderOrder,
                'chosenSlidersArray' => $chosenSlidersArray,
            ];

            return View::make('admin.page-content.home-page.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $HomePageModel                 = HomePage::find(1);
            $HomePageModel->home_page_text = "text";
            $chosenSliders                 = (count(Input::get('home_page_slider_upload_id')) >= 1
                ? [Input::get('home_page_slider_upload_id')]
                : []);
            $chosenSlidersOrder            = explode("&slider[]=", substr(Input::get('slider_order'), 10));


            if ($HomePageModel->isModelValid('saving')) {
                $isHomePageModelValid = true;
                $isSliderValid        = true;


                // Add Sliders to Holding Array
                foreach ($chosenSlidersOrder as $sliderID) {
                    $HomePageSliderModel               = new HomePageSlider();
                    $HomePageSliderModel->home_page_id = $HomePageModel->id;
                    $HomePageSliderModel->upload_id    = $sliderID;
                    $HomePageSliderModel->main_text    = Input::get('main_text_' . $sliderID);
                    $HomePageSliderModel->sub_text     = Input::get('sub_text_' . $sliderID);
                    $HomePageSliderModel->source_text  = Input::get('source_text_' . $sliderID);

                    if ($HomePageSliderModel->isModelValid()) {
                        $sliderHoldingArray[] = $HomePageSliderModel;
                    } else {
                        $rawErrors     = $HomePageSliderModel->getErrors();
                        $rawErrorArray = $HomePageSliderModel->getErrors()->toArray();
                        foreach ($rawErrorArray as $key => $rawMsg) {
                            foreach ($rawMsg as $msg) {
                                $rawErrors->add($key . '_' . $sliderID, $msg);
                                //unset($rawErrors[$key]);
                            }
                        }

                        return Redirect::to('admin/page-content/home-pages/create')
                            ->withInput(Input::all())
                            ->withErrors($rawErrors);
                    }
                }

                $HomePageModel->save();
                HomePageSlider::where('home_page_id', '=', 1)->delete();
                foreach ($sliderHoldingArray as $sliderModel) {
                    $sliderModel->save();
                }
                return Redirect::to('admin/page-content/home-pages')
                    ->with('success_message', trans('admin.page-content.home-page.update_success'));
            } else {
                return Redirect::to('admin/page-content/home-pages/create')
                    ->withInput(Input::all())
                    ->withErrors($HomePageModel->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resourceCount = HomePage::all()->count();

            if ($resourceCount > 1) {
                $resourceModel = HomePage::find($id);
                $resourceDate  = date("F jS Y", strtotime($resourceModel->created_at));

                if ($resourceModel->id >= 1) {
                    $resourceModel::destroy($id);
                    $affectedRows = HomePageSlider::where('home_page_id', '=', $id)->delete();

                    return Redirect::to('admin/page-content/home-pages')
                        ->with('success_message',
                            "The Home Page Content version, created at: " . $resourceDate . ", has been successfully deleted.");
                } else {
                    return Redirect::to('admin/page-content/home-pages')
                        ->with('error_message', trans('admin.page-content.home-page.delete_failure'));
                }
            } else {
                return Redirect::to('admin/page-content/home-pages')
                    ->with('error_message', trans('admin.page-content.home-page.delete_min'));
            }
        }


        public function savePreviewData()
        {
            Session::put('homePagePreviewData', Input::get('formData'));

            return json_encode(['success'=>1]);

        }


        public function preview()
        {
            $sliderInfo =   (array) json_decode(Session::pull('homePagePreviewData'), JSON_FORCE_OBJECT);

            if(count($sliderInfo) >= 1)
            {
                $getSliders =   [];
                foreach ($sliderInfo as $slide)
                {
                    $getSliders[]   =   [
                                            'download_folder'   => $slide['download_folder'],
                                            'download_file'     => $slide['download_file'],
                                            'main_text'         => $slide['main_text'],
                                            'sub_text'          => $slide['sub_text'],
                                            'source_text'       => $slide['source_text'],
                                        ];
                }

                $menuProducts   =   [
                    0 => [
                        'name'        => "Products",
                        'link'        => "/#",
                        'listClasses' => "primary-menu-link primary-parent-link",
                        'linkClasses' => "",
                        'subLinks'    => "",
                    ],
                    1 => [
                        'name'        => "WSJ. Insights",
                        'link'        => "#",
                        'listClasses' => "primary-menu-link",
                        'linkClasses' => "",
                        'subLinks'    => "",
                    ],
                    2 => [
                        'name'        => "WSJ. Custom Studios",
                        'link'        => "#",
                        'listClasses' => "primary-menu-link",
                        'linkClasses' => "",
                        'subLinks'    => "",
                    ],
                    3 => [
                        'name'        => "Rates &amp; Specs",
                        'link'        => "#",
                        'listClasses' => "primary-menu-link",
                        'linkClasses' => "",
                        'subLinks'    => "",
                    ]
                    ,
                    // 4 => [
                    //     'name'        => "Case Studies",
                    //     'link'        => "case-studies",
                    //     'listClasses' => "primary-menu-link mobile-hidden",
                    //     'linkClasses' => "",
                    //     'subLinks'    => "",
                    // ],
                ];

                $viewData = [
                    'menuProducts'          =>  $menuProducts,
                    'sourceText'            =>  "",
                    'currentPath'           =>  'index',
                    'marqueeLandingTitle'   =>  'Welcome to the WSJ Media Kit',
                    'sliders'               =>  $getSliders,
                ];

                return View::make('admin.page-content.home-page.preview', $viewData);
            }
            else
            {

            }


        }

    }