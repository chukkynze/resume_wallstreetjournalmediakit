<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       HomePageSliderController
     *
     * filename:    HomePageSliderController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/13/14 11:07 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin\PageContent;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Illuminate\Support\MessageBag;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\PageContent\HomePage;
    use Hyfn\Models\PageContent\HomePageSlider;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;


    class HomePageSliderController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Page Content");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {

        }


        /**
         * Show the form for creating a new resource.
         *
         *
         * @param $homePageID
         * @param $numberOfSliders
         *
         * @return \Illuminate\View\View
         */
        public function create($homePageID, $numberOfSliders)
        {
            $bannerImages = Upload::where('category', '=', 2)->get();

            foreach ($bannerImages as $bannerImage) {
                $uploadCategory            = UploadCategory::find($bannerImage->category);
                $bannerImage->categoryName = $uploadCategory->name;
            }

            $allSliders = [];
            for ($s = 1; $s <= $numberOfSliders; $s++) {
                $allSliders[$s]['homePageSlider' . $s] = new HomePageSlider();
                $allSliders[$s]['main_text']           = [
                    'labelID'          => 'main_text_' . $s,
                    'labelDisplay'     => 'Main Text',
                    'inputName'        => 'main_text_' . $s,
                    'inputID'          => 'main_text_' . $s,
                    'inputPlaceholder' => 'Main Text',
                    'errorID'          => 'main_text_' . $s,
                ];
                $allSliders[$s]['sub_text1']           = [
                    'labelID'          => '',
                    'labelDisplay'     => '',
                    'inputName'        => 'sub_text1_' . $s,
                    'inputID'          => 'sub_text1_' . $s,
                    'inputPlaceholder' => 'Sub Text 1',
                    'errorID'          => 'sub_text1_' . $s,
                ];
                $allSliders[$s]['sub_text2']           = [
                    'labelID'          => '',
                    'labelDisplay'     => '',
                    'inputName'        => 'sub_text2_' . $s,
                    'inputID'          => 'sub_text2_' . $s,
                    'inputPlaceholder' => 'Sub Text 2',
                    'errorID'          => 'sub_text2_' . $s,
                ];
                $allSliders[$s]['source_text']         = [
                    'labelID'          => '',
                    'labelDisplay'     => '',
                    'inputName'        => 'source_text_' . $s,
                    'inputID'          => 'source_text_' . $s,
                    'inputPlaceholder' => 'Source Text',
                    'errorID'          => 'source_text_' . $s,
                ];
                $allSliders[$s]['bannerImages']        = $bannerImages;
            }

            $viewData = [
                'cmsName'        => $this->cmsName,
                'copyrightName'  => $this->copyrightName,
                'homePage'       => HomePage::find($homePageID),
                'homePageSlider' => new HomePageSlider(),
                'sliderNumbers'  => $numberOfSliders,
                'bannerImages'   => $bannerImages,
                'allSliders'     => $allSliders,
            ];

            return View::make('admin.page-content.home-page-slider.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $numberOfSliders  = Input::get('number_of_sliders');
            $allErrors        = new MessageBag();
            $validatedSliders = [];
            for ($s = 1; $s <= $numberOfSliders; $s++) {
                $HomePageSliderModel               = new HomePageSlider();
                $HomePageSliderModel->home_page_id = (int)Input::get('home_page_id_' . $s);
                $HomePageSliderModel->upload_id    = (int)Input::get('upload_id_' . $s);
                $HomePageSliderModel->main_text    = Input::get('main_text_' . $s);
                $HomePageSliderModel->sub_text1    = Input::get('sub_text1_' . $s);
                $HomePageSliderModel->sub_text2    = Input::get('sub_text2_' . $s);
                $HomePageSliderModel->source_text  = Input::get('source_text_' . $s);


                if ($HomePageSliderModel->isModelValid('saving')) {
                    $validatedSliders[] = [
                        'home_page_id' => (int)Input::get('home_page_id_' . $s),
                        'upload_id'    => (int)Input::get('upload_id_' . $s),
                        'main_text'    => Input::get('main_text_' . $s),
                        'sub_text1'    => Input::get('sub_text1_' . $s),
                        'sub_text2'    => Input::get('sub_text2_' . $s),
                        'source_text'  => Input::get('source_text_' . $s),
                    ];
                } else {
                    $rawErrors        = $HomePageSliderModel->getErrors();
                    $rawErrorMessages = $rawErrors->getMessages();

                    foreach ($rawErrorMessages as $keyName => $errors) {
                        foreach ($errors as $err) {
                            $allErrors->add($keyName . "_" . $s, $err);
                        }
                    }
                }
            }

            if (count($validatedSliders) == $numberOfSliders) {
                foreach ($validatedSliders as $slider) {
                    $HomePageSliderModel               = new HomePageSlider();
                    $HomePageSliderModel->home_page_id = $slider['home_page_id'];
                    $HomePageSliderModel->upload_id    = $slider['upload_id'];
                    $HomePageSliderModel->main_text    = $slider['main_text'];
                    $HomePageSliderModel->sub_text1    = $slider['sub_text1'];
                    $HomePageSliderModel->sub_text2    = $slider['sub_text2'];
                    $HomePageSliderModel->source_text  = $slider['source_text'];
                    $HomePageSliderModel->save();
                }

                return Redirect::to('admin/page-content/home-pages')
                    ->with('success_message', trans('admin.page-content.home-page.create_success'));
            } else {
                return Redirect::to('admin/page-content/home-page-sliders/create/' . Input::get('home_page_id_1') . "/" . $numberOfSliders)
                    ->withInput(Input::all())
                    ->withErrors($allErrors);
            }
        }

    }