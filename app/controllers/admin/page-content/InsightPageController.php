<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       InsightController
     *
     * filename:    InsightController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:07 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin\PageContent;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\PageContent\InsightPage;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;


    class InsightPageController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Insights");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $insightPage                  = InsightPage::find(1);
            $bannerImage                  = Upload::find($insightPage->banner_img_id);
            $insightPage->bannerImgName   = $bannerImage->name;
            $insightPage->bannerImgFolder = $bannerImage->download_folder;
            $insightPage->bannerImgFile   = $bannerImage->download_file;

            $allModules = $this->getLocationModules(2, "Hyfn\\Models\\PageContent\\InsightPage", $insightPage->id, 1);
            $initialModuleOrder = "";
            foreach ($allModules as $module) {
                $moduleObject = $module['moduleObject'];
                $initialModuleOrder .= "&location-2-module-relationship[]=" . $moduleObject->relationshipOrder;
            }

            $viewData = [
                'cmsName'            => $this->cmsName,
                'copyrightName'      => $this->copyrightName,
                'allowInsightsMgt'   => ($this->loggedInUser->can("manage_insights") ? 1 : 0),
                'activeSection' => "Insights",
                'insights'           => $insightPage,
                'initialModuleOrder' => $initialModuleOrder,
                'allModules'         => $allModules,
                'locationID'  => 2,
                'moduleTypes' => $this->getModuleTypesForSelect(),
                'entityType'  => 'Hyfn\\Models\\PageContent\\InsightPage',
                'entityID'    => 1,
            ];


            return View::make('admin.page-content.insights-page.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $insightModel = InsightPage::orderBy("created_at", "DESC")->first();
            $bannerImages = Upload::where('category', '=', 2)->get();

            foreach ($bannerImages as $bannerImage) {
                $uploadCategory            = UploadCategory::find($bannerImage->category);
                $bannerImage->categoryName = $uploadCategory->name;
            }

            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'insight'       => $insightModel,
                'bannerImages'  => $bannerImages,
            ];

            return View::make('admin.page-content.insights-page.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $insightModel = InsightPage::find(1);
            $insightModel->banner_img_id = Input::get('banner_img_id');
            $insightModel->title         = Input::get('title');
            $insightModel->source_text   = Input::get('source_text');

            if ($insightModel->isModelValid('saving')) {
                $insightModel->save();

                // Get all existing associated modules and add them to this new page as new modules

                return Redirect::to('admin/page-content/insight-pages/')
                    ->with('success_message', trans('admin.insights.update_success'));
            } else {
                return Redirect::to('admin/page-content/insight-pages/create')
                    ->withInput(Input::all())
                    ->withErrors($insightModel->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resourceCount = InsightPage::all()->count();

            if ($resourceCount > 1) {
                $resourceModel = InsightPage::find($id);
                $resourceDate  = date("F jS Y", strtotime($resourceModel->created_at));
                $resourceModel::destroy($id);

                return Redirect::to('admin/page-content/insights')
                    ->with('success_message',
                        "The Insights Page Content version, created at: " . $resourceDate . ", has been successfully deleted.");
            } else {
                return Redirect::to('admin/page-content/insights/')
                    ->with('error_message', trans('admin.insights.delete_min'));
            }
        }

    }