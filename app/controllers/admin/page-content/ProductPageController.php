<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       ProductPageController
     *
     * filename:    ProductPageController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/13/14 11:07 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin\PageContent;

    use Hyfn\Controllers\AdminController;

    use Hyfn\Models\ModuleLocationRelationship;
    use Input;
    use Illuminate\Support\MessageBag;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\PageContent\ProductPage;
    use Hyfn\Models\Upload;
    use Hyfn\Models\SiteProduct;
    use Hyfn\Models\SiteProductSection;
    use Hyfn\Models\UploadCategory;


    class ProductPageController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Products");
        }


        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $ProductPage                   = ProductPage::find(1);
            $modLocIndex                   = [3, 4, 5];
            $sliderImageInfo               = Upload::find($ProductPage->banner_image_upload_id);
            $ProductPage->background_image = $sliderImageInfo->download_folder . $sliderImageInfo->download_file;

            $landingPageProducts = [];
            $detailsPageProducts = [];

            $SiteProducts = SiteProduct::orderBy('section_order', 'asc')->get();

            $loop = 0;
            foreach ($SiteProducts as $product) {
                $allLandingModules         = $this->getLocationModules(3, "Hyfn\\Models\\SiteProduct", $product->id, 1);
                $initialLandingModuleOrder = "";

                $liveLandingModuleExists = false;
                foreach ($allLandingModules as $module) {
                    $moduleObject = $module['moduleObject'];
                    $initialLandingModuleOrder .= "&location-" . $moduleObject->relationshipLocationID . "-module-relationship[]=" . $moduleObject->relationshipID;

                    if ($moduleObject->is_live == 1) {
                        $liveLandingModuleExists = true;
                    }
                }

                // $allMo     =

                $doMainSectionLiveModuleExist = false;
                $doSubsectionLiveModuleExist  = false;


                $allDetailsModules = $this->getLocationModules(4, "Hyfn\\Models\\SiteProduct", $product->id, 1);
                $initialDetailsModuleOrder = "";

                foreach ($allDetailsModules as $module) {
                    $moduleObject = $module['moduleObject'];
                    $initialDetailsModuleOrder .= "&location-" . $moduleObject->relationshipLocationID . "-module-relationship[]=" . $moduleObject->relationshipID;

                    if (!$doMainSectionLiveModuleExist && $moduleObject->is_live == 1) {
                        $doMainSectionLiveModuleExist = true;
                    }
                }

                $allSiteProductSections = SiteProductSection::where("site_product_id", "=",
                    $product->id)->orderBy("order", "ASC")->get();
                $childrenEntityIDs      = [];
                foreach ($allSiteProductSections as $section) {
                    $childrenEntityIDs[] = $section->id;

                    $allModulesForThisSection = ModuleLocationRelationship::where('entity_id', '=',
                        $section->id)->where('entity_type', '=', 'Hyfn\\Models\\SiteProductSection')->get();

                    foreach ($allModulesForThisSection as $moduleData) {
                        $moduleClassName = $moduleData->module_type;
                        $moduleID        = $moduleData->module_id;
                        $Module          = $moduleClassName::find($moduleID);

                        if ($Module->is_live == 1) {
                            $doSubsectionLiveModuleExist = true;
                            break;
                        }
                    }

                }


                $landingPageProducts[] = [
                    'name'                       => $product->name,
                    'name_slug'                  => $product->name_slug,
                    'initialModuleOrder'         => $initialLandingModuleOrder,
                    'allModules'                 => $allLandingModules,
                    'locationID'                 => 3,
                    'moduleTypes'                => $this->getModuleTypesForSelect(),
                    'entityType'                 => 'Hyfn\\Models\\SiteProduct',
                    'entityID'                   => $product->id,
                    'moduleLocationName'         => $product->name . " Section",
                    'subsectionExist'            => (count($allSiteProductSections) >= 1 ? 1 : 0),
                    'subsectionLiveModuleExist'  => ($doSubsectionLiveModuleExist ? 1 : 0),
                    'mainSectionLiveModuleExist' => ($doMainSectionLiveModuleExist ? 1 : 0),
                ];


                $detailsPageProducts[$loop][] = [
                    'name'               => $product->name,
                    'name_slug'             => $product->name_slug,
                    'initialModuleOrder' => $initialDetailsModuleOrder,
                    'allModules'         => $allDetailsModules,
                    'locationID'         => 4,
                    'moduleTypes'        => $this->getModuleTypesForSelect(),
                    'entityType'         => 'Hyfn\\Models\\SiteProduct',
                    'entityID'           => $product->id,
                    'moduleLocationName' => $product->name . " Main Section",
                    'parentEntityID'     => 0,
                    'childrenEntityIDs'  => $childrenEntityIDs,
                    'parentLiveModuleExist' => ($liveLandingModuleExists ? 1 : 0),
                ];


                foreach ($allSiteProductSections as $siteProductSection) {
                    $allSubSectionModules = $this->getLocationModules(5, "Hyfn\\Models\\SiteProductSection",
                        $siteProductSection->id, 1);
                    $initialSubSectionModuleOrder = "";

                    foreach ($allSubSectionModules as $module) {
                        $moduleObject = $module['moduleObject'];
                        $initialSubSectionModuleOrder .= "&location-" . $moduleObject->relationshipLocationID . "-module-relationship[]=" . $moduleObject->relationshipID;
                    }

                    $detailsPageProducts[$loop][] = [
                        'name'               => $siteProductSection->name,
                        'name_slug'          => $siteProductSection->name_slug,
                        'initialModuleOrder' => $initialSubSectionModuleOrder,
                        'allModules'         => $allSubSectionModules,
                        'locationID'         => 5,
                        'moduleTypes'        => $this->getModuleTypesForSelect(),
                        'entityType'         => 'Hyfn\\Models\\SiteProductSection',
                        'entityID'           => $siteProductSection->id,
                        'moduleLocationName' => $siteProductSection->name . " Subsection",
                        'parentEntityID'     => $siteProductSection->site_product_id,
                        'parentLiveModuleExist' => ($liveLandingModuleExists ? 1 : 0),
                    ];
                }


                $loop++;
            }


            $viewData = [
                'cmsName'             => $this->cmsName,
                'copyrightName'       => $this->copyrightName,
                'allowPageContentMgt' => ($this->loggedInUser->can("manage_page_content") ? 1 : 0),
                'activeSection'       => "Products",
                'ProductPage'         => $ProductPage,
                'landingPageProducts' => $landingPageProducts,
                'detailsPageProducts' => $detailsPageProducts,
            ];


            return View::make('admin.page-content.product-page.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $productPage = ProductPage::orderBy("created_at", "DESC")->first();

            $bannerImages = Upload::where('category', '=', 2)->get();
            foreach ($bannerImages as $bannerImage) {
                $uploadCategory            = UploadCategory::find($bannerImage->category);
                $bannerImage->categoryName = $uploadCategory->name;
            }

            $sectionOrder = json_decode($productPage->section_order, true);

            $productSection     = [];
            foreach ($sectionOrder as $section) {
                $siteProd         = SiteProduct::find($section);
                $productSection[] = [
                    'id'            => strtolower($siteProd->id),
                    'lowerCaseName' => strtolower($siteProd->name),
                    'camelCaseName' => ucwords($siteProd->name),
                ];
            }
            $productPage->productSections = $productSection;

            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'siteProduct'     => $productPage,
                'productPage'   => $productPage,
                'bannerImages'  => $bannerImages,
                'productSections' => $productSection,
                'sectionOrder'    => json_decode($productPage->section_order, true),
            ];

            return View::make('admin.page-content.product-page.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $ProductPageModel = ProductPage::find(1);
            $ProductPageModel->heading                = Input::get('heading');
            $ProductPageModel->banner_image_enabled   = (int)Input::get('banner_image_enabled');
            $ProductPageModel->banner_image_upload_id = Input::get('banner_image_upload_id');

            $sectionOrder = array_reverse(explode("&product-section-module[]=", Input::get("section_order")), true);
            array_pop($sectionOrder);
            $ProductPageModel->section_order = json_encode(array_reverse($sectionOrder));
            $moduleOrder                     = array_reverse(explode("&product-landing-module[]=",
                    Input::get("module_order")), true);
            array_pop($moduleOrder);
            $ProductPageModel->module_order = json_encode(array_reverse($moduleOrder));

            $ProductPageModel->source_text_enabled = (int)Input::get('source_text_enabled') == 1 ? 1 : 0;
            $ProductPageModel->source_text         = Input::get('source_text') ?: "";

            if ($ProductPageModel->isModelValid('saving')) {
                $ProductPageModel->save();

                return Redirect::to('admin/page-content/product-pages')
                    ->with('success_message', trans('admin.page-content.product-page.update_success'));
            } else {
                return Redirect::to('admin/page-content/product-pages/create')
                    ->withInput(Input::all())
                    ->withErrors($ProductPageModel->getErrors());
            }
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $productPage        = ProductPage::find($id);
            $bannerImages       = Upload::where('category', '=', 2)->get();
            $productSectionsRaw = SiteProduct::where("on_product_landing", "=", 1)->get();

            foreach ($bannerImages as $bannerImage) {
                $uploadCategory            = UploadCategory::find($bannerImage->category);
                $bannerImage->categoryName = $uploadCategory->name;
            }

            $productSection = [];
            foreach ($productSectionsRaw as $section) {
                $productSection[$section->id] = [
                    'id'            => strtolower($section->id),
                    'lowerCaseName' => strtolower($section->name),
                    'camelCaseName' => ucwords($section->name),
                ];
            }
            $productPage->productSections = $productSection;

            $viewData = [
                'cmsName'         => $this->cmsName,
                'copyrightName'   => $this->copyrightName,
                'siteProduct'     => ProductPage::find($id),
                'productPage'     => $productPage,
                'bannerImages'    => $bannerImages,
                'productSections' => $productSection,
                'sectionOrder'    => json_decode($productPage->section_order, true),
            ];

            return View::make('admin.page-content.product-page.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $ProductPageModel = ProductPage::find(1);
            $ProductPageModel->heading                = Input::get('heading');
            $ProductPageModel->banner_image_enabled   = (int)Input::get('banner_image_enabled');
            $ProductPageModel->banner_image_upload_id = Input::get('banner_image_upload_id');

            $sectionOrder = array_reverse(explode("&product-section-module[]=", Input::get("section_order")), true);
            array_pop($sectionOrder);
            $ProductPageModel->section_order = json_encode(array_reverse($sectionOrder));
            $moduleOrder                     = array_reverse(explode("&product-landing-module[]=",
                    Input::get("module_order")), true);
            array_pop($moduleOrder);
            $ProductPageModel->module_order = json_encode(array_reverse($sectionOrder));

            $ProductPageModel->source_text_enabled = (int)Input::get('source_text_enabled') ?: 0;
            $ProductPageModel->source_text         = Input::get('source_text') ?: "";

            if ($ProductPageModel->isModelValid('updating')) {
                $ProductPageModel->save();

                return Redirect::to('admin/page-content/product-pages')
                    ->with('success_message', trans('admin.page-content.product-page.create_success'));
            } else {
                return Redirect::to('admin/page-content/product-pages/' . 1 . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($ProductPageModel->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resourceCount = ProductPage::all()->count();

            if ($resourceCount > 1) {
                $resourceModel = ProductPage::find($id);
                $resourceDate  = date("F jS Y", strtotime($resourceModel->created_at));

                if ($resourceModel->id >= 1) {
                    $resourceModel::destroy($id);

                    return Redirect::to('admin/page-content/product-pages')
                        ->with('success_message',
                            "The Product Page Content version, created at: " . $resourceDate . ", has been successfully deleted.");
                } else {
                    return Redirect::to('admin/page-content/product-pages')
                        ->with('error_message', trans('admin.page-content.product-page.delete_failure'));
                }
            } else {
                return Redirect::to('admin/page-content/product-pages')
                    ->with('error_message', trans('admin.page-content.product-page.delete_min'));
            }
        }

    }