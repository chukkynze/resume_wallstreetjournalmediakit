<?php
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Hyfn\Models\CaseStudyFilterItem;
    use Hyfn\Models\CaseStudyFilter;
    use Hyfn\Models\Client;
    use Hyfn\Models\SiteProduct;
    use Input;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\CaseStudy;
    use Hyfn\Models\CaseStudyPlatformRelationship;
    use Hyfn\Models\CaseStudySiteProductRelationship;
    use Hyfn\Models\CaseStudyCategoryRelationship;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;

    class CaseStudyFilterController extends AdminController
    {


        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Case Study");

        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allCaseStudyFilters = CaseStudyFilter::orderBy('order', 'asc')->get();

            $initIndexOrder = '';
            foreach ($allCaseStudyFilters as $filter) {
                $initIndexOrder .= "&index-row[]=" . $filter->id;
            }


            $viewData = [
                'cmsName'             => $this->cmsName,
                'copyrightName'       => $this->copyrightName,
                'allowCaseStudyMgt'   => ($this->loggedInUser->can("manage_case_studies") ? 1 : 0),
                'allCaseStudyFilters' => $allCaseStudyFilters,
                'initIndexOrder' => $initIndexOrder,
            ];

            return View::make('admin.case_study_filters.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {/*
            $caseStudy = new CaseStudy();

            $caseStudy->chosenPlatforms  = [];
            $caseStudy->chosenProducts   = [];
            $caseStudy->chosenCategories = [];

            $clients       = Client::all();
            $siteProducts  = SiteProduct::where('on_case_studies', '=', 1)->get();
            $platforms     = CaseStudyPlatform::all();
            $categories    = CaseStudyCategory::all();
            $listingImages = Upload::where('category', '=', 6)->get();

            $clientList = [0 => 'Choose A Client'];
            foreach ($clients as $client) {
                $clientList[$client->id] = $client->name;
            }

            foreach ($siteProducts as $siteProduct) {

            }

            foreach ($platforms as $platform) {

            }

            foreach ($categories as $category) {

            }

            foreach ($listingImages as $listingImage) {
                $uploadCategory             = UploadCategory::find($listingImage->category);
                $listingImage->categoryName = $uploadCategory->name;
            }


            $viewData = [
                'cmsName'          => $this->cmsName,
                'copyrightName'    => $this->copyrightName,
                'caseStudy'        => $caseStudy,
                'clients'          => $clients,
                'clientList'       => $clientList,
                'siteProducts'     => $siteProducts,
                'platforms'        => $platforms,
                'categories'       => $categories,
                'listingImages'    => $listingImages,
                'moduleOrderArray' => [],
            ];

            return View::make('admin.case_studies.create', $viewData);
        */
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {/*
            // Get form data, create new case study
            $caseStudy = new CaseStudy();

            $caseStudy->main_heading          = Input::get('main_heading');
            $caseStudy->main_description      = Input::get('main_description');
            $caseStudy->listing_pic_upload_id = Input::get('listing_pic_upload_id');
            $caseStudy->client_id             = Input::get('client_id');

            $chosenPlatforms    = Input::get('platform_id');
            $chosenSiteProducts = Input::get('site_product_id');
            $chosenCategories   = Input::get('category_id');

            $moduleOrder = array_reverse(explode("&case-study-module[]=", Input::get("module_order")), true);
            array_pop($moduleOrder);
            $caseStudy->module_order = json_encode(array_reverse($moduleOrder));

            if ($caseStudy->isModelValid()) {
                $caseStudy->save();

                // Add Platforms
                foreach ($chosenPlatforms as $platformID) {
                    $CaseStudyPlatformRelationship                = new CaseStudyPlatformRelationship();
                    $CaseStudyPlatformRelationship->case_study_id = $caseStudy->id;
                    $CaseStudyPlatformRelationship->platform_id   = $platformID;
                    $CaseStudyPlatformRelationship->save();
                }

                // Add SiteProducts
                foreach ($chosenSiteProducts as $siteProductID) {
                    $CaseStudySiteProductRelationship                = new CaseStudySiteProductRelationship();
                    $CaseStudySiteProductRelationship->case_study_id = $caseStudy->id;
                    $CaseStudySiteProductRelationship->product_id    = $siteProductID;
                    $CaseStudySiteProductRelationship->save();
                }

                // Add Categories
                foreach ($chosenCategories as $categoryID) {
                    $CaseStudyCategoryRelationship                = new CaseStudyCategoryRelationship();
                    $CaseStudyCategoryRelationship->case_study_id = $caseStudy->id;
                    $CaseStudyCategoryRelationship->category_id   = $categoryID;
                    $CaseStudyCategoryRelationship->save();
                }

                return Redirect::to('admin/case-studies')
                    ->with('success_message', trans('admin.case-studies.create_success'));
            } else {
                return Redirect::to('admin/case-studies/create')
                    ->withInput(Input::all())
                    ->withErrors($caseStudy->getErrors());
            }
        */
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $CaseStudyFilter = CaseStudyFilter::find($id);


            $viewData = [
                'cmsName'         => $this->cmsName,
                'copyrightName'   => $this->copyrightName,
                'caseStudyFilter' => $CaseStudyFilter,
            ];

            return View::make('admin.case_study_filters.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $CaseStudyFilter = CaseStudyFilter::find($id);
            $CaseStudyFilter->fill(Input::all());

            if ($CaseStudyFilter->isModelValid()) {
                $CaseStudyFilter->save();

                return Redirect::to('admin/gallery-filter-items/filter/' . $id)
                    ->with('success_message', trans('admin.case-study-filter.update_success'));
            } else {
                return Redirect::to('admin/gallery-filters/' . $id . '/edit')
                    ->withErrors($CaseStudyFilter->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resourceCount = CaseStudy::all()->count();

            if ($resourceCount > 1) {
                $CaseStudy     = CaseStudy::find($id);
                $CaseStudyName = $CaseStudy->main_heading;

                if ($CaseStudy->id >= 1) {
                    CaseStudy::destroy($id);

                    return Redirect::to('admin/gallery')
                        ->with('success_message',
                            "Gallery Entry: " . $CaseStudyName . " has been deleted successfully.");
                } else {
                    return Redirect::to('admin/gallery')
                        ->with('error_message', "Gallery Entry was not deleted.");
                }
            } else {
                return Redirect::to('admin/gallery')
                    ->with('error_message', trans('admin.case-studies.delete_min'));
            }
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource        = CaseStudyFilter::find($id);
                $Resource->order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }


    }
