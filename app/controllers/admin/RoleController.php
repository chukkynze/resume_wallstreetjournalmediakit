<?php
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Hyfn\Models\User;
    use Input;
    use Redirect;
    use Response;
    use Session;
    use View;
    use Validator;

    use Hyfn\Models\Role;

    class RoleController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Users");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allRoles = Role::all();
            $allUsers = User::all();

            foreach ($allUsers as $user) {
                $user->displayValue = $user->first_name . " " . $user->last_name;
            }

            foreach ($allRoles as $role) {
                $permissionsList = $role->perms;

                foreach ($permissionsList as $permissions) {
                    $role->permissionsList .= $permissions->display_name . ", ";
                }

                $role->permissionsList = substr($role->permissionsList, 0, -2);
            }


            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'allowRoleMgt'  => ($this->loggedInUser->can("manage_roles") ? 1 : 0),
                'activeSection' => "Users",
                'roles'         => $allRoles,
                'users'         => $allUsers,
            ];


            return View::make('admin.roles.index', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $newRole       = new Role;
            $newRole->name = Input::get('roleName');
            $newRole->save();

            $headerArray = array
            (
                'Content-Type'  => 'application/json',
                'Cache-Control' => 'no-cache, must-revalidate',
            );

            $contentArray = array
            (
                "status"  => 'success',
                "message" => "The role, " . $newRole->name . ", was successfully created.",
            );

            Session::put('success_message', "The role, " . $newRole->name . ", was successfully created.");

            $response = Response::make($contentArray, 200, $headerArray);

            return $response;
        }


        public function saveAssignedRole()
        {
            $role = Role::find((int)Input::get("roleID"));
            $user = User::find((int)Input::get("userID"));
            $user->attachRole($role);

            $headerArray = array
            (
                'Content-Type'  => 'application/json',
                'Cache-Control' => 'no-cache, must-revalidate',
            );

            $contentArray = array
            (
                "status"  => 'success',
                "message" => "The role, " . $role->name . ", was successfully assigned to the user, " . $user->first_name . " " . $user->last_name . ".",
            );

            Session::put('success_message',
                "The role, " . $role->name . ", was successfully assigned to the user, " . $user->first_name . " " . $user->last_name . ".");

            $response = Response::make($contentArray, 200, $headerArray);

            return $response;
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $Role     = Role::find($id);
            $roleName = $Role->name;

            if ($Role->id > 1) {
                Role::destroy($id);

                return Redirect::to('admin/roles')
                    ->with('success_message', "Role: " . $roleName . " successfully deleted.");
            } else {
                return Redirect::to('admin/roles')
                    ->with('error_message', "Role was not deleted.");
            }
        }


    }
