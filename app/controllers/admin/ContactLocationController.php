<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       ContactLocationController
     *
     * filename:    ContactLocationController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:07 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\ContactLocation;


    class ContactLocationController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Contact");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allContactLocations = ContactLocation::orderBy("order", "asc")->get();

            $initIndexOrder = '';
            foreach ($allContactLocations as $product) {
                $initIndexOrder .= "&index-row[]=" . $product->id;
            }
            $viewData            = [
                'cmsName'                 => $this->cmsName,
                'copyrightName'           => $this->copyrightName,
                'allowContactLocationMgt' => ($this->loggedInUser->can("manage_contact_locations") ? 1 : 0),
                'contactLocations'        => $allContactLocations,
                'initIndexOrder' => $initIndexOrder,
            ];


            return View::make('admin.contact-locations.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $viewData = [
                'cmsName'         => $this->cmsName,
                'copyrightName'   => $this->copyrightName,
                'contactLocation' => new ContactLocation(),
            ];

            return View::make('admin.contact-locations.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $formData = Input::all();
            $resource = new ContactLocation();
            $resource->fill($formData);

            if ($resource->isModelValid()) {
                $resource->save();

                return Redirect::to('admin/contact-locations')
                    ->with('success_message', trans('admin.contact-locations.create_success'));
            } else {
                return Redirect::to('admin/contact-locations/create')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $viewData = [
                'cmsName'         => $this->cmsName,
                'copyrightName'   => $this->copyrightName,
                'contactLocation' => ContactLocation::find($id),
            ];

            return View::make('admin.contact-locations.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $formData = Input::all();
            $resource = ContactLocation::find($id);
            $resource->fill($formData);

            if ($resource->isModelValid()) {
                $resource->save();

                return Redirect::to('admin/contact-locations')
                    ->with('success_message', trans('admin.contact-locations.update_success'));
            } else {
                return Redirect::to('admin/contact-locations/' . $id . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resource     = ContactLocation::find($id);
            $resourceName = $resource->name;

            if ($resource->id >= 1) {
                ContactLocation::destroy($id);

                return Redirect::to('admin/contact-locations')
                    ->with('success_message', "Contact Location: " . $resourceName . " has been deleted successfully.");
            } else {
                return Redirect::to('admin/contact-locations')
                    ->with('error_message', trans('admin.contact-locations.delete_failure'));
            }
        }


        public function getRateProducts()
        {
            $allProducts = ContactLocation::orderBy("order", "asc")->get();

            $output = [];
            foreach ($allProducts as $product) {
                $output[$product->id] = $product->name;
            }

            return json_encode($output);
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource        = ContactLocation::find($id);
                $Resource->order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }


    }
