<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       SiteProductController
     *
     * filename:    SiteProductController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:07 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use Str;
    use View;

    use Hyfn\Models\ContactProduct;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;


    class ContactProductController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Contact");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allProducts = ContactProduct::orderBy("order", "asc")->get();

            $initIndexOrder = '';
            foreach ($allProducts as $product) {
                $initIndexOrder .= "&index-row[]=" . $product->id;
            }
            $viewData    = [
                'cmsName'                => $this->cmsName,
                'copyrightName'          => $this->copyrightName,
                'allowContactProductMgt' => ($this->loggedInUser->can("manage_contact_products") ? 1 : 0),
                'allProducts'            => $allProducts,
                'initIndexOrder' => $initIndexOrder,
            ];


            return View::make('admin.contact-products.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $ContactProduct = new ContactProduct();

            $viewData = [
                'cmsName'        => $this->cmsName,
                'copyrightName'  => $this->copyrightName,
                'contactProduct' => $ContactProduct,
            ];

            return View::make('admin.contact-products.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $resource = new ContactProduct();
            $resource->fill(Input::all());

            if ($resource->isModelValid()) {
                $resource->save();

                return Redirect::to('admin/contact-products')
                    ->with('success_message', trans('admin.contact-products.create_success'));
            } else {
                return Redirect::to('admin/contact-products/create')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $ContactProduct = ContactProduct::find($id);

            $viewData = [
                'cmsName'        => $this->cmsName,
                'copyrightName'  => $this->copyrightName,
                'contactProduct' => $ContactProduct,
            ];

            return View::make('admin.contact-products.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $resource = ContactProduct::find($id);
            $resource->fill(Input::all());

            if ($resource->isModelValid()) {
                $resource->save();

                return Redirect::to('admin/contact-products')
                    ->with('success_message', trans('admin.contact-products.update_success'));
            } else {
                return Redirect::to('admin/contact-products/' . $id . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resource     = ContactProduct::find($id);
            $resourceName = $resource->name;

            if ($resource->id >= 1) {
                ContactProduct::destroy($id);

                return Redirect::to('admin/contact-products')
                    ->with('success_message', "Contact Product: " . $resourceName . " has been deleted successfully.");
            } else {
                return Redirect::to('admin/site-products')
                    ->with('error_message', trans('admin.contact-products.delete_failure'));
            }
        }


        public function getContactProducts()
        {
            $allProducts = ContactProduct::orderBy("order", "asc")->get();

            $output = [];
            foreach ($allProducts as $product) {
                $output[$product->id] = $product->name;
            }

            return json_encode($output);
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource        = ContactProduct::find($id);
                $Resource->order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }

    }
