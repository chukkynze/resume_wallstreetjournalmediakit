<?php
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\Client;

    class ClientController extends AdminController
    {
        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Case Study");

        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $viewData = [
                'cmsName'        => $this->cmsName,
                'copyrightName'  => $this->copyrightName,
                'allowClientMgt' => ($this->loggedInUser->can("manage_clients") ? 1 : 0),
            ];

            return View::make('admin.clients.index', $viewData)->with('clients', Client::all());
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
            ];

            return View::make('admin.clients.create', $viewData)->with('client', new Client());
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            // Get form data, create new user
            $formData = Input::all();
            $model    = new Client();
            $model->fill($formData);

            if ($model->isModelValid()) {
                $model->save();

                return Redirect::to('admin/clients')
                    ->with('success_message', trans('admin.clients.create_success'));
            } else {
                return Redirect::to('admin/clients/create')
                    ->withInput(Input::all())
                    ->withErrors($model->getErrors());
            }
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function edit($id)
        {
            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
            ];

            return View::make('admin.clients.edit', $viewData)->with('client', Client::find($id));
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function update($id)
        {
            // Get form data, create new user
            $formData = Input::all();
            $model    = Client::find($id);
            $model->fill($formData);

            if ($model->isModelValid()) {
                $model->save();

                return Redirect::to('admin/clients')
                    ->with('success_message', trans('admin.clients.create_success'));
            } else {
                return Redirect::to('admin/clients/' . $id . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($model->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $Client     = Client::find($id);
            $ClientName = $Client->name;

            if ($Client->id >= 1) {
                Client::destroy($id);

                return Redirect::to('admin/clients')
                    ->with('success_message', "Client: " . $ClientName . " has been deleted successfully.");
                #->with('success_message',trans('admin.delete_success'));
            } else {
                return Redirect::to('admin/clients')
                    ->with('error_message', "Client was not deleted.");
            }
        }


    }
