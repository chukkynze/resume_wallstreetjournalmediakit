<?php
    namespace Hyfn\Controllers\Admin;

    use Illuminate\Support\MessageBag;
    use Hyfn\Controllers\AdminController;
    use Hyfn\Models\Product;

    use View;

    class ProductsController extends AdminController
    {


        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Products");

        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $viewData = [
                'cmsName'         => $this->cmsName,
                'copyrightName'   => $this->copyrightName,
                'allowProductMgt' => ($this->loggedInUser->can("manage_products") ? 1 : 0),
            ];

            return View::make('admin.users.index', $viewData)->with('users', Product::all());
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            //
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            //
        }


        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function show($id)
        {
            //
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            //
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            //
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            //
        }


    }
