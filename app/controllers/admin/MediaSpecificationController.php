<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       MediaSpecificationController
     *
     * filename:    MediaSpecificationController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/3/14 11:25 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     *
     */
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\Rate;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;
    use Hyfn\Models\SiteProduct;
    use Hyfn\Models\RateProduct;


    class MediaSpecificationController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Rates");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allRates = Rate::orderBy("created_at", "DESC")->get();

            foreach ($allRates as $rateKey => $rate) {
                unset($rate->rateProductName);

                $uploadsArrays = (array)json_decode($rate->uploads_array, true);

                $i = 0;
                foreach ($uploadsArrays as $key => $uploadsArray) {
                    $RateProduct                 = RateProduct::find($key);
                    $rateProductName[$i]['key']  = $key;
                    $rateProductName[$i]['name'] = $RateProduct->name;
                    $dataFiles                   = [];
                    foreach ($uploadsArray as $uploadID) {
                        if ($uploadID >= 1) {
                            $uploadFileData = Upload::find($uploadID);
                            $dataFiles[]    = [
                                'name' => $uploadFileData->name,
                                'file' => $uploadFileData->download_folder . $uploadFileData->download_file
                            ];
                        } else {
                            $dataFiles[] = [
                                'name' => 0,
                                'file' => 0
                            ];
                        }
                    }

                    $rateProductName[$i]['data'] = $dataFiles;

                    $i++;
                }

                $rate->rateProductName = $rateProductName;

            }

            $viewData = [
                'cmsName'       => $this->cmsName,
                'copyrightName' => $this->copyrightName,
                'allowRatesMgt' => ($this->loggedInUser->can("manage_rates") ? 1 : 0),
                'allRates'      => $allRates,
            ];


            return View::make('admin.rates.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $rateModel         = Rate::orderBy("created_at", "DESC")->first();
            $rateModelArray    = (array)json_decode($rateModel->uploads_array, true);
            $rateProductRows   = RateProduct::all();
            $rateProductsArray = [];

            foreach ($rateProductRows as $rate) {
                $uploadFiles = Upload::whereIn('category', [1])->get();

                foreach ($uploadFiles as $uploadFile) {
                    $uploadCategory           = UploadCategory::find($uploadFile->category);
                    $uploadFile->categoryName = $uploadCategory->name;

                }

                $RateProduct                                 = RateProduct::find($rate->id);
                $rateProductsArray[$rate->id]['name']        = $RateProduct->name;
                $rateProductsArray[$rate->id]['uploadFiles'] = $uploadFiles;
                $rateProductsArray[$rate->id]['uploads']     = [];

                if (array_key_exists($rate->id, $rateModelArray)) {
                    $initOrder = "";
                    if (count($rateModelArray[$rate->id]) > 0) {
                        foreach ($rateModelArray[$rate->id] as $key => $upload) {

                            if (is_numeric($key)) {
                                $rateProductsArray[$rate->id]['uploads'][] = $upload;
                                $uploadNbr                                 = (int)$upload * 1;
                                $initOrder .= "&rate" . $rate->id . "_" . $upload;

                                if ($uploadNbr >= 1) {
                                    $theUpload                                       = Upload::find($uploadNbr);
                                    $rateProductsArray[$rate->id]['chosenUploads'][] = [
                                        'uploadID'   => $uploadNbr,
                                        'uploadName' => $theUpload->name,
                                    ];
                                } else {
                                    $rateProductsArray[$rate->id]['chosenUploads'][] = [
                                        'uploadID'   => 0,
                                        'uploadName' => "",
                                    ];
                                }
                            }
                        }
                        $rateProductsArray[$rate->id]['init_order'] = $initOrder;
                    } else {
                        $rateProductsArray[$rate->id]['chosenUploads'][] = [
                            'uploadID'   => 0,
                            'uploadName' => "",
                        ];
                        $rateProductsArray[$rate->id]['init_order']      = '';
                    }
                } else {
                    $rateProductsArray[$rate->id]['chosenUploads'][] = [
                        'uploadID'   => 0,
                        'uploadName' => "",
                    ];
                    $rateProductsArray[$rate->id]['init_order']      = '';
                }
            }

            $viewData = [
                'cmsName'        => $this->cmsName,
                'copyrightName'  => $this->copyrightName,
                'rate'           => $rateModel,
                'rateModelArray' => $rateProductsArray,
            ];

            return View::make('admin.rates.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $productsArray = array_reverse(explode("&siteproduct[]=", Input::get("site_product_order")), true);
            array_pop($productsArray);
            $productsArray = array_reverse($productsArray);

            $uploadsArray = [];

            foreach ($productsArray as $siteProduct) {
                $uploadSet = array_reverse(explode("&rate" . $siteProduct . "_",
                    Input::get("siteproduct_rate" . $siteProduct . "_order")));
                array_pop($uploadSet);

                foreach ($uploadSet as $uploadID) {
                    if ($uploadID >= 1) {
                        $Upload          = Upload::find($uploadID);
                        $Upload->is_live = 1;

                        if ($Upload->isModelValid()) {
                            $Upload->save();
                        } else {
                            echo "<pre>" . print_r($Upload->getErrors(), 1) . "</pre>\n";
                        }
                    }
                }

                /**
                 * There's some complicated shit going on with Rates that I realized
                 * The problem is I can't remember what I realized!!!!
                 * So, this note is just a reminder to remember what it was you realized that was so complicated
                 *
                 * ..had to do with how rates are actually used as opposed to being checked for use... or something like that
                 */

                $uploadsArray[$siteProduct] = $uploadSet;
                // $uploadsArray[$siteProduct]['used']    = 1;
                // $uploadsArray[$siteProduct]['uploads'] = array_reverse($uploadSet);


            }

            $rateModel                 = Rate::find(1);
            $rateModel->products_array = json_encode($productsArray);
            $rateModel->uploads_array  = json_encode($uploadsArray, JSON_FORCE_OBJECT);

            if ($rateModel->isModelValid('saving')) {
                $rateModel->save();

                return Redirect::to('admin/rates_specs/')
                    ->with('success_message', trans('admin.rates.update_success'));
            } else {
                return Redirect::to('admin/rates/create')
                    ->withInput(Input::all())
                    ->withErrors($rateModel->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {

            $resourceCount = Rate::all()->count();

            if ($resourceCount > 1) {
                $resourceModel = Rate::find($id);
                $resourceDate  = date("F jS Y", strtotime($resourceModel->created_at));
                $uploadArray   = (array)json_decode($resourceModel->uploads_array, true);

                if ($resourceModel->id >= 1) {

                    foreach ($uploadArray as $uploadID) {
                        if ($uploadID >= 1) {
                            $Upload          = Upload::find($uploadID);
                            $Upload->is_live = 0;
                            $Upload->save();
                        }
                    }

                    $resourceModel::destroy($id);

                    return Redirect::to('admin/rates')
                        ->with('success_message',
                            "The Rates &amp; Specifications Page Content version, created at: " . $resourceDate . ", has been successfully deleted.");
                } else {
                    return Redirect::to('admin/rates/')
                        ->with('error_message', trans('admin.rates.delete_failure'));
                }
            } else {
                return Redirect::to('admin/rates/')
                    ->with('error_message', trans('admin.rates.delete_min'));
            }
        }


        /**
         * Getting Rate Product Information from the Rate Page
         *
         * The URL you'll use to receive a products rate info is
         * http://<domain>/admin/rates/get-rate-specs/<site product id from select box>
         *
         * Example
         * http://vm.wsj.local:8000/admin/rates/get-rate-specs/8
         *
         * This will return a JSON object with "data" containing the filenames and file locations for the associated pdf's
         * and additional data such as submit ad links and other text if they exist
         */


        /**
         * @param $rateProductID
         *
         * @return string
         */
        public function getRateSpecificationByRateProduct($rateProductID)
        {
            $initialProducts       = Rate::orderBy("created_at", "DESC")->first();
            $allRateSpecifications = (array)json_decode($initialProducts->uploads_array);
            $rateProductRateSpecs  = [];

            foreach ($allRateSpecifications as $key => $rateUploads) {
                if ($key == $rateProductID) {
                    foreach ($rateUploads as $uploadID) {
                        $uploadID = (int)$uploadID;
                        if ($uploadID >= 1) {
                            $Upload                         = Upload::find($uploadID);
                            $rateProductRateSpecs['data'][] = [
                                'name'      => $Upload->name,
                                'file'      => $Upload->download_file,
                                'iconClass' => substr($Upload->download_file,
                                    -4) == ".xls" || substr($Upload->download_file,
                                    -5) == ".xlsx" ? "excel-link" : "pdf-link",
                                'location'  => $Upload->download_folder,
                            ];
                        } else {
                            $rateProductRateSpecs['data'][] = [
                                'name'      => "",
                                'file'      => "",
                                'iconClass' => "",
                                'location'  => "",
                            ];
                        }
                    }

                    $RateProduct                            = RateProduct::find($key);
                    $rateProductRateSpecs['submit_ad_link'] = ($RateProduct->submit_ad_link != ""
                        ? $RateProduct->submit_ad_link
                        : 0);

                    // Weird Edge Cases
                    switch ($RateProduct->name) {
                        case 'Classified'   :
                            $rateProductRateSpecs['classified_text'] = "Please visit <a href='http://classifieds.wsj.com' target='_blank'>classifieds.wsj.com</a> for rates, specs and to submit ad materials. ";
                    }
                }
            }


            if (count($rateProductRateSpecs['data']) > 1) {
                foreach ($rateProductRateSpecs['data'] as $key => $specs) {
                    if (
                        $specs['name'] == ""
                        && $specs['file'] == ""
                        && $specs['iconClass'] == ""
                        && $specs['location'] == ""
                    ) {
                        unset($rateProductRateSpecs['data'][$key]);
                    }
                }
            }
            $rateProductRateSpecs['data'] = array_values($rateProductRateSpecs['data']);

            return json_encode($rateProductRateSpecs);
        }
    }
