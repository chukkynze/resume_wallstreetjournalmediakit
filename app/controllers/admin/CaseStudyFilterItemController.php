<?php
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Hyfn\Models\CaseStudyFilter;
    use Hyfn\Models\CaseStudyFilterItem;
    use Hyfn\Models\CaseStudyFilterItemRelationship;
    use Hyfn\Models\Client;
    use Hyfn\Models\SiteProduct;
    use Input;
    use Redirect;
    use Response;
    use View;

    use Hyfn\Models\CaseStudy;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;

    class CaseStudyFilterItemController extends AdminController
    {


        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Case Study");

        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $CaseStudyFilterItems = CaseStudyFilterItem::orderBy('order', 'asc')->get();

            $initIndexOrder = '';
            foreach ($CaseStudyFilterItems as $item) {
                $initIndexOrder .= "&index-row[]=" . $item->id;
                $CaseStudyFilter  = CaseStudyFilter::find($item->filter_id);
                $item->filterName = $CaseStudyFilter->name;
            }


            $viewData = [
                'cmsName'              => $this->cmsName,
                'copyrightName'        => $this->copyrightName,
                'allowCaseStudyMgt'    => ($this->loggedInUser->can("manage_case_studies") ? 1 : 0),
                'caseStudyFilterItems' => $CaseStudyFilterItems,
                'initIndexOrder' => $initIndexOrder,
            ];

            return View::make('admin.case_study_filter_items.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $caseStudyFilterItem = new CaseStudyFilterItem();
            $caseStudyFilters    = CaseStudyFilter::all();

            $filtersArray = [];
            foreach ($caseStudyFilters as $filter) {
                $filtersArray[$filter->id] = $filter->name;
            }


            $viewData = [
                'cmsName'             => $this->cmsName,
                'copyrightName'       => $this->copyrightName,
                'filtersArray'        => $filtersArray,
                'caseStudyFilterItem' => $caseStudyFilterItem,
                'filterID' => 0,
            ];

            return View::make('admin.case_study_filter_items.create', $viewData);
        }


        /**
         * @param $filterID
         *
         * @return \Illuminate\View\View
         */
        public function createFilterItem($filterID)
        {
            $caseStudyFilterItem = new CaseStudyFilterItem();
            $caseStudyFilters    = CaseStudyFilter::all();

            $filtersArray = [];
            foreach ($caseStudyFilters as $filter) {
                $filtersArray[$filter->id] = $filter->name;
            }


            $viewData = [
                'cmsName'             => $this->cmsName,
                'copyrightName'       => $this->copyrightName,
                'filtersArray'        => $filtersArray,
                'caseStudyFilterItem' => $caseStudyFilterItem,
                'filterID'            => $filterID,
            ];

            return View::make('admin.case_study_filter_items.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $CaseStudyFilterItem = new CaseStudyFilterItem();
            $CaseStudyFilterItem->fill(Input::all());

            if ($CaseStudyFilterItem->isModelValid()) {
                $CaseStudyFilterItem->save();

                return Redirect::to('admin/gallery-filter-items/filter/' . $CaseStudyFilterItem->filter_id)
                    ->with('success_message', trans('admin.case-study-filter-items.create_success'));
            } else {
                return Redirect::to('admin/gallery-filter-items/create')
                    ->withErrors($CaseStudyFilterItem->getErrors());
            }
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $caseStudyFilterItem = CaseStudyFilterItem::find($id);
            $caseStudyFilter = CaseStudyFilter::find($caseStudyFilterItem->filter_id);
            $caseStudyFilters    = CaseStudyFilter::all();

            $filtersArray = [];
            foreach ($caseStudyFilters as $filter) {
                $filtersArray[$filter->id] = $filter->name;
            }


            $viewData = [
                'cmsName'             => $this->cmsName,
                'copyrightName'       => $this->copyrightName,
                'filtersArray'        => $filtersArray,
                'filterName' => $caseStudyFilter->name,
                'filterID'   => $caseStudyFilter->id,
                'caseStudyFilterItem' => $caseStudyFilterItem,
            ];

            return View::make('admin.case_study_filter_items.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $CaseStudyFilterItem = CaseStudyFilterItem::find($id);
            $CaseStudyFilterItem->fill(Input::all());

            if ($CaseStudyFilterItem->isModelValid()) {
                $CaseStudyFilterItem->save();

                return Redirect::to('admin/gallery-filter-items/filter/' . $CaseStudyFilterItem->filter_id)
                    ->with('success_message', trans('admin.case-study-filter-items.update_success'));
            } else {
                return Redirect::to('admin/gallery-filter-items/' . $id . '/edit')
                    ->withErrors($CaseStudyFilterItem->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $CaseStudyFilterItem     = CaseStudyFilterItem::find($id);
            $CaseStudyFilterItemName = $CaseStudyFilterItem->name;
            $CaseStudyFilterID = $CaseStudyFilterItem->filter_id;

            if ($CaseStudyFilterItem->id >= 1) {
                CaseStudyFilterItem::destroy($id);
                CaseStudyFilterItemRelationship::where('filter_item_id', '=', $id)->delete();

                return Redirect::to('admin/gallery-filter-items/filter/' . $CaseStudyFilterID)
                    ->with('success_message',
                        "Case Study Filter Item: " . $CaseStudyFilterItemName . " has been deleted successfully.");
            } else {
                return Redirect::to('admin/gallery-filter-items/filter/' . $CaseStudyFilterID)
                    ->with('error_message', "Case Study Filter Item was not deleted.");
            }
        }


        public function getFilterItemsByFilterID($filterID)
        {
            $CaseStudyFilterItems = CaseStudyFilterItem::where('filter_id', '=', $filterID)->orderBy('order',
                'asc')->get();
            $CaseStudyFilter      = CaseStudyFilter::find($filterID);
            $filterName           = $CaseStudyFilter->name;

            $initIndexOrder = '';
            foreach ($CaseStudyFilterItems as $item) {
                $initIndexOrder .= "&index-row[]=" . $item->id;
            }

            $viewData = [
                'cmsName'              => $this->cmsName,
                'copyrightName'        => $this->copyrightName,
                'allowCaseStudyMgt'    => ($this->loggedInUser->can("manage_case_studies") ? 1 : 0),
                'caseStudyFilterItems' => $CaseStudyFilterItems,
                'initIndexOrder' => $initIndexOrder,
                'filterName'     => $filterName,
                'filterID'       => $filterID,
            ];

            return View::make('admin.case_study_filter_items.index', $viewData);
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource        = CaseStudyFilterItem::find($id);
                $Resource->order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }


    }
