<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Class:       SiteProductSectionController
     *
     * filename:    SiteProductSectionController.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:07 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use Str;
    use View;

    use Hyfn\Models\SiteProduct;
    use Hyfn\Models\SiteProductSection;


    class SiteProductSectionController extends AdminController
    {

        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Products");
        }

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $SiteProducts    = SiteProduct::orderBy('section_order', 'asc')->get();
            $allSiteProducts = [];
            foreach ($SiteProducts as $product) {
                $p                      = $product->id;
                $allSiteProducts[$p]    = [
                    'productID'   => $product->id,
                    'productName' => $product->name,
                    'sections'    => [],
                    'initOrder'   => '',
                ];
                $SiteProductSections    = SiteProductSection::where('site_product_id', '=',
                    $product->id)->orderBy('order', 'asc')->get();
                $allSiteProductSections = [];
                foreach ($SiteProductSections as $section) {
                    $allSiteProductSections[] = [

                        'sectionID'    => $section->id,
                        'sectionName'  => $section->name,
                        'sectionOrder' => $section->order,
                    ];
                }

                $allSiteProducts[$p]['sections'] = $allSiteProductSections;
            }


            $viewData = [
                'cmsName'             => $this->cmsName,
                'copyrightName'       => $this->copyrightName,
                'allowSiteProductMgt' => ($this->loggedInUser->can("manage_site_products") ? 1 : 0),
                'allSiteProducts'     => $allSiteProducts,
            ];


            return View::make('admin.site-product-sections.index', $viewData);
        }


        /**
         * @param $productID
         *
         * @return \Illuminate\View\View
         */
        public function viewSectionIndex($productID)
        {
            $SiteProduct                 = SiteProduct::find($productID);
            $allSiteProducts[$productID] = [
                'productID'   => $SiteProduct->id,
                'productName' => $SiteProduct->name,
                'initOrder'   => '',
            ];

            $SiteProductSections    = SiteProductSection::where('site_product_id', '=', $productID)->orderBy('order',
                'asc')->get();
            $allSiteProductSections = [];
            foreach ($SiteProductSections as $section) {
                $allSiteProducts[$productID]['initOrder'] .= '&section[]=' . $section->id;
                $allSiteProductSections[] = [
                    'sectionID'    => $section->id,
                    'sectionName'  => $section->name,
                    'sectionOrder' => $section->order,
                ];
            }

            $allSiteProducts[$productID]['sections'] = $allSiteProductSections;


            $viewData = [
                'cmsName'             => $this->cmsName,
                'copyrightName'       => $this->copyrightName,
                'allowSiteProductMgt' => ($this->loggedInUser->can("manage_site_products") ? 1 : 0),
                'allSiteProducts'     => $allSiteProducts,
            ];

            return View::make('admin.site-product-sections.section-index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $siteProducts          = SiteProduct::where("on_product_landing", "=", 1)->get();
            $siteProductsOnLanding = [];
            foreach ($siteProducts as $siteProduct) {
                $siteProductsOnLanding[$siteProduct->id] = $siteProduct->name;
            }


            $viewData = [
                'cmsName'            => $this->cmsName,
                'copyrightName'      => $this->copyrightName,
                'siteProductArray'   => $siteProductsOnLanding,
                'siteProductSection' => new SiteProductSection(),
                'productID' => 0,
            ];

            return View::make('admin.site-product-sections.create', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function createSection($productID)
        {
            $siteProducts          = SiteProduct::where("on_product_landing", "=", 1)->get();
            $siteProductsOnLanding = [];
            foreach ($siteProducts as $siteProduct) {
                $siteProductsOnLanding[$siteProduct->id] = $siteProduct->name;
            }


            $viewData = [
                'cmsName'            => $this->cmsName,
                'copyrightName'      => $this->copyrightName,
                'siteProductArray'   => $siteProductsOnLanding,
                'siteProductSection' => new SiteProductSection(),
                'productID'          => $productID,
            ];

            return View::make('admin.site-product-sections.create', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            $formData = Input::all();
            $resource = new SiteProductSection();
            $resource->fill($formData);
            $resource->name_slug = Str::slug($resource->name);
            $resource->order = SiteProductSection::count() + 1;
            $resource->enable_link = 1;
            $resource->link_text = $resource->name;

            if ($resource->isModelValid()) {
                $resource->save();

                //return Redirect::to('admin/site-product-sections/product/' . $resource->site_product_id . '/view')
                return Redirect::to('admin/page-content/product-pages')
                    ->with('success_message', trans('admin.site-product-sections.create_success'))
                    ->with('tab', 5)
                    ->with('entityID', $resource->id);
            } else {
                return Redirect::to('admin/site-product-sections/create')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $siteProducts          = SiteProduct::where("on_product_landing", "=", 1)->get();
            $siteProductsOnLanding = [];
            foreach ($siteProducts as $siteProduct) {
                $siteProductsOnLanding[$siteProduct->id] = $siteProduct->name;
            }


            $viewData = [
                'cmsName'            => $this->cmsName,
                'copyrightName'      => $this->copyrightName,
                'siteProductArray'   => $siteProductsOnLanding,
                'siteProductSection' => SiteProductSection::find($id),
                'productID' => 0,
            ];

            return View::make('admin.site-product-sections.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $formData = Input::all();
            $resource = SiteProductSection::find($id);
            $resource->fill($formData);
            $resource->name_slug = Str::slug($resource->name);
            $resource->enable_link = 1;
            $resource->link_text = $resource->name;

            if ($resource->isModelValid()) {
                $resource->save();

                //return Redirect::to('admin/site-product-sections/product/' . $resource->site_product_id . '/view')
                return Redirect::to('admin/page-content/product-pages')
                    ->with('success_message', trans('admin.site-product-sections.update_success'))
                    ->with('tab', 5)
                    ->with('entityID', $resource->id);
            } else {
                return Redirect::to('admin/site-product-sections/' . $id . '/edit')
                    ->withInput(Input::all())
                    ->withErrors($resource->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resource     = SiteProductSection::find($id);
            $resourceName = $resource->name;
            $resourcePID = $resource->site_product_id;

            if ($resource->id >= 1) {
                SiteProductSection::destroy($id);

                return Redirect::to('admin/site-product-sections/product/' . $resourcePID . '/view')
                    ->with('success_message', "Site Product: " . $resourceName . " has been deleted successfully.");
            } else {
                return Redirect::to('admin/site-product-sections/product/' . $resourcePID . '/view')
                    ->with('error_message', trans('admin.site-product-sections.delete_failure'));
            }
        }


        public function getSiteProductSectionNameSlug($siteProductSectionName)
        {
            return json_encode(Str::slug($siteProductSectionName));
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource        = SiteProductSection::find($id);
                $Resource->order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }

    }
