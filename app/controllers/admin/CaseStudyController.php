<?php
    namespace Hyfn\Controllers\Admin;

    use Hyfn\Controllers\AdminController;

    use Input;
    use Redirect;
    use Response;
    use Session;
    use View;

    use Hyfn\Models\Client;
    use Hyfn\Models\CaseStudy;
    use Hyfn\Models\CaseStudyFilter;
    use Hyfn\Models\CaseStudyFilterItem;
    use Hyfn\Models\CaseStudyFilterItemRelationship;
    use Hyfn\Models\ModuleLocationRelationship;
    use Hyfn\Models\Upload;
    use Hyfn\Models\UploadCategory;

    class CaseStudyController extends AdminController
    {
        public function __construct()
        {
            parent::__construct();

            View::share('activeSection', "Case Study");
        }


        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $allCaseStudies = CaseStudy::orderBy('order', 'asc')->orderBy('headline', 'asc')->get();
            $initIndexOrder = '';

            $allFilters = CaseStudyFilter::orderBy('order', 'asc')->get();

            foreach ($allCaseStudies as $caseStudy) {
                $initIndexOrder .= "&index-row[]=" . $caseStudy->id;
                /*
                 if ($caseStudy->client_id >= 1) {
                    $Client                 = Client::find($caseStudy->client_id);
                    $caseStudy->client_name = $Client->name;
                } else {
                    $caseStudy->client_name = "Client not selected.";
                }
                 */


                $filterItemArray = [];
                foreach ($allFilters as $filter) {
                    $GalleryRelationships = CaseStudyFilterItemRelationship::where("case_study_id", "=", $caseStudy->id)
                        ->where("filter_id", "=", $filter->id)
                        ->distinct()
                        ->get(['filter_item_id'])
                        ->toArray();
                    $filterItems          = "";
                    foreach ($GalleryRelationships as $rel) {
                        $CaseStudyFilterItem = CaseStudyFilterItem::find($rel['filter_item_id']);
                        $filterItems .= $CaseStudyFilterItem->name . ", ";
                    }

                    $filterItemArray[] = $filterItems == "" ? "" : substr($filterItems, 0, -2);
                }

                $caseStudy->filterItems = $filterItemArray;
            }

            $viewData = [
                'cmsName'           => $this->cmsName,
                'copyrightName'     => $this->copyrightName,
                'allowCaseStudyMgt' => ($this->loggedInUser->can("manage_case_studies") ? 1 : 0),
                'caseStudies'       => $allCaseStudies,
                'initIndexOrder'    => $initIndexOrder,
                'allFilters'        => $allFilters,
            ];

            return View::make('admin.case_studies.index', $viewData);
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $caseStudy        = new CaseStudy();
            $allFilters       = CaseStudyFilter::orderBy('order', 'asc')->get();
            $allListingImages = Upload::where('category', '=', 11)->get();

            foreach ($allFilters as $filter) {
                $allFilterItems   = CaseStudyFilterItem::where('filter_id', '=', $filter->id)->orderBy('order',
                    'asc')->get();
                $filterItemsArray = [];
                foreach ($allFilterItems as $filterItem) {
                    $filterItemsArray[] = [
                        'id'   => $filterItem->id,
                        'name' => $filterItem->name,
                    ];
                }

                $filter->items = $filterItemsArray;
                $filter->chosenFilterItems = [];
            }

            foreach ($allListingImages as $listingImage) {
                $uploadCategory             = UploadCategory::find($listingImage->category);
                $listingImage->categoryName = $uploadCategory->name;
            }

            $caseStudy->chosenFilterItems = [];

            $viewData = [
                'cmsName'            => $this->cmsName,
                'copyrightName'      => $this->copyrightName,
                'caseStudy'          => $caseStudy,
                'allFilters'         => $allFilters,
                'listingImages'      => $allListingImages,
                'allowCaseStudyMgt'  => ($this->loggedInUser->can("manage_case_studies") ? 1 : 0),
            ];

            return View::make('admin.case_studies.create', $viewData);
        }


        /**
         * @param $caseStudyID
         *
         * @return \Illuminate\View\View
         */
        public function manageModules($caseStudyID)
        {
            $caseStudy = CaseStudy::find($caseStudyID);

            if ($caseStudy->listing_pic_id >= 1) {
                $ListingPic             = Upload::find($caseStudy->listing_pic_id);
                $caseStudy->listing_pic = $ListingPic->download_folder . $ListingPic->download_file;
            } else {
                $caseStudy->listing_pic = "/files/uploads/201410/CaseStudyDefault.png";
            }

            $allClients       = Client::all();
            $allListingImages = Upload::where('category', '=', 11)->get();
            $clientList       = [0 => 'Choose A Client'];
            foreach ($allClients as $client) {
                $clientList[$client->id] = $client->name;
            }

            $selectedFilters   = CaseStudyFilterItemRelationship::where('case_study_id', '=',
                $caseStudyID)->distinct()->get(['filter_id']);
            $toViewFilterArray = [];
            foreach ($selectedFilters as $filter) {
                $CaseStudyFilter     = CaseStudyFilter::find($filter->filter_id);
                $selectedFilterItems = CaseStudyFilterItemRelationship::where('case_study_id', '=',
                    $caseStudyID)->where('filter_id', '=', $filter->filter_id)->distinct()->get(['filter_item_id']);
                $items               = [];
                foreach ($selectedFilterItems as $filterItem) {
                    $CaseStudyFilterItem = CaseStudyFilterItem::find($filterItem->filter_item_id);
                    $items[]             = $CaseStudyFilterItem->name;
                }

                $toViewFilterArray[] = [
                    'name'  => $CaseStudyFilter->name,
                    'items' => $items,
                ];
            }

            /*
             $filter_array = [];
            foreach ($selectedFilters as $filter)
            {
                $chosenFilterItemsArray = CaseStudyFilterItemRelationship::where('case_study_id', '=', $caseStudyID)->where('filter_id', '=', $filter->id)->get(['filter_item_id'])->toArray();
                $chosenFilterItems      = "";
                $previewFilterArray     = [];
                foreach ($chosenFilterItemsArray as $chosenFilterItem)
                {
                    $CaseStudyFilterItem = CaseStudyFilterItem::find($chosenFilterItem['filter_item_id']);
                    $chosenFilterItems .= $CaseStudyFilterItem->name . ", ";
                    $previewFilterArray[$filter->id][] = $chosenFilterItem['filter_item_id'];
                }

                $filter_array[]            = $previewFilterArray;
                $filter->chosenFilterItems = $chosenFilterItems;
                $filter->chosenFilterItems = $chosenFilterItems == "" ? "" : substr($chosenFilterItems, 0, -2);;
            }
             */

            foreach ($allListingImages as $listingImage) {
                $uploadCategory             = UploadCategory::find($listingImage->category);
                $listingImage->categoryName = $uploadCategory->name;
            }

            //$caseStudy->filter_array      = json_encode($filter_array);

            $allModules         = $this->getLocationModules(9, "Hyfn\\Models\\CaseStudy", $caseStudy->id, 1);
            $initialModuleOrder = "";
            foreach ($allModules as $module) {
                $moduleObject = $module['moduleObject'];
                $initialModuleOrder .= "&location-9-module-relationship[]=" . $moduleObject->relationshipOrder;
            }

            $viewData = [
                'cmsName'            => $this->cmsName,
                'copyrightName'      => $this->copyrightName,
                'caseStudy'          => $caseStudy,
                'allClients'         => $allClients,
                'clientList'         => $clientList,
                'selectedFilters' => $toViewFilterArray,
                'listingImages'      => $allListingImages,
                'allowCaseStudyMgt'  => ($this->loggedInUser->can("manage_case_studies") ? 1 : 0),
                'initialModuleOrder' => $initialModuleOrder,
                'allModules'         => $allModules,
                'locationID'         => 9,
                'moduleTypes'        => $this->getModuleTypesForSelect(),
                'entityType'         => 'Hyfn\\Models\\CaseStudy',
                'entityID' => $caseStudy->id,
            ];

            return View::make('admin.case_studies.modules.index', $viewData);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            // Get form data, create new case study
            $caseStudy = new CaseStudy();

            $caseStudy->headline       = Input::get('headline');
            $caseStudy->description    = Input::get('description');
            $caseStudy->keywords       = Input::get('keywords');
            $caseStudy->listing_pic_id = (!is_null(Input::get('listing_pic_id')) && Input::get('listing_pic_id') != '' ? Input::get('listing_pic_id') : 0);
            $caseStudy->client_name = Input::get('client_name');
            $caseStudy->is_live = 0;
            $allFilters                = CaseStudyFilter::all();

            $chosenFilterArray = [];
            foreach ($allFilters as $filter) {
                $postedValueArray = Input::get('filter_id_' . $filter->id);
                if (is_array($postedValueArray) && count($postedValueArray) >= 1) {
                    $chosenFilterArray[$filter->id] = $postedValueArray;
                }
            }

            if ($caseStudy->isModelValid()) {
                $caseStudy->save();

                // Add Filters
                foreach ($chosenFilterArray as $filterID => $filterArray) {
                    foreach ($filterArray as $filterItemID) {
                        $CaseStudyFilterRelationship                 = new CaseStudyFilterItemRelationship();
                        $CaseStudyFilterRelationship->case_study_id  = $caseStudy->id;
                        $CaseStudyFilterRelationship->filter_id      = $filterID;
                        $CaseStudyFilterRelationship->filter_item_id = $filterItemID;
                        $CaseStudyFilterRelationship->save();
                    }
                }

                return Redirect::to('admin/gallery/' . $caseStudy->id . '/add-modules')->with('success_message',
                    trans('admin.case-studies.create_success'));
            } else {
                return Redirect::to('admin/gallery/create')->withInput(Input::all())->withErrors($caseStudy->getErrors());
            }
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            $caseStudy        = CaseStudy::find($id);
            $allClients       = Client::all();
            $allFilters       = CaseStudyFilter::orderBy('order', 'asc')->get();
            $allListingImages = Upload::where('category', '=', 11)->get();
            $clientList       = [0 => 'Choose A Client'];
            foreach ($allClients as $client) {
                $clientList[$client->id] = $client->name;
            }

            foreach ($allFilters as $filter) {
                $allFilterItems         = CaseStudyFilterItem::where('filter_id', '=', $filter->id)->orderBy('order',
                    'asc')->get();
                $chosenFilterItemsArray = CaseStudyFilterItemRelationship::where('case_study_id', '=',
                    $id)->where('filter_id', '=', $filter->id)->get(['filter_item_id'])->toArray();
                $chosenFilterItems      = [];
                foreach ($chosenFilterItemsArray as $chosenFilterItem) {
                    $chosenFilterItems[] = $chosenFilterItem['filter_item_id'];
                }

                $filterItemsArray = [];
                foreach ($allFilterItems as $filterItem) {
                    $filterItemsArray[] = [
                        'id'   => $filterItem->id,
                        'name' => $filterItem->name,
                    ];
                }

                $filter->items             = $filterItemsArray;
                $filter->chosenFilterItems = $chosenFilterItems;
            }

            foreach ($allListingImages as $listingImage) {
                $uploadCategory             = UploadCategory::find($listingImage->category);
                $listingImage->categoryName = $uploadCategory->name;
            }

            $caseStudy->chosenFilterItems = [];

            $viewData = [
                'cmsName'           => $this->cmsName,
                'copyrightName'     => $this->copyrightName,
                'caseStudy'         => $caseStudy,
                'allClients'        => $allClients,
                'clientList'        => $clientList,
                'allFilters'        => $allFilters,
                'listingImages'     => $allListingImages,
                'allowCaseStudyMgt' => ($this->loggedInUser->can("manage_case_studies") ? 1 : 0),
            ];

            return View::make('admin.case_studies.edit', $viewData);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            $caseStudy = CaseStudy::find($id);

            $caseStudy->headline       = Input::get('headline');
            $caseStudy->description    = Input::get('description');
            $caseStudy->keywords       = Input::get('keywords');
            $caseStudy->listing_pic_id = Input::get('listing_pic_id');
            $caseStudy->client_name = Input::get('client_name');
            $caseStudy->is_live = Input::get('is_live');
            $allFilters                = CaseStudyFilter::all();

            $chosenFilterArray = [];
            foreach ($allFilters as $filter) {
                $postedValueArray = Input::get('filter_id_' . $filter->id);
                if (is_array($postedValueArray) && count($postedValueArray) >= 1) {
                    $chosenFilterArray[$filter->id] = $postedValueArray;
                }
            }

            if ($caseStudy->isModelValid()) {
                $caseStudy->save();

                // Delete Old Filters
                CaseStudyFilterItemRelationship::where('case_study_id', '=', $caseStudy->id)->delete();

                // Add Filters
                foreach ($chosenFilterArray as $filterID => $filterArray) {
                    foreach ($filterArray as $filterItemID) {
                        $CaseStudyFilterRelationship                 = new CaseStudyFilterItemRelationship();
                        $CaseStudyFilterRelationship->case_study_id  = $caseStudy->id;
                        $CaseStudyFilterRelationship->filter_id      = $filterID;
                        $CaseStudyFilterRelationship->filter_item_id = $filterItemID;
                        $CaseStudyFilterRelationship->save();
                    }
                }

                return Redirect::to('admin/gallery/' . $caseStudy->id . '/add-modules')->with('success_message',
                    trans('admin.case-studies.update_success'));
            } else {
                return Redirect::to('admin/gallery/' . $id . '/edit')->withInput(Input::all())->withErrors($caseStudy->getErrors());
            }
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $resourceCount = CaseStudy::all()->count();

            if ($resourceCount > 1) {
                $CaseStudy     = CaseStudy::find($id);
                $CaseStudyName = $CaseStudy->headline;

                if ($CaseStudy->id >= 1) {
                    CaseStudy::destroy($id);
                    $affectedFilterRelationships = CaseStudyFilterItemRelationship::where('case_study_id', '=',
                        $id)->delete();

                    return Redirect::to('admin/gallery')
                        ->with('success_message', "Case Study: " . $CaseStudyName . " has been deleted successfully.");
                } else {
                    return Redirect::to('admin/gallery')
                        ->with('error_message', "Case Study was not deleted.");
                }
            } else {
                return Redirect::to('admin/gallery')
                    ->with('error_message', trans('admin.case-studies.delete_min'));
            }
        }


        public function saveIndexOrder()
        {
            $x = 1;
            foreach (explode("&index-row[]=", substr(Input::get("order"), 13)) as $id) {
                $Resource        = CaseStudy::find($id);
                $Resource->order = $x;
                $Resource->save();
                $x++;
            }

            return Response::make
            (
                [
                    'status' => 'success'
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }


        public function publishOrHideCaseStudy()
        {
            $CaseStudy = CaseStudy::find(Input::get('caseStudyID'));

            $doLiveModulesExist = false;

            $caseStudyModules = ModuleLocationRelationship::where("entity_id", "=", $CaseStudy->id)->
            where("entity_type", "=", "Hyfn\\Models\\CaseStudy")->
            get(['module_id', 'module_type']);

            foreach ($caseStudyModules as $module) {
                $ModelName = $module->module_type;
                $modelID   = $module->module_id;
                $Model     = $ModelName::find($modelID);
                if ($Model->is_live == 1) {
                    $doLiveModulesExist = true;
                    break;
                }
            }

            if ($CaseStudy->is_live == 0 && $doLiveModulesExist) {
                $CaseStudy->is_live = 1;
                $succes             = 'success';
                $message            = $CaseStudy->headline . " is now live.";
            } elseif ($CaseStudy->is_live == 0 && !$doLiveModulesExist) {
                $CaseStudy->is_live = 0;
                $succes             = 'error';
                $message            = "No live modules associated with " . $CaseStudy->headline;
            } else {
                $CaseStudy->is_live = 0;
                $succes             = 'success';
                $message            = $CaseStudy->headline . " has been depublished.";
            }

            $CaseStudy->save();

            return Response::make
            (
                [
                    'status'  => $succes,
                    'message' => $message,
                    'is_live' => $CaseStudy->is_live,
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }


        public function sessionPreviewListing()
        {
            if (Input::get('listing_PicID') >= 1) {
                $Upload     = Upload::find(Input::get('listing_PicID'));
                $listingPic = $Upload->download_folder . $Upload->download_file;
            } else {
                $listingPic = "/files/uploads/201410/CaseStudyDefault.png";
            }
            Session::put('preview_listing_pic', $listingPic);
            Session::put('preview_headline', Input::get('headline'));
            Session::put('preview_description', Input::get('description'));

            return Response::make
            (
                [
                    'status' => 'success',
                ],
                200,
                [
                    'Content-Type'  => 'application/json',
                    'Cache-Control' => 'no-cache, must-revalidate',
                ]
            );
        }


        public function previewListing()
        {
            View::share('menuProducts', []);
            View::share('currentPath', '');
            View::share('marqueeLandingTitle', "");
            View::share('sourceText', '');
            $pulledDesc = Session::pull('preview_description');
            return View::make('home.case-studies-listing',
                ['pulledDesc' => strlen($pulledDesc) >= 135 ? substr($pulledDesc, 0, 132) . '...' : $pulledDesc]);
        }
    }
