
      <div class="global-footer-outer">

        <div class="footer-source-info">

          @if($sourceText != "")
          <div class="footer-source-info-inner">
            {{$sourceText}}
          </div>
          @endif
        </div>


        <footer class="global-footer">
          <div class="legal">
          &copy; 2015 Dow Jones &amp; Company, Inc. All Rights Reserved.
          </div>

          <div class="links">
            <ul>
              <li>
                <a href="http://online.wsj.com" target="_blank">WSJ.com</a>
              </li>
              <li>
                <a href="http://online.wsj.com/public/page/privacy_policy.html" target="_blank">Privacy Policy</a>
              </li>
              <li>
                <a href="http://new.dowjones.com/about/about-us/" target="_blank">About Dow Jones</a>
              </li>
                <li>
                    <a href="http://wsjmediakit.com/files/uploads/201410/2015_Terms_and_Conditions.pdf" target="_blank">Terms
                        and Conditions</a>
                </li>
            </ul>
          </div>
        </footer>

      </div>


    </div>

    <!-- pdf modal -->

    <div class="modal-overlay" id="js-modal-overlay">

      <div class="modal-outer">

        <div class="modal-inner">

          <div class="modal-content">

            <button class="modal-close-button" id="js-modal-close-button"></button>

            <div class="modal-header">
              <p>Please provide your contact information so WSJ. Insights can keep you updated with the latest research.</p>
              <p class="sub-text">All fields are required.</p>
            </div>

            <form action="" method="post" id="js-pdf-request-form" autocomplete="off" novalidate>

              <div class="form-wrapper">

                <div class="form-row">
                  <input type="text" class="shadowed input-text" placeholder="First Name" name="first_name" required autocomplete="off" />
                </div>

                <div class="form-row">
                  <input type="text" class="shadowed input-text" placeholder="Last Name" name="last_name" required autocomplete="off" />
                </div>

                <div class="form-row">
                  <input type="text" class="shadowed input-text" placeholder="Company" name="company" required autocomplete="off" />
                </div>

                <div class="form-row">
                  <input type="text" class="shadowed input-text" placeholder="Title" name="title" required autocomplete="off" />
                </div>

                <div class="form-row">
                  <input type="email" class="shadowed input-text" placeholder="Email Address" name="email" required autocomplete="off" />
                </div>

                <div class="form-row">
                  <input type="submit" value="View Report" class="input-submit shadowed" disabled='disabled' id="js-form-submit"  />

                  <div class="submit-logo">
                    WSJ. Insights
                  </div>

                </div>

              </div>

            </form>

          </div>

        </div>

      </div>

    </div>

    <!-- /pdf modal -->

    <script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', '{{getenv("GA_property")}}', 'auto');
      ga('send', 'pageview');
    </script>

    <!-- build:js assets/js/bundle.js -->
    @if (getenv('APPLICATION_ENV') === 'production')
    <script src="/assets/frontend/js/bundle.min.js"></script>
    @else
    <script src="/assets/frontend/js/bundle.js"></script>
    @endif
    <!-- endbuild -->

    <div class="ie-message" id="ie-message">
      <p><b>Please upgrade your browser.</b>  For the best viewing experience, please upgrade your browser below.</p>
      <div class="browser-links">
        <a href="https://www.mozilla.org/en-US/firefox/new/" class="browser-link firefox" target="_blank">&nbsp;</a>
        <a href="https://www.google.com/intl/en/chrome/browser/" class="browser-link chrome" target="_blank">&nbsp;</a>
        <a href="http://support.apple.com/downloads/#safari" class="browser-link safari" target="_blank">&nbsp;</a>
        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" class="browser-link ie" target="_blank">&nbsp;</a>
      </div>
    </div>

  </body>



</html>

