<!DOCTYPE html>
<!--[if lt IE 8]>      <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9 ie-8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 ie-9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="author" content="Chukky Nze">
  <meta name="author" content="Mike Curts">
  <title>WSJ. Media Kit</title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta property="og:title" content="WSJ. Media Kit"/>
  <meta property="og:site_name" content="WSJ. Media Kit"/>
  <meta property="og:description" content="The WSJ. Media Kit"/>
  
  <link rel="icon" href="/assets/frontend/img/favicon.ico" type="image/x-icon" />

  <link href="//cloud.webtype.com/css/857057c7-b2f6-4175-9a01-1fdd4fba8511.css" rel="stylesheet" type="text/css" />

  <!-- build:css assets/frontent/styles/main.css -->
  <link rel="stylesheet" href="/assets/frontend/styles/main.css">
  <!-- endbuild --> 

  <!--[if lte IE 8]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
  <![endif]-->

  @section('css')
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
  @show

</head>

<body data-path="{{$currentPath}}" class="@if(isset($specialClass)){{$specialClass}}@endif {{$currentPath}}">

    <div id="wjs-app" class="wjs-app">

      <div class="global-header-outer">
     
        <header class="global-header">
          <a href="/" class="global-logo" style="pointer-events: none; cursor: default;">
            <div class="site-logo"></div>
          </a>

          <div class="header-right">

            <div class="search-trigger" id="js-search-trigger">
                <i class="search-icon"></i>
            </div>

            <nav class="primary-menu">
                <ul>
                    @foreach($menuProducts as $key => $parent)

                        @if($key < 4)

                    <li class="{{$parent['listClasses']}}">
                        <a href="/{{$parent['link']}}" class="{{$parent['linkClasses']}}" style="pointer-events: none; cursor: default;">{{$parent['name']}}</a>

                        @if(is_array($parent['subLinks']) && count($parent['subLinks'] >= 1))

                            <ul class="primary-subnav">

                                @foreach($parent['subLinks'] as $subLink)

                                    <li class="{{$subLink['listClasses']}}">
                                        <a href="/{{$parent['link']}}/{{$subLink['link']}}" class="{{$subLink['linkClasses']}}">{{$subLink['name']}}</a>
                                    </li>

                                @endforeach

                            </ul>

                        @endif
                    </li>

                        @endif

                @endforeach
                </ul>
            </nav>

          </div>

        </header>

        <div class="global-search-block is-collapsed is-empty" id="js-search-block">
          <input type="text" class="global-search input-text" id="js-global-search" placeholder="Click to Search" />
          <div class="clear-search" id="js-clear-search">X</div>
        </div>

        <div class="global-subnav">
          <ul>
            <li>
              <a href="https://classifieds.wsj.com/" style="pointer-events: none; cursor: default;" target="_blank">Classifieds</a>
            </li>
            <li>
              <a href="https://www.epic.dowjones.com" style="pointer-events: none; cursor: default;" target="_blank">Submit Ad</a>
            </li>
            <li>
              <a href="/contact" style="pointer-events: none; cursor: default;">Contact</a>
            </li>
          </ul>

        </div>

      </div>