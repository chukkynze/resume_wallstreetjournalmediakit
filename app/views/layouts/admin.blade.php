<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Dashboard :: Base Admin</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">

	@section('css')
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

		<link href="/assets/admin/css/bootstrap.min.css" rel="stylesheet">
		<link href="/assets/admin/css/bootstrap-responsive.min.css" rel="stylesheet">

		<link href="/assets/admin/css/font-awesome.min.css" rel="stylesheet">

		<link href="/assets/admin/css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">

		<link href="/assets/admin/css/base-admin-3.css" rel="stylesheet">
		<link href="/assets/admin/css/base-admin-3-responsive.css" rel="stylesheet">

		<link href="/assets/admin/css/pages/dashboard.css" rel="stylesheet">
		<link href="/assets/admin/js/plugins/msgbox/jquery.msgbox.css" rel="stylesheet">


		<link href="/assets/admin/css/admin-frontend.css" rel="stylesheet">
		<link href="/assets/admin/css/custom.css" rel="stylesheet">

  		<link href="//cloud.webtype.com/css/857057c7-b2f6-4175-9a01-1fdd4fba8511.css" rel="stylesheet" type="text/css" />
  		<link href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>

		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	@show
</head>

<body>
@section('body')

<nav class="navbar navbar-inverse" role="navigation">

	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<i class="icon-cog"></i>
			</button>
			<a class="navbar-brand" href="/admin">{{ $cmsName }}</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">

					@if( !empty($loggedInUser))
						<a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							{{$loggedInUser->username}}
							<b class="caret"></b>
						</a>


						<ul class="dropdown-menu">
							<li><a href="/admin/users/{{$loggedInUser->id}}/edit">Account Settings</a></li>
							<li class="divider"></li>
							<li><a href="/admin/logout">Logout</a></li>
						</ul>
					@endif
				</li>
			</ul>



			<div class="navbar-right">
                <a class="navbar-brand" href="/" target="_blank" style="font-size:13px;">View Site</a>
		    </div>

		</div><!-- /.navbar-collapse -->
	</div> <!-- /.container -->
</nav>



@if( !empty($loggedInUser))

<div class="subnavbar">

	<div class="subnavbar-inner">

		<div class="container">

			<a href="javascript:;" class="subnav-toggle" data-toggle="collapse" data-target=".subnav-collapse">
				<span class="sr-only">Toggle navigation</span>
				<i class="icon-reorder"></i>

			</a>

			<div class="collapse subnav-collapse">
				<ul class="mainnav">

					<li class="@if ($activeSection == "Home") active @endif">
						<a href="/admin">
							<i class="icon-home"></i>
							<span>Home</span>
						</a>
					</li>

					<li class="dropdown  @if ($activeSection == "Users") active @endif ">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<span>Users</span>
							<b class="caret"></b>
						</a>

						<ul class="dropdown-menu">
							<li><a href="/admin/users">All Users</a></li>
							<li><a href="/admin/users/{{$loggedInUser->id}}/edit">My Account</a></li>
							<li><a href="/admin/users/create">Create New Account</a></li>
							<li><a href="/admin/roles">User Roles</a></li>
							<li><a href="/admin/permissions">Role Permissions</a></li>
						</ul>
					</li>

					<li class="dropdown  @if ($activeSection == "Tasks") active @endif ">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-tasks"></i>
							<span>Reports</span>
							<b class="caret"></b>
						</a>

						<ul class="dropdown-menu">
							<li><a href="/admin/tasks/get-leads">Download Leads</a></li>
						</ul>
					</li>

					<li class="dropdown  @if ($activeSection == "Uploads") active @endif ">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-cloud-upload"></i>
							<span>Uploads</span>
							<b class="caret"></b>
						</a>

						<ul class="dropdown-menu">
							<li><a href="/admin/uploads">Uploads</a></li>
							<li><a href="/admin/upload-categories">Upload Categories</a></li>
						</ul>
					</li>


					<li class="nav-divider"></li>


					<li class="dropdown  @if ($activeSection == "Page Content") active @endif ">
						<a href="/admin/page-content/home-pages">
							<i class="icon-circle-arrow-down"></i>
							<span>Home Page</span>
						</a>
					</li>

					<li class="dropdown  @if ($activeSection == "Products") active @endif ">
						<a href="/admin/page-content/product-pages">
							<i class="icon-sitemap"></i>
							<span>Product Page</span>
						</a>
					</li>

					<li class="dropdown  @if ($activeSection == "Insights") active @endif ">
						<a href="/admin/page-content/insight-pages">
							<i class="icon-eye-open"></i>
							<span>Insights</span>
						</a>
					</li>

					<li class="dropdown  @if ($activeSection == "Custom Studios") active @endif ">
						<a href="/admin/page-content/studios-pages">
							<i class="icon-archive"></i>
							<span>Custom Studios</span>
						</a>
					</li>

					<li class="dropdown  @if ($activeSection == "Rates") active @endif ">
						<a href="/admin/rate-products">
							<i class="icon-dollar"></i>
							<span>Rates & Specs</span>
						</a>
					</li>

					<li class="dropdown  @if ($activeSection == "Case Study") active @endif ">
						<a href="/admin/gallery">
							<i class="icon-th-list"></i>
							<span>Gallery</span>
						</a>
					</li>

					<li class="dropdown  @if ($activeSection == "Contact") active @endif ">
                        <a href="/admin/contacts">
							<i class="icon-inbox"></i>
							<span>Contacts</span>
						</a>
					</li>

                    {{-- Hidden for now
					<li class="dropdown  @if ($activeSection == "Help") active @endif ">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-question-sign"></i>
							<span>Help</span>
						</a>
					</li>
                    --}}
				</ul>
			</div> <!-- /.subnav-collapse -->

		</div> <!-- /container -->

	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
@endif

<div class="main">

	<div class="container">

	@if(Session::get('error_message'))
	<div class="row">
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<strong>{{ Session::pull('error_message') }}</strong>
		</div>
	</div>
	@endif


	@if(Session::get('warning_message'))
	<div class="row">
		<div class="alert alert-warning alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<strong>{{ Session::pull('warning_message') }}</strong>
		</div>
	</div>
	@endif


	@if(Session::get('success_message'))
	<div class="row">
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<strong>{{ Session::pull('success_message') }}</strong>
		</div>
	</div>
	@endif

		@section('content')
		<div class="row">
			<div class="widget stacked">

				<div class="widget-header">
					<i class="icon-file"></i>
					<h3>Welcome</h3>
				</div> <!-- /widget-header -->

				<div class="widget-content">
					<p>
						<strong>Welcome to the CMS for The Wall Street Journal Media Kit!</strong><br><br>
					</p>

					<ul>
						<li>
							<a href="https://www.google.com/analytics/web/?hl=en#report/visitors-overview/a6913302w13292037p13974705/" target="_blank">Google Analytics</a>
							- Site Analytics and Reporting
						</li>
					</ul>
				</div> <!-- /widget-content -->

			</div> <!-- /widget -->

		</div> <!-- /row -->
		@show

	</div> <!-- /container -->

</div> <!-- /main -->

@show

<div class="footer">

	<div class="container">

		<div class="row">

			<div id="footer-copyright" class="col-md-6">
				&copy; <?php echo date("Y"); ?> {{ $copyrightName }}.
			</div> <!-- /span6 -->

			<div id="footer-terms" class="col-md-6">
				CMS By <a href="http://www.hyfn.com" target="_blank">Hyfn</a>
			</div> <!-- /.span6 -->

		</div> <!-- /row -->

	</div> <!-- /container -->

</div> <!-- /footer -->


@section('js')
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/assets/admin/js/libs/jquery-1.9.1.min.js"></script>
<script src="/assets/admin/js/libs/jquery-ui.min.js"></script>
<script src="/assets/admin/js/libs/bootstrap.min.js"></script>

<script src="/assets/admin/js/plugins/lightbox/jquery.lightbox.min.js"></script>
<script src="/assets/admin/js/plugins/msgbox/jquery.msgbox.min.js"></script>

<script src="/assets/admin/js/Application.js"></script>
<script src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
<!-- CDN hosted by Cachefly -->
<!-- <script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script> -->

<script src="/assets/admin/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
    selector:'textarea.editme',
    menubar: 'false',
    content_css: ["/assets/frontend/styles/admin-frontend.css?" + new Date().getTime(), '//cloud.webtype.com/css/857057c7-b2f6-4175-9a01-1fdd4fba8511.css'],
    theme: 'modern',
    force_hex_style_colors : true,
    plugins: ['link textcolor wsjimage paste'],
    paste_auto_cleanup_on_paste : true,
    paste_remove_styles: true,
    paste_remove_styles_if_webkit: true,
    paste_strip_class_attributes: true,
    image_dimensions: false,
    image_description: false,
    toolbar1: '| undo | redo |',
    toolbar2: '| paragraph | headline | subhead | stat',
    toolbar3: '| forecolor | bold | italic | link | colors | wsjimage',
            textcolor_map: [
        "3eb4e4", "Light Blue Text",
        "58595b", "Grey Text",
        "000000", "Black Text"
            ],
    setup: function(editor) {

      editor.addButton('subhead', {
          text: 'Add Subhead (H3)',
          icon: false,
          onclick: function() {
              editor.insertContent('<h3>This is a Subhead, change this to your desired text.</h3>');
          }
      });

      editor.addButton('headline', {
          text: 'Add Headline (h2)',
          icon: false,
          onclick: function() {
              editor.insertContent('<h2>This is a Headline, change this to your desired text.</h2>');
          }
      });

      editor.addButton('paragraph', {
          text: 'Add Paragraph (p)',
          icon: false,
          onclick: function() {
              editor.insertContent('<p>This is a Paragraph, change this to your desired text.</p>');
          }
      });

      editor.addButton('stat', {
          text: 'Add a Stat',
          icon: false,
          onclick: function() {
              editor.insertContent('<p class="feature-text">This is a Stat, change this to your desired text, then select text and change colors.</p>');
          }
      });

  }

});



</script>

@show


</body>
</html>
