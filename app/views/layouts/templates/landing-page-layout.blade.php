
@include('layouts.home.partials.header')

<div class="landing-layout">

  <div class="landing-marquee-outer js-viewport-scale">

    <div class="landing-marquee" style="background-image: url('@if($marqueeLandingImgURL != "") {{$marqueeLandingImgURL}} @else /assets/frontend/img/marquee/wsj-products-marquee.jpg @endif')">
    </div>

    <div class="standard-marquee-outer">

      <div class="marquee-left">&nbsp;</div>

      <div class="marquee-middle">
        <div class="landing-marquee-title">
          {{$marqueeLandingTitle}}
        </div>
      </div>

      <div class="marquee-right">&nbsp;</div>

    </div>

  </div>


  <section class="content-main">

    <div class="global-crumbs">
      <ul>
        <li>
          <a href="/">Home</a>
        </li>
        <li>
          <a href="products.html" class="is-active">Products</a>
        </li>
      </ul>
    </div>

    @section('content')
    @show

  </section>

</div>

@include('layouts.home.partials.footer')


