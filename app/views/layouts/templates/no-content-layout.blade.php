
@include('layouts.home.partials.header')


<div class="landing-layout">

  <div class="landing-marquee-outer">
    <div class="landing-marquee" style="background-image: url('/assets/frontend/img/marquee/wsj-contact-marquee.jpg')">
    </div>
    <div class="landing-marquee-title">
      Contact
    </div>



  </div>

</div>

<section class="content-main" style="width: 900px;">

    @section('content')
    @show

</section>

@include('layouts.home.partials.footer')


