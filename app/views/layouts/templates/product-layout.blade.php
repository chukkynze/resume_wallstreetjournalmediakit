
@include('layouts.home.partials.header')

<div class="standard-layout">

  <section class="content-main">

    @section('content')
    @show

  </section>

</div>


@include('layouts.home.partials.footer')


