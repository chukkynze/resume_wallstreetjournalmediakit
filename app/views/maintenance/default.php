<!DOCTYPE html>
<!--[if lt IE 8]>      <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9 ie-8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 ie-9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>WSJ. Media Kit</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="apple-mobile-web-app-capable" content="yes">

        <link rel="icon" href="/assets/frontend/img/favicon.ico" type="image/x-icon" />

        <link href="//cloud.webtype.com/css/857057c7-b2f6-4175-9a01-1fdd4fba8511.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="/assets/frontend/styles/main.css">

        <!--[if lte IE 8]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
        <![endif]-->

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    </head>

    <body data-path="404" class="dark-logo 404">
        <div id="wjs-app" class="wjs-app">
            <div class="global-header-outer">
                <header class="global-header">
                    <div class="site-logo"></div>
                </header>

                <div class="global-search-block is-collapsed is-empty" id="js-search-block">
                    <input type="text" class="global-search input-text" id="js-global-search" placeholder="Click to Search" />
                    <div class="clear-search" id="js-clear-search">X</div>
                </div>
            </div>
            <section class="content-main">
                <div class="error-container">
                    <br /><br /><br /><br />
                    <h2 class="centered-text">Just Checking Under the Hood</h2>
                    <p class="centered-text">
                        Sorry, we are undergoing scheduled maintenance. Please try back in a few minutes.
                        <br>
                        <br> We apologize for the inconvenience.
                    </p>
                </div>
            </section>

            <div class="global-footer-outer">
                <div class="footer-source-info">
                </div>

                <footer class="global-footer">
                    <div class="legal">
                        &copy; 2015 Dow Jones &amp; Company, Inc. All Rights Reserved.
                    </div>
                </footer>
            </div>
        </div>

        <div class="ie-message" id="ie-message">
            <p><b>Please upgrade your browser.</b> For the best viewing experience, please upgrade your browser below.</p>
            <div class="browser-links">
                <a href="https://www.mozilla.org/en-US/firefox/new/" class="browser-link firefox" target="_blank">&nbsp;</a>
                <a href="https://www.google.com/intl/en/chrome/browser/" class="browser-link chrome" target="_blank">&nbsp;</a>
                <a href="http://support.apple.com/downloads/#safari" class="browser-link safari" target="_blank">&nbsp;</a>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" class="browser-link ie" target="_blank">&nbsp;</a>
            </div>
        </div>
    </body>
</html>
