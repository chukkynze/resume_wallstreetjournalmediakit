@extends('layouts.admin')
@section('content')




<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-th-list"></i>
			<h3>Role Permissions
			<a class="btn btn-xs btn-success" data-toggle="modal" href="#addPermissionModal">Add New Permission</a>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Permission</th>

						@if ($allowPermissionMgt == 1)
							<th class="td-actions">Assign to Role</th>
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody>
				<?php $x=0; ?>
				@foreach ($permissions as $permission)
					<tr>
						<td><?php $x++; echo $x; ?></td>
						<td>{{$permission->name}}</td>

						@if ($allowPermissionMgt == 1)
							<td class="td-actions">
								<a class="btn btn-xs btn-primary roleModalLink" data-toggle="modal" data-permission-id="{{$permission->id}}" data-permission-name="{{$permission->display_name}}">
								    <i class="btn-icon-only icon-edit"></i>
								</a>
							</td>
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/permissions/' . $permission->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-permission')) }}
									<button type="submit" class="btn btn-xs btn-danger">
										<i class="btn-icon-only icon-remove"></i>
							        </button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>


<!-- /Add Permission Model -->
<div id="addPermissionModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add a New Permission</h4>
            </div>

            <div class="modal-body">
                <p>Permission Name: <br><input id="newPermissionName" type="text" value=""></p>
                <p>(Permission names must be lowercase, have no spaces, can use underscores but not dashes) <br><br></p>
                <p>Permission Display Name: <br><input id="newPermissionDisplayName" type="text"></p>
                <div>
                    <button type="button" class="btn btn-primary pull-right" id="saveNewPermissionButton">Save</button>
                </div>
                <br />
            </div>
        </div>
    </div>
</div>


<!-- /Roles Form Model -->
<div id="roleModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Assign Permissions to Roles</h4>
                <input type="hidden" id="permissionId" value="">
            </div>

            <div class="modal-body">
                <p>Assign the <strong><span id="permissionName"></span></strong> permission to the  <select id="permissionRole">
                    @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->displayValue}}</option>
                    @endforeach
                </select> role. </p>
                <div>
                    <button type="button" class="btn btn-primary pull-right" id="saveAssignedRoleButton">Save</button>
                </div>
                <br />
            </div>
        </div>
    </div>
</div>
@stop