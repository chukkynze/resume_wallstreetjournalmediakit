@extends('layouts.admin')
@section('content')
<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-file-text-alt"></i>
				<h3>Create New Upload</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/uploads', 'files'=>true,  'method' => 'post', 'id' => 'create-upload', 'class' => 'form-horizontal col-md-12')) }}
						@include('admin/uploads/form')
				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop