@section('summarymodule2')

<tr id="case-study-module-summarymodule2"   data-siteProduct-name="case-study-module-summarymodule2">
                    <td>
                        <div class="form-group">

                            <hr>

                            <div class="col-md-1"></div>

                            <div class="col-md-11">
                                {{ Form::checkbox('summary_module2_enabled', 1, ($caseStudy->summary_module2_enabled == 1 ? true : null), []) }} Enable/Disable this Module
                                <table class="table table-striped table-bordered">
                                    <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
                                        <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to show/collapse modules.">
                                            <th>
                                                Summary Module 2
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
			                                    <label for="summary_module2_html" class="col-md-3">Summary Text</label>
			                                    <br>
			                                    {{ Form::textarea('summary_module2_html', $caseStudy->summary_module2_html, array('class' => 'form-control editme', 'id' => 'summary_module2_html')) }}
				                                <span class="text-danger">{{ $errors->first('summary_module2_html') }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <div>
                                                    <label for="summary_module1_upload_id" class="col-md-3"><span><a href="/admin/uploads/create" target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Available Summary Images</label>
                                                    <br>
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Name</th>
                                                                <th>Category</th>
                                                                <th>Content File</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($summaryImages as $summaryImage)
                                                            <tr>
                                                                <td>{{ Form::radio('summary_module2_upload_id',$summaryImage->id, ($summaryImage->id  == $caseStudy->summary_module2_upload_id ? true : null), []) }}</td>
                                                                <td>{{$summaryImage->name}}</td>
                                                                <td>{{$summaryImage->categoryName}}</td>
                                                                <td><a href="{{$summaryImage->download_folder}}{{$summaryImage->download_file}}" target="_blank">{{$summaryImage->download_file}}</a></td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <span class="text-danger">{{ $errors->first('summary_module2_upload_id') }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <div>
                                                    <label for="summary_module2_pic_orientation" class="col-md-3">Summary Pic Orientation</label>
                                                    <br>
                                                    {{ Form::select
                                                    (
                                                        'summary_module2_pic_orientation',
                                                        [
                                                            'Right' =>  'Right',
                                                            'Left'  =>  'Left',
                                                        ],
                                                        ($caseStudy->summary_module2_pic_orientation ?: 'Right') ,
                                                        [
                                                            'class' => 'form-control',
                                                            'id'    => 'summary_module2_pic_orientation'
                                                        ]
                                                    ) }}
                                                </div>
                                                <span class="text-danger">{{ $errors->first('summary_module2_pic_orientation') }}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- /controls -->

                        </div> <!-- /control-group -->

                    </td>
                </tr>

@show