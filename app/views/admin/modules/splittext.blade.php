@section('splittext')

<tr id="case-study-module-splittext"        data-siteProduct-name="case-study-module-splittext">
                    <td>
                        <div class="form-group">

                            <hr>

                            <div class="col-md-1"></div>

                            <div class="col-md-11">
                                {{ Form::checkbox('split_text_module_enabled', 1, ($caseStudy->split_text_module_enabled == 1 ? true : null), []) }} Enable/Disable this Module
                                <table class="table table-striped table-bordered">
                                    <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
                                        <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to show/collapse modules.">
                                            <th>
                                                Split Text Module
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
			                                    <label for="split_text" class="col-md-3">Text to Split</label>
			                                    {{ Form::textarea('split_text', $caseStudy->split_text_module_col1 . " " . $caseStudy->split_text_module_col2, array('class' => 'form-control', 'id' => 'split_text')) }}
				                                <span class="text-danger">{{ $errors->first('split_text') }}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- /controls -->

                        </div> <!-- /control-group -->

                    </td>
                </tr>

@show