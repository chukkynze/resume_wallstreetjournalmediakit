@section('text-column')
<div class="form-group">
    <div class="col-md-1"></div>
    <label class="col-md-2" for="upload_id"><span><a href="/admin/uploads/create" target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Heading Images<br><button class="btn btn-xs btn-primary" id="clearHeadingIconList">Clear</button></label>
    <div class="col-md-8">
        <div>
            <table class="table table-striped table-bordered">
                <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
                    <tr>
                        <th>
                            <span class="icon-down" style="display: none;"><i class="btn-icon-only icon-chevron-down"></i></span>
                            <span class="icon-right"><i class="btn-icon-only icon-chevron-right"></i></span>
                        </th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Content File</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($headingImages as $headingImage)
                    <tr>
                        <td>{{ Form::radio('upload_id', $headingImage->id, ($headingImage->id  == $formModule->upload_id ? true : null), ['class' => 'headingIconList']) }}</td>
                        <td>{{$headingImage->name}}</td>
                        <td>{{$headingImage->categoryName}}</td>
                        <td><a href="{{$headingImage->download_folder}}{{$headingImage->download_file}}" target="_blank">{{$headingImage->download_file}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <span class="text-danger">{{ $errors->first('upload_id') }}</span>
    </div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="form-group">
    <div class="col-md-1"></div>
    <label for="body" class="col-md-2">Body</label>
    <div class="col-md-8">
        {{ Form::textarea('body', $formModule->body, array('class' => 'form-control editme', 'id' => 'body')) }}
        <span class="text-danger">{{ $errors->first('body') }}</span>
    </div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="form-group">
    <div class="col-md-1"></div>
    <label for="body" class="col-md-2">
    Text Columns<br>
    Click the buttons to Add/Remove Column Boxes<br>
    Each Box represents a column<br>
    Max 8<br>
    <i id="textColumnMinusIcon" class="icon-large icon-minus-sign" style="cursor: pointer;"></i>&nbsp;<i id="textColumnPlusIcon" class="icon-large icon-plus-sign" style="cursor: pointer;"></i></label>
    <div class="col-md-8">

    {{ Form::hidden('colDivNum', $initColDivID, ['id'=>'colDivNum']) }}

        <div id="textBoxCol1">
        <label for="column_1" class="">Column 1</label>
            {{ Form::textarea('column_1', $formModule->column_1, array('class' => 'form-control editme', 'id' => 'column_1')) }}
            <span class="text-danger">{{ $errors->first('column_1') }}</span>
        </div>

        <div id="textBoxCol2" style="@if(trim($formModule->column_2) == "") display: none; @endif">
        <label for="column_2" class="">Column 2</label>
            {{ Form::textarea('column_2', $formModule->column_2, array('class' => 'form-control editme', 'id' => 'column_2')) }}
            <span class="text-danger">{{ $errors->first('column_2') }}</span>
        </div>

        <div id="textBoxCol3" style="@if(trim($formModule->column_3) == "") display: none; @endif">
        <label for="column_3" class="">Column 3</label>
            {{ Form::textarea('column_3', $formModule->column_3, array('class' => 'form-control editme', 'id' => 'column_3')) }}
            <span class="text-danger">{{ $errors->first('column_3') }}</span>
        </div>

        <div id="textBoxCol4" style="@if(trim($formModule->column_4) == "") display: none; @endif">
        <label for="column_4" class="">Column 4</label>
            {{ Form::textarea('column_4', $formModule->column_4, array('class' => 'form-control editme', 'id' => 'column_4')) }}
            <span class="text-danger">{{ $errors->first('column_4') }}</span>
        </div>

        <div id="textBoxCol5" style="@if(trim($formModule->column_5) == "") display: none; @endif">
        <label for="column_5" class="">Column 5</label>
            {{ Form::textarea('column_5', $formModule->column_5, array('class' => 'form-control editme', 'id' => 'column_5')) }}
            <span class="text-danger">{{ $errors->first('column_5') }}</span>
        </div>

        <div id="textBoxCol6" style="@if(trim($formModule->column_6) == "") display: none; @endif">
        <label for="column_6" class="">Column 6</label>
            {{ Form::textarea('column_6', $formModule->column_6, array('class' => 'form-control editme', 'id' => 'column_6')) }}
            <span class="text-danger">{{ $errors->first('column_6') }}</span>
        </div>

        <div id="textBoxCol7" style="@if(trim($formModule->column_7) == "") display: none; @endif">
        <label for="column_7" class="">Column 7</label>
            {{ Form::textarea('column_7', $formModule->column_7, array('class' => 'form-control editme', 'id' => 'column_7')) }}
            <span class="text-danger">{{ $errors->first('column_7') }}</span>
        </div>

        <div id="textBoxCol8" style="@if(trim($formModule->column_8) == "") display: none; @endif">
        <label for="column_8" class="">Column 8</label>
            {{ Form::textarea('column_8', $formModule->column_8, array('class' => 'form-control editme', 'id' => 'column_8')) }}
            <span class="text-danger">{{ $errors->first('column_8') }}</span>
        </div>

    </div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="form-group">
    <div class="col-md-1"></div>
    <label for="body_2" class="col-md-2">Body 2</label>
    <div class="col-md-8">
        {{ Form::textarea('body_2', $formModule->body_2, array('class' => 'form-control editme', 'id' => 'body_2')) }}
        <span class="text-danger">{{ $errors->first('body_2') }}</span>
    </div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="form-group">
    <div class="col-md-1"></div>
    <label for="has_border" class="col-md-2"></label>
    <div class="col-md-8">
        {{ Form::checkbox('has_border', 1, ($formModule->has_border == 1 ? true : null), []) }} Click to add a line
        border above
        this module
        <span class="text-danger">{{ $errors->first('has_border') }}</span>
    </div> <!-- /controls -->
</div> <!-- /control-group -->

@show
