@section('gridpic')

<tr id="case-study-module-gridpic"          data-siteProduct-name="case-study-module-gridpic">
                    <td>
                        <div class="form-group">

                            <hr>

                            <div class="col-md-1"></div>

                            <div class="col-md-11">
                                {{ Form::checkbox('grid_pic_module_enabled', 1, ($caseStudy->grid_pic_module_enabled == 1 ? true : null), []) }} Enable/Disable this Module
                                <table class="table table-striped table-bordered">
                                    <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
                                        <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to show/collapse modules.">
                                            <th>
                                                Image Grid Module
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div>
                                                <label for="grid_pic_module_upload_id" class="col-md-3"><span><a href="/admin/uploads/create" target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Available Grid Images</label>
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Name</th>
                                                                <th>Category</th>
                                                                <th>Content File</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($gridPicImages as $gridPicImage)
                                                            <tr>
                                                                <td>{{ Form::radio('grid_pic_module_upload_id',$gridPicImage->id, ($gridPicImage->id  == $caseStudy->grid_pic_module_upload_id ? true : null), []) }}</td>
                                                                <td>{{$gridPicImage->name}}</td>
                                                                <td>{{$gridPicImage->categoryName}}</td>
                                                                <td><a href="{{$gridPicImage->download_folder}}{{$gridPicImage->download_file}}" target="_blank">{{$gridPicImage->download_file}}</a></td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <span class="text-danger">{{ $errors->first('grid_pic_module_upload_id') }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- /controls -->

                        </div> <!-- /control-group -->

                    </td>
                </tr>

@show