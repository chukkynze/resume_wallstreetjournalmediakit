@section('slideshow')




<div class="form-group">
    <div class="col-md-1"></div>
    <label class="col-md-2" for="upload_ids"><span><a href="/admin/uploads/create" target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Slide Show Images</label>
    <div class="col-md-8">
        <div>
            <table class="table table-striped table-bordered">
                <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
                    <tr>
                        <th>
                            <span class="icon-down" style="display: none;"><i class="btn-icon-only icon-chevron-down"></i></span>
                            <span class="icon-right"><i class="btn-icon-only icon-chevron-right"></i></span>
                        </th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Content File</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($mediaImages as $contentImage)
                    <tr>
                        <td>{{ Form::checkbox('upload_ids[]', $contentImage->id, ( in_array($contentImage->id, $formModule->chosenMedia) ? true : null ), []) }}</td>
                        <td>{{$contentImage->name}}</td>
                        <td>{{$contentImage->categoryName}}</td>
                        <td><a href="{{$contentImage->download_folder}}{{$contentImage->download_file}}" target="_blank">{{$contentImage->download_file}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <span class="text-danger">{{ $errors->first('upload_ids') }}</span>
    </div> <!-- /controls -->
</div> <!-- /control-group -->




<div class="form-group">
    <div class="col-md-1"></div>
    <label for="has_border" class="col-md-2"></label>
    <div class="col-md-8">
        {{ Form::checkbox('has_border', 1, ($formModule->has_border == 1 ? true : null), []) }} Click to add a line
        border above
        this module
        <span class="text-danger">{{ $errors->first('has_border') }}</span>
    </div> <!-- /controls -->
</div> <!-- /control-group -->

@show