@section('sliders')

<tr id="case-study-module-sliders"          data-siteProduct-name="case-study-module-sliders">
                    <td>
                        <div class="form-group">

                            <hr>

                            <div class="col-md-1"></div>

                            <div class="col-md-11">
                                {{ Form::checkbox('slider_module_enabled', 1, ($caseStudy->slider_module_enabled == 1 ? true : null), []) }} Enable/Disable this Module
                                <table class="table table-striped table-bordered">
                                    <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
                                        <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to show/collapse modules.">
                                            <th>
                                                Slider Module
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label for="video_module_url" class="col-md-3"><span><a href="/admin/uploads/create" target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Available Sliders</label>
                                                <table class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Name</th>
                                                            <th>Category</th>
                                                            <th>Content File</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($sliders as $slider)
                                                        <tr>
                                                            <td>{{ Form::checkbox('case_study_slider_upload_id[]', $slider->id, ( in_array($slider->id, $caseStudy->chosenSliders) ? true : null ), ['class' => 'sliderDetailsCheckbox', 'data-slider-details-id' => 'sliderDetails'.$slider->id]) }}</td>
                                                            <td>{{$slider->name}}</td>
                                                            <td>{{$slider->categoryName}}</td>
                                                            <td><a href="{{$slider->download_folder}}{{$slider->download_file}}" target="_blank">{{$slider->download_file}}</a></td>
                                                        </tr>
                                                        <tr id="sliderDetails{{$slider->id}}"  style="display: none;">
                                                            <td colspan="4">
                                                                <label for="main_text_{{$slider->id}}" class="col-md-3">Slider Heading</label><br>
                                                                {{ Form::text('main_text_'.$slider->id.'', $slider->mainText, array('class' => 'form-control', 'id' => 'main_text_'.$slider->id.'')) }}
                                                                <label for="name" class="col-md-3">Slider Summary</label><br>
                                                                {{ Form::textarea('sub_text_'.$slider->id.'', $slider->subText, array('class' => 'form-control', 'id' => 'sub_text_'.$slider->id.'')) }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <span class="text-danger">{{ $errors->first('case_study_slider_upload_id') }}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- /controls -->

                        </div> <!-- /control-group -->

                    </td>
                </tr>

@show