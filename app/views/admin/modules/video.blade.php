@section('video')
<div class="form-group">
    <div class="col-md-1"></div>
    <label class="col-md-2" for="upload_id"><span><a href="/admin/uploads/create" target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Heading Images<br><button class="btn btn-xs btn-primary" id="clearHeadingIconList">Clear</button></label>
    <div class="col-md-8">
        <div>
            <table class="table table-striped table-bordered">
                <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
                    <tr>
                        <th>
                            <span class="icon-down" style="display: none;"><i class="btn-icon-only icon-chevron-down"></i></span>
                            <span class="icon-right"><i class="btn-icon-only icon-chevron-right"></i></span>
                        </th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Content File</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($headingImages as $headingImage)
                    <tr>
                        <td>{{ Form::radio('upload_id', $headingImage->id, ($headingImage->id  == $formModule->upload_id ? true : null), ['class' => 'headingIconList']) }}</td>
                        <td>{{$headingImage->name}}</td>
                        <td>{{$headingImage->categoryName}}</td>
                        <td><a href="{{$headingImage->download_folder}}{{$headingImage->download_file}}" target="_blank">{{$headingImage->download_file}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <span class="text-danger">{{ $errors->first('upload_id') }}</span>
    </div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="form-group">
    <div class="col-md-1"></div>
    <label for="url" class="col-md-2">Video URL</label>
    <div class="col-md-8">
        {{ Form::text('url', $formModule->url, array('class' => 'form-control', 'id' => 'url')) }}
        <span class="text-danger">{{ $errors->first('url') }}</span>
    </div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="form-group">
    <div class="col-md-1"></div>
    <label for="has_border" class="col-md-2"></label>
    <div class="col-md-8">
        {{ Form::checkbox('has_border', 1, ($formModule->has_border == 1 ? true : null), []) }} Click to add a line
        border above
        this module
        <span class="text-danger">{{ $errors->first('has_border') }}</span>
    </div> <!-- /controls -->
</div> <!-- /control-group -->

@show
