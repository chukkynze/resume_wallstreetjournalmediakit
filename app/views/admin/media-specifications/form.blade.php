@section('form')

    <fieldset>

        <div class="form-group">
            <label for="name" class="col-md-10">Rate Product Name</label>

            <div class="col-md-10">
                {{ Form::text('name', $rateProduct->name, array('class' => 'form-control', 'id' => 'name')) }}
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
            <!-- /controls -->
        </div>
        <!-- /control-group -->

        <div class="form-group">
            <label for="submit_ad_link" class="col-md-10">Submit Ad Link</label>

            <div class="col-md-10">
                {{ Form::text('submit_ad_link', $rateProduct->submit_ad_link, array('class' => 'form-control', 'id' => 'submit_ad_link')) }}
                <span class="text-danger">{{ $errors->first('submit_ad_link') }}</span>
            </div>
        </div>


        <!--
		<div class="form-group">
			<label for="order" class="col-md-10">Order</label>
			<div class="col-md-10">
				{{ Form::text('order', $rateProduct->order, array('class' => 'form-control', 'id' => 'order')) }}
				<span class="text-danger">{{ $errors->first('order') }}</span>
			</div>
		</div>
		-->

        <hr/>
        <br/>

        <div class="form-group">

            <div class="col-md-offset-4 col-md-8">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="/admin/rate-products" class="btn btn-default">Cancel</a>
            </div>
        </div>
        <!-- /form-actions -->
    </fieldset>
@show