@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="widget stacked widget-table action-table">

            <div class="widget-header">
                <i class="icon-dollar"></i>

                <h3>
                    Rate Products
                    <a class="btn btn-xs btn-success" href="/admin/rate-products/create">Add New Rate Product</a>
                </h3>
            </div>
            <!-- /widget-header -->

            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>

                        @if ($allowRateProductMgt == 1)
                            <th class="td-actions">Edit</th>
                            <th class="td-actions">Delete</th>
                        @endif

                    </tr>
                    </thead>
                    <tbody>
                    <?php $x = 0; ?>
                    @foreach ($allProducts as $product)
                        <tr>
                            <td><?php $x++; echo $x; ?></td>
                            <td>{{$product->name}}</td>
                            @if ($allowRateProductMgt == 1)
                                <td class="td-actions">
                                    <a href="/admin/rate-products/{{$product->id}}/edit" class="btn btn-xs btn-primary">
                                        <i class="btn-icon-only icon-edit"></i>
                                    </a>
                                </td>
                                <td class="td-actions">
                                    {{ Form::open(array('url' => 'admin/rate-products/' . $product->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'site_product_id')) }}
                                    <button type="submit" class="btn btn-xs btn-danger"><i
                                                class="btn-icon-only icon-remove"></i></button>
                                    {{ Form::close() }}
                                </td>
                            @endif
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>
            <!-- /widget-content -->
        </div>
    </div>
@stop