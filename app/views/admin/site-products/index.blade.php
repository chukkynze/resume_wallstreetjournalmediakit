@extends('layouts.admin')
@section('content')

	{{Form::hidden('index_row_order', $initIndexOrder, ['id'=>'index_row_order'])}}

<div class="row">
	<div class="widget stacked widget-table action-table">

        <div style="color: red;">Click and drag the name below to reorder. Re-ordering here affects the site in
            real-time.
        </div>

		<div class="widget-header">
			<i class="icon-sitemap"></i>
			<h3>
				Products
				<a class="btn btn-xs btn-success" href="/admin/site-products/create">Add New Product</a>
				&nbsp;&nbsp;
				<a class="btn btn-xs btn-success" href="/admin/site-product-sections/create">Add New Product
					Subsection</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Name</th>

						@if ($allowSiteProductMgt == 1)
							<th class="td-actions">Subsections</th>
							<th class="td-actions">Edit</th>
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody id="sortSiteProductsOrder">
				<?php $x=0; ?>
				@foreach ($siteProducts as $siteProduct)
					<tr style="cursor: pointer;" id="index-row-{{$siteProduct->id}}">
						<td>{{$siteProduct->name}}</td>
						@if ($allowSiteProductMgt == 1)
							<td class="td-actions">
								<a href="/admin/site-product-sections/product/{{$siteProduct->id}}/view"
								   class="btn btn-xs btn-info">
									<i class="btn-icon-only icon-search"></i>
								</a>
							</td>
							<td class="td-actions">
								<a href="/admin/site-products/{{$siteProduct->id}}/edit" class="btn btn-xs btn-primary">
									<i class="btn-icon-only icon-edit"></i>
								</a>
							</td>
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/site-products/' . $siteProduct->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'site_product_id')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>

</div>
@stop