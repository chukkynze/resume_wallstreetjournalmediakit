@section('form')
        {{ Form::hidden('on_product_landing'  , 0) }}
        {{ Form::hidden('on_menu'  , 0) }}
        {{ Form::hidden('heading_vs_image'  , 0) }}
	<fieldset>
		<div class="form-group">
            <label for="name" class="col-md-10">Product Name</label>
			<div class="col-md-10">
				{{ Form::text('name', $siteProduct->name, array('class' => 'form-control', 'id' => 'name')) }}
				<span class="text-danger">{{ $errors->first('name') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->

        <div class="form-group">
            <label for="name_slug" class="col-md-10">Product Name URL</label>

            <div class="col-md-10">
                {{ Form::text('name_slug', $siteProduct->name_slug, array('class' => 'form-control', 'id' => 'name_slug')) }}
                <span class="text-danger">{{ $errors->first('name_slug') }}</span>
            </div>
            <!-- /controls -->
        </div>
        <!-- /control-group -->


        <div class="form-group">
            <label for="source_text" class="col-md-3">Source Text</label>

            <div class="col-md-10">
                {{ Form::textarea('source_text', $siteProduct->source_text, array('class' => 'form-control', 'id' => 'source_text')) }}
                <span class="text-danger">{{ $errors->first('source_text') }}</span>
            </div>
            <!-- /controls -->
        </div>
        <!-- /control-group -->

		<hr class="col-md-10" />

            <div class="form-group">

                <div class="col-md-9">
                    <label for="heading_vs_image" class="col-md-9">All Product Detail Pages can have text or an image as
                        a heading (e.g. the WSJ. Magazine Product Detail Page). Check this box to use an icon or logo
                        instead of text. Then, choose an image from the list that will display below.:</label>
                    <span class="text-danger">{{ $errors->first('heading_vs_image') }}</span>
                </div>

                <div class="col-md-1">
                    {{ Form::checkbox('heading_vs_image', 1, $siteProduct->heading_vs_image == 0 ? null : true, ['class' => 'form-control col-md-1', 'id' => 'heading_vs_image']) }}
                </div>

            </div> <!-- /control-group -->

        <hr class="col-md-10" />

        <div class="form-group" id="detailsForHeadingVsImage"
             style="display: {{($siteProduct->heading_vs_image == 0 ? "none" : "block")}}};">
                <label class="col-md-10" for="heading_icon_upload_id"><span><a href="/admin/uploads/create" target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Details Page Heading Icon Image</label>
                <div class="col-md-10">
                    <div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Content File</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($headingImages as $headingImage)
                                <tr>
                                    <td>{{ Form::radio('heading_icon_upload_id', $headingImage->id, ($headingImage->id  == $siteProduct->heading_icon_upload_id ? true : null), []) }}</td>
                                    <td>{{$headingImage->name}}</td>
                                    <td>{{$headingImage->categoryName}}</td>
                                    <td><a href="{{$headingImage->download_folder}}{{$headingImage->download_file}}" target="_blank">{{$headingImage->download_file}}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <span class="text-danger">{{ $errors->first('heading_icon_upload_id') }}</span>
                </div> <!-- /controls -->
            </div> <!-- /control-group -->

        <hr class="col-md-10" id="detailsForHeadingVsImageHR"
            style="display: {{($siteProduct->heading_vs_image == 0 ? "none" : "block")}}};"/>


        <!--
            <div class="form-group">

                <div class="col-md-9">
                    <label for="on_product_landing" class="col-md-10">Display Product on the Product Landing
                        Page?</label>
                    <span class="text-danger">{{ $errors->first('on_product_landing') }}</span>
                </div>

                <div class="col-md-1">
                    {{ Form::checkbox('on_product_landing', 1, $siteProduct->on_product_landing == 0 ? null : true, ['class' => 'form-control', 'id' => 'on_product_landing']) }}
                </div>

            </div>


        <hr class="col-md-10">

            <div class="form-group">

                <div class="col-md-9">
                    <label for="on_menu" class="col-md-10">Display Product in the Menu?</label>
                    <span class="text-danger">{{ $errors->first('on_menu') }}</span>
                </div>

                <div class="col-md-1">
                    {{ Form::checkbox('on_menu', 1, $siteProduct->on_menu == 0 ? null : true, ['class' => 'form-control', 'id' => 'on_menu']) }}
                </div>

            </div>


        <hr class="col-md-10">
        -->

        {{ Form::hidden('section_order', $siteProduct->section_order, ['id' => 'sectionOrder']) }}

            @if(count($siteProductSubSections) >= 1)

                <div class="form-group">
                    <label for="subsections" class="col-md-12">
                        These are the linked subsections you have added to this Product <a
                                href="/admin/site-product-sections/product/{{$siteProduct->id}}/view">here</a>.<br>
                        Drag to re-order.<br>
                        This order will be reflected throughout the site (Menu, Product Landing Page, etc) after you
                        press Publish.<br>
                        </label>
                    <div class="col-md-1"></div>
                    <div class="col-md-6" style="background-color: gainsboro; min-height: 20px;">

                        <table class="col-md-12">

                            <tbody id="sortSiteProductSubSectionOrder">

                                @foreach($siteProductSubSections as $subSection)

                                <tr id="sub-section-{{$subSection['id']}}"        data-siteProduct-name="sub-section-{{$subSection['id']}}">
                                    <td>
                                        <hr>

                                        <div class="col-md-1"></div>
                                        <div class="col-md-11">{{$subSection['name']}}</div>

                                    </td>
                                </tr>

                                @endforeach

                            </tbody>

                        </table>
                        <br>

                    </div> <!-- /controls -->
                </div> <!-- /control-group -->

            @endif

        <br/>

		<div class="form-group">

			<div class="col-md-offset-4 col-md-8">
                <button type="submit" class="btn btn-primary">Save</button>
				<a href="/admin/site-products" class="btn btn-default">Cancel</a>
			</div>
		</div> <!-- /form-actions -->
	</fieldset>
@show