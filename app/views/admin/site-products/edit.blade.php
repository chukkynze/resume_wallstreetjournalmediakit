@extends('layouts.admin')
@section('content')
<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-sitemap"></i>

				<h3>Edit Product</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/site-products/' . $siteProduct->id , 'method' => 'put', 'id' => 'edit-site-product', 'class' => 'form-horizontal col-md-12')) }}
						@include('admin/site-products/form')
				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop