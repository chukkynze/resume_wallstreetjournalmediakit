@extends('layouts.admin')
@section('content')
<div class="row">

	<div class="col-md-8">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-inbox"></i>

				<h3>Create New Contact Location</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/contact-locations', 'method' => 'post', 'id' => 'create-contact-location', 'class' => 'form-horizontal col-md-8')) }}
						@include('admin/contact-locations/form')
				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop