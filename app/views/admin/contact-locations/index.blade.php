@extends('layouts.admin')
@section('content')

	{{Form::hidden('index_row_order', $initIndexOrder, ['id'=>'index_row_order'])}}

<div class="row">
	<div class="widget stacked widget-table action-table">

        <div style="color: red;">Click and drag the name below to reorder. Re-ordering here affects the site in
            real-time.
        </div>

        <div class="widget-header">
			<i class="icon-inbox"></i>

			<h3>
				Contact Locations
				<a class="btn btn-xs btn-success" href="/admin/contact-locations/create">Add New Contact Location</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Name</th>

						@if ($allowContactLocationMgt == 1)
							<th class="td-actions">Edit</th>
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody id="sortContactLocationsOrder">
				<?php $x=0; ?>
				@foreach ($contactLocations as $contactLocation)
					<tr style="cursor: pointer;" id="index-row-{{$contactLocation->id}}">
						<td>{{$contactLocation->name}}</td>
						@if ($allowContactLocationMgt == 1)
							<td class="td-actions">
								<a href="/admin/contact-locations/{{$contactLocation->id}}/edit"
								   class="btn btn-xs btn-primary">
									<i class="btn-icon-only icon-edit"></i>
								</a>
							</td>
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/contact-locations/' . $contactLocation->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'contact_location_id')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>
@stop