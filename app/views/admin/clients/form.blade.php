@section('form')

	<fieldset>
		<div class="form-group">
			<label for="username" class="col-md-4">Client Name</label>
			<div class="col-md-8">
				{{ Form::text('name', $client->name, array('class' => 'form-control', 'id' => 'name')) }}
				<span class="text-danger">{{ $errors->first('name') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->

		<br />

		<div class="form-group">

			<div class="col-md-offset-4 col-md-8">
				<button type="submit" class="btn btn-primary">Save</button>
				<a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
			</div>
		</div> <!-- /form-actions -->
	</fieldset>
@show