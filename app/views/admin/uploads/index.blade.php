@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-cloud-upload"></i>
			<h3>
			    Uploads
			    <a class="btn btn-xs btn-success" href="/admin/uploads/create">New Upload</a>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table id="uploadsTable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Heading/Description</th>
						<th>Category</th>
						<th>Access Form</th>
						<th>Upload Date</th>
						<th>Uploaded By</th>
						<th>Content File</th>

						@if ($allowUploadMgt == 1)
							<th class="td-actions">Edit</th>
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody>
				@foreach ($uploads as $upload)
					<tr>
						<td>{{$upload->id}}</td>
						<td>{{$upload->name}}</td>
						<td>{{$upload->description}}</td>
						<td>{{$upload->categoryName}}</td>
						<td>@if($upload->gated == 1 ) Yes @else No @endif</td>
						<td>
                            {{$upload->updated_at}}
						</td>
						<th>{{$upload->uploader}}</th>
						<td>
						    <a href="{{$upload->download_folder}}{{$upload->download_file}}" target="_blank">{{$upload->download_file}}</a>
						</td>

						@if ($allowUploadMgt == 1)
							<td class="td-actions">
								<a href="/admin/uploads/{{$upload->id}}/edit" class="btn btn-xs btn-primary">
									<i class="btn-icon-only icon-edit"></i>
								</a>
							</td>
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/uploads/' . $upload->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-user')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>
@stop