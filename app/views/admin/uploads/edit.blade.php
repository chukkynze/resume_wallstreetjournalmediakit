@extends('layouts.admin')
@section('content')
<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-cloud-upload"></i>
				<h3>Edit Upload</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/uploads/' . $upload->id, 'files'=>true, 'method' => 'put', 'id' => 'edit-upload', 'class' => 'form-horizontal col-md-12')) }}
						@include('admin/uploads/form')
				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop