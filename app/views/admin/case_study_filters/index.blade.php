@extends('layouts.admin')
@section('content')

    {{Form::hidden('index_row_order', $initIndexOrder, ['id'=>'index_row_order'])}}

    <div class="row">
        <div class="widget stacked widget-table action-table">

            <div style="color: red;">Click and drag the name below to reorder. Re-ordering here affects the site in
                real-time.
            </div>

            <div class="widget-header">
                <i class="icon-th-list"></i>

                <h3>
                    Gallery Filters
                    <!--
                    <a class="btn btn-xs btn-success" href="/admin/case-study-filter/create">Add New Gallery Filter</a>
                    -->
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
                </h3>
            </div>
            <!-- /widget-header -->

            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>

                        @if ($allowCaseStudyMgt == 1)
                            <th class="td-actions">Items</th>
                            <th class="td-actions">Edit</th>
                            <!--<th class="td-actions">Delete</th>-->
                        @endif

                    </tr>
                    </thead>
                    <tbody id="sortGalleryFiltersOrder">
                    @foreach ($allCaseStudyFilters as $filter)
                        <tr style="cursor: pointer;" id="index-row-{{$filter->id}}">
                            <td>{{$filter->name}}</td>

                            @if ($allowCaseStudyMgt == 1)
                                <td class="td-actions">
                                    <a href="/admin/gallery-filter-items/filter/{{$filter->id}}"
                                       class="btn btn-xs btn-info">
                                        <i class="btn-icon-only icon-search"></i>
                                    </a>
                                </td>
                                <td class="td-actions">
                                    <a href="/admin/gallery-filters/{{$filter->id}}/edit"
                                       class="btn btn-xs btn-primary">
                                        <i class="btn-icon-only icon-edit"></i>
                                    </a>
                                </td>
                                <!--
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/gallery-filters/' . $filter->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-user')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
							-->
                            @endif
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>
            <!-- /widget-content -->
        </div>
    </div>
@stop