@section('form')

    <fieldset>

        <h4>Gallery Filter Item Details</h4><br/>

        <hr>

        <div class="form-group">
            <div class="col-md-1"></div>
            <div class="col-md-3">
                {{ Form::label('name', 'New Filter Item Name', array('class' => '')) }}
            </div>
            <div class="col-md-8">
                {{ Form::text('name', $caseStudyFilterItem->name, array('class' => 'form-control', 'id' => 'name')) }}
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
            <!-- /controls -->
        </div>
        <!-- /control-group -->

        <div class="form-group">
            <div class="col-md-1"></div>
            <div class="col-md-3">
                {{ Form::label('filter_id', 'Assigned Filter', array('class' => '')) }}
            </div>
            <div class="col-md-8">
                {{ Form::select('filter_id', $filtersArray, ($caseStudyFilterItem->filter_id ?: ($filterID ?: 0)), array('class' => 'form-control', 'id' => 'name')) }}
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
            <!-- /controls -->
        </div>
        <!-- /control-group -->

        <!--
		<div class="form-group">
		    <div class="col-md-1"></div>
		    <div class="col-md-3">
			    {{ Form::label('description', 'Description', array('class' => '')) }}
			</div>
			<div class="col-md-8">
			{{ Form::text('description', $caseStudyFilterItem->description, array('class' => 'form-control', 'id' => 'description')) }}
				<span class="text-danger">{{ $errors->first('description') }}</span>
			</div>
		</div>


		<div class="form-group">
		    <div class="col-md-1"></div>
		    <div class="col-md-3">
			    {{ Form::label('order', 'Filter Order', array('class' => '')) }}
			</div>
			<div class="col-md-8">
			{{ Form::text('order', $caseStudyFilterItem->order, array('class' => 'form-control', 'id' => 'order')) }}
				<span class="text-danger">{{ $errors->first('order') }}</span>
			</div>
		</div>
-->

        <hr/>

        <div class="form-group">

            <div class="col-md-offset-4 col-md-8">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="/admin/gallery-filter-items/filter/{{$filterID}}" class="btn btn-default">Cancel</a>
            </div>
        </div>
        <!-- /form-actions -->
    </fieldset>
@show