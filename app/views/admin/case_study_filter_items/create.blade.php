@extends('layouts.admin')
@section('content')
    <div class="row">

        <div class="col-md-12">

            <div class="widget stacked">

                <div class="widget-header">
                    <i class="icon-th-list"></i>

                    <h3>Create New Gallery Filter Item</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">
                    <!-- use form.blade.php -->
                    {{ Form::open(array('url' => 'admin/gallery-filter-items', 'files'=>true,  'method' => 'post', 'id' => 'create-case-study-filter-item', 'class' => 'form-horizontal col-md-12')) }}
                    @include('admin.case_study_filter_items.form')
                    {{ Form::close() }}
                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->

        </div>
        <!-- /span8 -->

    </div> <!-- /row -->
@stop