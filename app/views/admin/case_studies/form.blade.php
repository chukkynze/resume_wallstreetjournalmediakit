@section('form')

    {{Form::hidden('is_live', $caseStudy->is_live, '')}}

	<fieldset>

        <hr>


		<div class="form-group">
            <label for="client_name" class="col-md-4">Client Name <span style="color:#808080">(Optional)</span></label>
			<div class="col-md-8">
                {{ Form::text('client_name', $caseStudy->client_name, array('class' => 'form-control', 'id' => 'client_name')) }}
                <span class="text-danger">Will not display on the Front End.</span>
                <span class="text-danger">{{ $errors->first('client_name') }}</span>
            </div>
            <!-- /controls -->
        </div>
        <!-- /control-group -->

		<table class="table table-striped table-bordered">
            <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
                <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to show/collapse details.">
                    <th>
                        Listing
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="headline" class="col-md-3">Title</label>
                            <div class="col-md-8">
                                {{ Form::text('headline', $caseStudy->headline, array('class' => 'form-control', 'id' => 'headline')) }}
                                <span class="text-danger">{{ $errors->first('headline') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="description" class="col-md-3">Description</label>
                            <div class="col-md-8">
                                {{ Form::textarea('description', $caseStudy->description, array('class' => 'form-control', 'id' => 'description')) }}
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="keywords" class="col-md-3">Keywords</label>

                            <div class="col-md-8">
                                {{ Form::text('keywords', $caseStudy->keywords, array('class' => 'form-control', 'id' => 'keywords')) }}
                                <span class="text-danger">{{ $errors->first('keywords') }}</span>
                            </div>
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label class="col-md-3" for="listing_pic_id"><span><a href="/admin/uploads/create"
                                                                                  target="_blank"
                                                                                  style="font-weight: bolder;">+</a>&nbsp;</span>Listing
                                Image</label>
                            <div class="col-md-8">
                                <div>
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Content File</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($listingImages as $listingImage)
                                            <tr>
                                                <td>{{ Form::radio('listing_pic_id', $listingImage->id, ($listingImage->id  == $caseStudy->listing_pic_id ? true : null), ['id' => 'listing_pic_id']) }}</td>
                                                <td>{{$listingImage->name}}</td>
                                                <td>{{$listingImage->categoryName}}</td>
                                                <td><a href="{{$listingImage->download_folder}}{{$listingImage->download_file}}" target="_blank">{{$listingImage->download_file}}</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <span class="text-danger">{{ $errors->first('listing_pic_id') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->

                    </td>
                </tr>
            </tbody>
        </table>

		<hr>

		<table class="table table-striped table-bordered">
            <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
                <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to show/collapse filters.">
                    <th>
                        Filters
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        @foreach($allFilters as $filter)

                            <div class="form-group">
                                <div class="col-md-1"></div>
                                <label for="filter_id_{{$filter->id}}" class="col-md-3"><span><a
                                                href="/admin/gallery-filter-items/filter/{{$filter->id}}"
                                                target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Choose
                                    the {{$filter->name}}</label>

                                <div class="col-md-8">
                                    @foreach ($filter->items as $filterItem)
                                        <div>
                                            {{ Form::checkbox('filter_id_' . $filter->id . '[]',$filterItem['id'], ( in_array($filterItem['id'], $filter->chosenFilterItems) ? true : null ), []) }} {{$filterItem['name']}}
                                        </div>
                                    @endforeach
                                    <span class="text-danger">{{ $errors->first('filter_id_' . $filter->id . '') }}</span>
                                </div>
                                <!-- /controls -->
                            </div> <!-- /control-group -->

                        @endforeach
                    </td>
                </tr>
            </tbody>
        </table>
        <hr/>
        <br>

		<div class="form-group">

			<div class="col-md-offset-4 col-md-8">
				<button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-disabled" id="previewListingFormButton">Preview Listing</button>
				<a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
			</div>
		</div> <!-- /form-actions -->
	</fieldset>
@show