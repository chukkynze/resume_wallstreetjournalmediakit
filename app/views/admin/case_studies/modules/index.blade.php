@extends('layouts.admin')
@section('content')

    {{Form::hidden('client_id'      , $caseStudy->client_id        , ['id'=>'client_id'])}}
    {{Form::hidden('headline'       , $caseStudy->headline         , ['id'=>'headline'])}}
    {{Form::hidden('description'    , $caseStudy->description      , ['id'=>'description'])}}
    {{Form::hidden('keywords'       , $caseStudy->keywords         , ['id'=>'keywords'])}}
    {{Form::hidden('listing_pic_id' , $caseStudy->listing_pic_id   , ['id'=>'listing_pic_id'])}}
    {{--Form::hidden('filter_array'   , $caseStudy->filter_array     , ['id'=>'filter_array'])--}}

    <div class="row">
        <div class="widget stacked widget-table action-table">

            <div class="widget-header">
                <i class="icon-th-list"></i>

                <h3>
                    Gallery Entry Details
                    <a class="btn btn-xs btn-info" href="#" id="galleryListingPreviewButton">Preview Entry</a>
                    <a class="btn btn-xs btn-success" href="/admin/gallery/{{$caseStudy->id}}/edit">Edit Entry</a>
                    <button id="togglePublishCaseStudy" data-case-study-id="{{$caseStudy->id}}"
                            class="btn btn-xs btn-primary">{{($caseStudy->is_live == 0 ? "Publish" : "Hide")}}
                        Entry
                    </button>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
                </h3>
            </div>
            <!-- /widget-header -->

            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <tbody>
                    <tr>
                        <td class="col-lg-2">
                            <img src="{{$caseStudy->listing_pic}}" height="230px" width="auto">
                        </td>
                        <td class="col-lg-5">
                            <div style="font-weight: bold;">Title</div>
                            <div style="margin-bottom: 10px;">{{$caseStudy->headline}}</div>
                            <div style="font-weight: bold;">Description</div>
                            <div style="margin-bottom: 10px;">{{$caseStudy->description}}</div>
                            <div style="font-weight: bold;">Keywords</div>
                            <div style="margin-bottom: 10px;">{{$caseStudy->keywords}}</div>
                            <div style="font-weight: bold;">Client</div>
                            <div>{{($caseStudy->client_name != "" ? $caseStudy->client_name : "No Client Selected")}}</div>
                        </td>
                        <td class="col-lg-5">
                            <div id="caseStudyStatus"
                                 class="alert alert-{{($caseStudy->is_live == 0 ? "danger" : "success")}}">This Gallery
                                Item is {{($caseStudy->is_live == 0 ? "not " : " ")}}Published
                            </div>
                            <div style="font-weight: bold;">Selected Filters:</div>
                            <ul style="margin-bottom: 10px;">
                                @foreach ($selectedFilters as $filter)
                                    <li style="">{{$filter['name']}}</li>
                                    <ul>
                                        @foreach($filter['items'] as $filterItem)
                                            <li style="">{{$filterItem}}</li>
                                        @endforeach
                                    </ul>

                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <br><br>

            <div class="widget-header col-lg-12">
                <i class="icon-eye-open"></i>

                <h3>
                    Gallery Details Page Modules
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="btn btn-xs btn-info" href="/preview/gallery/{{$caseStudy->id}}"
                       target="_blank">Preview {{$caseStudy->headline}} Page</a>
                    @if ($allowCaseStudyMgt == 1)
                        <a class="btn btn-xs btn-success" data-toggle="modal" href="#moduleModal">Add Modules</a>
                    @endif
                    <a id="saveLiveOrderLocation9Button" class="btn btn-xs btn-primary" href="#">Publish</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <span id="indexStatusSpan2" class="hidden" style="color: blue;">asfasf</span>
                </h3>
            </div>


            <div class="widget-content">


                {{ Form::hidden('location9ModuleOrder', '', array('id' => 'location9ModuleOrder')) }}
                {{ Form::hidden('location9ModulePage', 'gallery/' . $caseStudy->id . '/add-modules', array('id' => 'location9ModulePage')) }}

                {{ Form::hidden('location9ModuleLocationID'  , 9             , array('id' => 'location9ModuleLocationID')) }}
                {{ Form::hidden('location9ModuleEntityID'    , $entityID     , array('id' => 'location9ModuleEntityID')) }}
                {{ Form::hidden('location9ModuleEntityType'  , $entityType   , array('id' => 'location9ModuleEntityType')) }}

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th class="td-actions">Live?</th>
                        <th>Module Title</th>

                        @if ($allowCaseStudyMgt == 1)
                            <th class="td-actions">Edit</th>
                            <th class="td-actions">Delete</th>
                        @endif

                    </tr>
                    </thead>
                    <tbody id="sortLocation9ModulesOrder" style="padding:5px;">
                    @foreach ($allModules as $key=>$module)
                        <?php $moduleObject = $module['moduleObject']; ?>
                        <tr style="cursor: pointer;"
                                id="location-{{$moduleObject->relationshipLocationID}}-module-relationship-{{$moduleObject->relationshipID}}"
                                data-location-{{$moduleObject->relationshipLocationID}}-module-relationship-id="location9module-{{$moduleObject->relationshipID}}"
                                >


                            <td class="text-center col-md-1">
                                {{ Form::checkbox('location9ModuleCheckbox[]', $moduleObject->relationshipID, ($moduleObject->is_live ?: null), ['class' => 'location9ModuleCheckbox']) }}
                            </td>


                            <td>
                                <div class="col-lg-12"
                                     style="">{{ $moduleObject->title != "" ? $moduleObject->title : "Empty Title"}}
                                    <span class="module-mgt-row-icon pull-right"
                                          data-module-details-type="{{$module['partial_name']}}"
                                          data-module-details-id="{{$moduleObject->id}}"><i
                                                id="module-mgt-row-icon-{{$module['partial_name']}}-{{$moduleObject->id}}"
                                                class="btn-icon-only icon-chevron-down"></i></span></div>
                                <div id="module-details-{{$module['partial_name']}}-{{$moduleObject->id}}"
                                     style="display: none;">
                                    <br>

                                    <div class="view-preview">
                                        @include("/home/modules/" . $module['partial_name'], ['sectionKey' => 2, 'key' => $key, 'moduleObject' => $module['moduleObject']])
                                    </div>
                                </div>
                            </td>

                            @if ($allowCaseStudyMgt == 1)
                                <td class="td-actions">
                                    <a href="/admin/page-content/modules/{{$moduleObject->relationshipID}}/edit"
                                       class="btn btn-xs btn-primary">
                                        <i class="btn-icon-only icon-edit"></i>
                                    </a>
                                </td>
                                <td class="td-actions">
                                    {{ Form::open(array('url' => 'admin/page-content/modules/' . $moduleObject->relationshipID, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-module')) }}
                                    <button type="submit" class="btn btn-xs btn-danger"><i
                                                class="btn-icon-only icon-remove"></i></button>
                                    {{ Form::close() }}
                                </td>
                            @endif
                        </tr>
                    @endforeach

                    </tbody>
                </table>


            </div>


        </div>
    </div>





    <div id="moduleModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            {{ Form::open(array('url' => 'admin/page-content/modules/create', 'method' => 'post', 'id' => 'create-module', 'class' => 'form-horizontal col-md-12')) }}

            {{ Form::hidden('locationID', $locationID) }}
            {{ Form::hidden('entityType', $entityType) }}
            {{ Form::hidden('entityID'  , $entityID) }}

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Choose Module Type</h4>
                </div>
                <div class="modal-body clearfix">

                    <p>Select the type of module you want to add from the drop down below and click create module to add
                        your custom content.</p>

                    <hr>

                    {{Form::select('moduleType', $moduleTypes, 0, ['class' => 'select-module-type createModuleSelectField', 'data-button-id' => 'createModuleSubmitButton'])}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="createModuleSubmitButton" type="submit" class="btn btn-primary disabled">Create Module
                    </button>
                </div>

            </div>
            <!-- /.modal-content -->

            {{ Form::close() }}

        </div>
        <!-- /.modal-dialog -->
    </div>
@stop

@section('css')
    @parent

    <style>
        .module.slideshow .slides-outer { max-height: none; }
    </style>
@stop
