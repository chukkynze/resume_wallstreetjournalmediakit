@section('form')

    <fieldset>

        <hr>

        <div class="form-group">
            <div class="col-md-1"></div>
            <label for="client_id" class="col-md-3"><span><a href="/admin/clients/create" target="_blank"
                                                             style="font-weight: bolder;">+</a>&nbsp;</span>Choose the
                Client</label>

            <div class="col-md-8">
                {{ Form::select
                (
                    'client_id',
                    $clientList,
                    ($caseStudy->client_id ?: 0) ,
                    [
                        'class' => 'form-control',
                        'id'    => 'client_id'
                    ]
                )}}
                <span class="text-danger">{{ $errors->first('client_id') }}</span>
            </div>
        </div>

        <table class="table table-striped table-bordered">
            <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
            <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title=""
                data-original-title="Click to show/collapse details.">
                <th>
                    Listing
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>

                    <div class="form-group">
                        <div class="col-md-1"></div>
                        <label for="main_heading" class="col-md-3">Heading</label>

                        <div class="col-md-8">
                            {{ Form::text('main_heading', $caseStudy->main_heading, array('class' => 'form-control', 'id' => 'main_heading')) }}
                            <span class="text-danger">{{ $errors->first('main_heading') }}</span>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->

                    <div class="form-group">
                        <div class="col-md-1"></div>
                        <label for="main_description" class="col-md-3">Description</label>

                        <div class="col-md-8">
                            {{ Form::textarea('main_description', $caseStudy->main_description, array('class' => 'form-control', 'id' => 'main_description')) }}
                            <span class="text-danger">{{ $errors->first('main_description') }}</span>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->

                    <div class="form-group">
                        <div class="col-md-1"></div>
                        <label class="col-md-3" for="listing_pic_upload_id"><span><a href="/admin/uploads/create"
                                                                                     target="_blank"
                                                                                     style="font-weight: bolder;">+</a>&nbsp;</span>Listing
                            Image</label>

                        <div class="col-md-8">
                            <div>
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Content File</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($listingImages as $listingImage)
                                        <tr>
                                            <td>{{ Form::radio('listing_pic_upload_id', $listingImage->id, ($listingImage->id  == $caseStudy->listing_pic_upload_id ? true : null), []) }}</td>
                                            <td>{{$listingImage->name}}</td>
                                            <td>{{$listingImage->categoryName}}</td>
                                            <td>
                                                <a href="{{$listingImage->download_folder}}{{$listingImage->download_file}}"
                                                   target="_blank">{{$listingImage->download_file}}</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <span class="text-danger">{{ $errors->first('listing_pic_upload_id') }}</span>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->

                </td>
            </tr>
            </tbody>
        </table>

        <hr>

        <table class="table table-striped table-bordered">
            <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
            <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title=""
                data-original-title="Click to show/collapse filters.">
                <th>
                    Filters
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    @foreach($allFilters as $filter)

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="filter_id_{{$filter->id}}" class="col-md-3"><span><a
                                            href="/admin/gallery-filter-items/filter/{{$filter->id}}"
                                            target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Choose
                                the {{$filter->name}}</label>

                            <div class="col-md-8">
                                @foreach ($filter->items as $filterItem)
                                    <div>
                                        {{ Form::checkbox('filter_id_' . $filter->id . '[]',$filterItem['id'], ( in_array($filterItem['id'], $caseStudy->chosenFilterItems) ? true : null ), []) }} {{$filterItem['name']}}
                                    </div>
                                @endforeach
                                <span class="text-danger">{{ $errors->first('filter_id_' . $filter->id . '') }}</span>
                            </div>
                            <!-- /controls -->
                        </div> <!-- /control-group -->

                    @endforeach
                </td>
            </tr>
            </tbody>
        </table>

        <hr>

        <table class="table table-striped table-bordered">
            <thead class="caseStudyModuleHeaderOptions" style="cursor: pointer;">
            <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title=""
                data-original-title="Click to show/collapse filters.">
                <th>
                    Modules (This will not work here)
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <div class="widget stacked widget-table action-table">


                        <div class="widget-header col-lg-12">
                            <i class="icon-th-list"></i>

                            <h3>
                                Gallery Item Modules
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a class="btn btn-xs btn-info" href="/preview/gallery" target="_blank">Preview
                                    Gallery Item</a>
                                @if ($allowCaseStudyMgt == 1)
                                    <a class="btn btn-xs btn-success" data-toggle="modal" href="#moduleModal">Add
                                        Modules</a>
                                    <a id="saveLiveOrderLocation2Button" class="btn btn-xs btn-primary"
                                       href="#">Publish</a>
                                @endif
                            </h3>
                        </div>

                        <div class="widget-content col-lg-12">

                            <table class="table table-striped table-bordered col-lg-12">

                                <thead>
                                <tr>
                                    <th class="text-center">Live?</th>
                                    <th>Module Title</th>

                                    @if ($allowCaseStudyMgt == 1)
                                        <th class="td-actions">Edit</th>
                                        <th class="td-actions">Delete</th>
                                    @endif

                                </tr>
                                </thead>

                                <tbody id="sortLocation2ModulesOrder" style="padding:5px;">

                                @foreach ($allModules as $key=>$module)
                                    <?php $moduleObject = $module['moduleObject']; ?>
                                    <tr
                                            id="location-{{$moduleObject->relationshipLocationID}}-module-relationship-{{$moduleObject->relationshipID}}"
                                            data-location-{{$moduleObject->relationshipLocationID}}-module-relationship-id="location2module-{{$moduleObject->relationshipID}}"
                                             style="cursor: pointer;">
                                        <td style="cursor: pointer;" class="text-center col-md-1">{{ Form::checkbox('location2ModuleCheckbox[]', $moduleObject->relationshipID, ($moduleObject->is_live ?: null), ['class' => 'location2ModuleCheckbox']) }}</td>
                                        <td style="cursor: pointer;">
                                            <div class="col-lg-12"
                                                 style="">{{ $moduleObject->title != "" ? $moduleObject->title : "Empty Title"}}
                                                <span class="module-mgt-row-icon pull-right"
                                                      data-module-details-type="{{$module['partial_name']}}"
                                                      data-module-details-id="{{$moduleObject->id}}"><i
                                                            id="module-mgt-row-icon-{{$module['partial_name']}}-{{$moduleObject->id}}"
                                                            class="btn-icon-only icon-chevron-down"></i></span></div>
                                            <div id="module-details-{{$module['partial_name']}}-{{$moduleObject->id}}"
                                                 style="display: none;">
                                                <br>

                                                <div class="view-preview">
                                                    @include("/home/modules/" . $module['partial_name'], ['sectionKey' => 2, 'key' => $key, 'moduleObject' => $module['moduleObject']])
                                                </div>
                                            </div>
                                        </td>

                                        @if ($allowInsightsMgt == 1)
                                            <td class="td-actions">
                                                <a href="/admin/page-content/modules/{{$moduleObject->relationshipID}}/edit"
                                                   class="btn btn-xs btn-primary">
                                                    <i class="btn-icon-only icon-edit"></i>
                                                </a>
                                            </td>
                                            <td class="td-actions">
                                                {{ Form::open(array('url' => 'admin/page-content/modules/' . $moduleObject->relationshipID, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-module')) }}
                                                <button type="submit" class="btn btn-xs btn-danger"><i
                                                            class="btn-icon-only icon-remove"></i></button>
                                                {{ Form::close() }}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>

                        </div>

                    </div>
                </td>
            </tr>
            </tbody>
        </table>


        <hr/>
        <br>

        <div class="form-group">

            <div class="col-md-offset-4 col-md-8">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="" class="btn btn-disabled">Preview</button>
                <a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
            </div>
        </div>
        <!-- /form-actions -->
    </fieldset>
@show