@extends('layouts.admin')
@section('content')
    <div class="row">

        <div class="col-md-12">

            <div class="widget stacked">

                <div class="widget-header">
                    <i class="icon-th-list"></i>

                    <h3>Create New Gallery Item</h3>
                </div>

                <div class="widget-content">

                    {{ Form::open(array('url' => 'admin/gallery', 'method' => 'post', 'id' => 'create-case-study', 'class' => 'form-horizontal col-md-12')) }}
                    @include('admin.case_studies.form')
                    {{ Form::close() }}

                </div>
                <!-- /widget -->

            </div>
            <!-- /span8 -->

        </div>
        <!-- /row -->
@stop