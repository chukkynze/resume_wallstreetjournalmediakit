@extends('layouts.admin')
@section('content')

	{{Form::hidden('index_row_order', $initIndexOrder, ['id'=>'index_row_order'])}}

<div class="row">

    <div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-th-list"></i>
			<h3>
				Gallery Entries
				<a class="btn btn-xs btn-success" href="/admin/gallery/create">Add New Gallery Entry</a>
                <!--<a class="btn btn-xs btn-success" href="/admin/gallery-filters">Manage Gallery Entry Filters</a> -->
                <!-- <a class="btn btn-xs btn-success" href="/admin/clients">Manage Clients</a> -->
				&nbsp;&nbsp;&nbsp;&nbsp;
				<span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
			</h3>

            <div class="pull-right">
                @foreach($allFilters as $filter)
                    <a class="btn btn-xs btn-success"
                       href="/admin/gallery-filter-items/filter/{{$filter->id}}">Show/Edit {{$filter->name}}
                    </a>
                @endforeach
            </div>
        </div>

		<div class="widget-content">

			<table id="sortCaseStudyItemsTable" class="table table-striped table-bordered">
				<thead>
					<tr>
                        <th class="td-actions">Live?</th>
						<th>Title</th>
						<th>Client</th>



                        @foreach($allFilters as $filter)
                            <th>{{$filter->name}}</th>
                        @endforeach






						@if ($allowCaseStudyMgt == 1)
                            <!--<th class="td-actions">View/Modules</th>-->
							<th class="td-actions">Edit</th>
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody id="sortCaseStudyItemsOrder">
				@foreach ($caseStudies as $caseStudy)
                    <tr id="index-row-{{$caseStudy->id}}" style="">
                        <td class="td-actions">
                            {{
                                Form::checkbox
                                (
                                    'caseStudyLiveStatus_' . $caseStudy->id,
                                    $caseStudy->id,
                                    ($caseStudy->is_live == 1 ? true : null ),
                                    [
                                        'id'                    =>  'caseStudyLiveStatus_' . $caseStudy->id,
                                        'data-case-study-id'    =>  $caseStudy->id,
                                        'class'                 =>  'caseStudyLiveStatus'
                                    ]
                                )
                            }}
                        </td>
						<td>{{$caseStudy->headline}}</td>
						<td>{{$caseStudy->client_name}}</td>


                        @foreach($caseStudy->filterItems as $filter)
                            <td>{{$filter}}</td>
                        @endforeach



						@if ($allowCaseStudyMgt == 1)
                            <!--
							<td class="td-actions">
								<a href="/admin/gallery/{{$caseStudy->id}}/add-modules"
								   class="btn btn-xs btn-info">
									<i class="btn-icon-only icon-search"></i>
								</a>
							</td>
							-->
							<td class="td-actions">
                                <a href="/admin/gallery/{{$caseStudy->id}}/add-modules" class="btn btn-xs btn-primary">
									<i class="btn-icon-only icon-edit"></i>
								</a>
							</td>
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/gallery/' . $caseStudy->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-user')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>
@stop