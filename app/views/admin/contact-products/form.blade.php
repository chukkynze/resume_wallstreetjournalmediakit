@section('form')

    <fieldset>

        <div class="form-group">
            <label for="name" class="col-md-10">Contact Product Name</label>

            <div class="col-md-10">
                {{ Form::text('name', $contactProduct->name, array('class' => 'form-control', 'id' => 'name')) }}
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
            <!-- /controls -->
        </div>
        <!-- /control-group -->

        <!--
		<div class="form-group">
			<label for="description" class="col-md-10">Description</label>
			<div class="col-md-10">
				{{ Form::text('description', $contactProduct->description, array('class' => 'form-control', 'id' => 'description')) }}
				<span class="text-danger">{{ $errors->first('description') }}</span>
			</div>
		</div>


		<div class="form-group">
			<label for="order" class="col-md-10">Order</label>
			<div class="col-md-10">
				{{ Form::text('order', $contactProduct->order, array('class' => 'form-control', 'id' => 'order')) }}
				<span class="text-danger">{{ $errors->first('order') }}</span>
			</div>
		</div>
		-->

        <hr/>
        <br/>

        <div class="form-group">

            <div class="col-md-offset-4 col-md-8">
                <button type="submit" class="btn btn-primary">Publish</button>
                <a href="/admin/contact-products" class="btn btn-default">Cancel</a>
            </div>
        </div>
        <!-- /form-actions -->
    </fieldset>
@show