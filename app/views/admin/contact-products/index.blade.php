@extends('layouts.admin')
@section('content')

    {{Form::hidden('index_row_order', $initIndexOrder, ['id'=>'index_row_order'])}}

    <div class="row">
        <div class="widget stacked widget-table action-table">

            <div style="color: red;">Click and drag the name below to reorder. Re-ordering here affects the site in
                real-time.
            </div>

            <div class="widget-header">
                <i class="icon-sitemap"></i>

                <h3>
                    Contact Products
                    <a class="btn btn-xs btn-success" href="/admin/contact-products/create">Add New Contact Product</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
                </h3>
            </div>
            <!-- /widget-header -->

            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>

                        @if ($allowContactProductMgt == 1)
                            <th class="td-actions">Edit</th>
                            <th class="td-actions">Delete</th>
                        @endif

                    </tr>
                    </thead>
                    <tbody id="sortContactProductsOrder">
                    <?php $x = 0; ?>
                    @foreach ($allProducts as $product)
                        <tr style="cursor: pointer;" id="index-row-{{$product->id}}">
                            <td>{{$product->name}}</td>
                            @if ($allowContactProductMgt == 1)
                                <td class="td-actions">
                                    <a href="/admin/contact-products/{{$product->id}}/edit"
                                       class="btn btn-xs btn-primary">
                                        <i class="btn-icon-only icon-edit"></i>
                                    </a>
                                </td>
                                <td class="td-actions">
                                    {{ Form::open(array('url' => 'admin/contact-products/' . $product->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'site_product_id')) }}
                                    <button type="submit" class="btn btn-xs btn-danger"><i
                                                class="btn-icon-only icon-remove"></i></button>
                                    {{ Form::close() }}
                                </td>
                            @endif
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>
            <!-- /widget-content -->
        </div>
    </div>
@stop