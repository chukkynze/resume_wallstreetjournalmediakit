@extends('layouts.admin')
@section('content')
    <div class="row">

        <div class="col-md-12">

            <div class="widget stacked">

                <div class="widget-header">
                    <i class="icon-archive"></i>

                    <h3>Edit Contact Product</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">

                    {{ Form::open(array('url' => 'admin/contact-products/' . $contactProduct->id , 'method' => 'put', 'id' => 'edit-contact-product', 'class' => 'form-horizontal col-md-12')) }}

                    @include('admin/contact-products/form')

                    {{ Form::close() }}

                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->

        </div>
        <!-- /span8 -->

    </div> <!-- /row -->
@stop