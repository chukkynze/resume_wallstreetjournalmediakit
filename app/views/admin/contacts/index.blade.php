@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-inbox"></i>
			<h3>
                Contacts
			    <a class="btn btn-xs btn-success" href="/admin/contacts/create">New Contact</a>
                <a class="btn btn-xs btn-primary" href="/admin/contact-locations">Manage Contact Locations</a>
                <a class="btn btn-xs btn-success" href="/admin/contact-products">Manage Contact Products</a>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table id="contactsTable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Full Name</th>
						<th>Department</th>
						<th>Job Title</th>
						<th>Phone Number</th>
						<th>Email</th>
						<th>Location</th>
						<th>Product</th>

						@if ($allowContactMgt == 1)
							<th class="td-actions">Order</th>
							<th class="td-actions">Edit</th>
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody>
				@foreach ($contacts as $contact)
					<tr>
						<td>{{$contact->full_name}}</td>
						<td>{{$contact->department}}</td>
						<td>{{$contact->job_title}}</td>
						<td>{{$contact->phone_number}}</td>
						<td>{{$contact->email}}</td>
						<td>{{$contact->location}}</td>
						<td>{{$contact->product}}</td>

						@if ($allowContactMgt == 1)
							<td class="td-actions">
								<a href="/admin/contacts/{{$contact->id}}/re-order" class="btn btn-xs btn-primary">
									<i class="btn-icon-only icon-list-alt"></i>
								</a>
							</td>
							<td class="td-actions">
								<a href="/admin/contacts/{{$contact->id}}/edit" class="btn btn-xs btn-primary">
									<i class="btn-icon-only icon-edit"></i>
								</a>
							</td>
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/contacts/' . $contact->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-contact')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>

@stop