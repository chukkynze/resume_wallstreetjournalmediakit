@section('form')

	<fieldset>

        <h4>Contact Details</h4><br />

		<div class="form-group">
		    <div class="col-md-1"></div>
			<label for="full_name" class="col-md-3">Full Name</label>
			<div class="col-md-8">
				{{ Form::text('full_name', $contact->full_name, array('class' => 'form-control', 'id' => 'full_name')) }}
				<span class="text-danger">{{ $errors->first('full_name') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<div class="form-group">
		    <div class="col-md-1"></div>
			<label for="department" class="col-md-3">Department</label>
			<div class="col-md-8">
				{{ Form::text('department', $contact->department, array('class' => 'form-control', 'id' => 'department')) }}
				<span class="text-danger">{{ $errors->first('department') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<div class="form-group">
		    <div class="col-md-1"></div>
			<label for="job_title" class="col-md-3">Job Title</label>
			<div class="col-md-8">
				{{ Form::text('job_title', $contact->job_title, array('class' => 'form-control', 'id' => 'job_title')) }}
				<span class="text-danger">{{ $errors->first('job_title') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<div class="form-group">
		    <div class="col-md-1"></div>
			<label for="phone_number" class="col-md-3">Phone Number</label>
			<div class="col-md-8">
				{{ Form::text('phone_number', $contact->phone_number, array('class' => 'form-control', 'id' => 'phone_number')) }}
				<span class="text-danger">{{ $errors->first('phone_number') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<div class="form-group">
		    <div class="col-md-1"></div>
			<label for="email" class="col-md-3">Email</label>
			<div class="col-md-8">
				{{ Form::text('email', $contact->email, array('class' => 'form-control', 'id' => 'email')) }}
				<span class="text-danger">{{ $errors->first('email') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<hr /><br />

        <h4>Assign Location &amp; Product</h4><br />

		<div class="form-group">
		    <div class="col-md-1"></div>
			<label class="col-md-3" for="location_id">Assign a Location</label>
			<div class="col-md-8">
				{{ Form::select('location_id', $locations, ($contact->location ?: 0) , array('class' => 'form-control', 'id' => 'location_id')) }}
				<span class="text-danger">{{ $errors->first('location_id') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->

		<div class="form-group">
		    <div class="col-md-1"></div>
			<label class="col-md-3" for="product_id">Assign a Product</label>
			<div class="col-md-8">
				{{ Form::select('product_id', $products, ($contact->product_id ?: 0) , array('class' => 'form-control', 'id' => 'product_id')) }}
				<span class="text-danger">{{ $errors->first('product_id') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<hr />

		<div class="form-group">

			<div class="col-md-offset-4 col-md-8">
				<button type="submit" class="btn btn-primary">Publish</button>
				<a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
			</div>
		</div> <!-- /form-actions -->
	</fieldset>
@show