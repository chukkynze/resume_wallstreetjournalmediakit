@extends('layouts.admin')
@section('content')
<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-inbox"></i>
				<h3>Create Contact</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/contacts', 'method' => 'post', 'id' => 'create-contact', 'class' => 'form-horizontal col-md-12')) }}
						@include('admin/contacts/form')
				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop