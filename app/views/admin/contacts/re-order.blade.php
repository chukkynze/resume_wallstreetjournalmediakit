@extends('layouts.admin')
@section('content')

    {{Form::hidden('index_row_order', $initIndexOrder, ['id'=>'index_row_order'])}}



    <div class="row">
        <div class="widget stacked widget-table action-table">

            <div style="color: red;">Click and drag the name below to reorder. Re-ordering here affects the site in
                real-time.
            </div>

            <div class="widget-header">
                <i class="icon-inbox"></i>

                <h3>
                    Contact Order for {{$Product->name}} at {{$Location->name}}
                    <a class="btn btn-xs btn-success" href="/admin/contacts">Back to Contacts</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
                </h3>
            </div>
            <!-- /widget-header -->

            <div class="widget-content">

                <table id="contactsOrderTable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>Department</th>
                        <th>Job Title</th>
                        <th>Phone Number</th>
                        <th>Email</th>

                    </tr>
                    </thead>
                    <tbody id="sortContactsOrder">
                    @foreach ($contacts as $contact)
                        <tr style="cursor: pointer;" id="index-row-{{$contact->id}}">
                            <td>{{$contact->full_name}}</td>
                            <td>{{$contact->department}}</td>
                            <td>{{$contact->job_title}}</td>
                            <td>{{$contact->phone_number}}</td>
                            <td>{{$contact->email}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>
            <!-- /widget-content -->
        </div>
    </div>

@stop