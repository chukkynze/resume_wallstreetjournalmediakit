@extends('......layouts.admin')
@section('content')

<style>
.hyfnSlimAlert
{
    padding:5px 25px 5px 15px;
    margin-bottom:5px;
}
</style>



<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-eye-open"></i>
				<h3>Create New Insight Page</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/insights', 'method' => 'post', 'id' => 'create-insight', 'class' => 'form-horizontal col-md-12')) }}


                    <fieldset>

                        <h4>Insight Page Details</h4><br />


                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label class="col-md-3" for="banner_img_id">Banner Image Upload</label>
                            <div class="col-md-8">
                                <div>
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Content File</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($bannerImages as $bannerImage)
                                            <tr>
                                                <td>{{ Form::radio('banner_img_id',$bannerImage->id, null, []) }}</td>
                                                <td>{{$bannerImage->name}}</td>
                                                <td>{{$bannerImage->categoryName}}</td>
                                                <td><a href="{{$bannerImage->download_folder}}{{$bannerImage->download_file}}" target="_blank">{{$bannerImage->download_file}}</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <span class="text-danger">{{ $errors->first('banner_img_id') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="name" class="col-md-3">Insight Page Title</label>
                            <div class="col-md-8">
                                {{ Form::text('title', $insight->title, array('class' => 'form-control', 'id' => 'title')) }}
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->


                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="subheading" class="col-md-3">Sub Heading</label>
                            <div class="col-md-8">
                                {{ Form::text('subheading', $insight->subheading, array('class' => 'form-control', 'id' => 'subheading', 'placeholder' => '')) }}
                                <span class="text-danger">{{ $errors->first('subheading') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->


                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="content1" class="col-md-3">Content Box 1</label>
                            <div class="col-md-8">
                                {{ Form::textarea('content1', $insight->content1, array('class' => 'form-control', 'id' => 'content1')) }}
                                <span class="text-danger">{{ $errors->first('content1') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->


                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="content2" class="col-md-3">Content Box 2</label>
                            <div class="col-md-8">
                                {{ Form::textarea('content2', $insight->content2, array('class' => 'form-control', 'id' => 'content2')) }}
                                <span class="text-danger">{{ $errors->first('content2') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->


                        <div class="form-group">
                            <div class="col-md-1"></div>

                            <div class="col-md-3">
                                <label for="banner_img_id">Reports</label>
                                <span class="text-danger">{{ $errors->first('report_order') }}</span>
                                {{ Form::hidden('report_order', '', array('id' => 'report_order')) }}

                                <table id="sortChosenInsightReportsOrderTable">
                                    <thead id="sortChosenInsightReportsOrderHdg" style="display: none;">
                                        <tr>
                                            <td>Drag to Reorder Your Reports</td>
                                        </tr>
                                    </thead>
                                    <tbody id="sortChosenInsightReportsOrder">

                                    </tbody>
                                </table>


                            </div>

                            <div class="col-md-8">


                                <div>

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Content File</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($reportFiles as $reportFile)
                                            <tr>
                                                <td>
                                                    {{ Form::checkbox('report_img_ids',$reportFile->id, null, ['class' => 'insightReportImageCheckBox', 'data-report-name' => ''. $reportFile->name .'']) }}
                                                    <span id="orderBox{{$reportFile->id}}"></span>
                                                </td>
                                                <td>{{$reportFile->name}}</td>
                                                <td>{{$reportFile->categoryName}}</td>
                                                <td><a href="{{$reportFile->download_folder}}{{$reportFile->download_file}}" target="_blank">{{$reportFile->download_file}}</a></td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>


                                <span class="text-danger">{{ $errors->first('insight_reports') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->


                        <hr />

                        <div class="form-group">

                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
                            </div>
                        </div> <!-- /form-actions -->
                    </fieldset>


				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop