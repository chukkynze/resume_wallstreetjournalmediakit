@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-eye-open"></i>
			<h3>Insight Page Content</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Updated At</th>
						<th>Page Title</th>
						<th>Sub Heading</th>
						<th>Banner Image</th>
						<th>Reports</th>

						@if ($allowInsightsMgt == 1)
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody>
				@foreach ($insights as $insight)
					<tr>
						<td>{{$insight->created_at}}</td>
						<td>{{$insight->title}}</td>
						<td>{{$insight->subheading}}</td>
						<td>
						    <a href="{{$insight->bannerImgFolder}}{{$insight->bannerImgFile}}">{{$insight->bannerImgName}}</a>
						</td>
						<td>
						    <div    class="ui-popover"
						            data-container="body"
						            data-toggle="popover"
						            data-trigger="hover"
						            data-placement="left"
						            data-content="{{$insight->reportFiles}}"
						            title=""
						            data-original-title="Selected Reports">
						        {{$insight->reportFilesCount > 0 ? $insight->reportFilesCount : 0}} Reports
						    </div>
						</td>


						@if ($allowInsightsMgt == 1)
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/insights/' . $insight->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-insight')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>
@stop