@section('form')

	<fieldset>
		<div class="form-group">
			<label for="username" class="col-md-4">Category Name</label>
			<div class="col-md-8">
				{{ Form::text('name', $uploadCategory->name, array('class' => 'form-control', 'id' => 'name')) }}
				<span class="text-danger">{{ $errors->first('name') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->

		<!--
		<div class="form-group">
			<label for="order" class="col-md-4">CMS Display Order</label>
			<div class="col-md-8">
				{{ Form::text('order', $uploadCategory->order, array('class' => 'form-control', 'id' => 'order')) }}
				<span class="text-danger">{{ $errors->first('order') }}</span>
			</div>
		</div>
		-->

		<br />

		<div class="form-group">

			<div class="col-md-offset-4 col-md-8">
				<button type="submit" class="btn btn-primary">Publish</button>
				<a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
			</div>
		</div> <!-- /form-actions -->
	</fieldset>
@show