@extends('layouts.admin')
@section('content')
<div class="row">

	<div class="col-md-8">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-cloud-upload"></i>
				<h3>Create New Upload Category</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/upload-categories', 'method' => 'post', 'id' => 'create-upload-category', 'class' => 'form-horizontal col-md-8')) }}
						@include('admin/upload-categories/form')
				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop