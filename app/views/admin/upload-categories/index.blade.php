@extends('layouts.admin')
@section('content')

	{{Form::hidden('index_row_order', $initIndexOrder, ['id'=>'index_row_order'])}}

<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-th-list"></i>
			<h3>
                Upload Categories
			    <a class="btn btn-xs btn-success" href="/admin/upload-categories/create">New Upload Category</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Name</th>

						@if ($allowUploadCategoryMgt == 1)
							<th class="td-actions">Edit</th>
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody id="sortUploadCategoriesOrder">

				@foreach ($uploadCategories as $uploadCategory)
					<tr style="cursor: pointer;" id="index-row-{{$uploadCategory->id}}">
						<td>{{$uploadCategory->name}}</td>
						@if ($allowUploadCategoryMgt == 1)
							<td class="td-actions">
								<a href="/admin/upload-categories/{{$uploadCategory->id}}/edit"
								   class="btn btn-xs btn-primary">
									<i class="btn-icon-only icon-edit"></i>
								</a>
							</td>
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/upload-categories/' . $uploadCategory->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'category_id')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>
@stop