@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-th-list"></i>
			<h3>User Roles
			<a class="btn btn-xs btn-success" data-toggle="modal" href="#addRoleModal">Add New Role</a>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Role Name</th>
						<th>Current Permissions</th>

						@if ($allowRoleMgt == 1)
							<th class="td-actions">Assign to Users</th>
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody>
				@foreach ($roles as $role)
					<tr>
						<td>{{$role->name}}</td>
						<td>{{$role->permissionsList}}</td>

						@if ($allowRoleMgt == 1)
							<td class="td-actions">
								<a class="btn btn-xs btn-primary userModalLink" data-toggle="modal" data-role-id="{{$role->id}}" data-role-name="{{$role->name}}">
								    <i class="btn-icon-only icon-edit"></i>
								</a>
							</td>
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/roles/' . $role->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-role')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>


<!-- /Add Role Model -->
<div id="addRoleModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add a New Role</h4>
            </div>

            <div class="modal-body">
                <p>Role Name: <br><input id="newRoleName" type="text" value=""></p>
                <div>
                    <button type="button" class="btn btn-primary pull-right" id="saveNewRoleButton">Save</button>
                </div>
                <br />
            </div>
        </div>
    </div>
</div>


<!-- /User Form Model -->
<div id="userModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Assign Role to User</h4>
                <input type="hidden" id="roleId" value="">
            </div>

            <div class="modal-body">
                <p>Assign the <strong><span id="roleName"></span></strong> role to the user:  <select id="roleUser">
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->displayValue}}</option>
                    @endforeach
                </select>. </p>
                <div>
                    <button type="button" class="btn btn-primary pull-right" id="saveAssignedUserButton">Save</button>
                </div>
                <br />
            </div>
        </div>
    </div>
</div>

@stop