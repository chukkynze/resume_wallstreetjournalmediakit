@section('form')

	<fieldset>

        <h4>Upload Details</h4><br />

		<div class="form-group">
		    <div class="col-md-1"></div>
			<label for="name" class="col-md-3">Upload Name</label>
			<div class="col-md-8">
				{{ Form::text('name', $upload->name, array('class' => 'form-control', 'id' => 'name')) }}
				<span class="text-danger">{{ $errors->first('name') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<div class="form-group">
		    <div class="col-md-1"></div>
			<label for="description" class="col-md-3">Description</label>
			<div class="col-md-8">
				{{ Form::textarea('description', $upload->description, array('class' => 'form-control', 'id' => 'description')) }}
				<span class="text-danger">{{ $errors->first('description') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<div class="form-group">
		    <div class="col-md-1"></div>
			<label class="col-md-3" for="download_file">Download File</label>
			<div class="col-md-8">
				{{ Form::file('download_file', array('class' => 'form-control', 'id' => 'download_file')) }}
				<span class="text-danger">{{ $errors->first('download_file') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<div class="form-group">
		    <div class="col-md-1"></div>
			<label for="keywords" class="col-md-3">Keywords</label>
			<div class="col-md-8">
				{{ Form::textarea('keywords', $upload->keywords, array('class' => 'form-control', 'id' => 'keywords')) }}
				<span class="text-danger">{{ $errors->first('keywords') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<hr /><br />

        <h4>Meta Information</h4><br />

		<div class="form-group">
		    <div class="col-md-1"></div>
			<label class="col-md-3" for="status">Current Status</label>
			<div class="col-md-8">
				{{ Form::select('status', ['Draft'=> 'Draft','Published'=>'Published'], ($upload->status ?: 'Draft') , array('class' => 'form-control', 'id' => 'status')) }}
				<span class="text-danger">{{ $errors->first('status') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->

		<div class="form-group">
		    <div class="col-md-1"></div>
			<label class="col-md-3" for="uploader_id">Uploaded By</label>
			<div class="col-md-8">
				{{ Form::select('uploader_id', $usersArray, ($upload->uploader_id ?: 0) , array('class' => 'form-control', 'id' => 'uploader_id')) }}
				<span class="text-danger">{{ $errors->first('uploader_id') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<hr />

		<div class="form-group">

			<div class="col-md-offset-4 col-md-8">
				<button type="submit" class="btn btn-primary">Save</button>
				<a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
			</div>
		</div> <!-- /form-actions -->
	</fieldset>
@show