@extends('layouts.admin')

@section('css')
@parent
<link href="/assets/admin/css/pages/signin.css" rel="stylesheet">
@stop

@section('body')
<nav class="navbar navbar-inverse" role="navigation">

	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.html">{{ $cmsName }}</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="">
                    <a href="/" target="_blank">
                        View Site
					</a>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div> <!-- /.container -->
</nav>



<div class="account-container stacked">
	@if ( Session::get('error') )
		<div class="alert alert-danger alert-dismissable">
			{{ $errors->first('login_error') }}
		</div>
	@endif
	@if ( Session::get('notice') )
		<div class="alert alert-warning alert-dismissable">
			{{{ Session::get('notice') }}}
		</div>
	@endif
	<div class="content clearfix">

		<form method="POST" action="{{ URL::to('/admin/users/reset-password') }}" accept-charset="UTF-8">
		    <input type="hidden" name="token" value="{{{ $token }}}">
		    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

			<h1>Reset Password</h1>

			<div class="form-group">
		        <label for="password">{{{ Lang::get('confide::confide.password') }}}</label>
		        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
		    </div>
		    <div class="form-group">
		        <label for="password_confirmation">{{{ Lang::get('confide::confide.password_confirmation') }}}</label>
		        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation">
		    </div>

			<div class="form-actions form-group">
		        <button type="submit" class="btn btn-primary">{{{ Lang::get('confide::confide.forgot.submit') }}}</button>
		    </div>

		</form>

	</div> <!-- /content -->

</div> <!-- /account-container -->


<!-- Text Under Box -->
<div class="login-extra">
	<a href="/admin/login">Back to Login</a>
</div> <!-- /login-extra -->
@overwrite