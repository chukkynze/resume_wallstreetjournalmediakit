@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="widget stacked widget-table action-table">

        <div class="bs-example">
            <a class="btn btn-xs btn-success" href="/admin/site-products/create">Add New Product</a>
            <a class="btn btn-xs btn-success" href="/admin/site-products">Manage Products and Subsections</a>
            <br><br>

            <ul id="myTab" class="nav nav-tabs">

                <li class="@if( Session::get('tab') == 3 ) active @elseif(Session::get('tab') == 4 || Session::get('tab') == 5) @else active @endif">
                    <a href="#landingSections" class="dropdown-toggle" data-toggle="tab">
                        Product Landing Page
                    </a>
                </li>

                <li class="dropdown @if( Session::get('tab') == 3 )  @elseif(Session::get('tab') == 4 || Session::get('tab') == 5) active @else  @endif">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Product Details Pages <b class="caret"></b>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        @foreach($detailsPageProducts as $key=>$detailsPageProduct)
                            <li>
                                <a href="#detailsPages{{$detailsPageProduct[0]['entityID']}}" tabindex="-1" data-toggle="tab">
                                    {{$detailsPageProduct[0]['name']}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>

            </ul>


        <div class="tab-content">

            <div class="tab-pane fade @if( Session::get('tab') == 3 ) active @elseif(Session::get('tab') == 4 || Session::get('tab') == 5) @else active @endif in"
                 id="landingSections">

                <div class="widget-header">
                    <i class="icon-circle-arrow-down"></i>

                    <h3>
                        Product Landing Page Content
                        <a class="btn btn-xs btn-success" href="/admin/page-content/product-pages/create">Edit</a>
                        <a class="btn btn-xs btn-info" href="/preview/products" target="_blank">Preview Landing Page</a>
                    </h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Updated At</th>
                            <th>Heading</th>
                            <th>Background Image</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$ProductPage->created_at}}</td>
                            <td>{{$ProductPage->heading}}</td>
                            <td><img src="{{$ProductPage->background_image}}" height="140" width="auto"/></td>
                        </tr>
                        </tbody>
                    </table>

                </div>
                <!-- /widget-content -->


                <br><br>
                <h4>
                    Product Modules
                </h4>
                <br>
            @foreach($landingPageProducts as $key=>$landingPageProduct)
                    <div class="widget-header col-lg-12">
                        <i class="icon-eye-open"></i>
                        <h3>
                            {{$landingPageProduct['name']}} Section
                            Modules {{( $landingPageProduct['mainSectionLiveModuleExist'] == 0 ? '<span class="alert alert-danger inline" style="padding:5px;"> !!! No Live Modules in Main Section !!! </span>' : '')}}
                            @if($landingPageProduct['subsectionExist'] == 1)
                                {{( $landingPageProduct['subsectionLiveModuleExist'] == 0 ? '<span class="alert alert-danger inline" style="padding:5px;"> !!! No Live Modules in Subsection !!! </span>' : '')}}
                            @endif
                        </h3>
                        <span class="pull-right">
                            @if ($allowPageContentMgt == 1)
                                <a class="btn btn-xs btn-success" data-toggle="modal" href="#moduleModalLandingLocation{{$landingPageProduct['locationID']}}Section{{$landingPageProduct['entityID']}}">Add Modules</a>
                                <a
                                    class="btn btn-xs btn-primary saveLiveOrderButton"
                                    href="#"

                                    data-module-location-area="landing"
                                    data-module-location-id="{{$landingPageProduct['locationID']}}"
                                    data-module-entity-id="{{$landingPageProduct['entityID']}}"
                                >
                                    Publish Changes to {{$landingPageProduct['moduleLocationName']}}
                                </a>
                            @endif
                        </span>
                    </div> <!-- /widget-header -->

                    <div class="widget-content">

                       {{ Form::hidden('landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleOrder'       , $landingPageProduct['initialModuleOrder']    , array('id' => 'landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleOrder')) }}
                       {{ Form::hidden('landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModulePage'        , 'product-pages'                              , array('id' => 'landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModulePage')) }}

                       {{ Form::hidden('landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleLocationID'  , $landingPageProduct['locationID']            , array('id' => 'landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleLocationID')) }}
                       {{ Form::hidden('landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleEntityID'    , $landingPageProduct['entityID']              , array('id' => 'landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleEntityID')) }}
                       {{ Form::hidden('landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleEntityType'  , $landingPageProduct['entityType']            , array('id' => 'landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleEntityType')) }}

                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Live?</th>
                                    <th>Module Title</th>

                                    @if ($allowPageContentMgt == 1)
                                        <th class="td-actions">Edit</th>
                                        <th class="td-actions">Delete</th>
                                    @endif

                                </tr>
                            </thead>
                                <tbody class="sortTableRowModuleOrder" id="landingLocation{{$landingPageProduct['locationID']}}Section{{$landingPageProduct['entityID']}}TBody">
                                @foreach ($landingPageProduct['allModules'] as $key=>$module)
                                <?php $moduleObject = $module['moduleObject']; ?>
                                    <tr style="cursor: pointer;"
                                    id="location-{{$moduleObject->relationshipLocationID}}-module-relationship-{{$moduleObject->relationshipID}}"
                                    data-location-{{$moduleObject->relationshipLocationID}}-module-relationship-id="location{{$landingPageProduct['locationID']}}module-{{$moduleObject->relationshipID}}"

                                    data-module-location-area="landing"
                                    data-module-location-id="{{$landingPageProduct['locationID']}}"
                                    data-module-entity-id="{{$landingPageProduct['entityID']}}"
                                    data-module-relationship-id="{{$moduleObject->relationshipID}}"
                                    >
                                        <td class="text-center col-md-1">{{ Form::checkbox('landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleCheckbox[]', $moduleObject->relationshipID, ($moduleObject->is_live ?: null), ['class' => 'landingLocation'.$landingPageProduct['locationID'].'Section'.$landingPageProduct['entityID'].'ModuleCheckbox'] ) }}</td>
                                        <td>
                                            <div class="col-lg-12" style="">
                                                {{ $moduleObject->title != "" ? $moduleObject->title : "Empty Title"}}
                                                <span
                                                    class="module-mgt-row-icon pull-right"
                                                    data-module-details-type="{{$module['partial_name']}}"
                                                    data-module-details-id="{{$moduleObject->relationshipID}}"
                                                    >
                                                    <i id="module-mgt-row-icon-{{$module['partial_name']}}-{{$moduleObject->relationshipID}}" class="btn-icon-only icon-chevron-down"></i>
                                                </span>
                                            </div>
                                            <div id="module-details-{{$module['partial_name']}}-{{$moduleObject->relationshipID}}" style="display: none;">
                                                <br>
                                                <div style="" class="view-preview">
                                                    @include("/home/modules/" . $module['partial_name'], ['sectionKey' => "-" . $landingPageProduct['locationID'] . "-" . $moduleObject->relationshipID . "-", 'key' => $key, 'moduleObject' => $module['moduleObject']])
                                                </div>
                                            </div>
                                        </td>

                                        @if ($allowPageContentMgt == 1)
                                            <td class="td-actions">
                                                <a href="/admin/page-content/modules/{{$moduleObject->relationshipID}}/edit" class="btn btn-xs btn-primary">
                                                    <i class="btn-icon-only icon-edit"></i>
                                                </a>
                                            </td>
                                            <td class="td-actions">
                                                {{ Form::open(array('url' => 'admin/page-content/modules/' . $moduleObject->relationshipID, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-module')) }}
                                                        <button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
                                                {{ Form::close() }}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>

		            </div> <!-- /widget-content -->

                    <br><br><br>
            @endforeach
            </div>



            @foreach($detailsPageProducts as $key=>$detailsPageProduct)
                <div class="tab-pane fade
            @if(Session::get('tab') == 3 && Session::get('entityID') == $detailsPageProduct[0]['entityID'])

            @elseif(Session::get('tab') == 4 && Session::get('entityID') == $detailsPageProduct[0]['entityID'])
                active in
            @elseif(Session::get('tab') == 5 && true == in_array((int) Session::get('entityID'), $detailsPageProduct[0]['childrenEntityIDs']))
                active in
            @endif " id="detailsPages{{$detailsPageProduct[0]['entityID']}}">

                @foreach($detailsPageProduct as $key=>$subSectionModule)

                        @if($key == 0 && $subSectionModule['parentLiveModuleExist'] == 0)
                            <div class="alert alert-danger inline" style=""> !!! {{$subSectionModule['name']}} has no
                                Live Modules in Product Landing Page !!!
                            </div>@endif

                        <h4>{{$subSectionModule['name']}}</h4>
                        @if($key == 0)
                            <a class="btn btn-xs btn-info"
                               href="/preview/products/{{$detailsPageProduct[0]['name_slug']}}"
                               target="_blank">
                                Preview Product Details Page
                            </a>
                            <br><br>
                        @endif

                    <div class="widget-header col-lg-12">
                        <i class="icon-eye-open"></i>
                        <h3>
                            {{$subSectionModule['name']}} @if($key == 0) Section @else Subsection @endif Modules
                        </h3>
                        <span class="pull-right">

                            @if ($allowPageContentMgt == 1)
                                <a class="btn btn-xs btn-success" data-toggle="modal" href="#moduleModalDetailsLocation{{$subSectionModule['locationID']}}Section{{$subSectionModule['parentEntityID']}}SubSection{{$subSectionModule['entityID']}}">Add Modules</a>
                                <a
                                    class="btn btn-xs btn-primary saveDetailsLiveOrderButton"
                                    href="#"

                                    data-module-location-area="details"
                                    data-module-location-id="{{$subSectionModule['locationID']}}"
                                    data-module-parent-entity-id="{{$subSectionModule['parentEntityID']}}"
                                    data-module-entity-id="{{$subSectionModule['entityID']}}"
                                >
                                    Publish Changes to {{$subSectionModule['moduleLocationName']}}
                                </a>
                            @endif

                        </span>
                    </div>
                    <!-- /widget-header -->

                    <div class="widget-content">

                       {{ Form::hidden('detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleOrder'       , $subSectionModule['initialModuleOrder']    , array('id' => 'detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleOrder')) }}
                       {{ Form::hidden('detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModulePage'        , 'product-pages'                            , array('id' => 'detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModulePage')) }}

                       {{ Form::hidden('detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleLocationID'  , $subSectionModule['locationID']            , array('id' => 'detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleLocationID')) }}
                       {{ Form::hidden('detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleEntityID'    , $subSectionModule['entityID']              , array('id' => 'detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleEntityID')) }}
                       {{ Form::hidden('detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleEntityType'  , $subSectionModule['entityType']            , array('id' => 'detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleEntityType')) }}

                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Live?</th>
                                    <th>Module Title</th>

                                    @if ($allowPageContentMgt == 1)
                                        <th class="td-actions">Edit</th>
                                        <th class="td-actions">Delete</th>
                                    @endif

                                </tr>
                            </thead>
                            <tbody class="sortTableRowModuleOrder" id="detailsLocation{{$subSectionModule['locationID']}}Section{{$subSectionModule['parentEntityID']}}SubSection{{$subSectionModule['entityID']}}TBody">

                                @foreach ($subSectionModule['allModules'] as $key=>$module)
                                    <?php $moduleObject = $module['moduleObject']; ?>
                                    <tr style="cursor: pointer;"
                                    id="location-{{$moduleObject->relationshipLocationID}}-module-relationship-{{$moduleObject->relationshipID}}"
                                    data-location-{{$moduleObject->relationshipLocationID}}-module-relationship-id="location{{$subSectionModule['locationID']}}module-{{$moduleObject->relationshipID}}"

                                    data-module-location-area="details"
                                    data-module-location-id="{{$subSectionModule['locationID']}}"
                                    data-module-entity-id="{{$subSectionModule['entityID']}}"
                                    data-module-parent-entity-id="{{$subSectionModule['parentEntityID']}}"
                                    data-module-relationship-id="{{$moduleObject->relationshipID}}"
                                    >
                                        <td class="text-center col-md-1">{{ Form::checkbox('detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleCheckbox[]', $moduleObject->relationshipID, ($moduleObject->is_live ?: null), ['class' => 'detailsLocation'.$subSectionModule['locationID'].'Section'.$subSectionModule['parentEntityID'].'SubSection'.$subSectionModule['entityID'].'ModuleCheckbox'] ) }}</td>
                                        <td>
                                            <div class="col-lg-12" style="">
                                                {{ $moduleObject->title != "" ? $moduleObject->title : "Empty Title"}}
                                                <span
                                                    class="module-mgt-row-icon pull-right"
                                                    data-module-details-type="{{$module['partial_name']}}"
                                                    data-module-details-id="{{$moduleObject->relationshipID}}"
                                                    >
                                                    <i id="module-mgt-row-icon-{{$module['partial_name']}}-{{$moduleObject->relationshipID}}" class="btn-icon-only icon-chevron-down"></i>
                                                </span>
                                            </div>
                                            <div id="module-details-{{$module['partial_name']}}-{{$moduleObject->relationshipID}}" style="display: none;">
                                                <br>
                                                <div  class="view-preview">
                                                    @include("/home/modules/" . $module['partial_name'], ['sectionKey' => "-" . $subSectionModule['locationID'] . "-" . $moduleObject->relationshipID . "-", 'key' => $key, 'moduleObject' => $module['moduleObject']])
                                                </div>
                                            </div>
                                        </td>

                                        @if ($allowPageContentMgt == 1)
                                            <td class="td-actions">
                                                <a href="/admin/page-content/modules/{{$moduleObject->relationshipID}}/edit" class="btn btn-xs btn-primary">
                                                    <i class="btn-icon-only icon-edit"></i>
                                                </a>
                                            </td>
                                            <td class="td-actions">
                                                {{ Form::open(array('url' => 'admin/page-content/modules/' . $moduleObject->relationshipID, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-module')) }}
                                                        <button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
                                                {{ Form::close() }}
                                            </td>
                                        @endif
                                    </tr>

                                @endforeach

                            </tbody>
                        </table>

		            </div> <!-- /widget-content -->

                    <br><br><br>

                @endforeach

            </div>
            @endforeach

        </div>
    </div>





	</div>
</div>




@foreach($landingPageProducts as $key=>$moduleLandingPageProduct)

    <div id="moduleModalLandingLocation{{$moduleLandingPageProduct['locationID']}}Section{{$moduleLandingPageProduct['entityID']}}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            {{ Form::open(array('url' => 'admin/page-content/modules/create', 'method' => 'post', 'id' => 'create-module', 'class' => 'form-horizontal col-md-12')) }}

                {{ Form::hidden('locationID', $moduleLandingPageProduct['locationID']) }}
                {{ Form::hidden('entityType', $moduleLandingPageProduct['entityType']) }}
                {{ Form::hidden('entityID'  , $moduleLandingPageProduct['entityID']) }}

                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Choose Module Type</h4>
                    </div>
                    <div class="modal-body clearfix">

                        <p>Select the type of module you want to add to the <strong>{{$moduleLandingPageProduct['moduleLocationName']}} section</strong> from the drop down below and click create module to add your custom content.</p>

                        <hr>

                        {{Form::select('moduleType', $moduleLandingPageProduct['moduleTypes'], 0, ['class' => 'select-module-type createModuleSelectField', 'data-button-id' => 'createModuleSubmitButton1-' . $moduleLandingPageProduct['locationID'] . '-' . $moduleLandingPageProduct['entityID']])}}

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="createModuleSubmitButton1-{{$moduleLandingPageProduct['locationID'] . '-' . $moduleLandingPageProduct['entityID']}}"
                                type="submit" class="btn btn-primary disabled">Create
                            Module
                        </button>
                    </div>

                </div>
                <!-- /.modal-content -->

            {{ Form::close() }}

        </div>
        <!-- /.modal-dialog -->
    </div>

@endforeach




@foreach($detailsPageProducts as $key=>$moduleDetailPageProduct)

    @foreach($moduleDetailPageProduct as $key=>$moduleDetails)

        <div id="moduleModalDetailsLocation{{$moduleDetails['locationID']}}Section{{$moduleDetails['parentEntityID']}}SubSection{{$moduleDetails['entityID']}}" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">

                {{ Form::open(array('url' => 'admin/page-content/modules/create', 'method' => 'post', 'id' => 'create-module', 'class' => 'form-horizontal col-md-12')) }}

                    {{ Form::hidden('locationID', $moduleDetails['locationID']) }}
                    {{ Form::hidden('entityType', $moduleDetails['entityType']) }}
                    {{ Form::hidden('entityID'  , $moduleDetails['entityID']) }}

                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Choose Module Type</h4>
                        </div>
                        <div class="modal-body">

                            <p>Select the type of module you want to add to the <strong>{{$moduleDetails['moduleLocationName']}} section</strong> from the drop down below and click create module to add your custom content.</p>

                            <hr>

                            {{Form::select('moduleType', $moduleDetails['moduleTypes'], 0, ['class' => 'select-module-type createModuleSelectField', 'data-button-id' => 'createModuleSubmitButton2-' . $moduleDetails['locationID'] . '-' . $moduleDetails['parentEntityID'] . '-' . $moduleDetails['entityID'] ])}}

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button id="createModuleSubmitButton2-{{$moduleDetails['locationID'] . '-' . $moduleDetails['parentEntityID'] . '-' . $moduleDetails['entityID']}}"
                                    type="submit" class="btn btn-primary disabled">Create
                                Module
                            </button>
                        </div>

                    </div>
                    <!-- /.modal-content -->

                {{ Form::close() }}

            </div>
            <!-- /.modal-dialog -->
        </div>

    @endforeach

@endforeach
@stop
