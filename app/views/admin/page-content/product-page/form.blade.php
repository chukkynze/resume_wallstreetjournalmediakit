@section('form')

	<fieldset>

        {{ Form::hidden('section_order', null, array('class' => 'form-control', 'id' => 'section_order')) }}
        {{ Form::hidden('module_order' , null, array('class' => 'form-control', 'id' => 'module_order')) }}
        {{ Form::hidden('banner_image_enabled'  , 0) }}
        {{ Form::hidden('source_text_enabled'   , 0) }}

        <div class="form-group">
            <label for="heading" class="col-md-3">Page Heading</label>
            <div class="col-md-11">
                {{ Form::text('heading', $productPage->heading, array('class' => 'form-control', 'id' => 'heading')) }}
                <span class="text-danger">{{ $errors->first('heading') }}</span>
            </div> <!-- /controls -->
        </div> <!-- /control-group -->

        <div class="form-group">

            <label for="banner_image_upload_id" class="col-md-3">Banner Images</label>
            <div class="col-md-11">
                <span>{{ Form::checkbox('banner_image_enabled', 1, ($productPage->banner_image_enabled == 1 ? true : null), []) }}
                    Click to display banner image selected below.</span>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Content File</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($bannerImages as $bannerImage)
                        <tr>
                            <td>{{ Form::radio('banner_image_upload_id', $bannerImage->id, ( $bannerImage->id == $productPage->banner_image_upload_id ? true : null ), []) }}</td>
                            <td>{{$bannerImage->name}}</td>
                            <td>{{$bannerImage->categoryName}}</td>
                            <td><a href="{{$bannerImage->download_folder}}{{$bannerImage->download_file}}" target="_blank">{{$bannerImage->download_file}}</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <span><a href="/admin/uploads/create" target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Available Banner Images
                <span class="text-danger">{{ $errors->first('home_page_slider_upload_id') }}</span>
            </div> <!-- /controls -->
        </div> <!-- /control-group -->


        <div class="form-group">
            <label for="source_text" class="col-md-3">Source Text</label>

            <div class="col-md-11">
            <span>
                {{ Form::checkbox('source_text_enabled', 1, ($productPage->source_text_enabled == 1 ? true : null), []) }}
                    Click to display source text entered below.</span>
                {{ Form::textarea('source_text', $productPage->source_text, array('class' => 'form-control', 'id' => 'source_text')) }}
                <span class="text-danger">{{ $errors->first('source_text') }}</span>
            </div> <!-- /controls -->
        </div> <!-- /control-group -->

        <hr />

        <!--
        <div class="form-group">
            <label for="source_text" class="col-md-12">Products selected for Landing Page.<br>Drag to re-order.</label>
            <div class="col-md-1"></div>
            <div class="col-md-6" style="background-color: gainsboro; min-height: 20px;">

                <table class="col-md-12">

                    <tbody id="sortProductPageSectionModulesOrder">

                        @foreach($productSections as $productSection)

                        <tr id="product-section-module-{{$productSection['id']}}"        data-siteProduct-name="product-section-module-{{$productSection['id']}}">
                            <td>
                                <hr>

                                <div class="col-md-1"></div>
                                <div class="col-md-11">{{$productSection['camelCaseName']}}</div>

                            </td>
                        </tr>

                        @endforeach

                    </tbody>

                </table>
                <br>

        </div>
        -->

        <hr />

        <div class="form-group">

            <div class="col-md-offset-4 col-md-8">
                <button type="submit" class="btn btn-primary">Publish</button>
                <a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
            </div>
        </div> <!-- /form-actions -->
    </fieldset>
@show