@extends('......layouts.admin')
@section('content')

<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-circle-arrow-down"></i>

				<h3>Edit Product Landing Page Content</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/page-content/product-pages', 'method' => 'post', 'id' => 'create-product-page', 'class' => 'form-horizontal col-md-12')) }}

                    @include('admin.page-content.product-page.form')

				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop