@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-eye-open"></i>
			<h3>
			    Insight Page Content
			    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    <a class="btn btn-xs btn-success" href="/admin/page-content/insight-pages/create">Edit Insight Page Content</a>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Updated At</th>
						<th>Page Title</th>
						<th>Banner Image</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{{$insights->created_at}}</td>
						<td>{{$insights->title}}</td>
						<td>
						    <img src="{{$insights->bannerImgFolder}}{{$insights->bannerImgFile}}" height="140" width="auto" alt="{{$insights->bannerImgName}}" />
						</td>
					</tr>

				</tbody>
			</table>



		</div> <!-- /widget-content -->

        <br><br>

		<div class="widget-header col-lg-12">
			<i class="icon-eye-open"></i>
			<h3>
			    Insight Page Modules
			    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    <a class="btn btn-xs btn-info" href="/preview/insights" target="_blank">Preview Insights Page</a>
                @if ($allowInsightsMgt == 1)
                    <a class="btn btn-xs btn-success" data-toggle="modal" href="#moduleModal">Add Modules</a>
					<a id="saveLiveOrderLocation2Button" class="btn btn-xs btn-primary" href="#">Publish</a>
                @endif
			</h3>
		</div> <!-- /widget-header -->


        <div class="widget-content">


           {{ Form::hidden('location2ModuleOrder', '', array('id' => 'location2ModuleOrder')) }}
           {{ Form::hidden('location2ModulePage', 'insight-pages', array('id' => 'location2ModulePage')) }}

           {{ Form::hidden('location2ModuleLocationID'  , 2             , array('id' => 'location2ModuleLocationID')) }}
           {{ Form::hidden('location2ModuleEntityID'    , $entityID     , array('id' => 'location2ModuleEntityID')) }}
           {{ Form::hidden('location2ModuleEntityType'  , $entityType   , array('id' => 'location2ModuleEntityType')) }}

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th class="text-center">Live?</th>
						<th>Module Title</th>

						@if ($allowInsightsMgt == 1)
							<th class="td-actions">Edit</th>
							<th class="td-actions">Delete</th>
						@endif

					</tr>
				</thead>
				<tbody id="sortLocation2ModulesOrder" style="padding:5px;">
				@foreach ($allModules as $key=>$module)
				<?php $moduleObject = $module['moduleObject']; ?>
					<tr style="cursor: pointer;"
					id="location-{{$moduleObject->relationshipLocationID}}-module-relationship-{{$moduleObject->relationshipID}}"
					data-location-{{$moduleObject->relationshipLocationID}}-module-relationship-id="location2module-{{$moduleObject->relationshipID}}"
					>
						<td class="text-center col-md-1">{{ Form::checkbox('location2ModuleCheckbox[]', $moduleObject->relationshipID, ($moduleObject->is_live ?: null), ['class' => 'location2ModuleCheckbox']) }}</td>
						<td>
						    <div class="col-lg-12" style="">{{ $moduleObject->title != "" ? $moduleObject->title : "Empty Title"}}<span class="module-mgt-row-icon pull-right" data-module-details-type="{{$module['partial_name']}}" data-module-details-id="{{$moduleObject->id}}"><i id="module-mgt-row-icon-{{$module['partial_name']}}-{{$moduleObject->id}}" class="btn-icon-only icon-chevron-down"></i></span></div>
						    <div id="module-details-{{$module['partial_name']}}-{{$moduleObject->id}}" style="display: none;">
						        <br>
						        <div class="view-preview">
						            @include("/home/modules/" . $module['partial_name'], ['sectionKey' => 2, 'key' => $key, 'moduleObject' => $module['moduleObject']])
						        </div>
						    </div>
						</td>

						@if ($allowInsightsMgt == 1)
							<td class="td-actions">
								<a href="/admin/page-content/modules/{{$moduleObject->relationshipID}}/edit" class="btn btn-xs btn-primary">
									<i class="btn-icon-only icon-edit"></i>
								</a>
							</td>
							<td class="td-actions">
								{{ Form::open(array('url' => 'admin/page-content/modules/' . $moduleObject->relationshipID, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-module')) }}
										<button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
								{{ Form::close() }}
							</td>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>



		</div> <!-- /widget-content -->


	</div>
</div>



<div id="moduleModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">

        {{ Form::open(array('url' => 'admin/page-content/modules/create', 'method' => 'post', 'id' => 'create-module', 'class' => 'form-horizontal col-md-12')) }}

            {{ Form::hidden('locationID', $locationID) }}
            {{ Form::hidden('entityType', $entityType) }}
            {{ Form::hidden('entityID'  , $entityID) }}

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Choose Module Type</h4>
                </div>
                <div class="modal-body clearfix">

                    <p>Select the type of module you want to add from the drop down below and click create module to add your custom content.</p>

                    <hr>

					{{Form::select('moduleType', $moduleTypes, 0, ['class' => 'select-module-type createModuleSelectField', 'data-button-id' => 'createModuleSubmitButton'])}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button id="createModuleSubmitButton" type="submit" class="btn btn-primary disabled">Create Module
					</button>
                </div>

            </div>
            <!-- /.modal-content -->

        {{ Form::close() }}

    </div>
    <!-- /.modal-dialog -->
</div>
@stop
