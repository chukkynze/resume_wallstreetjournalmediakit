@extends('......layouts.admin')
@section('content')

<style>
.hyfnSlimAlert
{
    padding:5px 25px 5px 15px;
    margin-bottom:5px;
}
</style>



<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
                <i class="icon-archive"></i>
				<h3>Edit Custom Studios Page</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/page-content/studios-pages', 'method' => 'post', 'id' => 'create-studios', 'class' => 'form-horizontal col-md-12')) }}


                    <fieldset>

                        <h4>Custom Studios Page Details</h4><br />


                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label class="col-md-3" for="banner_img_id">Banner Image Upload</label>
                            <div class="col-md-8">
                                <div>
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Content File</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($bannerImages as $bannerImage)
                                            <tr>
                                                <td>{{ Form::radio('banner_img_id', $bannerImage->id, ( $bannerImage->id == $studiosPage->banner_img_id ? true : null ), []) }}</td>
                                                <td>{{$bannerImage->name}}</td>
                                                <td>{{$bannerImage->categoryName}}</td>
                                                <td><a href="{{$bannerImage->download_folder}}{{$bannerImage->download_file}}" target="_blank">{{$bannerImage->download_file}}</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <span class="text-danger">{{ $errors->first('banner_img_id') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="name" class="col-md-3">Custom Studios Page Title</label>
                            <div class="col-md-8">
                                {{ Form::text('title', $studiosPage->title, array('class' => 'form-control', 'id' => 'title')) }}
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->


                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label for="source_text" class="col-md-3">Source Text</label>
                            <div class="col-md-8">
                                {{ Form::textarea('source_text', $studiosPage->source_text, array('class' => 'form-control', 'id' => 'source_text')) }}
                                <span class="text-danger">{{ $errors->first('source_text') }}</span>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->


                        <hr />

                        <div class="form-group">

                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" class="btn btn-primary">Publish</button>
                                <a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
                            </div>
                        </div> <!-- /form-actions -->
                    </fieldset>


				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop