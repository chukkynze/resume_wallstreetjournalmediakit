@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
            <i class="icon-archive"></i>
			<h3>
			    Custom Studios Page Content
			    <a class="btn btn-xs btn-success" href="/admin/page-content/studios-pages/create">Edit Custom Studios Page Content</a>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Updated At</th>
						<th>Page Title</th>
						<th>Banner Image</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{{$studiosPages->created_at}}</td>
						<td>{{$studiosPages->title}}</td>
						<td>
						    <img src="{{$studiosPages->bannerImgFolder}}{{$studiosPages->bannerImgFile}}" height="140" width="auto" alt="{{$studiosPages->bannerImgName}}" />
						</td>
					</tr>
				</tbody>
			</table>

		</div> <!-- /widget-content -->


		@foreach($moduleMatrix as $key => $matrix)

            <br><br>

            <div class="widget-header">
                <i class="icon-eye-open"></i>
                <h3>
                    {{$matrix['moduleLocationName']}} Modules
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			        @if($key == 0) <a class="btn btn-xs btn-info" href="/preview/wsjcustomstudios" target="_blank">Preview Page</a> @endif
                    @if ($allowStudiosMgt == 1)
                        <a class="btn btn-xs btn-success" data-toggle="modal" href="#moduleModalLocation{{$matrix['locationID']}}">Add Modules</a>
                        <a id="saveLiveOrderLocation{{$matrix['locationID']}}Button" class="btn btn-xs btn-primary "
                           href="#">Publish to {{$matrix['moduleLocationName']}}</a>
                    @endif
                </h3>
            </div> <!-- /widget-header -->

            <div class="widget-content">


               {{ Form::hidden('location'.$matrix['locationID'].'ModuleOrder', '', array('id' => 'location'.$matrix['locationID'].'ModuleOrder')) }}
               {{ Form::hidden('location'.$matrix['locationID'].'ModulePage', 'studios-pages', array('id' => 'location'.$matrix['locationID'].'ModulePage')) }}

               {{ Form::hidden('location'.$matrix['locationID'].'ModuleLocationID'  , $matrix['locationID'] , array('id' => 'location'.$matrix['locationID'].'ModuleLocationID')) }}
               {{ Form::hidden('location'.$matrix['locationID'].'ModuleEntityID'    , $matrix['entityID']   , array('id' => 'location'.$matrix['locationID'].'ModuleEntityID')) }}
               {{ Form::hidden('location'.$matrix['locationID'].'ModuleEntityType'  , $matrix['entityType'] , array('id' => 'location'.$matrix['locationID'].'ModuleEntityType')) }}

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Live?</th>
                            <th>Module Title</th>

                            @if ($allowStudiosMgt == 1)
                                <th class="td-actions">Edit</th>
                                <th class="td-actions">Delete</th>
                            @endif

                        </tr>
                    </thead>
                    <tbody id="sortLocation{{$matrix['locationID']}}ModulesOrder">
                    @foreach ($matrix['allModules'] as $key=>$module)
                    <?php $moduleObject = $module['moduleObject']; ?>
                        <tr style="cursor: pointer;"
                        id="location-{{$moduleObject->relationshipLocationID}}-module-relationship-{{$moduleObject->relationshipID}}"
                        data-location-{{$moduleObject->relationshipLocationID}}-module-relationship-id="location{{$matrix['locationID']}}module-{{$moduleObject->relationshipID}}"
                        >
                            <td class="text-center col-md-1">{{ Form::checkbox('location'.$matrix['locationID'].'ModuleCheckbox[]', $moduleObject->relationshipID, ($moduleObject->is_live ?: null), ['class' => 'location'.$matrix['locationID'].'ModuleCheckbox']) }}</td>
                            <td>
                                <div class="col-lg-12" style="">{{ $moduleObject->title != "" ? $moduleObject->title : "Empty Title"}}<span class="module-mgt-row-icon pull-right" data-module-details-type="{{$module['partial_name']}}" data-module-details-id="{{$moduleObject->id}}"><i id="module-mgt-row-icon-{{$module['partial_name']}}-{{$moduleObject->id}}" class="btn-icon-only icon-chevron-down"></i></span></div>
                                <div id="module-details-{{$module['partial_name']}}-{{$moduleObject->id}}" style="display: none;">
                                    <br>
                                    <div style="" class="view-preview">
                                        @include("/home/modules/" . $module['partial_name'], ['sectionKey' => $matrix['locationID'], 'key' => $key, 'moduleObject' => $module['moduleObject']])
                                    </div>
                                </div>
                            </td>

                            @if ($allowStudiosMgt == 1)
                                <td class="td-actions">
                                    <a href="/admin/page-content/modules/{{$moduleObject->relationshipID}}/edit" class="btn btn-xs btn-primary">
                                        <i class="btn-icon-only icon-edit"></i>
                                    </a>
                                </td>
                                <td class="td-actions">
                                    {{ Form::open(array('url' => 'admin/page-content/modules/' . $moduleObject->relationshipID, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'delete-module')) }}
                                            <button type="submit" class="btn btn-xs btn-danger"><i class="btn-icon-only icon-remove"></i></button>
                                    {{ Form::close() }}
                                </td>
                            @endif
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div> <!-- /widget-content -->

		@endforeach

	</div>
</div>



@foreach($moduleMatrix as $matrix)

    <div id="moduleModalLocation{{$matrix['locationID']}}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            {{ Form::open(array('url' => 'admin/page-content/modules/create', 'method' => 'post', 'id' => 'create-module', 'class' => 'form-horizontal col-md-12')) }}

                {{ Form::hidden('locationID', $matrix['locationID']) }}
                {{ Form::hidden('entityType', $matrix['entityType']) }}
                {{ Form::hidden('entityID'  , $matrix['entityID']) }}

                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Choose Module Type</h4>
                    </div>
                    <div class="modal-body clearfix">

                        <p>Select the type of module you want to add to the <strong>{{$matrix['moduleLocationName']}} section</strong> from the drop down below and click create module to add your custom content.</p>

                        <hr>

                        {{Form::select('moduleType', $matrix['moduleTypes'], 0, ['class' => 'select-module-type createModuleSelectField', 'data-button-id' => 'createModuleSubmitButton'])}}

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="createModuleSubmitButton" type="submit" class="btn btn-primary disabled">Create
                            Module
                        </button>
                    </div>

                </div>
                <!-- /.modal-content -->

            {{ Form::close() }}

        </div>
        <!-- /.modal-dialog -->
    </div>

@endforeach


@stop

@section('css')
    @parent

    <style>
        .module.slideshow .slides-outer { max-height: none; }
    </style>
@stop
