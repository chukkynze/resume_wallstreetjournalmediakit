@extends('......layouts.admin')
@section('content')

<style>
.hyfnSlimAlert
{
    padding:5px 25px 5px 15px;
    margin-bottom:5px;
}
</style>



<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-circle-arrow-down"></i>
				<h3>Create New Home Page Content</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/page-content/home-page-sliders', 'method' => 'post', 'id' => 'create-home-page', 'class' => 'form-horizontal col-md-12')) }}
                    {{ Form::hidden('number_of_sliders', $sliderNumbers, array('id' => 'number_of_sliders')) }}


                    <fieldset>

                        <h4>Home Page Sliders ({{$sliderNumbers}}) for Home Page Version [ID: {{$homePage->id}}] created at {{$homePage->created_at}}</h4>
                        <br />

                        @foreach($allSliders as $key => $slider)
                        {{ Form::hidden('home_page_id_'.$key, $homePage->id, array('id' => 'homePageID')) }}
                        <hr>

                            <h5>Slide {{$key}}</h5>

                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="{{$slider['main_text']['labelID']}}" class="col-md-3">{{$slider['main_text']['labelDisplay']}}</label>
                                    <div class="col-md-8">
                                        {{ Form::text($slider['main_text']['inputName'], $homePageSlider->main_text, array('class' => 'form-control tm-input', 'id' => $slider['main_text']['inputID'], 'placeholder' => $slider['main_text']['inputPlaceholder'])) }}
                                        <span class="text-danger">{{ $errors->first($slider['main_text']['errorID']) }}</span>
                                    </div> <!-- /controls -->
                                </div> <!-- /control-group -->

                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="{{$slider['sub_text1']['labelID']}}" class="col-md-3">{{$slider['sub_text1']['labelDisplay']}}</label>
                                    <div class="col-md-8">
                                        {{ Form::text($slider['sub_text1']['inputName'], $homePageSlider->sub_text1, array('class' => 'form-control tm-input', 'id' => $slider['sub_text1']['inputID'], 'placeholder' => $slider['sub_text1']['inputPlaceholder'])) }}
                                        <span class="text-danger">{{ $errors->first($slider['sub_text1']['errorID']) }}</span>
                                    </div> <!-- /controls -->
                                </div> <!-- /control-group -->

                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="{{$slider['sub_text2']['labelID']}}" class="col-md-3">{{$slider['sub_text2']['labelDisplay']}}</label>
                                    <div class="col-md-8">
                                        {{ Form::text($slider['sub_text2']['inputName'], $homePageSlider->sub_text2, array('class' => 'form-control tm-input', 'id' => $slider['sub_text2']['inputID'], 'placeholder' => $slider['sub_text2']['inputPlaceholder'])) }}
                                        <span class="text-danger">{{ $errors->first($slider['sub_text2']['errorID']) }}</span>
                                    </div> <!-- /controls -->
                                </div> <!-- /control-group -->

                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="{{$slider['source_text']['labelID']}}" class="col-md-3">{{$slider['source_text']['labelDisplay']}}</label>
                                    <div class="col-md-8">
                                        {{ Form::text($slider['source_text']['inputName'], $homePageSlider->source_text, array('class' => 'form-control tm-input', 'id' => $slider['source_text']['inputID'], 'placeholder' => $slider['source_text']['inputPlaceholder'])) }}
                                        <span class="text-danger">{{ $errors->first($slider['source_text']['errorID']) }}</span>
                                    </div> <!-- /controls -->
                                </div> <!-- /control-group -->

                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label class="col-md-3" for="banner_img_id">Banner Image Upload</label>
                                    <div class="col-md-8">
                                        <div>
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th>Category</th>
                                                        <th>Content File</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($slider['bannerImages'] as $bannerImage)
                                                    <tr>
                                                        <td>{{ Form::radio('upload_id_'.$key,$bannerImage->id, null, []) }}</td>
                                                        <td>{{$bannerImage->name}}</td>
                                                        <td>{{$bannerImage->categoryName}}</td>
                                                        <td><a href="{{$bannerImage->download_folder}}{{$bannerImage->download_file}}" target="_blank">{{$bannerImage->download_file}}</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <span class="text-danger">{{ $errors->first('upload_id_'.$key) }}</span>
                                    </div> <!-- /controls -->
                                </div> <!-- /control-group -->

                        @endforeach

                        <hr />

                        <div class="form-group">

                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
                            </div>
                        </div> <!-- /form-actions -->
                    </fieldset>


				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop