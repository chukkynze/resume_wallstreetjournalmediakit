@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-circle-arrow-down"></i>
			<h3>
			    Home Page Content
			    <a class="btn btn-xs btn-success" href="/admin/page-content/home-pages/create">Edit Home Page Content</a>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Home&nbsp;Page&nbsp;Version&nbsp;as&nbsp;at...</th>
						<th>Page Contents</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($allHomePages as $homePage)
					<tr>
						<td>{{$homePage->created_at}}</td>
						<td>
						    {{count($homePage->sliders) > 0 ? count($homePage->sliders) : 0}} Sliders<br>
						    @foreach($homePage->sliders as $slider)
						    <div class="view-preview">
						        <div style="margin: 0 -50px 0 0;">
						            <img src="{{$slider['download_folder']}}{{$slider['download_file']}}" height="140" width="auto" />
						        </div>
						        <div style="">
						            <dl class="dl-horizontal">
                                        <dt>Main Text</dt>      <dd>{{$slider['main_text']}}</dd>
                                        <dt>Sub Text</dt>     <dd>{{$slider['sub_text']}}</dd>
                                        <dt>Source Text</dt>    <dd>{{$slider['source_text']}}</dd>
                                    </dl>
						        </div>
						    </div>
						    @endforeach
						</td>
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>
@stop