@extends('layouts.templates.preview-layout')
@section('content')

<div class="landing-layout">

    <div class="landing-marquee-outer homepage js-viewport-scale">
        <div class="landing-marquee">
            <ul class="landing-slideshow" id="js-landing-marquee">

                @foreach($sliders as $slider)
                <li style="background-image: url('{{$slider['download_folder']}}{{$slider['download_file']}}')" class="slide">

                    <div class="landing-marquee-titles">
                        
                        <div class="landing-marquee-title">
                          <div class="main-text">
                            {{$slider['main_text']}}
                          </div>
                        </div>

                        <div class="sub-text">
                          {{$slider['sub_text']}}
                        </div>

                    </div>

                    <div class="footer-source-info">
                      <div class="footer-source-info-inner">
                        {{$slider['source_text']}}
                      </div>
                    </div>

                </li>
                @endforeach

            </ul>
        </div>
  </div>

  <div class="landing-link-list">
    <ul>
      <li class="landing-link"><a href="#" class="landing-link" style="pointer-events: none; cursor: default;">Products</a></li>
      <li class="landing-link"><a href="#" class="landing-link" style="pointer-events: none; cursor: default;">WSJ. Insights</a></li>
      <li class="landing-link"><a href="#" class="landing-link" style="pointer-events: none; cursor: default;">WSJ. Custom Studios</a></li>
      <li class="landing-link"><a href="#" class="landing-link" style="pointer-events: none; cursor: default;">Rates &amp; Specs</a></li>
      <li class="landing-link mobile-hidden"><a href="#" class="landing-link" style="pointer-events: none; cursor: default;">Case Studies</a></li>
      <li class="landing-link"><a href="#" class="landing-link secondary" style="pointer-events: none; cursor: default;">Classifieds</a></li>
      <li class="landing-link"><a href="#" class="landing-link secondary" style="pointer-events: none; cursor: default;">Submit Ad</a></li>
      <li class="landing-link"><a href="#" class="landing-link secondary" style="pointer-events: none; cursor: default;">Contact</a></li>
    </ul>
  </div>

</div>



@stop
