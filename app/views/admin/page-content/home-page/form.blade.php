@section('form')

	<fieldset>

        <h4>Home Page Details</h4>
        <h6>Check the sliders you want to add.</h6>
        <h6>Uncheck the sliders you want to remove.</h6>
        <h6>Once checked a corresponding green slider box will appear.</h6>
        <h6>Drag green slider boxes to rearrange the slider order as you wish.</h6>
        <br>

        {{ Form::hidden('sliderIDs', $allSliderIDs, ['id'=>'sliderIDs']) }}


        <div class="form-group">
            <div class="col-md-3">


                {{ Form::hidden('slider_order', $initSliderOrder, array('id' => 'slider_order')) }}

                <table id="">
                    <tbody id="homePageChosenSliderOrder">
                        @foreach ($chosenSlidersArray as $sliderUnit)

                                <tr id="slider-{{$sliderUnit['id']}}" data-upload-name="{{$sliderUnit['name']}}">
                                    <td>
                                        <div class="alert alert-success" style="padding:5px 25px 5px 15px;margin-bottom:5px;">
                                            <strong>{{$sliderUnit['name']}}</strong>
                                        </div>
                                    </td>
                                </tr>

                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class="col-md-9">
                <label for="video_module_url" class="col-md-3"><span><a href="/admin/uploads/create" target="_blank" style="font-weight: bolder;">+</a>&nbsp;</span>Available Sliders</label>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Content File</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($sliders as $slider)
                        <tr>
                            <td class=""><i class="sliderRowHomePageChevron icon-chevron-down" data-slider-id="{{$slider->id}}"></i></td>
                            <td class="">{{ Form::checkbox('home_page_slider_upload_id[]', $slider->id, ( in_array($slider->id, $homePage->chosenSliders) ? true : null ), ['class' => 'sliderDetailsCheckbox', 'id' => 'sliderDetailsCheckbox'.$slider->id, 'data-slider-details-id' => 'sliderDetails'.$slider->id, 'data-slider-id' => ''. $slider->id .'', 'data-slider-name' => ''. $slider->name .'']) }}</td>
                            <td class="">{{$slider->name}}</td>
                            <td class="">{{$slider->categoryName}}</td>
                            <td class="">
                                <a href="{{$slider->download_folder}}{{$slider->download_file}}" target="_blank">{{$slider->download_file}}</a>
                                {{ Form::hidden('download_folder_'.$slider->id.''   , $slider->download_folder  , ['id'=>'download_folder_'.$slider->id.'']) }}
                                {{ Form::hidden('download_file_'.$slider->id.''     , $slider->download_file    , ['id'=>'download_file_'.$slider->id.'']) }}
                            </td>
                        </tr>
                        <tr id="sliderDetails{{$slider->id}}" style="display: none;">
                            <td colspan="5">

                                <label for="main_text_{{$slider->id}}" class="col-md-3">Main Text</label><br>
                                {{ Form::text('main_text_'.$slider->id.'', $slider->main_text, array('class' => 'form-control', 'id' => 'main_text_'.$slider->id.'')) }}
                                <span class="text-danger">{{ $errors->first('main_text_'.$slider->id.'') }}</span><br><br>

                                <label for="sub_text1_{{$slider->id}}" class="col-md-3">Sub Text</label><br>
                                {{ Form::textarea('sub_text_'.$slider->id.'', $slider->sub_text, array('class' => 'form-control editme', 'id' => 'sub_text_'.$slider->id.'')) }}
                                <span class="text-danger">{{ $errors->first('sub_text_'.$slider->id.'') }}</span><br><br>

                                <label for="source_text_{{$slider->id}}" class="col-md-3">Source Text</label><br>
                                {{ Form::text('source_text_'.$slider->id.'', $slider->source_text, array('class' => 'form-control', 'id' => 'source_text_'.$slider->id.'')) }}
                                <span class="text-danger">{{ $errors->first('source_text_'.$slider->id.'') }}</span><br><br>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <span class="text-danger">{{ $errors->first('home_page_slider_upload_id') }}</span>
            </div> <!-- /controls -->
        </div> <!-- /control-group -->

        <hr />

        <div class="form-group">

            <div class="col-md-offset-5 col-md-12">
                <button type="submit" class="btn btn-primary">Publish</button>
                <button id="homePagePreviewButton" type="button" class="btn btn-info">Preview</button>
                <a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
            </div>
        </div> <!-- /form-actions -->
    </fieldset>
@show