@extends('......layouts.admin')
@section('content')

<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-circle-arrow-down"></i>
				<h3>Edit Home Page Content</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
                {{ Form::open(array('url' => 'admin/page-content/home-pages/' . $caseStudy->id, 'files'=>true, 'method' => 'put', 'id' => 'edit-gallery', 'class' => 'form-horizontal col-md-12')) }}

						@include('admin.page-content.home-page.form')

				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop