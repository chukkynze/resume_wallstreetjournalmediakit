@extends('......layouts.admin')
@section('content')

<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-circle-arrow-down"></i>
				<h3>Edit Your Module</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/page-content/modules/' . $mlrID, 'method' => 'put', 'id' => 'edit-modules', 'class' => 'form-horizontal col-md-12')) }}

                <fieldset>

                    <h4>Editing A {{$moduleTypeDisplayName}} from the {{$entityTypeName}} (ID: {{ $formModule->id }})</h4>

				    @include('admin.page-content.modules.form')

				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop
