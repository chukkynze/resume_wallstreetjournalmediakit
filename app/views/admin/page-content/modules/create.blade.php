@extends('......layouts.admin')
@section('content')

<div class="row">

	<div class="col-md-12">

		<div class="widget stacked">

			<div class="widget-header">
				<i class="icon-circle-arrow-down"></i>
				<h3>Create A Module</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/page-content/modules', 'method' => 'post', 'id' => 'create-module', 'class' => 'form-horizontal col-md-12')) }}

                <fieldset>

                    <h4>Adding A {{$moduleTypeDisplayName}} to the {{$entityTypeName}}</h4>

                    @include('admin.page-content.modules.form')

				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop