@section('form')


        <hr />
        <br>
        {{ Form::hidden('locationID', $locationID) }}
        {{ Form::hidden('moduleType', $moduleType) }}
        {{ Form::hidden('entityType', $entityType) }}
        {{ Form::hidden('entityID'  , $entityID) }}
        {{ Form::hidden('is_live'  , 0) }}
        {{ Form::hidden('has_border'  , 0) }}

		<div class="form-group">
		    <div class="col-md-1"></div>
            <label for="title" class="col-md-2">CMS Module Title</label>
			<div class="col-md-8">
				{{ Form::text('title', $formModule->title, array('class' => 'form-control', 'id' => 'title')) }}
				<span class="text-danger">{{ $errors->first('title') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->

        @include('admin.modules.'.$modulePartial)

        <div class="form-group">
            <div class="col-md-1"></div>
            <label for="is_live" class="col-md-2"></label>

            <div class="col-md-8">
                {{ Form::hidden('is_live', 0) }}
                <label for="is_live">{{ Form::checkbox('is_live', 1, ($formModule->is_live == 1 ? true : null), ['id' => 'is_live']) }}
                    Publish Module to the {{$entityTypeName}}</label>
                <span class="text-danger">{{ $errors->first('is_live') }}</span>
            </div>
            <!-- /controls -->
        </div> <!-- /control-group -->

        <hr />

        <div class="form-group">

            <div class="col-md-offset-4 col-md-8">
                <button type="submit" class="btn btn-primary"
                        id="moduleSaveButton">{{($formModule->is_live == 1 ? "Publish" : "Save")}}</button>
                <a href="/admin/{{$entityRedirect}}" class="btn btn-default">Cancel</a>
            </div>
        </div> <!-- /form-actions -->
    </fieldset>
@show

@section('js')
<script>
var image_list = {{ json_encode($images) }};
var category_list = {{ json_encode($categories) }};
</script>
    @parent
@show
