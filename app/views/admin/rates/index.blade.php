@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="widget stacked widget-table action-table">

		<div class="widget-header">
			<i class="icon-credit-card"></i>
			<h3>
                Rate Specifications
                <a class="btn btn-xs btn-success" href="/admin/rates_specs/create">Edit Rates &amp; Specs Page Content</a>
			</h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Updated At</th>
						<th>Rate &amp; Specification</th>

					</tr>
				</thead>
				<tbody>
				@foreach ($allRates as $rate)
					<tr>
						<td>{{$rate->created_at}}</td>
						<td>
						    <ul>
								@foreach($rate->rateProductName as $rateProduct)
						            <li>
										{{$rateProduct['name']}}
						                <ul>
											@foreach($rateProduct['data']  as $linkData)

												@if($linkData['name'] != "")
													<li><a href="{{$linkData['file']}}">{{$linkData['name']}}</a>
													</li> @endif

						                    @endforeach
						                </ul>
						            </li>
						        @endforeach
						    </ul>
						</td>
					</tr>
				@endforeach

				</tbody>
			</table>

		</div> <!-- /widget-content -->
	</div>
</div>
@stop