@extends('......layouts.admin')
@section('content')

<style>
.hyfnSlimAlert
{
    padding:5px 25px 5px 15px;
    margin-bottom:5px;
}
</style>



<div class="row">

	<div class="col-md-11">

		<div class="widget stacked">

			<div class="widget-header">
                <i class="icon-dollar"></i>
				<h3>Create New Rates &amp; Specifications Page</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<!-- use form.blade.php -->
				{{ Form::open(array('url' => 'admin/rates_specs', 'method' => 'post', 'id' => 'create-rates_specs', 'class' => 'form-horizontal col-md-12')) }}

                {{ Form::hidden('site_product_order', '', array('id' => 'site_product_order')) }}

                    <fieldset>

                        <h4>Rates &amp; Specifications Page Details</h4>
                        <h6>Drag each product section to rearrange their order</h6>
                        <h6>Click the bar in each section to display and choose the content you want to appear in each section.</h6>
                        <h6>The order in which you check/uncheck each content row is the order it will be displayed.</h6>
                        <br />


                        <table class="col-md-12">

                            <thead>
                                <tr>
                                    <td>

                                    </td>
                                </tr>
                            </thead>

                            <tbody id="sortRateProductsOrder">

                            @foreach($rateModelArray as $rateProductID => $rateProductRow)
                                <tr id="siteproduct_{{$rateProductID}}"
                                    data-siteProduct-name="{{$rateProductRow['name']}}">
                                    <td>
                                        <div class="form-group">

                                            <hr>

                                            <div class="col-md-3">
                                                <label for="product_row_id">{{$rateProductRow['name']}}</label>
                                                {{ Form::hidden('siteproduct_rate'.$rateProductID.'_order', $rateProductRow['init_order'], array('id' => 'siteproduct_rate'.$rateProductID.'_order')) }}

                                                <table id="sortChosenRateProductID{{$rateProductID}}OrderTable">
                                                    <thead id="sortChosenRateProductID{{$rateProductID}}OrderHdg"
                                                           style="display: none;">
                                                        <tr>
                                                            <td>Check the uploads in the order you want them to appear</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="sortChosenRateProductID{{$rateProductID}}Order"
                                                           style="display: none;">

                                                        @foreach($rateProductRow['chosenUploads'] as $chosenUploads)
                                                            <tr id="rate{{$rateProductID}}_{{$chosenUploads['uploadID']}}"
                                                                data-upload-name="{{$chosenUploads['uploadName']}}">
                                                                <td>
                                                                    <div class="alert alert-success"
                                                                         style="padding:5px 25px 5px 15px;margin-bottom:5px;">
                                                                        <strong>{{$chosenUploads['uploadName']}}</strong>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    </tbody>
                                                </table>

                                            </div>

                                            <div class="col-md-8">

                                                <div>

                                                    <table class="table table-striped table-bordered">
                                                        <thead
                                                            class="rateSiteProductHeaderOptions"
                                                            style="cursor: pointer;"
                                                            data-chevron-id="siteProductChevron{{$rateProductID}}"
                                                            data-chosen-list-id="sortChosenRateProductID{{$rateProductID}}OrderHdg"
                                                            data-tbody-id="siteProductTbody{{$rateProductID}}"
                                                            >
                                                            <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click the header to show/collapse table rows.">
                                                                <th><i id="siteProductChevron{{$rateProductID}}"
                                                                       class="btn-icon-only icon-chevron-down"></i></th>
                                                                <th>Name</th>
                                                                <th>Category</th>
                                                                <th>Content File</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="siteProductTbody{{$rateProductID}}">

                                                            @foreach ($rateProductRow['uploadFiles'] as $uploadFile)
                                                                <tr>
                                                                    <td>
                                                                        {{ Form::checkbox('rates_img_ids', $uploadFile->id, ( in_array($uploadFile->id, $rateProductRow['uploads']) ? true : false), ['class' => 'rateSpecImageCheckBox', 'data-rate-product-id' => ''. $rateProductID .'', 'data-rate-file-name' => ''. $uploadFile->name .'']) }}
                                                                        <span id="orderBox{{$uploadFile->id}}"></span>
                                                                    </td>
                                                                    <td>{{$uploadFile->name}}</td>
                                                                    <td>{{$uploadFile->categoryName}}</td>
                                                                    <td><a href="{{$uploadFile->download_folder}}{{$uploadFile->download_file}}" target="_blank">{{$uploadFile->download_file}}</a></td>
                                                                </tr>
                                                            @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>

                                                <span class="text-danger">{{ $errors->first('rate_reports') }}</span>
                                            </div> <!-- /controls -->
                                        </div> <!-- /control-group -->
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>


                        </table>




                        <hr />

                        <div class="form-group">

                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" class="btn btn-primary">Publish</button>
                                <a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
                            </div>
                        </div> <!-- /form-actions -->
                    </fieldset>


				{{ Form::close() }}
			</div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div> <!-- /span8 -->

</div> <!-- /row -->
@stop