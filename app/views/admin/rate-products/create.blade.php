@extends('layouts.admin')
@section('content')
    <div class="row">

        <div class="col-md-12">

            <div class="widget stacked">

                <div class="widget-header">
                    <i class="icon-dollar"></i>

                    <h3>Create New Rates & Specs Product</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">
                    <!-- use form.blade.php -->
                    {{ Form::open(array('url' => 'admin/rate-products', 'method' => 'post', 'id' => 'create-rate-product', 'class' => 'form-horizontal col-md-12')) }}

                    @include('admin/rate-products/form')

                    {{ Form::close() }}
                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->

        </div>
        <!-- /span8 -->

    </div> <!-- /row -->
@stop