@extends('layouts.admin')
@section('content')

    {{Form::hidden('index_row_order', $initIndexOrder, ['id'=>'index_row_order'])}}

    <div class="row">
        <div class="widget stacked widget-table action-table">

            <div style="color: red;">Click and drag the name below to reorder. Re-ordering here affects the site in
                real-time.
            </div>

            <div class="widget-header">
                <i class="icon-dollar"></i>

                <h3>
                    Rates &amp; Specs
                    <a class="btn btn-xs btn-success" href="/admin/rate-products/create">Add New Rates & Specs
                        Product</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
                </h3>
            </div>
            <!-- /widget-header -->

            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>

                        @if ($allowRateProductMgt == 1)
                            <th class="td-actions">Edit</th>
                            <th class="td-actions">Delete</th>
                        @endif

                    </tr>
                    </thead>
                    <tbody id="sortRateProductsOrder">
                    <?php $x = 0; ?>
                    @foreach ($allProducts as $product)
                        <tr style="cursor: pointer;" id="index-row-{{$product->id}}">
                            <td>{{$product->name}}</td>
                            @if ($allowRateProductMgt == 1)
                                <td class="td-actions">
                                    <a href="/admin/rate-products/{{$product->id}}/edit" class="btn btn-xs btn-primary">
                                        <i class="btn-icon-only icon-edit"></i>
                                    </a>
                                </td>
                                <td class="td-actions">
                                    {{ Form::open(array('url' => 'admin/rate-products/' . $product->id, 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'site_product_id')) }}
                                    <button type="submit" class="btn btn-xs btn-danger"><i
                                                class="btn-icon-only icon-remove"></i></button>
                                    {{ Form::close() }}
                                </td>
                            @endif
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>
            <!-- /widget-content -->
        </div>
    </div>
@stop