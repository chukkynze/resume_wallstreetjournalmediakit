@section('form')

    <fieldset>

        <div class="form-group">
            <label for="name" class="col-md-10">Rates & Specs Product Name</label>

            <div class="col-md-10">
                {{ Form::text('name', $rateProduct->name, array('class' => 'form-control', 'id' => 'name')) }}
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
            <!-- /controls -->
        </div>
        <!-- /control-group -->

        <div class="form-group">
            <label for="submit_ad_link" class="col-md-10">Submit Ad Link (<span
                        style="color: grey;">Optional</span>)</label>

            <div class="col-md-10">
                {{ Form::text('submit_ad_link', $rateProduct->submit_ad_link, array('class' => 'form-control', 'id' => 'submit_ad_link')) }}
                <span class="text-danger">{{ $errors->first('submit_ad_link') }}</span>
            </div>
        </div>



        <hr/>
        <br/>


        <div class="form-group">


            <div class="col-md-3">

                {{ Form::hidden('rate-media-order', $init_order, ['id' => 'rate-media-order'] ) }}

                <table id="sortChosenRateProductMediaOrderTable">

                    <thead id="sortChosenRateProductMediaOrderHdg">
                    <tr>
                        <td>Check the uploads in order to see them in the sidebar. Drag and drop to reorder for how you
                            would like these to display on the website.
                        </td>
                    </tr>
                    </thead>
                    <tbody id="sortChosenRateProductMediaOrder">

                    @foreach($chosenUploads as $chosenUploads)
                        <tr id="rate_{{$chosenUploads['uploadID']}}"
                            data-upload-name="{{$chosenUploads['uploadName']}}">
                            <td>
                                <div class="alert alert-success"
                                     style="padding:5px 25px 5px 15px;margin-bottom:5px;">
                                    <strong>{{$chosenUploads['uploadName']}}</strong>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>
            <div class="col-md-8">

                <table id="rateProductMediaTable" class="table table-striped table-bordered">
                    <thead
                            class="rateRateProductHeaderOptions"
                            style="cursor: pointer;"
                            data-chevron-id="rateProductChevron"
                            data-chosen-list-id="sortChosenRateProductMediaOrderHdg"
                            data-tbody-id="rateProductTbody"
                            >
                    <tr class="ui-tooltip" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Click the header to show/collapse table rows.">
                        <th><i id="rateProductChevron"
                               class="btn-icon-only icon-chevron-down"></i></th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Content File</th>
                    </tr>
                    </thead>
                    <tbody id="rateProductTbody">

                    @foreach ($uploadFiles as $uploadFile)
                        <tr>
                            <td>
                                {{
                                    Form::checkbox
                                    (
                                        'rates_img_ids',
                                        $uploadFile->id,
                                        ( in_array($uploadFile->id, $chosenUploadsIDArray) ? true : false),
                                        [
                                            'class'                 =>  'rateSpecImageCheckBox',
                                            'data-rate-product-id'  =>  $rateProduct->id,
                                            'data-rate-file-name'   =>  ''. $uploadFile->name .''
                                        ]
                                    )
                                }}
                                <span id="orderBox{{$uploadFile->id}}"></span>
                            </td>
                            <td>{{$uploadFile->name}}</td>
                            <td>{{$uploadFile->categoryName}}</td>
                            <td><a href="{{$uploadFile->download_folder}}{{$uploadFile->download_file}}"
                                   target="_blank">{{$uploadFile->download_file}}</a></td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>


        </div>


        <hr/>
        <br/>

        <div class="form-group">

            <div class="col-md-offset-4 col-md-8">
                <button type="submit" class="btn btn-primary">Publish</button>
                <a href="/admin/rate-products" class="btn btn-default">Cancel</a>
            </div>
        </div>
        <!-- /form-actions -->
    </fieldset>
@show