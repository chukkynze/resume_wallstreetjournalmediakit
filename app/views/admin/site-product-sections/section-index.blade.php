@extends('layouts.admin')
@section('content')

    <div class="row">
        <div class="widget stacked widget-table action-table">

            <div style="color: red;">Click and drag the name below to reorder. Re-ordering here affects the site in
                real-time.
            </div>

            @foreach($allSiteProducts as $product)

                {{Form::hidden('index_row_order', $product['initOrder'], ['id'=>'index_row_order'])}}

                <div class="widget-header">
                    <i class="icon-sitemap"></i>

                    <h3>
                        {{$product['productName']}} Subsections
                        <a class="btn btn-xs btn-success"
                           href="/admin/site-product-sections/product/{{$product['productID']}}/create">Add New Product
                            Subsection</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <span id="indexStatusSpan" class="hidden" style="color: blue;"></span>
                    </h3>
                </div>

                <div class="widget-content">

                    <table id="siteProductTable" data-site-product-id="{{$product['productID']}}"
                           class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Section</th>

                            @if ($allowSiteProductMgt == 1)
                                <th class="td-actions">Edit</th>
                                <th class="td-actions">Delete</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody id="sortSiteProductSectionsOrder" data-site-product-id="{{$product['productID']}}">
                        @foreach ($product['sections'] as $section)
                            <tr style="cursor: pointer;" id="index-row-{{$section['sectionID']}}">
                                <td>{{$section['sectionName']}}</td>
                                @if ($allowSiteProductMgt == 1)
                                    <td class="td-actions">
                                        <a href="/admin/site-product-sections/{{$section['sectionID']}}/edit"
                                           class="btn btn-xs btn-primary">
                                            <i class="btn-icon-only icon-edit"></i>
                                        </a>
                                    </td>
                                    <td class="td-actions">
                                        {{ Form::open(array('url' => 'admin/site-product-sections/' . $section['sectionID'], 'method' => 'delete','class' => 'form-confirm-delete', 'id' => 'site_product_section_id')) }}
                                        <button type="submit" class="btn btn-xs btn-danger"><i
                                                    class="btn-icon-only icon-remove"></i></button>
                                        {{ Form::close() }}
                                    </td>
                                @endif
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            @endforeach

        </div>
    </div>
@stop