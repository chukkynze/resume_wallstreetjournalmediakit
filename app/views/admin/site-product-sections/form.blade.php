@section('form')

	<fieldset>
        {{ Form::hidden('module_order' , null, array('class' => 'form-control', 'id' => 'module_order')) }}




		<div class="form-group">
			<label class="col-md-10" for="site_product_id">What product is this subsection associated with?</label>
			<div class="col-md-10">
				{{ Form::select
				(
				    'site_product_id',
				    $siteProductArray,
				    ($siteProductSection->site_product_id ?: ($productID ?: 0)) ,
				    [
				        'class' => 'form-control',
				        'id'    => 'site_product_id'
				    ]
				) }}
				<span class="text-danger">{{ $errors->first('site_product_id') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->



		<div class="form-group">
			<label for="name" class="col-md-10">Product Subsection Name</label>
			<div class="col-md-10">
				{{ Form::text('name', $siteProductSection->name, array('class' => 'form-control', 'id' => 'name')) }}
				<span class="text-danger">{{ $errors->first('name') }}</span>
			</div> <!-- /controls -->
		</div> <!-- /control-group -->


		<!--
		<div class="form-group">
			<label for="heading" class="col-md-10">Section Link Heading</label>
			<div class="col-md-10">
				{{ Form::text('heading', $siteProductSection->heading, array('class' => 'form-control', 'id' => 'heading')) }}
				<span class="text-danger">{{ $errors->first('heading') }}</span>
			</div>
		</div> -->


		<!--
		<div class="form-group">
			<label for="only_headings_on_landing" class="col-md-10">Do you want to use only headings as links on landing pages?</label>
			<div class="col-md-10">
				{{ Form::checkbox('only_headings_on_landing', 1, ($siteProductSection->only_headings_on_landing == 1 ? true : null), []) }} Enable/Disable Headings as Links
				<span class="text-danger">{{ $errors->first('only_headings_on_landing') }}</span>
			</div>
		</div> -->


		<!--
		<div class="form-group">
			<label for="enable_link" class="col-md-10">Do you want active links to this section</label>
			<div class="col-md-10">
				{{ Form::checkbox('enable_link', 1, ($siteProductSection->enable_link == 1 ? true : null), []) }} Enable/Disable Section Links
				<span class="text-danger">{{ $errors->first('enable_link') }}</span>
			</div>
		</div> -->


		<!--
		<div class="form-group">
			<label for="enable_link" class="col-md-10">Do you want the links in all caps or 'as is' text?</label>
			<div class="col-md-10">
				{{ Form::checkbox('enable_all_caps', 1, ($siteProductSection->enable_all_caps == 1 ? true : null), []) }} Enable/Disable All Cap Links
				<span class="text-danger">{{ $errors->first('enable_all_caps') }}</span>
			</div>
		</div> -->

		<!--
		<div class="form-group">
			<label for="link_text" class="col-md-10">Enter the Text to Display in the Actual Link</label>
			<div class="col-md-10">
				{{ Form::text('link_text', $siteProductSection->link_text, array('class' => 'form-control', 'id' => 'link_text', 'size' => 200)) }}
				<span class="text-danger">{{ $errors->first('link_text') }}</span>
			</div>
		</div> -->


		<hr /><br />

		<div class="form-group">

			<div class="col-md-offset-4 col-md-8">
                <button type="submit" class="btn btn-primary">Save</button>
				<a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
			</div>
		</div> <!-- /form-actions -->
	</fieldset>
@show