<!-- adding the specialClass will cause the site logo to use darker text (used on pages without hero images) -->
@extends('layouts.templates.layout', array('specialClass' => "dark-logo"))
@section('content')

<div class="standard-layout">

  <section class="content-main">

    <div class="global-crumbs">
      <ul>
        <li>
          <a href="/">Home</a>
        </li>
        <li>
          <a href="rates" class="is-active">Rates &amp; Specs</a>
        </li>
      </ul>
    </div>


    <h1>Rates &amp; Specs</h1>

    <div class="option-chooser">

      <div class="option-container">

        <div class="select-container">
          <select id="js-rates-product-select" class="rates-product-select">
          <option value="0">Select a Product...</option>
          @foreach($rateProducts as $key=>$rateProduct)
          <option value="{{$key}}">{{$rateProduct}}</option>
          @endforeach
          </select>
        </div>

      </div>


      <div class="option-result-container" id="js-rates-results">

        <div class="option-result-column" id="js-option-result-column">

        </div>
        
      </div>

    </div>

  </section>

</div>




@stop