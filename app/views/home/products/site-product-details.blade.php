@extends('layouts.templates.product-layout', array('specialClass' => "dark-logo"))
@section('content')

    <!-- this needs to be dynamic (only visible on small breakpoint )-->
    <div class="global-crumbs">
        <ul>
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/products">Products</a>
            </li>
            <li>
                <a href="/products/{{$siteProductID}}" class="is-active">{{$ucwordHeading}}</a>
            </li>
        </ul>
    </div>

    <h1>{{$heading_vs_image ? '<img src="' . $headingIcon . '">' : $ucwordHeading}}</h1>

    <div class="sections">


        <div class="section-summary">

            <div class="module-wrapper">

                <div class="col-right @if(count($siteProductSections) == 0) is-empty @endif">

                    <div class="editions">

                        <ul class="editions-list">

                            @foreach($siteProductSections as $key => $subSection)

                                @if($key != "EMPTY")
                                    <li>{{$key}}</li>
                                @endif

                                @foreach($subSection as $sectionData)
                                    <li><a href="#@if($key != "EMPTY"){{Str::slug($key)}}--@endif{{$sectionData['name_slug']}}">{{$sectionData['link_text']}}</a></li>
                                @endforeach

                                @if($key != "EMPTY")
                                    <li></li>
                                @endif

                            @endforeach

                        </ul>

                    </div>

                </div>

                <div id="searchable-content">

                    <div id="search-description">
                        @foreach($modulesArray as $key => $module)
                            @include("/home/modules/" . $module['partial_name'], ['sectionKey' => $siteProductID, 'key' => $key, 'moduleObject' => $module['moduleObject']])
                        @endforeach
                    </div>

                    @foreach($siteProductSections as $mainKey => $subSection)

                        @if($mainKey != "EMPTY")

                            <div>{{$mainKey}}</div>
                            <a name="@if($mainKey != "EMPTY"){{Str::slug($mainKey)}}@endif"></a>

                        @endif

                        @foreach($subSection as $sectionDataKey=>$sectionData)

                            <div id="@if($mainKey != "EMPTY" && !is_numeric($mainKey)){{Str::slug($mainKey)."--"}}@endif{{$sectionData['name_slug']}}">

                                @foreach($sectionData['modulesArray'] as $moduleKey => $module)
                                    @include("/home/modules/" . $module['partial_name'], ['sectionKey' => $siteProductID."-".$sectionDataKey, 'key' => $moduleKey, 'moduleObject' => $module['moduleObject']])
                                @endforeach

                            </div>

                        @endforeach

                    @endforeach

                </div>

            </div>
        </div>
    </div>
@stop
