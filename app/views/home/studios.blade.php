<!-- adding the specialClass will cause the site logo to use darker text (used on pages without hero images) -->
@extends('layouts.templates.landing-page-layout')
@section('content')

    <!-- this needs to be dynamic (only visible on small breakpoint )-->
    <span>

        <div class="global-crumbs">
            <ul>
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/wsjcustomstudios">WSJ. Custom studios</a>
                </li>
            </ul>
        </div>

        <h1>{{$ucwordHeading}}</h1>

        <div class="sections">


            <div class="section-summary">

                <div class="module-wrapper">

                    <div class="col-right">

                        <div class="editions">

                            <ul class="editions-list">

                                <li><a href="#capabilities">CAPABILITIES</a></li>
                                <li><a href="#awards">AWARDS</a></li>

                            </ul>

                        </div>

                    </div>

                    <div  id="searchable-content">

                        <div id="search-description">
                            @foreach($modulesArray as $key => $module)
                                @include("/home/modules/" . $module['partial_name'], ['sectionKey' => 1, 'key' => $key, 'moduleObject' => $module['moduleObject']])
                            @endforeach
                        </div>


                        <div id="capabilities">
                                <!-- insert module loop here: -->
                                @foreach($capabilitiesModulesArray as $key => $module)
                                    @include("/home/modules/" . $module['partial_name'], ['sectionKey' => 2, 'key' => $key, 'moduleObject' => $module['moduleObject']])
                                @endforeach
                                <!-- / insert module loop here: -->

                        </div>


                        <div id="awards">

                                <!-- insert module loop here: -->
                                @foreach($awardModulesArray as $key => $module)
                                    @include("/home/modules/" . $module['partial_name'], ['sectionKey' => 3, 'key' => $key, 'moduleObject' => $module['moduleObject']])
                                @endforeach
                                <!-- / insert module loop here: -->

                        </div>

                    </div>

                </div>
            </div>

        </div>

    </span>

@stop
