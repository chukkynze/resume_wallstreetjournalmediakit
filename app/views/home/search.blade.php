@extends('layouts.templates.layout', array('specialClass' => "dark-logo"))
@section('content')

    <div class="standard-layout">

        <section class="content-main">

            <div class="global-crumbs">
                <ul>
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/search" class="is-active">Search</a>
                    </li>
                </ul>
            </div>

            <h1>Search Results</h1>

            <div class="search-block">

                {{ Form::open(array('url' => 'search', 'method' => 'get', 'id' => 'search-box', '')) }}
                {{ Form::text('search', $originalSearchTerm, array('class' => '', 'id' => 'search', 'placeholder' => 'Search Site')) }}
                    <input type="submit" value="Search" class="input-submit" />
				{{ Form::close() }}

            </div>

            <div class="search-results">

                @if(count($searchResults) >= 1)

                    @foreach($searchResults as $result)

                        <!--
                        <h4>{{"term found in body - " . $result['foundInBody']}}</h4>
                        <h4>{{"term found In Description - " . $result['foundInDescription']}}</h4>
                    -->

                        @if($result['type'] == 'page')
                            <div class="result copy-result">
                                <h2><a href="{{$result['url']}}">{{$result['title']}}</a></h2>
                                @if($result['snippet'] != "")
                                    <p>{{$result['snippet']}}</p>
                                    @endif
                                            <!-- <p>relevance {{$result['score'] . '%'}}</p> -->
                            </div>
                        @elseif($result['type'] == 'pdf')
                            <div class="result copy-result">
                                <h2><a target="_blank" class="pdf-link" href="{{$result['url']}}">{{$result['title']}}</a></h2>
                                @if($result['snippet'] != "")
                                    <p>{{$result['snippet']}}</p>
                                    @endif
                                            <!-- <p>relevance {{$result['score'] . '%'}}</p> -->
                            </div>
                        @elseif($result['type'] == 'xls' || $result['type'] == 'lsx')
                            <div class="result copy-result">
                                <h2><a target="_blank" class="excel-link" href="{{$result['url']}}">{{$result['title']}}</a></h2>
                                @if($result['snippet'] != "")
                                    <p>{{$result['snippet']}}</p>
                                    @endif
                                            <!-- <p>relevance {{$result['score'] . '%'}}</p> -->
                            </div>
                        @elseif($result['type'] == 'video')
                            <div class="result copy-result">
                                <h2><a target="_blank" href="{{$result['url']}}">{{$result['title']}}</a></h2>
                                @if($result['snippet'] != "")
                                    <p>{{$result['snippet']}}</p>
                                    @endif
                                            <!-- <p>relevance {{$result['score'] . '%'}}</p> -->
                            </div>
                        @elseif($result['type'] == 'gallery-listing')
                            <div class="result copy-result">
                                <h2><a target="_blank" href="{{$result['url']}}">{{$result['title']}}</a></h2>
                                @if($result['snippet'] != "")
                                    <p>{{$result['snippet']}}</p>
                                    @endif
                                            <!-- <p>relevance {{$result['score'] . '%'}}</p> -->
                            </div>
                        @endif

                        <!--
                        <div>
                            <ul>
                                <li>title_score                 = {{$result['title_score']}}</li>
                                <li>body_score                  = {{$result['body_score']}}</li>
                                <li>description_score           = {{$result['description_score']}}</li>
                                <li>keywords_score              = {{$result['keywords_score']}}</li>

                                <li>titleWeight                 = {{$result['titleWeight']}}</li>
                                <li>bodyWeight                  = {{$result['bodyWeight']}}</li>
                                <li>descriptionWeight           = {{$result['descriptionWeight']}}</li>
                                <li>keywordsWeight              = {{$result['keywordsWeight']}}</li>

                                <li>title_scoreWeighted         = {{$result['title_scoreWeighted']}}</li>
                                <li>body_scoreWeighted          = {{$result['body_scoreWeighted']}}</li>
                                <li>{description_scoreWeighted  = {{$result['description_scoreWeighted']}}</li>
                                <li>keywords_scoreWeighted      = {{$result['keywords_scoreWeighted']}}</li>
                            </ul>
                        </div>
                        -->

                    @endforeach

                @else

                    <div>
                        Your search - <strong>{{$originalSearchTerm}}</strong> - did not match any pages or documents.
                    </div>

                @endif

            </div>

        </section>

    </div>


@stop