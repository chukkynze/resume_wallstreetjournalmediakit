@extends('layouts.templates.layout', array('specialClass' => "dark-logo"))
@section('content')

    <div class="standard-layout">

        <section class="content-main">

            <div class="column-image-wrapper" id="js-filter-results">

                <div class="study-summary two-col-image ">
                    <a href="javascript:;">
                        <img src="{{Session::pull('preview_listing_pic')}}">

                        <div class="text-overlay">
                            <h2>{{Session::pull('preview_headline')}}</h2>

                            <p>{{$pulledDesc}}</p>
                        </div>
                    </a>
                </div>

            </div>

        </section>

    </div>

@stop