<!-- adding the specialClass will cause the site logo to use darker text (used on pages without hero images) -->
@extends('layouts.templates.layout', array('specialClass' => "dark-logo"))
@section('content')


<div class="standard-layout">

    <section class="content-main">

        <div class="global-crumbs">
            <ul>
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/gallery" class="is-active">Gallery</a>
                </li>
            </ul>
        </div>

        <h1>Gallery</h1>
        @if(Session::get('viewMessage') != "")
            <div id="galleryAlertMessage" style="margin-bottom: 10px;">{{Session::pull('viewMessage')}}</div>
        @endif
        {{$tinyMceOutput}}

        <div class="case-studies-wrapper">

            <form action="/gallery-search" method="GET">

                <div class="filter-columns">
                    <input id="search" name="search" value="{{Input::get('search')}}" type="text"
                           placeholder="Search Gallery"/>
                </div>
            </form>

            <form action="javascript:void(0);" class="studies-filters" id="js-studies-filters">
                <div class="filter-columns">

                    @foreach($filterArray as $key => $filter)
                        <div class="filter-column">

                            <select class="study-product-select" id="js-study-product-select" multiple>
                                <option value="0">Select {{$filter['name']}}</option>
                                @foreach($filter['filterItems'] as $key => $filterItem)
                                <option value="{{$filterItem['htmlID']}}">{{$filterItem['displayName']}}</option>
                                @endforeach
                            </select>

                        </div>
                    @endforeach

                    <div class="submit-column">
                        <button class="button form" id="js-submit-study-filters">Submit</button>
                        <button class="button form" id="js-reset-study-filters">Reset</button>
                    </div>

                </div>
                <div id="selected-filters" class="is-invisible">
                    @foreach($filterArray as $key => $filter)
                    <div class="selected-filter-group">
                        <div class="selected-filter-name">{{$filter['name']}}:</div>

                        @foreach($filter['filterItems'] as $key => $filterItem)
                        <a href="#" class="selected-filter" data-filter-id="{{$filterItem['htmlID']}}">x {{$filterItem['displayName']}}</a>
                        @endforeach
                    </div>
                    @endforeach
                </div>

            </form>


            @if(count($caseStudyListings) >= 1)

                <div class="column-image-wrapper" id="js-filter-results">
                    <?php $x=0; ?>

                    @foreach($caseStudyListings as $key => $listing)

                            <div class="study-summary two-col-image
                        @if($x % 2 == 0) odd @endif
                            @foreach($listing['chosenFilters'] as $key => $cssTag)
                            {{$cssTag}}
                        @endforeach ">
                                <a href="gallery/{{$listing['id']}}">
                                <img src="{{$listing['listing_pic']}}">

                                <div class="text-overlay">
                                    <h2>{{$listing['headline']}}</h2>

                                    <p>{{(strlen($listing['description']) >= 135 ? substr($listing['description'], 0, 132) . '...' : $listing['description'])}}</p>
                                </div>
                            </a>
                        </div>

                        <?php $x++; ?>
                    @endforeach

                    <div class="error-message" id="js-filter-error-message">
                    <p>Sorry, no results for your filter criteria</p>
                    </div>

                </div>

            @else

                <div class="error-message" id="js-filter-error-message">
                    <p>Sorry, no results</p>
                </div>

            @endif


        </div>

  </section>

</div>


@stop
