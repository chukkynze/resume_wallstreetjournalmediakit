@extends('layouts.templates.landing-page-layout')
@section('content')

<h1>WSJ. Insights</h1>

<div id="searchable-content" class="module-wrapper">

    <div id="search-description">

        @foreach($modulesArray as $key => $module)
            @include("/home/modules/" . $module['partial_name'], ['sectionKey' => 1, 'key' => $key, 'moduleObject' => $module['moduleObject']])
        @endforeach

    </div>

</div>

@stop
