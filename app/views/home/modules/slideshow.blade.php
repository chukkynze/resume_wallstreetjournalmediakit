@section('slider-module' . ($sectionKey ?: 0) . $key)

    <!-- Slideshow -->
        <div class="module slideshow  @if($moduleObject->has_border == 1) has-border @endif" data-js-module="slideshow">

            <ul class="slides-outer js-slideshow">

                @foreach($moduleObject->slides as $slider)
                    <li class="slide">
                        <img src="{{$slider->download_folder}}{{$slider->download_file}}" alt="{{$slider->name}}" />
                    </li>
                @endforeach

            </ul>

            <div class="media-slideshow-nav prev"></div>
            <div class="media-slideshow-nav next"></div>

        </div>
    <!-- /Slideshow -->

@show