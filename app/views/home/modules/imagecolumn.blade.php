@section('image-columns-module' . ($sectionKey ?: 0) . $key)

    <!-- start image columns module -->
    <div class="module text-module image-columns  @if($moduleObject->has_border == 1) has-border @endif">

        <div class="module-content">

            @if(!is_null($moduleObject->iconHeading))
            <div class="icon-title">
                <img src="{{$moduleObject->iconHeading->download_folder}}{{$moduleObject->iconHeading->download_file}}" alt="{{$moduleObject->iconHeading->name}}" />
            </div>
            @endif

            {{$moduleObject->body}}

            <div class="module-columns image-columns">

                @if(count($moduleObject->uploadLoop) >= 1)
                    @foreach($moduleObject->uploadLoop as $upld)
                        <div class="column">
                            <img src="{{$upld->download_folder}}{{$upld->download_file}}" alt="{{$upld->name}}" />
                        </div>
                    @endforeach
                @endif

            </div>

            {{$moduleObject->body_2}}

        </div>

    </div>
    <!-- end image columns module -->

@show