@section('single-image-module' . ($sectionKey ?: 0) . $key)

    <!-- Single Image -->
    <div class="module single-image  @if($moduleObject->has_border == 1) has-border @endif">
        <a href="{{$moduleObject->link_url}}" target="{{$moduleObject->targetName}}">
            <img src="{{$moduleObject->url}}" alt="{{$moduleObject->alt_text}}" >
        </a>
    </div>
    <!-- /Single Image -->

@show