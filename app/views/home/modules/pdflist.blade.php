@section('pdflist-module' . ($sectionKey ?: 0) . $key)

    <!-- start pdf list module -->
    <div class="module text-module pdf-list  @if($moduleObject->has_border == 1) has-border @endif">

        <div class="module-content">

            @if(!is_null($moduleObject->iconHeading))
            <div class="icon-title">
                <img src="{{$moduleObject->iconHeading->download_folder}}{{$moduleObject->iconHeading->download_file}}" alt="{{$moduleObject->iconHeading->name}}" />
            </div>
            @endif

            {{$moduleObject->body}}

                @if(count($moduleObject->uploadLoop) >= 1)
                <div class="pdf-docs">
                    @foreach($moduleObject->uploadLoop as $upld)

                        <a href="{{$upld->gated ? "#" : $upld->download_folder.$upld->download_file }}" class="{{ substr($upld->download_file, -4) == ".xls" || substr($upld->download_file, -5) == ".xlsx" ? "excel-link" : "pdf-link"}} @if($upld->gated) requires-form-input @endif" @if(!$upld->gated) target="_blank" @endif data-upload-id="{{$upld->id}}">{{$upld->name}}</a>
                    @endforeach
                </div>
                @endif

            {{$moduleObject->body_2}}

        </div>

    </div>
    <!-- end pdf list module -->
@show
