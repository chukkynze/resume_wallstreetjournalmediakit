@section('summary-module' . ($sectionKey ?: 0) . $key)

    <!-- Summary Module -->
    <div class="module split-col-image  @if($moduleObject->has_border == 1) has-border @endif {{strtolower($moduleObject->orientation)}}">

        @if(!is_null($moduleObject->iconHeading))
            <div class="icon-title">
                <img src="{{$moduleObject->iconHeading->download_folder}}{{$moduleObject->iconHeading->download_file}}" alt="{{$moduleObject->iconHeading->name}}" />
            </div>
        @endif

        @if($moduleObject->orientation == 'Left')
            <div class="split-image-col">
                <img src="{{$moduleObject->image->download_folder}}{{$moduleObject->image->download_file}}" alt="{{$moduleObject->image->name}}" />
            </div>
        @endif

        <div class="split-text-col">{{$moduleObject->body}}</div>

        @if($moduleObject->orientation == 'Right')
            <div class="split-image-col">
                <img src="{{$moduleObject->image->download_folder}}{{$moduleObject->image->download_file}}" alt="{{$moduleObject->image->name}}" />
            </div>
        @endif

    </div>
    <!-- /Summary Module -->
@show