@section('video-module-' . ($sectionKey ?: 0) . $key)

    @if($moduleObject->source != "")
        <!-- Video Player -->
        <div class="module video  @if($moduleObject->has_border == 1) has-border @endif">
            <div class="video-player {{$moduleObject->source}}">
                @if($moduleObject->source == 'youtube')
                    <iframe width="100%" height="100%" src="//www.youtube.com/embed/{{$moduleObject->code}}" frameborder="0" allowfullscreen></iframe>
                @elseif($moduleObject->source == 'vimeo')
                    <iframe src="//player.vimeo.com/video/{{$moduleObject->code}}" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                @endif
            </div>
        </div>
        <!-- /Video Player -->
    @endif
    
@show