@section('text-columns-module' . ($sectionKey ?: 0) . $key)

    <!-- start text columns module -->
    <div class="module text-module text-columns @if($moduleObject->has_border == 1) has-border @endif">

        <div class="module-content">

            @if(!is_null($moduleObject->iconHeading))
            <div class="icon-title">
                <img src="{{$moduleObject->iconHeading->download_folder}}{{$moduleObject->iconHeading->download_file}}" alt="{{$moduleObject->iconHeading->name}}" />
            </div>
            @endif

            {{$moduleObject->body}}

            @if(
                $moduleObject->column_1 != ""   ||
                $moduleObject->column_2 != ""   ||
                $moduleObject->column_3 != ""   ||
                $moduleObject->column_4 != ""   ||
                $moduleObject->column_5 != ""   ||
                $moduleObject->column_6 != ""   ||
                $moduleObject->column_7 != ""   ||
                $moduleObject->column_8 != ""
            )
                <div class="module-columns text-columns">

                    @if($moduleObject->column_1 != "")
                        <div class="column">
                            <p>
                                {{$moduleObject->column_1}}
                            </p>
                        </div>
                    @endif

                    @if($moduleObject->column_2 != "")
                        <div class="column">
                            <p>
                                {{$moduleObject->column_2}}
                            </p>
                        </div>
                    @endif

                    @if($moduleObject->column_3 != "")
                        <div class="column">
                            <p>
                                {{$moduleObject->column_3}}
                            </p>
                        </div>
                    @endif

                    @if($moduleObject->column_4 != "")
                        <div class="column">
                            <p>
                                {{$moduleObject->column_4}}
                            </p>
                        </div>
                    @endif

                    @if($moduleObject->column_5 != "")
                        <div class="column">
                            <p>
                                {{$moduleObject->column_5}}
                            </p>
                        </div>
                    @endif

                    @if($moduleObject->column_6 != "")
                        <div class="column">
                            <p>
                                {{$moduleObject->column_6}}
                            </p>
                        </div>
                    @endif

                    @if($moduleObject->column_7 != "")
                        <div class="column">
                            <p>
                                {{$moduleObject->column_7}}
                            </p>
                        </div>
                    @endif

                    @if($moduleObject->column_8 != "")
                        <div class="column">
                            <p>
                                {{$moduleObject->column_8}}
                            </p>
                        </div>
                    @endif

                </div>
            @endif

            {{$moduleObject->body_2}}

        </div>

    </div>
    <!-- end text columns module -->

@show