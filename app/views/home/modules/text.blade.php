@section('text-module' . ($sectionKey ?: 0) . $key)

    <!-- start text columns module -->
    <div class="module text-module text-columns  @if($moduleObject->has_border == 1) has-border @endif">

        <div class="module-content">

            @if(!is_null($moduleObject->iconHeading))
            <div class="icon-title">
                <img src="{{$moduleObject->iconHeading->download_folder}}{{$moduleObject->iconHeading->download_file}}" alt="{{$moduleObject->iconHeading->name}}" />
            </div>
            @endif

            {{$moduleObject->body}}

        </div>

    </div>
    <!-- end text columns module -->

@show