@section('split-text-module' . ($sectionKey ?: 0) . $key)

    <!-- Split Text -->
    <div class="module split-col-text  @if($moduleObject->has_border == 1) has-border @endif">

        <div class="module grey-box">

            <div class="split-text-col">

                {{$moduleObject->col1}}

            </div>

            <div class="split-text-col">

                {{$moduleObject->col2}}

            </div>

        </div>

    </div>
    <!-- /Split Text -->

@show