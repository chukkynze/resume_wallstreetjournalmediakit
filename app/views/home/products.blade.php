@extends('layouts.templates.landing-page-layout')
@section('content')


<h1 class="full-width">{{$heading}}</h1>

<div class="product-listing">

    @foreach($Page->productSections as $sectionKey=>$section)
    <div class="section-summary">

        <div class="col-right @if(count($section['siteProductSections']) == 0) is-empty @endif">

            <div class="editions">

                <ul class="editions-list">

                    @foreach($section['siteProductSections'] as $key => $subSection)

                        @if($section['showOnlyHeadingLinks'] == 0)

                            @if($key != "EMPTY")
                                <li>{{$key}}</li>
                            @endif

                            @foreach($subSection as $headingName => $sectionData)
                                <li>
                                    <a href="/products/{{$section['name_slug']}}#@if($key != "EMPTY"){{Str::slug($key)}}--@endif{{$sectionData['name_slug']}}">{{$sectionData['name']}}</a>
                                </li>
                            @endforeach

                            @if($key != "EMPTY")
                                <li><br></li>
                            @endif

                        @endif

                        @if($section['showOnlyHeadingLinks'] == 1)

                            <li>
                                <a href="/products/{{$section['name_slug']}}#@if($key != "EMPTY"){{Str::slug($key)}}@endif">{{$key}}</a>
                            </li>

                        @endif

                    @endforeach

                </ul>

            </div>

        </div>


        @if($section['detailsPageLiveMods'] == 1)
            <a href="products/{{$section['name_slug']}}" title="{{$section['name']}}" class="product-link">
                @endif

                        <!-- insert module loop here: -->
                @foreach($section['modulesArray'] as $key => $module)

                    @include("/home/modules/" . $module['partial_name'], ['sectionKey' => $sectionKey, 'key' => $key, 'moduleObject' => $module['moduleObject']])

                    @endforeach
                            <!-- / insert module loop here: -->

                    @if($section['detailsPageLiveMods'] == 1)
            </a>
        @endif

    </div>
    @endforeach


@stop