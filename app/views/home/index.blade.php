@extends('layouts.templates.layout')
@section('content')

<div class="landing-layout">

    <div class="landing-marquee-outer homepage js-viewport-scale">
        <div class="landing-marquee">
            <ul class="landing-slideshow" id="js-landing-marquee">

                @foreach($sliders as $slider)
                <li style="background-image: url('{{$slider['download_folder']}}{{$slider['download_file']}}')" class="slide">

                    <div class="landing-marquee-titles">
                        
                        <div class="landing-marquee-title">
                          <div class="main-text">
                            {{$slider['main_text']}}
                          </div>
                        </div>

                        <div class="sub-text">
                          {{$slider['sub_text']}}
                        </div>

                    </div>

                    <div class="footer-source-info">
                      <div class="footer-source-info-inner">
                        {{$slider['source_text']}}
                      </div>
                    </div>

                </li>
                @endforeach

            </ul>
        </div>
  </div>

  <div class="landing-link-list">
    <ul>
      <li class="landing-link"><a href="products" class="landing-link">Products</a></li>
      <li class="landing-link"><a href="wsjinsights" class="landing-link">WSJ. Insights</a></li>
      <li class="landing-link"><a href="wsjcustomstudios" class="landing-link">WSJ. Custom Studios</a></li>
      <li class="landing-link"><a href="rates_specs" class="landing-link">Rates &amp; Specs</a></li>
        <li class="landing-link mobile-hidden"><a href="gallery" class="landing-link">Case Studies</a></li>
      <li class="landing-link"><a href="https://classifieds.wsj.com/" class="landing-link secondary">Classifieds</a></li>
      <li class="landing-link"><a href="https://www.epic.dowjones.com/epicaddrop/controller" class="landing-link secondary">Submit Ad</a></li>
      <li class="landing-link"><a href="contact" class="landing-link secondary">Contact</a></li>
    </ul>
  </div>

</div>



@stop
