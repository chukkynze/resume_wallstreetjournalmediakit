<!-- adding the specialClass will cause the site logo to use darker text (used on pages without hero images) -->
@extends('layouts.templates.layout', array('specialClass' => "dark-logo"))
@section('content')

<div class="standard-layout">

  <section class="content-main">

    <div class="global-crumbs">
      <ul>
        <li>
          <a href="/">Home</a>
        </li>
        <li>
          <a href="contact" class="is-active">Contact</a>
        </li>
      </ul>
    </div>

    <h1>Contact</h1>

    <div class="option-chooser">

      <div class="option-container">

        <div class="select-container">
            <select id="js-contact-product-select" class="contact-product-select">
              <option value="0">Select Product...</option>
              @foreach($products as $product)
                <option value="{{$product['id']}}">{{$product['name']}}</option>
              @endforeach
            </select>
        </div>

        <div class="select-container">
          <select id="js-contact-location-select" class="contact-location-select" disabled>
            <option value="0">Select Location Nearest You...</option>
          </select>
        </div>

      </div>


      <div class="option-result-container" id="js-contact-results">
        <div class="option-result-column" id="js-option-result-column">

        </div>
      </div>

    </div>

  </section>

</div>




@stop
