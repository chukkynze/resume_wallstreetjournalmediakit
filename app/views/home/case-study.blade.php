<!-- adding the specialClass will cause the site logo to use darker text (used on pages without hero images) -->
@extends('layouts.templates.layout', array('specialClass' => "dark-logo"))
@section('content')

<div class="standard-layout">

    <section class="content-main">

        <div class="global-crumbs">
            <ul>
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="products.html">Products</a>
                </li>
                <li>
                    <a href="newspaper.html" class="is-active">Newspaper</a>
                </li>
            </ul>
        </div>

        <h1 class="full-width">{{$caseStudy->headline}}</h1>

        <div id="searchable-content" class="module-wrapper">

        @foreach($modulesArray as $key => $module)
                @include("/home/modules/" . $module['partial_name'], ['sectionKey' => 1, 'key' => $key, 'moduleObject' => $module['moduleObject']])
            @endforeach

        </div>

    </section>

</div>


@stop