@extends('...layouts.templates.layout', array('specialClass' => "dark-logo"))
@section('content')
  <section class="content-main">
		<div class="error-container">
			<h1 class="centered-text">404 Error</h1>

			<p class="centered-text">
					Sorry, the page you were looking for can't be found.
			</p>

			<p class="centered-text">
				<a href="/">Return to Homepage</a>
			<p>

		</div> 
	</section>
@stop


