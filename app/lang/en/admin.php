<?php

    return array(
        'update_success'    => 'Updated Successfully',
        'create_success'    => 'Created Successfully',
        'delete_success'    => 'Deleted Successfully',
        'reports'           => [
            'delete_failure' => 'Report category was not deleted.',
            'create_success' => 'Successfully created a new report: ',
        ],
        'reportCategory'    => [
            'delete_failure' => 'Report category was not deleted.',
            'create_success' => 'Successfully created a new report category.',
        ],
        'clients'           => [
            'delete_failure' => 'Client was not deleted.',
            'create_success' => 'Successfully created a new client.',
        ],
        'caseStudyCategory' => [
            'delete_failure' => 'Case Study category was not deleted.',
            'create_success' => 'Successfully created a new case study category.',
        ],
        'caseStudyPlatform' => [
            'delete_failure' => 'Case Study platform was not deleted.',
            'create_success' => 'Successfully created a new case study platform.',
        ],
        'uploadCategory'    => [
            'delete_failure' => 'Upload category was not deleted.',
            'create_success' => 'Successfully created a new upload category.',
            'update_success' => 'Successfully updated: ',
        ],
        'uploads'           => [
            'delete_failure' => 'Uploaded image was not deleted.',
            'create_success' => 'Successfully uploaded a new file:',
            'edit_success' => 'Successfully edited your media content.',
        ],
        'insights'          => [
            'delete_min'     => 'Cannot delete the only version of Insight Page Content.',
            'delete_failure' => 'Insight Page Content was not deleted.',
            'create_success' => 'Successfully uploaded new Insight Page Content.',
            'update_success' => 'Successfully updated Insight Page Content.',
        ],
        'studios' => [
            'delete_min'     => 'Cannot delete the only version of Custom Studios Page Content.',
            'delete_failure' => 'Custom Studios Page Content was not deleted.',
            'create_success' => 'Successfully uploaded new Custom Studios Page Content.',
            'update_success' => 'Successfully updated Custom Studios Page Content.',
        ],
        'site-products'         => [
            'delete_failure' => 'Product was not deleted.',
            'create_success' => 'Successfully created new Product.',
            'update_success' => 'Successfully updated Product.',
        ],
        'rate-products'    => [
            'delete_failure' => 'Rate Product was not deleted.',
            'create_success' => 'Successfully created new Rate Product.',
            'update_success' => 'Successfully updated Rate Product.',
        ],
        'contact-products' => [
            'delete_failure' => 'Contact Product was not deleted.',
            'create_success' => 'Successfully created new Contact Product.',
            'update_success' => 'Successfully updated Contact Product.',
        ],
        'site-product-sections' => [
            'delete_failure' => 'Product Section was not deleted.',
            'create_success' => 'Successfully created new Product Section.',
            'update_success' => 'Successfully updated Product Section.',
        ],
        'contact-locations' => [
            'delete_failure' => 'Contact Location was not deleted.',
            'create_success' => 'Successfully created new Contact Location.',
            'update_success' => 'Successfully updated Contact Location.',
        ],
        'contacts'          => [
            'delete_failure' => 'Contact was not deleted.',
            'create_success' => 'Successfully created new Contact.',
            'update_success' => 'Successfully updated Contact.',
        ],
        'rates'             => [
            'delete_min'     => 'Cannot delete the only version of Rates &amp; Specifications Page Content.',
            'delete_failure' => 'Rate/Specification was not deleted.',
            'create_success' => 'Successfully created new content for Rates &amp; Specifications.',
            'update_success' => 'Successfully updated Rates &amp; Specifications.',
        ],
        'case-studies'      => [
            'delete_min'     => 'Cannot delete the only Gallery Entry.',
            'delete_failure' => 'Gallery Entry was not deleted.',
            'create_success' => 'Successfully created new Gallery Entry.',
            'update_success' => 'Successfully updated Gallery Entry.',
        ],
        'case-study-filter'       => [
            'delete_min'     => 'Cannot delete the only Gallery Filter.',
            'delete_failure' => 'Gallery Filter was not deleted.',
            'create_success' => 'Successfully created new Gallery Filter.',
            'update_success' => 'Successfully updated Gallery Filter.',
        ],
        'case-study-filter-items' => [
            'delete_min'     => 'Cannot delete the only Gallery Entry Filter Item.',
            'delete_failure' => 'Gallery Entry Filter Item was not deleted.',
            'create_success' => 'Successfully created new Gallery Entry Filter Item.',
            'update_success' => 'Successfully updated Gallery Entry Filter Item.',
        ],
        'page-content'      => [
            'product-page'      => [
                'delete_min'     => 'Cannot delete the only version of Product Page Content.',
                'delete_failure' => 'Product Page Content was not deleted.',
                'create_success' => 'Successfully created new Product Page.',
                'update_success' => 'Successfully updated Product Page.',
            ],
            'home-page'         => [
                'delete_min'     => 'Cannot delete the only version of Home Page Content.',
                'delete_failure' => 'Home Page Content was not deleted.',
                'create_success' => 'Successfully created new Home Page.',
                'update_success' => 'Successfully updated Home Page.',
            ],
            'home-page-sliders' => [
                'delete_failure' => 'Home Page Content Slider was not deleted.',
                'create_success' => 'Successfully created new Home Page Slider.',
                'update_success' => 'Successfully updated Home Page Slider.',
            ],
        ],
    );