<?php
    App::down(function () {
        return Response::make(View::make('maintenance.default'));
    });

    /*
    |--------------------------------------------------------------------------
    | Register The Laravel Class Loader
    |--------------------------------------------------------------------------
    |
    | In addition to using Composer, you may use the Laravel class loader to
    | load your controllers and models. This is useful for keeping all of
    | your classes in the "global" namespace without Composer updating.
    |
    */

    ClassLoader::addDirectories(array(

        app_path() . '/commands',
        app_path() . '/controllers',
        app_path() . '/models',
        app_path() . '/database/seeds',

    ));

    /*
    |--------------------------------------------------------------------------
    | Application Error Logger
    |--------------------------------------------------------------------------
    |
    | Here we will configure the error logger setup for the application which
    | is built on top of the wonderful Monolog library. By default we will
    | build a basic log file setup which creates a single file for logs.
    |
    */

    Log::useDailyFiles(storage_path() . '/logs/applog-' . date("Y-m-d") . '.log');

    /*
    |--------------------------------------------------------------------------
    | Application Error Handler
    |--------------------------------------------------------------------------
    |
    | Here you may handle any errors that occur in your application, including
    | logging them or displaying custom views for specific errors. You may
    | even register several error handlers to handle different types of
    | exceptions. If nothing is returned, the default error view is
    | shown, which includes a detailed stack trace during debug.
    |
    */

    App::error(function (Exception $exception, $code) {
        if (Config::get('sentry.enabled') === true) {
            // Grab the monolog singleton
            $Monolog = Log::getMonolog();

            // Create a new raven client with the Sentry project URL
            $RavenClient = new Raven_Client(Config::get('sentry.dsn'));

            // Next we tell the monolog raven handler the client we wish to use and the minimum error level to log
            $MonologHandler = new \Monolog\Handler\RavenHandler($RavenClient, \Monolog\Logger::ERROR);

            // Re-format the error message to remove the date/time
            $MonologHandler->setFormatter(new \Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));

            // Push the new handler onto monolog for when we encounter an error
            $Monolog->pushHandler($MonologHandler);
        }


        // Log the error to monolog (With the new handler in place)
        Log::error($exception,
            [
                'exception' => $exception
            ]);


        if (Config::get('app.debug') == true) {
            /*
            if (Request::is('api/*'))
            {
                return Api::error($exception->getMessage(), $code);
            }
            */

            Log::error($exception);
        } else {
            /*
            if (Request::is('api/*'))
            {
                return Api::error(Lang::get('500'), $code);
            }
            */

            return Response::view('errors.error', [], $code);
        }

    });


    /*
    |--------------------------------------------------------------------------
    | Custom IoC container registrations
    |--------------------------------------------------------------------------
    |
    | Use this area to "overwrite" code from other packages.
    |
    */

    App::bind('confide.user_validator', 'Hyfn\Library\CustomUserValidator');

    /*
    |--------------------------------------------------------------------------
    | Maintenance Mode Handler
    |--------------------------------------------------------------------------
    |
    | The "down" Artisan command gives you the ability to put an application
    | into maintenance mode. Here, you will define what is displayed back
    | to the user if maintenance mode is in effect for the application.
    |
    */

    App::down(function () {
        return Response::make("Be right back!", 503);
    });

    /*
    |--------------------------------------------------------------------------
    | Require The Filters File
    |--------------------------------------------------------------------------
    |
    | Next we will load the filters file for the application. This gives us
    | a nice separate location to store our route and application filter
    | definitions instead of putting them all in the main routes file.
    |
    */

    require app_path() . '/filters.php';
