<?php
    namespace Hyfn\Models;

    use Eloquent;

    use Watson\Validating\ValidatingTrait;

    class Tag extends Eloquent
    {
        use ValidatingTrait {
            ValidatingTrait::isValid as isModelValid;
        }


        public function uploads()
        {
            return $this->belongsToMany('Upload');
        }

        protected $rules = [
            'name' => 'required|between:6,60|unique:uploads,name,NULL,deleted_at',
        ];

        protected $validationMessages = [
            'name.required'   => 'A name is required.',
            'name.between'    => 'A name must be between 4 and 16 characters.',
            'name.alpha_dash' => 'A name can only have alpha-numeric characters, as well as dashes and underscores.',
            'name.unique'     => 'That name is already taken.',


        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'name' => 'required|between:6,60|unique:uploads,name,NULL,deleted_at',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'tags';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'name',
        ];
    }