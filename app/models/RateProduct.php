<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       RateProduct
     *
     * filename:    RateProduct.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Models;

    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;

    class RateProduct extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'name.required' => 'A rate product name is required.',
            'name.unique'   => 'That rate product name is already taken.',
        ];

        protected $rulesets = [
            'saving' => [
                'name' => 'required|unique:rate_products,name',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'rate_products';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'name',
            'submit_ad_link',
            'order',
        ];


        protected $dates = ['deleted_at'];
    }