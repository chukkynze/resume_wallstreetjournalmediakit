<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       UploadCategory
     *
     * filename:    UploadCategory.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Models;

    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class UploadCategory extends Eloquent
    {
        // categories not to use in the wysiwyg uploader
        // hardcoded for now
        public static $wysiwygBlacklist = ['PDFs + Spreadsheets'];
    
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'name.required'   => 'A category name is required.',
            'name.between'    => 'A category name must be between 4 and 20 characters.',
            'name.unique'     => 'That category name is already taken.',

        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'name' => 'required|between:4,150|unique:upload_category,name',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'upload_category';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'order',
            'name',
        ];


        protected $dates = ['deleted_at'];
    }