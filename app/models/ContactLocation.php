<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       ContactLocation
     *
     * filename:    ContactLocation.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Models;

    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;

    class ContactLocation extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $rules = [
            'name' => 'required|between:4,30|unique:contact_locations,name',
        ];

        protected $validationMessages = [
            'name.required' => 'A contact location is required.',
            'name.between'  => 'A contact location must be between 4 and 30 characters.',
            'name.unique'   => 'That contact location is already taken.',

        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'name' => 'required|between:4,30|unique:contact_locations,name',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'contact_locations';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'name',
            'order',
        ];


        protected $dates = ['deleted_at'];
    }