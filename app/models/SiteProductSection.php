<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       SiteProductSection
     *
     * filename:    SiteProductSection.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Models;

    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;

    class SiteProductSection extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'name.required'                     => 'A product section name is required.',
            'name.between'                      => 'A product section name must be between 4 and 50 characters.',
            'name_slug.unique_with'             => 'That product section name is already taken.',
            'name_slug.required'                => 'The product section name slug is required.',
            'site_product_id.required'          => 'Product id is required?',
            'site_product_id.numeric'           => 'That product section id is in the wrong format.',
            'enable_link.required'              => 'Will this product section be linkable or not?',
            'enable_link.numeric'               => 'That product section response is in the wrong format.',
            'enable_link.in'                    => 'That product section response is invalid.',
            'enable_all_caps.required'          => 'Will this product section link text be all caps or not?',
            'enable_all_caps.numeric'           => 'That product section link text response is in the wrong format.',
            'enable_all_caps.in'                => 'That product section link text response is invalid.',
            'only_headings_on_landing.required' => 'Will this product section use only headings as links on landing pages or not?',
            'only_headings_on_landing.numeric'  => 'That product section link text response for landing page headings is in the wrong format.',
            'only_headings_on_landing.in'       => 'That product section link text response for landing page headings is invalid.',
            'link_text.required'       => 'Link Text is required.',
            'link_text.between'        => 'Link Text must be between 4 and 50 characters.',
            'order.required' => 'The order of sub modules is required.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'name'                     => 'required|between:4,50',
                'name_slug' => 'required',
                'site_product_id'          => 'required|numeric',
                'order' => 'required',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'site_product_sections';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'name',
            'name_slug',
            'site_product_id',
            'enable_link',
            'enable_all_caps',
            'only_headings_on_landing',
            'link_text',
            'order',
        ];


        protected $dates = ['deleted_at'];
    }