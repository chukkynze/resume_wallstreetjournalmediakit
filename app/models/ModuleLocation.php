<?php
    /**
     * Project:     wsj
     *
     * Model:       ModuleLocation
     *
     * filename:    ModuleLocation.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;


    class ModuleLocation extends Eloquent
    {
        use ValidatingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $rules = [
            'name' => 'required|between:4,100',
            'slug' => 'required|alpha_dash',
        ];

        protected $validationMessages = [
            'name.required'   => 'A name is required.',
            'name.between'    => 'The name must be between 4 and 100 characters.',
            'slug.required'   => 'The name slug is required.',
            'slug.alpha_dash' => 'The name slug is invalid.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'name' => 'required|between:4,100',
                'slug' => 'required|alpha_dash',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'module_locations';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'name',
            'slug',
        ];
    }