<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       Rate
     *
     * filename:    Rate.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:09 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class Rate extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $rules = [
            'products_array' => 'required',
            'uploads_array'  => 'required',
        ];

        protected $validationMessages = [
            'products_array.required' => 'Please, recheck the products you added.',
            'uploads_array.required'  => 'Please, recheck the uploads you added.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'products_array' => 'required',
                'uploads_array'  => 'required',
            ]
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'rate_specs';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id',
        ];

        /**
         * @var array
         */
        protected $fillable = [
            'products_array',
            'uploads_array',
        ];

        protected $dates = ['deleted_at'];

    }