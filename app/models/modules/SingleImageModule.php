<?php namespace Hyfn\Models\Modules;

class SingleImageModule extends BaseModule
{
    protected $fillable = ['upload_id', 'has_border', 'is_live', 'title'];

    protected $rules = [
        'upload_id'  => 'required|numeric',
        'target'     => 'boolean',
        'link_url'   => 'url',
        'alt_text'   => 'between:4,100',
        'has_border' => 'boolean',
        'is_live' => 'boolean',
    ];

    public function image()
    {
        return $this->belongsTo('Hyfn\Models\Upload', 'upload_id');
    }
}
