<?php

    namespace Hyfn\Models\Modules;

class SummaryModule extends BaseModule
{
    protected $fillable = ['upload_id', 'body', 'orientation', 'has_border', 'is_live', 'title', 'image_id'];

    protected $rules = [
        'upload_id' => 'required|numeric',
        'body' => 'required',
        'orientation' => 'in:Left,Right',
        'has_border' => 'boolean',
        'is_live' => 'boolean',
        'image_id' => 'required|numeric',
    ];

    public function image()
    {
        return $this->hasOne('Hyfn\Models\Upload', 'id', 'image_id');
    }
}
