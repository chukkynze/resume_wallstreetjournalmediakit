<?php

    namespace Hyfn\Models\Modules;

class TextColumnModule extends BaseModule
{
    protected $fillable = ['upload_id', 'body', 'body_2', 'column_1', 'column_2', 'column_3', 'column_4', 'column_5', 'column_6', 'column_7', 'column_8', 'has_border', 'is_live', 'title'];

    protected $rules = [
        'upload_id' => 'numeric',
        'has_border' => 'boolean',
        'is_live' => 'boolean',
    ];
}
