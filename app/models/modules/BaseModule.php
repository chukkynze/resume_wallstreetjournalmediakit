<?php namespace Hyfn\Models\Modules;

use Eloquent;
use Watson\Validating\ValidatingTrait;

class BaseModule extends Eloquent
{
    use ValidatingTrait;

    public function module_location_relationships()
    {
        return $this->morphMany('ModuleLocationRelationship', 'module');
    }

    public function setHasBorderAttribute($value)
    {
        $this->attributes['has_border'] = (boolean) $value;
    }

    public function setIsLiveAttribute($value)
    {
        $this->attributes['is_live'] = (boolean)$value;
    }
}
