<?php

    namespace Hyfn\Models\Modules;

class ImageColumnModule extends BaseModule
{
    protected $fillable = ['upload_id', 'body', 'body_2', 'has_border', 'is_live', 'title'];

    protected $rules = [
        'upload_id' => 'numeric',
        'has_border' => 'boolean',
        'is_live' => 'boolean',
    ];

    public function title()
    {
        return $this->belongsTo('Hyfn\Models\Upload', 'upload_id');
    }

    public function images()
    {
        return $this->morphToMany('Hyfn\Models\Upload', 'uploadable')->orderBy('order');
    }
}
