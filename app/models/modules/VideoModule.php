<?php namespace Hyfn\Models\Modules;

    namespace Hyfn\Models\Modules;

class VideoModule extends BaseModule
{
    protected $fillable = ['upload_id', 'url', 'has_border', 'is_live', 'title'];

    protected $rules = [
        'upload_id' => 'numeric',
        'url' => 'required|url',
        'has_border' => 'boolean',
        'is_live' => 'boolean',
    ];
}
