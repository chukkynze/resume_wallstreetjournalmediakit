<?php

    namespace Hyfn\Models\Modules;

class SplitTextModule extends BaseModule
{
    protected $fillable = ['upload_id', 'col1', 'col2', 'has_border', 'is_live', 'title'];

    protected $rules = [
        'upload_id' => 'numeric',
        'col1' => 'required',
        'col2' => 'required',
        'has_border' => 'boolean',
        'is_live' => 'boolean',
    ];
}
