<?php namespace Hyfn\Models\Modules;

use Eloquent;
use Watson\Validating\ValidatingTrait;

class TextColumn extends Eloquent
{
    use ValidatingTrait;

    public $timestamps = false;

    protected $fillable = ['text_column_module_id', 'col_id', 'line', 'order', 'is_live', 'title'];

    protected $rules = [
        'text_column_module_id' => 'required|numeric',
        'col_id' => 'numeric',
        'line'   => 'required',
        'order' => 'numeric',
        'is_live' => 'boolean',
    ];

    public function text_column_module()
    {
        return $this->belongsTo('Hyfn\Models\Modules\TextColumnModule');
    }
}
