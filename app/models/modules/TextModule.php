<?php

    namespace Hyfn\Models\Modules;

class TextModule extends BaseModule
{
    protected $fillable = ['upload_id', 'body', 'has_border', 'is_live', 'title'];

    protected $rules = [
        'upload_id' => 'numeric',
        'body' => 'required',
        'has_border' => 'boolean',
        'is_live' => 'boolean',
    ];

    public function title()
    {
        return $this->belongsTo('Hyfn\Models\Upload', 'upload_id');
    }
}
