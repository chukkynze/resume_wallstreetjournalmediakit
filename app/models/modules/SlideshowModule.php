<?php namespace Hyfn\Models\Modules;

class SlideshowModule extends BaseModule
{
    protected $fillable = ['has_border', 'is_live', 'title'];

    protected $rules = [
        'has_border' => 'boolean',
        'is_live' => 'boolean',
    ];

    public function slides()
    {
        return $this->morphToMany('Hyfn\Models\Upload', 'uploadable')->orderBy('order');
    }
}
