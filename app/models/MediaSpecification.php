<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       Rate
     *
     * filename:    Rate.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:09 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;


    class MediaSpecification extends Eloquent
    {
        use ValidatingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'rate_product_id.required' => 'Please, recheck the products you added.',
            'upload_id.required'       => 'Please, recheck the uploads you added.',
        ];

        protected $rulesets = [
            'saving' => [
                'rate_product_id' => 'required',
                'upload_id'       => 'required',
            ]
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'rate_products_media_specs';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id',
        ];

        /**
         * @var array
         */
        protected $fillable = [
            'rate_product_id',
            'upload_id',
            'order',
        ];

    }