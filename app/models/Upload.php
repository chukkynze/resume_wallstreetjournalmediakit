<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       Insight
     *
     * filename:    Insight.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/16/14 10:48 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;

    use Eloquent;

    use Watson\Validating\ValidatingTrait;


    class Upload extends Eloquent
    {
        use ValidatingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'name.required'              => 'A name is required.',
            'name.between' => 'A name must be between 6 and 100 characters.',
            'name.alpha_dash'            => 'A name can only have alpha-numeric characters, as well as dashes and underscores.',
            'description.required'       => 'A description is required.',
            'description.between'        => 'A description must be between 6 and 200 characters.',
            'download_file.required'     => 'A download file is required.',
            'download_folder.required'   => 'A download file location is required.',
            'download_folder.alpha_dash' => 'The download file location may only contain letters, numbers, and dashes. No spaces.',
            'status.required'            => 'A status is required.',
            'status.in'                  => 'Please choose Draft or Published.',
            'uploader_id.integer'        => 'The uploader is not recognized.',
            'uploader_id.exists'         => 'The uploader is not an existing user.',
            'keywords.required'          => 'Please add keywords for this upload.',
            'keywords.alpha_dash'        => 'Keywords may have alpha-numeric characters, as well as dashes and underscores.',
            'category.required'          => 'An upload category is required.',
            'category.exists'            => 'That upload category does not exist in our records.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'name' => 'required|between:6,100',
                //'description'     => 'required|between:6,200',
                'download_file'   => 'required',
                'download_folder' => 'required',
                // 'status'          => 'required|in:Draft,Published',
                'uploader_id'     => 'integer|exists:users,id',
                'keywords' => 'required',
                'category'        => 'required|exists:upload_category,id',
                'gated' => 'boolean',
                'is_live' => 'boolean',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'uploads';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'name',
            'description',
            'download_file',
            'download_folder',
            'status',
            'uploader_id',
            'keywords',
            'category',
            'gated',
            'is_live',
        ];
    }
