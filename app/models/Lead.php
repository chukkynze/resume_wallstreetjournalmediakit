<?php namespace Hyfn\Models;

use Eloquent;
use Watson\Validating\ValidatingTrait;

class Lead extends Eloquent
{
    use ValidatingTrait;

    protected $fillable = ['email', 'first_name', 'last_name', 'company', 'title', 'upload_id'];

    protected $rules = [
        'email' => 'required|email',
        'first_name' => 'required',
        'last_name' => 'required',
        'company' => 'required',
        'title' => 'required',
        'upload_id' => 'required',
    ];

    protected $connection = 'db1';
}
