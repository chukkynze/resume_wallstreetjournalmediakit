<?php
    /**
     * Project:     WSJ MediaKit 2
     *
     * Model:       ModuleType
     *
     * filename:    ModuleType.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;


    class ModuleLocationRelationship extends Eloquent
    {
        use ValidatingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        public function module()
        {
            return $this->morphTo();
        }

        public function entity()
        {
            return $this->morphTo();
        }

        protected $validationMessages = [
            'location_id.required' => 'A location is required.',
            'entity_id.required'   => 'A module id is required.',
            'entity_type.required' => 'A module type is required.',
            'module_id.required'   => 'A module id is required.',
            'module_type.required' => 'A module type is required.',
        ];

        protected $rulesets = [
            'saving' => [
                'location_id' => 'required',
                'entity_id'   => '',
                'entity_type' => '',
                'module_id'   => 'required', // id of module
                'module_type' => 'required', // class name of module
                'order' => '',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'module_location_relationships';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'location_id',
            'entity_id',
            'entity_type',
            'module_id',
            'module_type',
            'order',
        ];
    }