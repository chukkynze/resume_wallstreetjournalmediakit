<?php
    /**
     * Project:     wsj
     *
     * Model:       CaseStudyFilterItem
     *
     * filename:    CaseStudyFilterItem.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class CaseStudyFilterItem extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'name.required'      => 'The filter item name is required.',
            'filter_id.required' => 'Please, specify the filter this item belongs to.',
        ];
        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'name'      => 'required',
                'filter_id' => 'required',
            ],
        ];
        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'case_study_filter_items';
        protected $connection = 'db1';
        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];
        /**
         * @var array
         */
        protected $fillable = [
            'name',
            'filter_id',
            'order',
            'description',
        ];
        protected $dates = ['deleted_at'];
    }