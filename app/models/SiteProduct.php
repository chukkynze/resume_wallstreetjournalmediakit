<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       SiteProduct
     *
     * filename:    SiteProduct.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Models;

    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class SiteProduct extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        public function module_location_relationships()
        {
            return $this->morphMany('ModuleLocationRelationship', 'entity');
        }

        protected $validationMessages = [
            'name.required'                   => 'A product name is required.',
            'name.between'                    => 'A product name must be between 4 and 30 characters.',
            'name.unique'                     => 'That product name is already taken.',
            'name_slug.required'              => 'The name slug is required.',
            'name_slug.alpha_dash' => 'The name slug may only have alpha-numeric characters, as well as dashes and underscores.',
            'name_slug.unique'     => 'The name slug must be unique in the CMS.',
            'heading_vs_image.required'       => 'Will this product use an image as a heading in its details page or not?',
            'on_menu.required'                => 'Will this product show up in the menu or not?',
            'on_menu.between'                 => 'That product menu response is invalid.',
            'on_menu.numeric'                 => 'That product menu response is in the wrong format.',
            'on_product_landing.required'     => 'Will this product show up on the Product Landing Page or not?',
            'on_product_landing.between'      => 'That product Product Landing Page response is invalid.',
            'on_product_landing.numeric'      => 'That product Product Landing Page response is in the wrong format.',
            'heading_icon_upload_id.required' => 'Will this product use an image as a heading on its Product Details Page or not?',
            'heading_icon_upload_id.numeric'  => 'That product Product image on details Page response is in the wrong format.',
            'section_order.required'          => 'The order of product sections is required.',
        ];

        protected $rulesets = [
            'saving' => [
                'name'                   => 'required|between:4,30|unique:site_products,name',
                'name_slug' => 'required|alpha_dash|unique:site_products,name_slug',
                'on_menu'                => 'required|between:0,1|numeric',
                'on_product_landing'     => 'required|between:0,1|numeric',
                'heading_icon_upload_id' => 'numeric',
                'section_order'          => 'required',
                'heading_vs_image'       => 'numeric',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'site_products';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'name',
            'name_slug',
            'on_menu',
            'on_product_landing',
            'heading_vs_image',
            'heading_icon_upload_id',
            'section_order',
            'source_text',
        ];


        protected $dates = ['deleted_at'];
    }