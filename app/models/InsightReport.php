<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       InsightReport
     *
     * filename:    InsightReport.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:14 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class InsightReport extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $rules = [
            'insights_id' => 'required|integer',
            'upload_id'   => 'required|integer',
            'order'       => 'required|integer',
        ];

        protected $validationMessages = [
            'insights_id.required' => 'An Insights Page version is required.',
            'insights_id.integer'  => 'This Insights Page version is not valid.',
            'upload_id.required'   => 'A uploaded report is required for this version of The Insights Page.',
            'upload_id.integer'    => 'The uploaded report for this version of The Insights Page is not valid.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'insights_id' => 'required|integer',
                'upload_id'   => 'required|integer',
            ]
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'insight_reports';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'insights_id',
            'upload_id',
        ];


        protected $dates = ['deleted_at'];

    }