<?php
    /**
     * Project:     wsj
     *
     * Model:       CaseStudy
     *
     * filename:    CaseStudy.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;

    /**
     * Class CaseStudy
     *
     * @package Hyfn\Models
     */
    class CaseStudy extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'headline.required'    => 'The main heading is required.',
            'description.required' => 'The main description is required.',
            'keywords.required'    => 'The keywords are required.',
            'is_live.required' => 'The publishing status is required.',
            'client_id.exists'     => 'The Client must exist in the CMS.',
        ];
        protected $rulesets = [
            'saving' => [
                'headline'       => 'required',
                'description'    => 'required',
                'keywords'       => 'required',
                'is_live' => 'required',
            ],
        ];
        protected $table = 'case_studies';
        protected $connection = 'db1';
        protected $hidden = [
            'id'
        ];
        protected $fillable = [
            'headline',
            'client_name',
            'description',
            'keywords',
            'listing_pic_id',
            'client_id',
            'is_live',
            'order',
        ];
        protected $dates = [
            'deleted_at'
        ];

    }