<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       Contact
     *
     * filename:    Contact.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:09 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class Contact extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'department.between' => 'A Contact\'s department must be between 0 to 160 characters.',
            'full_name.required'    => 'A contact\'s full name is required.',
            'full_name.between'     => 'A Contact\'s full name must be between 0 to 60 characters.',
            'job_title.required'    => 'A Contact\'s job title is required.',
            'job_title.between'     => 'A Contact\'s job title must be between 0 to 60 characters.',
            'phone_number.required' => 'A Contact\'s phone number is required.',
            'phone_number.phone'    => 'The format of this Contact\'s phone number is invalid.',
            'email.required'        => 'A Contact\'s email address is required.',
            'email.email'           => 'The format of this Contact\'s email is invalid.',
            'location_id.integer'   => 'Choose a contact\'s location from the list.',
            'location_id.exists'    => 'That location does not exist in our database.',
            'product_id.required'   => 'A Contact\'s product assignment is required.',
            'product_id.integer'    => 'Choose a contact\'s product assignment from the list.',
            'product_id.exists'     => 'That product assignment does not exist in our database.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'department' => 'between:0,160',
                'full_name'    => 'required|between:0,60',
                'job_title' => 'between:0,60',
                'phone_number' => 'phone:US,CA,CN,HK,JP,KP,KR,SG,GB,FR,AR,BR',
                'email'        => 'email',
                'location_id'  => 'integer|exists:contact_locations,id',
                'product_id'   => 'required|integer|exists:contact_products,id',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'contacts';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id',
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'department',
            'full_name',
            'job_title',
            'phone_number',
            'email',
            'order',
        ];


        protected $dates = ['deleted_at'];

    }