<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       Insight
     *
     * filename:    Insight.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:09 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class Insight extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $rules = [
            'title'         => 'required',
            'banner_img_id' => 'required|integer',
            'subheading'    => 'required|min:4|max:20',
            'content1'      => 'required|min:5|max:250',
            'content2'      => 'required|min:5|max:350',
        ];

        protected $validationMessages = [
            'title.required'         => 'The Insight Page Title is required',
            'title.alpha_dash'       => 'The Insight Page Title can only have alpha-numeric characters, as well as dashes and underscores.',
            'banner_img_id.required' => 'Please choose a banner image.',
            'banner_img_id.integer'  => 'Please choose a valid banner image.',
            'subheading.required'    => 'A sub heading is required.',
            'subheading.min'         => 'The sub heading must be at least 4 characters.',
            'subheading.max'         => 'The sub heading must be less than 21 characters.',
            'content1.required'      => 'Content box 1 is required.',
            'content1.min'           => 'Content box 1  must be at least 5 characters.',
            'content1.max'           => 'Content box 1  must be less than 250 characters.',
            'content2.required'      => 'Content box 2 is required.',
            'content2.min'           => 'Content box 2  must be at least 5 characters.',
            'content2.max'           => 'Content box 2  must be less than 350 characters.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'title'         => 'required',
                'banner_img_id' => 'required|integer',
                'subheading'    => 'required|min:4|max:20',
                'content1'      => 'required|min:5|max:250',
                'content2'      => 'required|min:5|max:350',
            ]
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'insights';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id',
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'title',
            'banner_img_id',
            'subheading',
            'content1',
            'content2',
        ];


        protected $dates = ['deleted_at'];

    }