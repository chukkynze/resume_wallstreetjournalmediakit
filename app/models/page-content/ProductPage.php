<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       ProductPage
     *
     * filename:    ProductPage.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/13/14 11:07 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models\PageContent;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class ProductPage extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'heading.required'                => 'The page heading is required.',
            'heading.between'                 => 'The page heading must be between 4 and 100 characters.',
            'banner_image_enabled.required'   => 'Banner choice is required.',
            'banner_image_upload_id.required' => 'A banner image is required.',
            'banner_image_upload_id.integer'  => 'A valid banner image is required.',
            'section_order.required'          => 'The order of product sections is required.',
            'module_order.required' => 'The order of sub modules is required.',
            'source_text_enabled.required'    => 'Source Text choice is required.',
            'source_text.required'            => 'Source text is required.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'heading'                => 'required|between:4,100',
                'banner_image_enabled'   => 'required|between:0,1|numeric',
                'banner_image_upload_id' => 'required|integer',
                'section_order'          => 'required',
                'module_order' => 'required',
                'source_text_enabled' => 'between:0,1|numeric',
                'source_text' => 'required',
            ]
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'product_pages';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id',
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'heading',
            'banner_image_enabled',
            'banner_image_upload_id',
            'section_order',
            'module_order',
            'source_text_enabled',
            'source_text',
        ];


        protected $dates = ['deleted_at'];

    }