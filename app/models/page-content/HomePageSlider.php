<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       HomePageSlider
     *
     * filename:    HomePageSlider.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/13/14 11:07 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models\PageContent;

    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class HomePageSlider extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'home_page_id.required' => 'Please start from <a href="/admin/page-content/home-pages/create">here</a>.',
            'home_page_id.integer'  => 'Please specify a number.',
            'upload_id.required'    => 'Please, choose an image for the slider.',
            'upload_id.integer'     => 'Please, choose a valid image for the slider.',
            'main_text.required'    => 'Main slider text is required.',
            'main_text.min'         => 'Your main slider text is too short (Min 4 characters).',
            'main_text.max'         => 'Your main slider text is too long (Max 100 characters).',
            'sub_text.required' => 'Secondary text is required.',
            'sub_text.min'      => 'Secondary text is too short (Min 4 characters).',
            'sub_text.max'      => 'Secondary text is too long (Max 100 characters).',
            'source_text.required'  => 'Source text is required.',
            'source_text.min'       => 'Your source text is too short (Min 10 characters).',
            'source_text.max'       => 'Your source text is too long (Max 300 characters).',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'home_page_id' => 'required|integer',
                'upload_id'    => 'required|integer',
                'main_text'    => 'required|min:4',
                'sub_text'     => 'min:4',
                'source_text'  => 'min:10',
            ]
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'home_page_sliders';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id',
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'home_page_id',
            'upload_id',
            'main_text',
            'sub_text',
            'source_text',
        ];


        protected $dates = ['deleted_at'];

    }
