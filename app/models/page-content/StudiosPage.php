<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       StudiosPage
     *
     * filename:    StudiosPage.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/24/14 4:09 PM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models\PageContent;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;


    class StudiosPage extends Eloquent
    {
        use ValidatingTrait,
            SoftDeletingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'title.required'   => 'The Custom Studios Page Title is required',
            'title.alpha_dash' => 'The Custom Studios Page Title can only have alpha-numeric characters, as well as dashes and underscores.',
            'banner_img_id.required' => 'Please choose a banner image.',
            'banner_img_id.integer'  => 'Please choose a valid banner image.',
            'source_text.required'   => 'Source text is required.',
            'source_text.min'        => 'Your source text is too short (Min 10 characters).',
            'source_text.max'        => 'Your source text is too long (Max 1000 characters).',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'title'         => 'required',
                'banner_img_id' => 'required|integer',
                'source_text' => 'max:1000',
            ]
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'studios_page';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id',
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'title',
            'banner_img_id',
            'source_text',
        ];


        protected $dates = ['deleted_at'];

    }