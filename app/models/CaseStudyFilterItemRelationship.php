<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       CaseStudyFilterItemRelationship
     *
     * filename:    CaseStudyFilterItemRelationship.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       10/20/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */
    namespace Hyfn\Models;

    use Eloquent;

    use Watson\Validating\ValidatingTrait;

    class CaseStudyFilterItemRelationship extends Eloquent
    {
        use ValidatingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'case_study_id.required'  => 'A case study id is required.',
            'case_study_id.integer'   => 'Case study id must be valid.',
            'filter_item_id.required' => 'A filter item id is required.',
            'filter_item_id.integer'  => 'Filter items must be valid.',
            'filter_id.required'      => 'A filter id is required.',
            'filter_id.integer'       => 'Filters must be valid.',
        ];
        protected $rulesets = [
            'saving' => [
                'case_study_id'  => 'required|integer',
                'filter_item_id' => 'required|integer',
                'filter_id'      => 'required|integer',
            ],
        ];
        protected $table = 'case_study_filter_item_relationship';
        protected $connection = 'db1';
        protected $hidden = [
            'id'
        ];
        protected $fillable = [
            'case_study_id',
            'filter_id',
            'filter_item_id',
        ];
    }