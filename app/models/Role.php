<?php
    namespace Hyfn\Models;

    use Zizaco\Entrust\EntrustRole;

    class Role extends EntrustRole
    {


        public function permissions()
        {
            return $this->hasMany('Permission');
        }

        protected $connection = 'db1';

        public static $rules = [
            'name' => 'required|between:3,16',
        ];
    }