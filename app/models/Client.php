<?php
    /**
     * Project:     wsj
     *
     * Model:       Client
     *
     * filename:    Client.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;

    use Eloquent;
    use Watson\Validating\ValidatingTrait;

    class Client extends Eloquent
    {
        use ValidatingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $rules = [
            'name' => 'required|between:2,100|unique:clients,name',
        ];

        protected $validationMessages = [
            'name.required'   => 'A client name is required.',
            'name.between'    => 'A client name must be between 2 and 100 characters.',
            'name.alpha_dash' => 'A client name can only have alpha-numeric characters, as well as dashes and underscores.',
            'name.unique'     => 'That client name is already taken.',

        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'name' => 'required|between:2,100|unique:clients,name',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'clients';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'name',
        ];


        protected $dates = ['deleted_at'];
    }