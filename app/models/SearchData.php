<?php
    /**
     * Project:     WSJ MediaKit
     *
     * Model:       SearchData
     *
     * filename:    SearchData.php
     *
     * @author      Chukwuma J. Nze <chukky.nze@hyfn.com>
     * @since       9/17/14 10:53 AM
     *
     * @copyright   Copyright (c) 2014 www.Hyfn.com
     */

    namespace Hyfn\Models;


    use Eloquent;

    use Watson\Validating\ValidatingTrait;


    class SearchData extends Eloquent
    {
        use ValidatingTrait {
            ValidatingTrait::isValid as isModelValid;
        }

        protected $validationMessages = [
            'title.required'    => 'The main heading is required.',
            'title.between'     => 'The main heading must be between 4 and 200 characters.',
            'url.required'      => 'The main description is required.',
            'url.between'       => 'The main description must be between 4 and 200 characters.',
            'body.required'     => 'The body is required.',
            'type.required'     => 'The type is required.',
            'description.required'     => 'The description is required.',
            'keywords.required'     => 'The keywords is required.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving' => [
                'title' => 'required|between:4,200',
                'url'   => 'required|between:4,200',
                //'body'  => 'required',
                'type'  => 'required',
                //'description'  => 'required',
                'keywords'  => 'required',
            ],
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'search_data';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'id'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'title',
            'type',
            'body',
            'description',
            'keywords',
            'url',
            'object_id',
        ];
    }