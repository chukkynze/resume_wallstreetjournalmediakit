<?php
    namespace Hyfn\Models;

    use Zizaco\Entrust\EntrustPermission;

    class Permission extends EntrustPermission
    {

        public function roles()
        {
            return $this->belongsToMany('Role');
        }


        protected $connection = 'db1';
    }