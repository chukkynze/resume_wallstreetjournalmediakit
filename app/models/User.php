<?php
    namespace Hyfn\Models;

    use Eloquent;
    use Cache;
    use Hash;
    use Config;

    use Illuminate\Database\Eloquent\SoftDeletingTrait;

    use Zizaco\Entrust\HasRole;
    use Zizaco\Confide\ConfideUser;
    use Zizaco\Confide\ConfideUserInterface;

    use Watson\Validating\ValidatingTrait;

    class User extends Eloquent
        implements ConfideUserInterface
    {
        use ConfideUser,
            HasRole,
            SoftDeletingTrait,
            ValidatingTrait {
            ConfideUser::isValid insteadof ValidatingTrait;
            ValidatingTrait::isValid as isModelValid;
        }

        protected $rules = [
            'username'              => 'required|between:4,16|alpha_dash|unique:users',
            'email'                 => 'required|email|unique:users',
            'first_name'            => 'required|between:2,16|alpha_dash',
            'last_name'             => 'required|between:2,16|alpha_dash',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => '',
        ];

        protected $validationMessages = [
            'username.required'              => 'A username is required.',
            'username.between'               => 'A username must be between 4 and 16 characters.',
            'username.alpha_dash'            => 'A username can only have alpha-numeric characters, as well as dashes and underscores.',
            'username.unique'                => 'That username is already taken.',
            'email.required'                 => 'An email address is required.',
            'email.email'                    => 'An email address must be in the correct format.',
            'email.unique'                   => 'That email address is already in use.',
            'password.required'              => 'A password is required.',
            'password.min'                   => 'A password must be at least 6 characters.',
            'password.confirmed'             => 'A password must be confirmed.',
            'password_confirmation.required' => 'A password is required.',
            'password_confirmation.between'  => 'A confirmed password must be between 6 and 16 characters.',
            'first_name.required'            => 'A first name is required.',
            'first_name.between'             => 'A first name must be between 2 and 16 characters.',
            'first_name.alpha_dash'          => 'A first name can only have alpha-numeric characters, as well as dashes and underscores.',
            'last_name.required'             => 'A last name is required.',
            'last_name.between'              => 'A last name must be between 2 and 16 characters.',
            'last_name.alpha_dash'           => 'A last name can only have alpha-numeric characters, as well as dashes and underscores.',
        ];

        protected $rulesets = [
            /**
             * All other rulesets will extend from saving defaults
             */
            'saving'   => [
                'username'   => 'required|between:4,16|alpha_dash|unique:users',
                'email'      => 'required|email|unique:users',
                'first_name' => 'required|between:2,16|alpha_dash',
                'last_name'  => 'required|between:2,16|alpha_dash',
            ],
            'creating' => [
                'password'              => 'required|min:6|confirmed',
            ],
            'updating' => [
                'password'              => 'sometimes|min:6|confirmed',
            ],
            'deleting' => [
                'user_id' => 'required|exists:users,id'
            ]
        ];


        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'users';
        protected $connection = 'db1';


        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'password',
            'remember_token'
        ];


        /**
         * @var array
         */
        protected $fillable = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password',
            'confirmation_code'
        ];


        protected $dates = ['deleted_at'];

        public static function boot() {
            parent::boot();

            User::saving(function($user) {
                unset($user->password_confirmation);
            });
            User::updating(function($user) {
                if ($user->isDirty('password')) {
                    $user->password = \Hash::make($user->password);
                }
            });
        }


        /**
         * Accessor for permissions will json decode the attribute
         *
         * @param $value
         *
         * @return mixed
         */
        public function getPermissionsAttribute($value)
        {
            return json_decode($value);
        }


        /**
         * Will return the user object from cache based on the token key
         *
         * @param $token
         *
         * @return \Illuminate\Support\Collection|static
         */
        public static function getFromToken($token)
        {
            $userId = Cache::get($token);
            $user   = User::find($userId);
            return $user;
        }

        /**
         * Check Redis for the valid user token
         *
         * @param $token
         *
         * @return bool
         */
        public static function isValidToken($token)
        {
            $valid = false;
            if (Cache::get($token)) {
                $valid = true;
            }
            return $valid;
        }

        /**
         * Sets the user's token and object in cache
         */
        private function setToken()
        {
            // Generate Auth Token
            $this->token = md5(Hash::make(microtime() . rand(0, 100000)) . \Confide::user()->id);

            $user = \Confide::user();

            // Set key
            Cache::add($this->token, $user->id, Config::get('cache.expire_auth_token'));
        }

        /**
         * Invalidates token and user object
         *
         * @param $token
         */
        public static function invalidateToken($token)
        {
            Cache::forget($token);
        }


        public function resetPassword($params)
        {
            $this->password              = array_get($params, 'password', '');
            $this->password_confirmation = array_get($params, 'password_confirmation', '');

            $validationResult   = $this->isModelValid('updating');

            if ($validationResult) {
                $this->save();
                return true;
            } else {
                return false;
            }
        }


        /**
         * Access the roles via Entrust for each user
         *
         * @return mixed
         */
        public function roles()
        {
            return $this->belongsToMany('Role', 'assigned_roles');
        }

    }
