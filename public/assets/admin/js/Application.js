$(function () {

    Application.init();
});


var Application = function () {

    var validationRules = getValidationRules();

    return {init: init, validationRules: validationRules};

    function init() {

        enableBackToTop();
        enableLightbox();
        enableCirque();
        enableEnhancedAccordion();
        enableDeleteConfirm();
        enablePassPermissionDataToRoleModal();
        enablePassRoleDataToUserModal();
        enableSaveAssignedRole();
        enableSaveAssignedPermission();
        saveNewPermission();
        saveNewRole();
        enableSortChosenInsightReportsOrder();
        enableAddRowsToInsightReportsOrder();

        enableSortSiteProductRatesOrder();
        enableCollapseAllRateDivsOnPageLoad();
        enableCollapseSiteProductRatesTables();
        enableAddRowsToRateOrder();

        enableSortCaseStudyModulesOrder();
        enableCollapseCaseStudyModules();
        enableHideCaseStudySliderDetailsRow();
        enableCollapseAllModulesOnPageLoad();

        enableSortProductPageSectionModulesOrder();
        enableAddRowsToHomePageSliderOrder();

        enableToggleModuleMgtRows();

        enableSortModulesOrderLocation1();
        enableSaveModuleLiveOrderLocation1();

        enableSortModulesOrderLocation2();
        enableSaveModuleLiveOrderLocation2();

        enableSortModulesOrderLocation6();
        enableSaveModuleLiveOrderLocation6();

        enableSortModulesOrderLocation7();
        enableSaveModuleLiveOrderLocation7();

        enableSortModulesOrderLocation8();
        enableSaveModuleLiveOrderLocation8();

        enableSortModulesOrderLocation9();
        enableSaveModuleLiveOrderLocation9();

        enableSaveProductLandingModuleLiveOrder();
        enableSaveProductDetailsModuleLiveOrder();
        enableSortProductLandingModules();
        enableSortProductDetailsModules();

        enableSortProductSubSectionsOrder();
        enableSortHomePageSliderOrder();

        enableSelectModuleHelper();

        enableClearHeadingIconRadioList();

        enableManageTextColumnDivs();
        enableToggleHomePageSliderRows();

        enableDataTables();
        enableHomePagePreview();

        enableChangeSaveButtonToPublishForLiveModules();

        handleCMSIndexDragNDropReOrderingUploadCategories();
        handleCMSIndexDragNDropReOrderingSiteProducts();
        handleCMSIndexDragNDropReOrderingRateProducts();
        handleCMSIndexDragNDropReOrderingContacts();
        handleCMSIndexDragNDropReOrderingContactProducts();
        handleCMSIndexDragNDropReOrderingContactLocations();
        handleCMSIndexDragNDropReOrderingSiteProductSections();
        handleCMSIndexDragNDropReOrderingGalleryFilters();
        handleCMSIndexDragNDropReOrderingGalleryFilterItems();
        handleCMSIndexDragNDropReOrderingGallery();
        enableToggleIconsLogosForImageUseOnProductDetailsPages();
        enableCaseStudyPublishing();
        enableCaseStudyPublishingFromIndex();
        handleChooseModuleSubmitButtonState();
        handlePreviewGalleryListing();
        handleGalleryAlertMsg();


        $('.ui-tooltip').tooltip();
        $('.ui-popover').popover();
    }


    function handleGalleryAlertMsg() {
        $(function () {
            $('#galleryAlertMessage').fadeOut(1000).addClass('hidden');
        });
    }

    function handleCMSIndexDragNDropReOrderingContacts() {
        var sortOrderTbody = $("#sortContactsOrder");
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/contacts/save-index-order",
                    data: {order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
    }


    function handlePreviewGalleryListing() {
        $("#previewListingFormButton").click(function (e) {
            console.log('listing_pic_id = ' + ($('#listing_pic_id:checked').val() >= 1 ? $('#listing_pic_id:checked').val() : 0));
            console.log('headline = ' + $('#headline').val());
            console.log('description = ' + $('#description').val());

            $.ajax
            ({
                url: '/admin/gallery/preview-listing',
                type: 'POST',
                data: {
                    listing_PicID: ($('#listing_pic_id:checked').val() >= 1 ? $('#listing_pic_id:checked').val() : 0),
                    headline: $('#headline').val(),
                    description: $('#description').val()
                },

                success: function (data, textStatus, jqXHR) {
                    var left = ($(window).width() / 2) - (900 / 2),
                        top = ($(window).height() / 2) - (600 / 2),
                        popup = window.open("/admin/gallery/preview-listing", "popup", "width=900, height=600, top=" + top + ", left=" + left);
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        });

        $("#galleryListingPreviewButton").click(function (e) {
            console.log('listing_pic_id = ' + $('#listing_pic_id').val());
            console.log('headline       = ' + $('#headline').val());
            console.log('description    = ' + $('#description').val());

            $.ajax
            ({
                url: '/admin/gallery/preview-listing',
                type: 'POST',
                data: {
                    listing_PicID: $('#listing_pic_id').val(),
                    headline: $('#headline').val(),
                    description: $('#description').val()
                },

                success: function (data, textStatus, jqXHR) {
                    var left = ($(window).width() / 2) - (900 / 2),
                        top = ($(window).height() / 2) - (600 / 2),
                        popup = window.open("/admin/gallery/preview-listing", "popup", "width=900, height=600, top=" + top + ", left=" + left);
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        });
    }


    function handleChooseModuleSubmitButtonState() {
        $('.createModuleSelectField').on('change', function () {
            console.log('select changed');
            var buttonID = $(this).data('button-id');
            console.log(buttonID);
            if ($(this).val() == 0) {
                console.log('select 0' + $(this).val());

                $("#" + buttonID).removeClass('disabled').addClass('disabled');
            }
            else {
                console.log('select not 0' + $(this).val());
                $("#" + buttonID).removeClass('disabled');
            }
        });
    }


    function enableCaseStudyPublishing() {
        $("#togglePublishCaseStudy").click(function (e) {
            var caseStudyID = $('#togglePublishCaseStudy').data("case-study-id");
            var indexStatusSpan = $("#indexStatusSpan");

            $.ajax
            ({
                url: '/admin/gallery/publish',
                type: 'POST',
                data: {caseStudyID: caseStudyID},

                success: function (data, textStatus, jqXHR) {
                    if (data.status == 'error') {
                        indexStatusSpan.text(data.message)
                            .css("color", "red")
                            .removeClass("hidden")
                            .fadeIn(1)
                            .fadeOut(2000);
                    }
                    else {
                        indexStatusSpan.text(data.message)
                            .css("color", "red")
                            .removeClass("hidden")
                            .fadeIn(1)
                            .fadeOut(2000);

                        if (data.is_live == 0) {
                            $('#togglePublishCaseStudy').text('Publish Entry');
                            $('#caseStudyStatus').text('This Gallery Entry is not live');
                            $('#caseStudyStatus').removeClass('alert-success, alert-danger').addClass('alert-danger');
                        }
                        else {
                            $('#togglePublishCaseStudy').text('Hide Entry');
                            $('#caseStudyStatus').text('This Gallery Entry is live');
                            $('#caseStudyStatus').removeClass('alert-success, alert-danger').addClass('alert-success');
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        });
    }


    function enableCaseStudyPublishingFromIndex() {
        $(".caseStudyLiveStatus").click(function (e) {
            var caseStudyID = $(this).data("case-study-id");
            var indexStatusSpan = $("#indexStatusSpan");

            console.log(caseStudyID);

            $.ajax
            ({
                url: '/admin/gallery/publish',
                type: 'POST',
                data: {caseStudyID: caseStudyID},

                success: function (data, textStatus, jqXHR) {
                    if (data.status == 'error') {
                        indexStatusSpan.text(data.message)
                            .css("color", "red")
                            .removeClass("hidden")
                            .fadeIn(1)
                            .fadeOut(2000);

                        $('#caseStudyLiveStatus_' + caseStudyID).attr('checked', false);
                    }
                    else {
                        indexStatusSpan.text(data.message)
                            .css("color", "blue")
                            .removeClass("hidden")
                            .fadeIn(1)
                            .fadeOut(2000);

                        if (data.is_live == 0) {
                            $('#caseStudyLiveStatus_' + caseStudyID).attr('checked', false);
                        }
                        else {
                            $('#caseStudyLiveStatus_' + caseStudyID).attr('checked', true);
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        });
    }


    function enableToggleIconsLogosForImageUseOnProductDetailsPages() {
        $("#heading_vs_image").click(function (e) {
            if ($('#heading_vs_image').is(":checked")) {
                $('#detailsForHeadingVsImage').show();
                $('#detailsForHeadingVsImageHR').show();
            }
            else {
                $('#detailsForHeadingVsImage').hide();
                $('#detailsForHeadingVsImageHR').hide();
            }
        })
    }

    function handleCMSIndexDragNDropReOrderingGallery() {
        /*
        var sortOrderTbody = $("#sortCaseStudyItemsOrder");
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/case-studies/save-index-order",
                    data: {order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
         */
    }

    function handleCMSIndexDragNDropReOrderingGalleryFilterItems() {
        var sortOrderTbody = $("#sortCaseStudyFilterItemsOrder");
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/gallery-filter-items/save-index-order",
                    data: {order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
    }

    function handleCMSIndexDragNDropReOrderingGalleryFilters() {
        var sortOrderTbody = $("#sortGalleryFiltersOrder");
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/gallery-filters/save-index-order",
                    data: {order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
    }

    function handleCMSIndexDragNDropReOrderingSiteProductSections() {
        var sortOrderTbody = $("#sortSiteProductSectionsOrder");
        var productID = sortOrderTbody.data('site-product-id');
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/site-product-sections/save-index-order",
                    data: {productID: productID, order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
    }

    function handleCMSIndexDragNDropReOrderingContactLocations() {
        var sortOrderTbody = $("#sortContactLocationsOrder");
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/contact-locations/save-index-order",
                    data: {order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
    }

    function handleCMSIndexDragNDropReOrderingContactProducts() {
        var sortOrderTbody = $("#sortContactProductsOrder");
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/contact-products/save-index-order",
                    data: {order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
    }

    function handleCMSIndexDragNDropReOrderingRateProducts() {
        var sortOrderTbody = $("#sortRateProductsOrder");
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/rate-products/save-index-order",
                    data: {order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
    }

    function handleCMSIndexDragNDropReOrderingSiteProducts() {
        var sortOrderTbody = $("#sortSiteProductsOrder");
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/site-products/save-index-order",
                    data: {order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
    }

    function handleCMSIndexDragNDropReOrderingUploadCategories() {
        var sortOrderTbody = $("#sortUploadCategoriesOrder");
        var indexStatusSpan = $("#indexStatusSpan");

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#index_row_order").val(order);

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/upload-categories/save-index-order",
                    data: {order: order},
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        indexStatusSpan.text("New Order Saved").css("color", "blue").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    },
                    failure: function (errMsg) {
                        indexStatusSpan.text("There was an error saving the order").css("color", "red").removeClass("hidden").fadeIn(1).fadeOut(2000);
                    }
                });

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable
        ({
            helper: fixHelperModified,
            containment: "parent",
            stop: updateIndex,
            cursor: 'move',
            opacity: 0.6
        }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#index_row_order").val(order);
    }

    function enableChangeSaveButtonToPublishForLiveModules() {
        $("#is_live").click(function (e) {
            if ($('#is_live').is(":checked")) {
                $('#moduleSaveButton').text("Publish");
            }
            else {
                $('#moduleSaveButton').text("Save");
            }
        })
    }


    function enableHomePagePreview()
    {
        $("#homePagePreviewButton").click(function (e)
        {
            var allSliderIDs    =   $('#slider_order').val().substr(10).split("&slider[]=");

            var formData = new Array();

            for (i = 0; i < allSliderIDs.length; i++)
            {
                console.log(allSliderIDs[i]);
                formData[formData.length] = {
                    "sliderID": allSliderIDs[i],
                    "download_folder": $('#download_folder_' + allSliderIDs[i]).val(),
                    "download_file": $('#download_file_' + allSliderIDs[i]).val(),
                    "main_text": $('#main_text_' + allSliderIDs[i]).val(),
                    "sub_text": $('#sub_text_' + allSliderIDs[i]).val(),
                    "source_text": $('#source_text_' + allSliderIDs[i]).val()
                };

            }

            $.ajax
            ({
                url         :   '/admin/page-content/home-pages/save-preview-data',
                type        :   'POST',
                data        :   {'formData':JSON.stringify(formData)},

                success     :   function(data, textStatus, jqXHR)
                                {
                                    console.log(data);

                                    window.open('/admin/page-content/home-pages/preview', '_blank');
                                },
                error       :   function (jqXHR, textStatus, errorThrown)
                                {

                                }
            });




        });
    }


    function enableDataTables()
    {
        $('#uploadsTable').DataTable();
        $('#rateProductMediaTable').DataTable();
        $('#siteProductSectionsTable').DataTable();
        $('#contactsTable').DataTable();
        $('#sortCaseStudyItemsTable').DataTable();
    }

    function enableToggleHomePageSliderRows() {
        $(".sliderRowHomePageChevron").click(function (e) {
            e.preventDefault();

            var sliderID = $(this).data('slider-id');
            var thisRow = $(this).closest('tr');

            if ($(this).hasClass('icon-chevron-down')) {
                $(this).removeClass("icon-chevron-down").addClass("icon-chevron-up");
                $('#sliderDetails' + sliderID).show();

                thisRow.children('td').each(function () {
                    $(this).addClass('darkerRow');
                });
            }
            else {
                $(this).removeClass("icon-chevron-up").addClass("icon-chevron-down");
                $('#sliderDetails' + sliderID).hide();


                thisRow.children('td').each(function () {
                    $(this).removeClass('darkerRow');
                });
            }

        });
    }


    function enableManageTextColumnDivs() {
        $("#textColumnMinusIcon").click(function (e) {
            console.log("minus");
            e.preventDefault();
            var idNum = $('#colDivNum').val();
            console.log("idNum => " + idNum);
            if (idNum >= 2) {
                $('#textBoxCol' + idNum).hide();
                $('#colDivNum').val(parseInt(parseInt(idNum) - parseInt(1)));

                console.log("- data-col-id => " + $('#colDivNum').val());
            }


        });

        $("#textColumnPlusIcon").click(function (e) {
            e.preventDefault();
            var idNum = $('#colDivNum').val();
            if (idNum >= 1 && idNum < 9) {
                $('#textBoxCol' + idNum).show();
                $('#colDivNum').val(parseInt(parseInt(idNum) + parseInt(1)));

                console.log("- data-col-id => " + $('#colDivNum').val());
            }
        });
    }


    function enableClearHeadingIconRadioList() {
        $("#clearHeadingIconList").click(function (e) {
            e.preventDefault();
            $('.headingIconList').attr('checked', false);
        });
    }


    function enableSaveModuleLiveOrderLocation1() {
        $("#saveLiveOrderLocation1Button").click(function (e) {
            e.preventDefault();
            var checkedValues = $('.location1ModuleCheckbox:checked').map(function () {
                return this.value;
            }).get();

            var locationPage = $("#location1ModulePage").val();

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/page-content/modules/save-order",
                    data: {
                        isLive: checkedValues,
                        order: $("#location1ModuleOrder").val(),
                        pageName: locationPage,
                        locationID: $("#location1ModuleLocationID").val(),
                        entityID: $("#location1ModuleEntityID").val(),
                        entityType: $("#location1ModuleEntityType").val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/page-content/" + locationPage;
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }

    function enableSortModulesOrderLocation1() {
        var sortOrderTbody = $("#sortLocation1ModulesOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {

            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#location1ModuleOrder").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });

            //$("#saveLiveOrderLocation1Button").removeClass("ui-state-disabled");
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#location1ModuleOrder").val(order);
    }

    function enableSaveModuleLiveOrderLocation2() {
        $("#saveLiveOrderLocation2Button").click(function (e) {
            e.preventDefault();
            var checkedValues = $('.location2ModuleCheckbox:checked').map(function () {
                return this.value;
            }).get();

            var locationPage = $("#location2ModulePage").val();

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/page-content/modules/save-order",
                    data: {
                        isLive: checkedValues,
                        order: $("#location2ModuleOrder").val(),
                        pageName: locationPage,
                        locationID: $("#location2ModuleLocationID").val(),
                        entityID: $("#location2ModuleEntityID").val(),
                        entityType: $("#location2ModuleEntityType").val()

                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/page-content/" + locationPage;
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }

    function enableSortModulesOrderLocation2() {
        var sortOrderTbody = $("#sortLocation2ModulesOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {

            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#location2ModuleOrder").val(order);
            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });

            //$("#saveLiveOrderLocation2Button").removeClass("ui-state-disabled");

        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#location2ModuleOrder").val(order);
    }


    function enableSaveProductLandingModuleLiveOrder() {
        $(".saveLiveOrderButton").click(function (e) {
            e.preventDefault();

            var locationArea = $(this).data('module-location-area');
            var locationID = $(this).data('module-location-id');
            var entityID = $(this).data('module-entity-id');
            var locationPage = $('#' + locationArea + 'Location' + locationID + 'Section' + entityID + 'ModulePage').val();// + '#landingSections' + entityID;
            var checkedValues = $('.' + locationArea + 'Location' + locationID + 'Section' + entityID + 'ModuleCheckbox:checked').map(function () {
                return this.value;
            }).get();

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/page-content/modules/save-order",
                    data: {
                        isLive: checkedValues,
                        order: $('#' + locationArea + 'Location' + locationID + 'Section' + entityID + 'ModuleOrder').val(),
                        pageName: locationPage,
                        locationID: $('#' + locationArea + 'Location' + locationID + 'Section' + entityID + 'ModuleLocationID').val(),
                        entityID: $('#' + locationArea + 'Location' + locationID + 'Section' + entityID + 'ModuleEntityID').val(),
                        entityType: $('#' + locationArea + 'Location' + locationID + 'Section' + entityID + 'ModuleEntityType').val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/page-content/" + locationPage;
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }

    function enableSaveProductDetailsModuleLiveOrder() {
        $(".saveDetailsLiveOrderButton").click(function (e) {
            e.preventDefault();

            var locationArea = $(this).data('module-location-area');
            var locationID = $(this).data('module-location-id');
            var parentEntityID = $(this).data('module-parent-entity-id');
            var entityID = $(this).data('module-entity-id');
            var locationPage = $('#' + locationArea + 'Location' + locationID + 'Section' + parentEntityID + 'SubSection' + entityID + 'ModulePage').val();// + '#landingSections' + entityID;
            var checkedValues = $('.' + locationArea + 'Location' + locationID + 'Section' + parentEntityID + 'SubSection' + entityID + 'ModuleCheckbox:checked').map(function () {
                return this.value;
            }).get();

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/page-content/modules/save-order",
                    data: {
                        isLive: checkedValues,
                        order: $('#' + locationArea + 'Location' + locationID + 'Section' + parentEntityID + 'SubSection' + entityID + 'ModuleOrder').val(),
                        pageName: locationPage,
                        locationID: locationID,
                        parentEntityID: parentEntityID,
                        entityID: entityID,
                        entityType: $('#' + locationArea + 'Location' + locationID + 'Section' + parentEntityID + 'SubSection' + entityID + 'ModuleEntityType').val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/page-content/" + locationPage;
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }

    function enableSortProductLandingModules() {
        var sortOrderTbody = $(".sortTableRowModuleOrder");
        var sortOrderInputText = "";
        var sortOrderInputID;

        var locationArea;
        var locationID;
        var entityID;
        var parentEntityID;
        var relationshipID;

        var fixHelperModified = function (e, tr) {
            locationArea = tr.data('module-location-area');
            locationID = tr.data('module-location-id');
            entityID = tr.data('module-entity-id');
            parentEntityID = tr.data('module-parent-entity-id') ? tr.data('module-parent-entity-id') : 0;
            relationshipID = tr.data('module-relationship-id');

            sortOrderInputText = (locationArea == "landing"
                ? locationArea + "Location" + locationID + "Section" + entityID + "ModuleOrder"
                : locationArea + "Location" + locationID + "Section" + parentEntityID + "SubSection" + entityID + "ModuleOrder");

            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var draggedRowText = (locationArea == "landing"
                ? locationArea + 'Location' + locationID + 'Section' + entityID + 'TBody'
                : locationArea + 'Location' + locationID + 'Section' + parentEntityID + 'SubSection' + entityID + 'TBody');
            var draggedRow = $('#' + draggedRowText);
            var order = "&" + draggedRow.sortable("serialize");

            sortOrderInputID = $('#' + sortOrderInputText);
            sortOrderInputID.val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });

            //$("#saveLiveOrderLocation3Button").removeClass("ui-state-disabled");
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        //var order = "&" + sortOrderTbody.sortable("serialize");
        //sortOrderInputID.val(order);
    }

    function enableSortProductDetailsModules() {
        var sortOrderTbody = $(".sortDetailsLocationModulesOrder");
        var sortOrderInputText = "";
        var sortOrderInputID;
        var fixHelperModified = function (e, tr) {
            var locationArea = tr.data('module-location-area');
            var locationID = tr.data('module-location-id');
            var entityID = tr.data('module-entity-id');
            var parentEntityID = tr.data('module-parent-entity-id') ? tr.data('module-parent-entity-id') : 0;
            var relationshipID = tr.data('module-relationship-id');

            sortOrderInputText = (locationArea == "landing"
                ? locationArea + "Location" + locationID + "Section" + entityID + "ModuleOrder"
                : locationArea + "Location" + locationID + "Section" + parentEntityID + "SubSection" + entityID + "ModuleOrder");


            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            sortOrderInputID = $('#' + sortOrderInputText);
            sortOrderInputID.val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });

            //$("#saveLiveOrderLocation3Button").removeClass("ui-state-disabled");
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        //var order = "&" + sortOrderTbody.sortable("serialize");
        //sortOrderInputID.val(order);
    }















    function enableSaveModuleLiveOrderLocation6() {
        $("#saveLiveOrderLocation6Button").click(function (e) {
            e.preventDefault();
            var checkedValues = $('.location6ModuleCheckbox:checked').map(function () {
                return this.value;
            }).get();


            var locationPage = $("#location6ModulePage").val();

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/page-content/modules/save-order",
                    data: {
                        isLive: checkedValues,
                        order: $("#location6ModuleOrder").val(),
                        pageName: locationPage,
                        locationID: $("#location6ModuleLocationID").val(),
                        entityID: $("#location6ModuleEntityID").val(),
                        entityType: $("#location6ModuleEntityType").val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/page-content/" + locationPage;
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }

    function enableSortModulesOrderLocation6() {
        var sortOrderTbody = $("#sortLocation6ModulesOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {

            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#location6ModuleOrder").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });

            //$("#saveLiveOrderLocation6Button").removeClass("ui-state-disabled");
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#location6ModuleOrder").val(order);
    }

    function enableSaveModuleLiveOrderLocation7() {
        $("#saveLiveOrderLocation7Button").click(function (e) {
            e.preventDefault();
            var checkedValues = $('.location7ModuleCheckbox:checked').map(function () {
                return this.value;
            }).get();


            var locationPage = $("#location7ModulePage").val();

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/page-content/modules/save-order",
                    data: {
                        isLive: checkedValues,
                        order: $("#location7ModuleOrder").val(),
                        pageName: locationPage,
                        locationID: $("#location7ModuleLocationID").val(),
                        entityID: $("#location7ModuleEntityID").val(),
                        entityType: $("#location7ModuleEntityType").val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/page-content/" + locationPage;
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }

    function enableSortModulesOrderLocation7() {
        var sortOrderTbody = $("#sortLocation7ModulesOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {

            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#location7ModuleOrder").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });

            //$("#saveLiveOrderLocation7Button").removeClass("ui-state-disabled");
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#location7ModuleOrder").val(order);
    }

    function enableSaveModuleLiveOrderLocation8() {
        $("#saveLiveOrderLocation8Button").click(function (e) {
            e.preventDefault();
            var checkedValues = $('.location8ModuleCheckbox:checked').map(function () {
                return this.value;
            }).get();

            var locationPage = $("#location8ModulePage").val();

            $.ajax(
                {
                    type: "POST",
                    url: "/admin/page-content/modules/save-order",
                    data: {
                        isLive: checkedValues,
                        order: $("#location8ModuleOrder").val(),
                        pageName: locationPage,
                        locationID: $("#location8ModuleLocationID").val(),
                        entityID: $("#location8ModuleEntityID").val(),
                        entityType: $("#location8ModuleEntityType").val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/page-content/" + locationPage;
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }

    function enableSortModulesOrderLocation8() {
        var sortOrderTbody = $("#sortLocation8ModulesOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {

            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#location8ModuleOrder").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });

            //$("#saveLiveOrderLocation8Button").removeClass("ui-state-disabled");
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#location8ModuleOrder").val(order);
    }

    function enableSaveModuleLiveOrderLocation9() {
        $("#saveLiveOrderLocation9Button").click(function (e) {
            e.preventDefault();
            var checkedValues = $('.location9ModuleCheckbox:checked').map(function () {
                return this.value;
            }).get();

            var locationPage = $("#location9ModulePage").val();

            $.ajax
            ({
                type: "POST",
                url: "/admin/page-content/modules/save-order",
                data: {
                    isLive: checkedValues,
                    order: $("#location9ModuleOrder").val(),
                    pageName: locationPage,
                    locationID: $("#location9ModuleLocationID").val(),
                    entityID: $("#location9ModuleEntityID").val(),
                    entityType: $("#location9ModuleEntityType").val()
                },
                contentType: "application/x-www-form-urlencoded",
                dataType: "json",
                success: function (data) {
                    var loc = window.location;
                    var toPath = "/admin/" + locationPage;
                    window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                },
                error: function (errMsg) {
                    alert(errMsg);
                }
            });
        });
    }

    function enableSortModulesOrderLocation9() {
        var sortOrderTbody = $("#sortLocation9ModulesOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#location9ModuleOrder").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });

            //$("#saveLiveOrderLocation9Button").removeClass("ui-state-disabled");
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#location9ModuleOrder").val(order);
    }




    function enableToggleModuleMgtRows() {
        $(".module-mgt-row-icon").click(function () {
            var moduleDetailsRowID = $(this).data('module-details-id');
            var moduleDetailsType = $(this).data('module-details-type');
            var moduleDetailsDiv = "#module-details-" + moduleDetailsType + "-" + moduleDetailsRowID + "";
            var moduleDetailsChevron = "#module-mgt-row-icon-" + moduleDetailsType + "-" + moduleDetailsRowID + "";

            $(moduleDetailsDiv).slideToggle(100);

            if ($(moduleDetailsDiv).height() == 1) {
                $(moduleDetailsChevron).removeClass("icon-chevron-down").addClass("icon-chevron-up");
            }
            else {
                $(moduleDetailsChevron).removeClass("icon-chevron-up").addClass("icon-chevron-down");
            }
        });
    }


    function enableAddRowsToHomePageSliderOrder() {
        $(".sliderDetailsCheckbox").click(function () {
            var siteProductID = $(this).data('slider_id');
            var uploadName = $(this).data('slider-name');
            var uploadID = $(this).val();


            var thisTableID = "#sliderOrderTable";
            var thisTbodyID = "#sliderOrder";
            var thisTheadID = "#sliderOrderHdg";
            var thisHiddenID = "#slider_order";


            if ($(this).is(':checked')) {
                $(thisTableID + ' > tbody:last').append
                (
                    '<tr id="' + uploadID + '" data-upload-name="' + uploadName + '"><td><div class="alert alert-success" style="padding:5px 25px 5px 15px;margin-bottom:5px;"><strong>' + uploadName + '</strong></div></td></tr>'
                );
            }
            else {
                var tblID = thisTbodyID.replace("#", "");
                var tbl = document.getElementById(tblID);
                var row = document.getElementById(uploadID);
                tbl.removeChild(row);
            }

            var rowCount = $(thisTableID + ' >tbody >tr').length;
            var order = "";

            $(thisTbodyID).find('tr').each(function () {
                order = order + "&" + $(this).attr('id');
            });

            $(thisHiddenID).val(order);

            if (rowCount > 0) {
                $(thisTheadID).fadeIn();
            }

            if (rowCount <= 0) {
                $(thisTheadID).fadeOut();
            }

        });
    }


    function enableSortHomePageSliderOrder() {
        var sortOrderTbody = $("#homePageChosenSliderOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {

            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#slider_order").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#slider_order").val(order);
    }


    function enableSortProductSubSectionsOrder() {
        var sortOrderTbody = $("#sortSiteProductSubSectionOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {

            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#section_order").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#section_order").val(order);
    }


    function enableSortProductPageSectionModulesOrder() {
        var sortOrderTbody = $("#sortProductPageSectionModulesOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#section_order").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#section_order").val(order);
    }


    function enableHideCaseStudySliderDetailsRow() {


        $('.sliderDetailsCheckbox').click(function () {
            var sliderRowID = $('#' + $(this).data('slider-details-id'));
            if ($(this).is(":checked")) {
                sliderRowID.show();
            }
            else {
                sliderRowID.hide();
            }

        });
    }


    function enableSortCaseStudyModulesOrder() {
        var sortOrderTbody = $("#sortCaseStudyModulesOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {

            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#case_study_modules_order").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#case_study_modules_order").val(order);
    }


    function enableAddRowsToRateOrder() {
        $(".rateSpecImageCheckBox").click(function () {
            var rateProductID = $(this).data('rate-product-id');
            var uploadName = $(this).data('rate-file-name');
            var uploadID = $(this).val();


            var thisTableID = "#sortChosenRateProductMediaOrderTable";
            var thisTbodyID = "#sortChosenRateProductMediaOrder";
            var thisTheadID = "#sortChosenRateProductMediaOrderHdg";
            var thisHiddenID = "#rate-media-order";


            if ($(this).is(':checked')) {
                $(thisTableID + ' > tbody:last').append
                (
                    '<tr id="rate_' + uploadID + '" data-upload-name="' + uploadName + '"><td><div class="alert alert-success" style="padding:5px 25px 5px 15px;margin-bottom:5px;"><strong>' + uploadName + '</strong></div></td></tr>'
                );
            }
            else {
                var tblID = thisTbodyID.replace("#", "");
                var tbl = document.getElementById(tblID);
                var row = document.getElementById("rate_" + uploadID);
                tbl.removeChild(row);
            }

            var rowCount = $(thisTableID + ' >tbody >tr').length;
            var order = "";

            $(thisTbodyID).find('tr').each(function () {
                var refinedOrder = $(this).attr('id').replace("_", "[]=");
                order = order + "&" + refinedOrder;
            });

            $(thisHiddenID).val(order);

            if (rowCount > 0) {
                $(thisTheadID).fadeIn();
            }

            if (rowCount <= 0) {
                $(thisTheadID).fadeOut();
            }

        });
    }


    function enableCollapseAllModulesOnPageLoad() {
        //$(".caseStudyModuleHeaderOptions").each(function () {
        ////    $(this).nextUntil('thead').hide(10);
        //    $(this).addClass("collapsed");
        //    $(this).find('th:first').css('width', '57px');
        //});
    }


    function enableCollapseAllRateDivsOnPageLoad() {
        $(".rateSiteProductHeaderOptions").each(function () {
            $(this).nextUntil('thead').hide(10);
        });
    }


    function enableCollapseCaseStudyModules() {
        $('.caseStudyModuleHeaderOptions').click(function () {
            $(this).nextUntil('thead').slideToggle(10);


            if ($(this).hasClass('collapsed')) {
                $(this).removeClass("collapsed");
                $(this).find('.icon-down').show();
                $(this).find('.icon-right').hide();
            }
            else {
                $(this).addClass("collapsed");
                $(this).find('.icon-right').show();
                $(this).find('.icon-down').hide();
            }


        });
    }


    function enableCollapseSiteProductRatesTables() {
        $('.rateSiteProductHeaderOptions').click(function () {
            $(this).nextUntil('thead').slideToggle(10);

            var chevron = $('#' + $(this).data('chevron-id'));
            var chosenList = $('#' + $(this).data('chosen-list-id'));

            if (chevron.hasClass("icon-chevron-down")) {
                chevron.removeClass("icon-chevron-down").addClass("icon-chevron-up");
                chosenList.nextUntil('thead').show(10);
            }
            else {
                chevron.removeClass("icon-chevron-up").addClass("icon-chevron-down");
                chosenList.nextUntil('thead').hide(10);
            }
        });
    }


    function enableSortSiteProductRatesOrder() {
        var sortOrderTbody = $("#sortChosenRateProductMediaOrder");
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + sortOrderTbody.sortable("serialize");
            $("#rate-media-order").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        sortOrderTbody.sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();

        var order = "&" + sortOrderTbody.sortable("serialize");
        $("#rate-media-order").val(order);
    }


    function enableAddRowsToInsightReportsOrder() {
        $(".insightReportImageCheckBox").click(function () {
            var reportName = $(this).data('report-name');
            var reportID = "report_" + $(this).val();

            if ($(this).is(':checked')) {
                $('#sortChosenInsightReportsOrderTable > tbody:last').append
                (
                    '<tr id="' + reportID + '" data-report-name="' + reportName + '"><td><div class="alert alert-success" style="padding:5px 25px 5px 15px;margin-bottom:5px;"><strong>' + reportName + '</strong></div></td></tr>'
                );

                var order = "&" + $("#sortChosenInsightReportsOrder").sortable("serialize");
                $("#report_order").val(order);
            }
            else {
                var tbl = document.getElementById('sortChosenInsightReportsOrder');
                var row = document.getElementById(reportID);
                tbl.removeChild(row);
            }

            var rowCount = $('#sortChosenInsightReportsOrderTable >tbody >tr').length;

            if (rowCount > 0) {
                $("#sortChosenInsightReportsOrderHdg").fadeIn();
            }

            if (rowCount <= 0) {
                $("#sortChosenInsightReportsOrderHdg").fadeOut();
            }


        });
    }


    function enableSortChosenInsightReportsOrder() {
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        var updateIndex = function (e, ui) {
            var order = "&" + $("#sortChosenInsightReportsOrder").sortable("serialize");
            $("#report_order").val(order);

            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
        };

        $("#sortChosenInsightReportsOrder").sortable(
            {
                helper: fixHelperModified,
                containment: "parent",
                stop: updateIndex,
                cursor: 'move',
                opacity: 0.6
            }).disableSelection();
    }


    function saveNewPermission() {
        $("#saveNewPermissionButton").click(function () {
            $.ajax(
                {
                    type: "POST",
                    url: "/admin/permissions",
                    data: {
                        permissionName: $("#newPermissionName").val(),
                        permissionDisplayName: $("#newPermissionDisplayName").val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/permissions";
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }


    function saveNewRole() {
        $("#saveNewRoleButton").click(function () {
            $.ajax(
                {
                    type: "POST",
                    url: "/admin/roles",
                    data: {
                        roleName: $("#newRoleName").val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/roles";
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }


    function enableSaveAssignedPermission() {
        $("#saveAssignedRoleButton").click(function () {
            $.ajax(
                {
                    type: "POST",
                    url: "/admin/save-assigned-permission",
                    data: {
                        permissionID: $("#permissionId").val(),
                        permissionRole: $("#permissionRole").val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/roles";
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }


    function enableSaveAssignedRole() {
        $("#saveAssignedUserButton").click(function () {
            $.ajax(
                {
                    type: "POST",
                    url: "/admin/save-assigned-role",
                    data: {
                        roleID: $("#roleId").val(),
                        userID: $("#roleUser").val()
                    },
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (data) {
                        var loc = window.location;
                        var toPath = "/admin/roles";
                        window.location = loc.protocol + "//" + loc.hostname + (loc.port && ":" + loc.port) + toPath;
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
        });
    }


    function enablePassPermissionDataToRoleModal() {
        $(".roleModalLink").click(function () {
            $("#permissionId").val($(this).data('permission-id'));
            $("#permissionName").html($(this).data('permission-name'));

            $('#roleModal').modal('show');
        });
    }


    function enablePassRoleDataToUserModal() {
        $(".userModalLink").click(function () {
            $("#roleId").val($(this).data('role-id'));
            $("#roleName").html($(this).data('role-name'));

            $('#userModal').modal('show');
        });
    }

    /**
     * Put .form-confirm-delete class on any form you want to verify delete
     */
    function enableDeleteConfirm() {
        if ($.msgbox) {
            $('.form-confirm-delete').on('click', function (e) {
                e.preventDefault();
                var self = $(this);
                $.msgbox("Are you sure that you want to permanently delete the selected element?", {
                    type: "confirm",
                    buttons: [
                        {type: "submit", value: "Yes"},
                        {type: "submit", value: "No"},
                        {type: "cancel", value: "Cancel"}
                    ]
                }, function (result) {
                    if (result != 'Yes') {
                        return false;
                    } else {
                        self.submit();
                    }
                });
            });
        }
    }

    function enableCirque() {
        if ($.fn.cirque) {
            $('.ui-cirque').cirque({});
        }
    }

    function enableLightbox() {
        if ($.fn.lightbox) {
            $('.ui-lightbox').lightbox();
            $('.lightbox-type').lightbox();
        }
    }

    function enableBackToTop() {
        var backToTop = $('<a>', {id: 'back-to-top', href: '#top'});
        var icon = $('<i>', {class: 'icon-chevron-up'});

        backToTop.appendTo('body');
        icon.appendTo(backToTop);

        backToTop.hide();

        $(window).scroll(function () {
            if ($(this).scrollTop() > 150) {
                backToTop.fadeIn();
            } else {
                backToTop.fadeOut();
            }
        });

        backToTop.click(function (e) {
            e.preventDefault();

            $('body, html').animate({
                scrollTop: 0
            }, 600);
        });
    }

    function enableEnhancedAccordion() {
        $('.accordion-toggle').on('click', function (e) {
            $(e.target).parent().parent().parent().addClass('open');
        });

        $('.accordion-toggle').on('click', function (e) {
            $(this).parents('.panel').siblings().removeClass('open');
        });

    }

    function getValidationRules() {
        var custom = {
            focusCleanup: false,

            wrapper: 'div',
            errorElement: 'span',

            highlight: function (element) {
                $(element).parents('.form-group').removeClass('success').addClass('error');
            },
            success: function (element) {
                $(element).parents('.form-group').removeClass('error').addClass('success');
                $(element).parents('.form-group:not(:has(.clean))').find('div:last').before('<div class="clean"></div>');
            },
            errorPlacement: function (error, element) {
                error.prependTo(element.parents('.form-group'));
            }

        };

        return custom;
    }

    function enableSelectModuleHelper() {
      var $selector = $('.select-module-type');
      $selector.after($('<div class="module-helper"></div>'));

      $selector.on('change', function() {
        var $helper = $(this).parent().find('.module-helper:first'),
          path = '/assets/admin/img/modules/', file;

        switch($(this).val()) {
          case 'ImageColumnModule': file = 'image-column.png'; break;
          case 'PdfListModule': file = 'pdf-list.png'; break;
          case 'SingleImageModule': file = 'single-image.png'; break;
          case 'SlideshowModule': file = 'slideshow.png'; break;
          case 'SplitTextModule': file = 'split-text.png'; break;
          case 'SummaryModule': file = 'summary.png'; break;
          case 'TextColumnModule': file = 'text-column.png'; break;
          case 'TextModule': file = 'text.png'; break;
          case 'VideoModule': file = 'video.png'; break;
          default: file = null; break;
        }

        $helper.html(file ? '<img src="' + path + file + '" />' : '');
      });
    }

}();
