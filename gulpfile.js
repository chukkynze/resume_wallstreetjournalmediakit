/*jshint node:true */
/* gulpfile.js - https://github.com/gulpjs/gulp */

'use strict';

var gulp = require('gulp'),
    gutil = require('gulp-util'),
    es = require('event-stream'),
    path = require('path'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    jshint = require('gulp-jshint'),
    coffee = require('gulp-coffee'),
    clean = require('gulp-clean'),
    connect = require('gulp-connect'),
    concat = require('gulp-concat'),
    browserify = require('gulp-browserify'),
    imagemin = require('gulp-imagemin'),
    inject = require('gulp-inject'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    rev = require('gulp-rev'),
    revReplace = require('gulp-rev-replace'),
    revall = require('gulp-rev-all'),
    minifyHtml = require('gulp-minify-html'),
    minifyCss = require('gulp-minify-css'),
    fileinclude = require('gulp-file-include'),
    hbsfy = require('browserify-handlebars'),
    spritesmith = require('gulp.spritesmith'),
    gulpif = require('gulp-if'),
    handlebars = require('gulp-handlebars'),
    defineModule = require('gulp-define-module'),
    livereload = require('gulp-livereload');

/* change to true for production options like uglify */
var production = false;

gulp.task('sprite', function () {
    var spriteData = gulp.src('./src/assets/pngs/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '/assets/frontend/img/sprite.png',
        cssName: '_sprite.scss',
        padding: 4,
        algorithm: 'left-right'
    }));
    spriteData.img.pipe(gulp.dest('./src/assets/img/'))
    spriteData.css.pipe(gulp.dest('./src/styles/sprites/'))
});


// sass compiler task
gulp.task('sass', function () {
    return gulp.src('./src/styles/main.scss')
        .pipe(plumber())
        .pipe(sass({
            style: 'compressed'
        }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest('./public/assets/frontend/styles'))
        .pipe(livereload());
});


// sass compiler task
gulp.task('admin-sass', function () {
    return gulp.src('./src/styles/admin-frontend.scss')
        .pipe(plumber())
        .pipe(sass({
            style: 'compressed',
            sourcemap: false
        }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest('./public/assets/admin/css/'))
        .pipe(livereload());
});


// Minify images
gulp.task('imagemin', function () {
    return gulp.src('./src/assets/img/**/*.{ico,jpeg,jpg,gif,bmp,png,webp,svg}')
        .pipe(plumber())
        .pipe(imagemin())
        .pipe(gulp.dest('./public/assets/frontend/img'))
});

gulp.task('fonts', function () {
    return gulp.src('./src/assets/fonts/**/*')
        .pipe(plumber())
        .pipe(gulp.dest('./public/assets/frontend/fonts'))
});

// CoffeeScript compiler task
gulp.task('coffee', function () {
    return gulp.src(['./src/**/*.coffee'])
        .pipe(plumber())
        .pipe(coffee({bare: true})).on('error', gutil.log)
        .pipe(gulp.dest('./src/assets/js/'))
});

gulp.task('templates', function(){
  gulp.src(['./src/**/*.hbs'])
    .pipe(handlebars())
    .pipe(defineModule('node'))
    .pipe(gulp.dest('./src/assets/js/'));
});

// Script task
gulp.task('src', ['coffee'], function () {
    return gulp.src('./src/assets/js/coffee/app.js')
        .pipe(plumber())
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(browserify({
            insertGlobals: true,
            transform: [hbsfy],
            extensions: ['.hbs', '.handlebars'],
            shim: {
                jQuery: {
                    path: 'src/vendor/jquery-1.11.1.min.js',
                    exports: '$'
                },
                validate: {
                    path: 'src/vendor/jquery.validate.min.js',
                    exports: 'validate'
                },
                chosen: {
                    path: 'src/vendor/chosen.jquery.min.js',
                    exports: 'chosen'
                },
                TweenLite: {
                    path: 'src/vendor/TweenLite.js',
                    exports: 'TweenLite'
                },
                CSSPlugin: {
                    path: 'src/vendor/greensock_deps/CSSPlugin.js',
                    exports: 'CSSPlugin'
                },
                placeholder: {
                    path: 'src/vendor/placeholder-polyfill.js',
                    exports: 'placeholder'
                },
                cycle: {
                    path: 'src/vendor/cycle.min.js',
                    exports: 'cycle'
                },
                lodash: {
                    path: 'src/vendor/lodash.js',
                    exports: '_'
                }
            }
        }))
        .pipe(rename(function (path) {
            path.basename = 'bundle';
        }))
        .pipe(gulp.dest('./public/assets/frontend/js'))
        .pipe(uglify())
        .pipe(rename(function (path) {
            path.extname = '.min.js';
        }))
        .pipe(gulp.dest('./public/assets/frontend/js'))
});


gulp.task('watch', function () {
    gulp.watch(['./src/styles/**/*.scss', './src/coffee/modules/**/*.scss', './src/coffee/components/**/*.scss'], ['sass']);
    gulp.watch(['./src/styles/admin-frontend.scss'], ['admin-sass']);
    gulp.watch(['./src' + '/**/*.coffee'], ['src']);
    gulp.watch(['./src/**/*.hbs'], ['templates']);

});

gulp.task('default', ['sass', 'templates', 'src', 'imagemin', 'fonts', 'watch']);


gulp.task('rev', ['html-build', 'sass', 'src'], function () {
    gulp.src('build/*.html')
        .pipe(plumber())
        .pipe(rev())
        .pipe(gulp.dest('build'))
});

gulp.task('clean-build', function () {
    return gulp.src('public/assets/frontend/**/*', {read: false})
        .pipe(plumber())
        .pipe(clean());
});

gulp.task('build', ['clean-build', 'sass', 'src', 'imagemin', 'fonts'], function () {
});




