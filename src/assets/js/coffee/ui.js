'use strict';
var $, analytics, cssoverrides, header, modules, pdfmodal, ui, viewportresize;

$ = require('jQuery');

pdfmodal = require('./components/pdf-modal/pdf-modal');

viewportresize = require('./components/viewport-resize/viewport-resize');

analytics = require('./components/analytics/analytics');

modules = require('./modules/module-shared/modules-init');

header = require('./modules/header/header');

cssoverrides = require('./components/css-overrides/css-overrides');

ui = {
  initEvents: function() {},
  init: function() {
    var module, section;
    this.initEvents();
    pdfmodal.init();
    viewportresize.init();
    analytics.init();
    modules.init();
    section = $('body').data('path');
    header.init();
    cssoverrides.init();
    if (section) {
      switch (section) {
        case 'contact':
          module = require('./modules/contact/contact');
          break;
        case 'index':
          module = require('./modules/index/index');
          break;
        case 'rates':
          module = require('./modules/rates-specs/rates');
          break;
        case 'gallery':
          module = require('./modules/case-studies/casestudies');
      }
    }
    if ((module != null ? module.init : void 0) != null) {
      return module.init();
    }
  }
};

module.exports = ui;
