var $, ui;

$ = require('jQuery');

ui = require('./ui');

$(function() {
  'use strict';
  return ui.init();
});
