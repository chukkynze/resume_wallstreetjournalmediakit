'use strict';
var $, CSSPlugin, Tweenlite, pdfmodal, validate;

$ = require('jQuery');

Tweenlite = require('TweenLite');

CSSPlugin = require('CSSPlugin');

validate = require('validate');

pdfmodal = {
  $modalElements: $('#js-modal-overlay'),
  $firstInput: $('#js-modal-overlay').find('.input-text').first(),
  $pdfForm: $('#js-pdf-request-form'),
  $formSubmit: $('#js-modal-overlay').find('#js-form-submit'),
  modalVisible: false,
  initEvents: function() {
    $('a.requires-form-input').on('click', (function(_this) {
      return function(e) {
        e.preventDefault();
        return _this.showModal();
      };
    })(this));
    $(document).on('keyup', (function(_this) {
      return function(e) {
        if (e.keyCode === 27 && _this.modalVisible === true) {
          return _this.hideModal();
        }
      };
    })(this));
    return this.$pdfForm.on('keyup blur', (function(_this) {
      return function() {
        if (_this.$pdfForm.valid()) {
          return _this.$formSubmit.removeAttr('disabled');
        } else {
          return _this.$formSubmit.attr('disabled', "disabled");
        }
      };
    })(this));
  },
  initValidation: function() {
    return this.$pdfForm.validate({
      onkeyup: false,
      submitHandler: function() {
        return console.log('form submitted via validation suite');
      },
      errorPlacement: function(error, element) {
        return true;
      }
    });
  },
  showModal: function() {
    if (!this.modalVisible) {
      TweenLite.to(this.$modalElements, 0.1, {
        opacity: 1,
        ease: Cubic.easeInOut
      });
      this.$modalElements.addClass('is-open');
      this.$firstInput.focus();
      return this.modalVisible = true;
    }
  },
  hideModal: function() {
    TweenLite.to(this.$modalElements, 0.1, {
      opacity: 0,
      ease: Cubic.easeInOut,
      onCompleteScope: this,
      onComplete: function() {
        return this.$modalElements.removeClass('is-open');
      }
    });
    return this.modalVisible = false;
  },
  init: function() {
    this.initEvents();
    return this.initValidation();
  }
};

module.exports = pdfmodal;
