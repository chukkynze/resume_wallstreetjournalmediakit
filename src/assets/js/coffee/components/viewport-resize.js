'use strict';
var $, CSSPlugin, Tweenlite, viewportResize, _;

$ = require('jQuery');

_ = require('lodash');

Tweenlite = require('TweenLite');

CSSPlugin = require('CSSPlugin');

viewportResize = {
  heroBlock: $('.js-viewport-scale'),
  viewportHeight: null,
  initialLoad: false,
  initEvents: function() {
    return $(window).resize(_.debounce((function(_this) {
      return function() {
        _this.scaleToViewport();
      };
    })(this), 100));
  },
  scaleToViewport: function() {
    this.viewportHeight = $(window).height();
    if (this.initialLoad === false) {
      this.heroBlock.css('height', this.viewportHeight);
      return this.initialLoad = true;
    } else {
      return TweenLite.to(this.heroBlock, 0.2, {
        ease: Cubic.easeOut,
        height: this.viewportHeight
      });
    }
  },
  init: function() {
    this.initEvents();
    this.scaleToViewport();
    return console.log('viewport resize checking in');
  }
};

module.exports = viewportResize;
