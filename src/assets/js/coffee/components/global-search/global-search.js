'use strict';
var $, globalSearch;

$ = require('jQuery');

globalSearch = {
  $searchTrigger: $('#js-search-trigger'),
  $searchSubmit: $('#js-submit-button'),
  $searchElement: $('#js-search-block'),
  $searchInput: $('#js-search-block').find('input[type="text"]'),
  searchURL: "/search?search=",
  initEvents: function() {
    this.$searchTrigger.on('click', (function(_this) {
      return function(e) {
        if (_this.$searchTrigger.hasClass('active')) {
          return _this.closeSearch();
        } else {
          return _this.showSearch();
        }
      };
    })(this));
    $(document).click((function(_this) {
      return function(e) {
        if ($(e.target).closest(".global-header-outer").length === 0) {
          return _this.closeSearch();
        }
      };
    })(this));
    $("#js-global-search").keydown((function(_this) {
      return function(e) {
        if (e.keyCode === 13) {
          return _this.submitSearch();
        }
      };
    })(this));
    this.$searchSubmit.on('click', (function(_this) {
      return function() {
        return _this.submitSearch();
      };
    })(this));
  },
  submitSearch: function() {
    var searchPath;
    searchPath = this.searchURL + $("#js-global-search").val();
    return window.location.href = searchPath;
  },
  clearSearch: function() {
    return this.$searchInput.val('');
  },
  closeSearch: function() {
    this.$searchTrigger.removeClass('active');
    return this.$searchElement.addClass('is-collapsed');
  },
  showSearch: function() {
    this.clearSearch();
    this.$searchTrigger.addClass('active');
    this.$searchElement.removeClass('is-collapsed');
    return this.$searchInput.focus();
  },
  init: function() {
    this.initEvents();
    return this.clearSearch();
  }
};

module.exports = globalSearch;
