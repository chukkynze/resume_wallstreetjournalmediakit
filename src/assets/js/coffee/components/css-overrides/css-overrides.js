'use strict';
var $, cssoverrides;

$ = require('jQuery');

cssoverrides = {
  setModuleWidth: function() {
    var firstModuleHeight, rightColOffset;
    if ($('.product-listing').length) {
      $('.module').each(function(i) {
        return $(this).addClass('narrow-module');
      });
      return;
    }
    if ($('.col-right').length) {
      rightColOffset = $('.col-right').outerHeight(true);
      firstModuleHeight = null;
      return $('.module').each(function(i) {
        if (i === 0) {
          $(this).addClass('narrow-module');
          firstModuleHeight = $(this).outerHeight(true);
        }
        if (i === 1) {
          if (firstModuleHeight < rightColOffset) {
            return $(this).addClass('narrow-module');
          }
        }
      });
    }
  },
  init: function() {
    return this.setModuleWidth();
  }
};

module.exports = cssoverrides;
