'use strict';
var $, globalSearch;

$ = require('jQuery');

globalSearch = {
  $searchTrigger: $('#js-search-trigger'),
  $searchClear: $('#js-clear-search'),
  $searchElement: $('#js-search-block'),
  $searchInput: $('#js-search-block').find('input[type="text"]'),
  searchURL: "/search?search=",
  initEvents: function() {
    this.$searchTrigger.on('click', (function(_this) {
      return function(e) {
        if (_this.$searchTrigger.hasClass('active')) {
          return _this.closeSearch();
        } else {
          return _this.showSearch();
        }
      };
    })(this));
    this.$searchClear.on('click', (function(_this) {
      return function(e) {
        _this.$searchInput.val('');
        return _this.$searchElement.addClass('is-empty');
      };
    })(this));
    this.$searchInput.on('keyup', (function(_this) {
      return function() {
        if (_this.$searchInput.val()) {
          return _this.$searchElement.removeClass('is-empty');
        } else {
          return _this.$searchElement.addClass('is-empty');
        }
      };
    })(this));
    $(document).click((function(_this) {
      return function(e) {
        if ($(e.target).closest(".global-header-outer").length === 0) {
          return _this.closeSearch();
        }
      };
    })(this));
    $("#js-global-search").keydown((function(_this) {
      return function(e) {
        var searchPath;
        if (e.keyCode === 13) {
          searchPath = _this.searchURL + $("#js-global-search").val();
          return window.location.href = searchPath;
        }
      };
    })(this));
  },
  emptySearch: function() {
    return $("#js-global-search").val('');
  },
  closeSearch: function() {
    this.$searchTrigger.removeClass('active');
    return this.$searchElement.addClass('is-collapsed');
  },
  showSearch: function() {
    this.$searchTrigger.addClass('active');
    return this.$searchElement.removeClass('is-collapsed');
  },
  init: function() {
    this.initEvents();
    return this.emptySearch();
  }
};

module.exports = globalSearch;
