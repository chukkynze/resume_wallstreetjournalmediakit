'use strict';
var $, CSSPlugin, Tweenlite, pdfmodal, validate;

$ = require('jQuery');

Tweenlite = require('TweenLite');

CSSPlugin = require('CSSPlugin');

validate = require('validate');

pdfmodal = {
  $modalElements: $('#js-modal-overlay'),
  $firstInput: $('#js-modal-overlay').find('.input-text').first(),
  $pdfForm: $('#js-pdf-request-form'),
  $inputs: $('.input-text', this.$pdfForm),
  $formSubmit: $('#js-modal-overlay').find('#js-form-submit'),
  $modalClose: $('#js-modal-close-button'),
  modalVisible: false,
  validator: null,
  initEvents: function() {
    $('body').on('click', 'a.requires-form-input', (function(_this) {
      return function(e) {
        e.preventDefault();
        _this.$pdfForm.data('download', $(e.target).data('upload-id'));
        return _this.showModal();
      };
    })(this));
    $(document).on('keyup', (function(_this) {
      return function(e) {
        if (e.keyCode === 27 && _this.modalVisible === true) {
          return _this.hideModal();
        }
      };
    })(this));
    this.$pdfForm.on('keyup blur', (function(_this) {
      return function() {
        if (_this.$pdfForm.valid()) {
          return _this.$formSubmit.removeAttr('disabled');
        } else {
          return _this.$formSubmit.attr('disabled', "disabled");
        }
      };
    })(this));
    return this.$modalClose.on('click', (function(_this) {
      return function(e) {
        return _this.hideModal();
      };
    })(this));
  },
  initValidation: function() {
    return this.validator = this.$pdfForm.validate({
      onkeyup: false,
      submitHandler: function(form) {
        var $form;
        $form = $(form);
        return $.post('download/' + $form.data('download'), $form.serialize(), function(response) {
          if (response.success) {
            return window.open(response.url, '_self');
          }
        });
      },
      errorPlacement: function(error, element) {
        return true;
      },
      highlight: function(element) {
        return $(element).parent('.form-row').addClass('field-error');
      },
      unhighlight: function(element) {
        return $(element).parent('.form-row').removeClass('field-error');
      }
    });
  },
  showModal: function() {
    if (!this.modalVisible) {
      TweenLite.to(this.$modalElements, 0.1, {
        opacity: 1,
        ease: Cubic.easeInOut
      });
      this.$modalElements.addClass('is-open');
      this.$firstInput.focus();
      this.$inputs.val('');
      this.validator.resetForm();
      return this.modalVisible = true;
    }
  },
  hideModal: function() {
    TweenLite.to(this.$modalElements, 0.1, {
      opacity: 0,
      ease: Cubic.easeInOut,
      onCompleteScope: this,
      onComplete: function() {
        return this.$modalElements.removeClass('is-open');
      }
    });
    return this.modalVisible = false;
  },
  init: function() {
    this.initEvents();
    return this.initValidation();
  }
};

module.exports = pdfmodal;
