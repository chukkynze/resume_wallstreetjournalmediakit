'use strict';
var $, analytics;

$ = require('jQuery');

analytics = {
  init: function() {
    $('body').on('click', 'a', function() {
      var $link, href;
      $link = $(this);
      href = $link.attr('href');
      if (!href) {
        return;
      }
      if (/^mailto:/.test(href)) {
        return ga('send', 'event', 'Contact', 'Send Email', href.replace('mailto:', ''));
      } else if (/\.pdf$/.test(href)) {
        return ga('send', 'event', 'Download', 'Download', href);
      } else if (this.host !== window.location.host) {
        return ga('send', 'event', 'External Link', 'Click', href);
      }
    });
    $('body').on('click', 'button', function() {
      var $button;
      $button = $(this);
      if ($button.attr('id') === 'js-submit-study-filters') {
        return ga('send', 'event', 'Case Study Filter', 'Filter', null);
      } else if ($button.attr('id') === 'js-reset-study-filters') {
        return ga('send', 'event', 'Case Study Filter', 'Reset', null);
      }
    });
    $('#js-contact-product-select').on('change', function() {
      return ga('send', 'event', 'Contact', 'Product', $('option:selected', this).text());
    });
    return $('#js-contact-location-select').on('change', function() {
      return ga('send', 'event', 'Contact', 'Location', $('option:selected', this).text());
    });
  }
};

module.exports = analytics;
