'use strict';
var $, chosen, customSelect;

$ = require('jQuery');

chosen = require('chosen');

customSelect = {
  initSelects: function() {
    return $('select').chosen({
      disable_search: true,
      display_disabled_options: false
    });
  },
  init: function() {
    console.log('select interface checking in');
    return this.initSelects();
  }
};

module.exports = customSelect;
