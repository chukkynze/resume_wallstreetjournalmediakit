'use strict';
var $, cycle, slideshow;

$ = require('jQuery');

cycle = require('cycle');

slideshow = {
  $slideContainer: $('.js-slideshow'),
  initSlideshow: function() {
    return this.$slideContainer.each(function() {
      var $nextButton, $prevButton, $this;
      $this = $(this);
      $prevButton = $this.siblings('.prev');
      $nextButton = $this.siblings('.next');
      return $this.cycle({
        speed: 300,
        timeOut: 0,
        prev: $prevButton,
        next: $nextButton,
        slides: '> .slide',
        swipe: true,
        log: false,
        fx: 'fade'
      });
    });
  },
  init: function() {
    return this.initSlideshow();
  }
};

module.exports = slideshow;
