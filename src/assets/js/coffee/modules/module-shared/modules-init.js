'use strict';
var $, modules;

$ = require('jQuery');

modules = {
  init: function() {
    return $('.module').each(function() {
      var module, moduleName;
      if ($(this).attr("data-js-module")) {
        moduleName = $(this).attr("data-js-module");
        if (moduleName) {
          switch (moduleName) {
            case 'slideshow':
              module = require('../slideshow/slideshow-module');
          }
        }
        if ((module != null ? module.init : void 0) != null) {
          return module.init();
        }
      }
    });
  }
};

module.exports = modules;
