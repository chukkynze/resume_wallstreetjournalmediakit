'use strict';
var $, CSSPlugin, Tweenlite, casestudies, chosen, cycle;

$ = require('jQuery');

chosen = require('chosen');

Tweenlite = require('TweenLite');

CSSPlugin = require('CSSPlugin');

cycle = require('cycle');

casestudies = {
  view: '.filter-columns',
  $detailSlides: $('#js-study-slideshow'),
  filterForm: $('#js-studies-filters'),
  $allSelects: $('.study-product-select', this.view),
  $allStudies: $('.study-summary', '#js-filter-results'),
  $selectedFilters: $('.selected-filter', '#selected-filters'),
  $resultsContainer: $('#js-filter-results'),
  $errorContainer: $('#js-filter-error-message'),
  $customizedSelects: null,
  $filterOptions: null,
  $filterOptionCheck: null,
  filteredItems: [],
  bindEvents: function() {
    var _self;
    _self = this;
    $('#js-submit-study-filters').on('click', (function(_this) {
      return function() {
        return TweenLite.to(_this.$resultsContainer, 0.2, {
          opacity: 0,
          ease: Cubic.easeInOut,
          onComplete: _this.submitFilters,
          onCompleteScope: _this
        });
      };
    })(this));
    $('#js-reset-study-filters').on('click', (function(_this) {
      return function() {
        return TweenLite.to(_this.$resultsContainer, 0.2, {
          opacity: 0,
          ease: Cubic.easeInOut,
          onComplete: _this.resetSelects,
          onCompleteScope: _this
        });
      };
    })(this));
    $('#selected-filters').on('click', '.selected-filter', (function(_this) {
      return function(e) {
        e.preventDefault();
        _this.activateOption($('.filter-columns .option[data-val="' + $(e.target).data('filter-id') + '"]'));
        return TweenLite.to(_this.$resultsContainer, 0.2, {
          opacity: 0,
          ease: Cubic.easeInOut,
          onComplete: _this.submitFilters,
          onCompleteScope: _this
        });
      };
    })(this));
    $(document).on('click', (function(_this) {
      return function(e) {
        if (!$(e.target).closest(_this.$customizedSelects).length) {
          if (_this.$customizedSelects.hasClass('is-expanded')) {
            return _this.$customizedSelects.removeClass('is-expanded');
          }
        }
      };
    })(this));
    return $('.filter-columns').on('click', '.option', (function(_this) {
      return function(e) {
        var $option;
        if ($(e.target).hasClass('.option')) {
          $option = $(e.target);
        } else {
          $option = $(e.target).closest('.option');
        }
        return _this.activateOption($option);
      };
    })(this));
  },
  initSelects: function() {
    var $wrapper, _self;
    $wrapper = '<div class="custom-multi-select"></div>';
    _self = this;
    $('select', this.view).each(function() {
      var $options, $select, $trigger;
      $(this).wrap($wrapper);
      $(this).before('<div class="select-trigger"></div><ul class="select-options"></ul>');
      $trigger = $(this).siblings('.select-trigger');
      $options = $(this).siblings('.select-options');
      $select = $(this);
      $select.hide();
      $trigger.html($select.find('option:eq(0)').html());
      $select.find('option:eq(0)').remove();
      $select.find('option').each(function(option) {
        var choice;
        choice = '<li class="option" data-val="' + $(this).val() + '"><input type="checkbox" class="option-checkbox" />' + $(this).text() + '</li>';
        return $options.append(choice);
      });
      return $trigger.on('click', function(e) {
        var $selectWrapper;
        $selectWrapper = $(e.target).closest('.custom-multi-select');
        return _self.showDropdown($selectWrapper);
      });
    });
    this.$customizedSelects = $('.custom-multi-select');
    this.$filterOptions = $('.option', $('.filter-columns'));
    return this.$filterCheck = $('.option-checkbox', $('.filter-columns'));
  },
  activateOption: function($option) {
    var $optionCheck, filterTag;
    $optionCheck = $option.find('.option-checkbox');
    filterTag = $option.data('val');
    if ($option.hasClass('is-selected')) {
      $option.removeClass('is-selected');
      $optionCheck.prop('checked', false);
      return this.removeFilter(filterTag);
    } else {
      $option.addClass('is-selected');
      $optionCheck.prop('checked', true);
      return this.addFilter(filterTag);
    }
  },
  resetOddElements: function() {
    return this.$allStudies.removeClass('odd');
  },
  resetSelects: function() {
    this.hideFilterError();
    this.$customizedSelects.find('option:selected').removeAttr('selected');
    this.$filterOptions.removeClass('is-selected');
    this.$filterCheck.prop('checked', false);
    this.filteredItems = [];
    this.resetOddElements();
    this.$allStudies.not(':odd').addClass('odd');
    this.$allStudies.removeClass('is-invisible');
    this.$selectedFilters.parents('#selected-filters').addClass('is-invisible');
    return this.revealResults();
  },
  addFilter: function(filter) {
    return this.filteredItems.push(filter);
  },
  removeFilter: function(filter) {
    var x;
    x = this.filteredItems.indexOf(filter);
    if (x !== -1) {
      return this.filteredItems.splice(x, 1);
    }
  },
  submitFilters: function() {
    var i, item, newSelector, _i, _len, _ref;
    this.$selectedFilters.parents('#selected-filters').removeClass('is-invisible');
    this.$allStudies.addClass('is-invisible');
    this.$selectedFilters.addClass('is-invisible');
    this.resetOddElements();
    newSelector = "";
    _ref = this.filteredItems;
    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
      item = _ref[i];
      if (i !== 0) {
        newSelector += ", ";
      }
      newSelector += "." + item;
      this.$selectedFilters.filter('[data-filter-id="' + item + '"]').removeClass('is-invisible');
    }
    if (!this.filteredItems.length) {
      this.resetSelects();
    } else if ($(newSelector).length < 1 || $(newSelector) === void 0) {
      this.displayFilterError();
    } else {
      this.hideFilterError();
      $(newSelector).not(':odd').addClass('odd');
      $(newSelector).removeClass('is-invisible');
    }
    return this.revealResults();
  },
  revealResults: function() {
    return TweenLite.to(this.$resultsContainer, 0.1, {
      opacity: 1,
      ease: Cubic.easeInOut,
      delay: 1
    });
  },
  showDropdown: function(clickedSelect) {
    $('.custom-multi-select', '.filter-columns').removeClass('is-expanded');
    if (clickedSelect.hasClass('is-expanded')) {
      return clickedSelect.removeClass('is-expanded');
    } else {
      return clickedSelect.addClass('is-expanded');
    }
  },
  displayFilterError: function() {
    return TweenLite.to(this.$errorContainer, 0.1, {
      opacity: 1,
      ease: Cubic.easeInOut
    });
  },
  hideFilterError: function() {
    return TweenLite.to(this.$errorContainer, 0.1, {
      opacity: 0,
      ease: Cubic.easeInOut
    });
  },
  init: function() {
    this.initSelects();
    return this.bindEvents();
  }
};

module.exports = casestudies;
