'use strict';
var $, globalsearch, header, plaecholder;

$ = require('jQuery');

plaecholder = require('placeholder');

globalsearch = require('../../components/global-search/global-search');

header = {
  initTabletFixes: function() {
    $('.primary-parent-link > a').on('touchend', function(e) {
      var $clickTarget, $clickTargetParent;
      e.preventDefault();
      $clickTarget = $(e.currentTarget);
      $clickTargetParent = $clickTarget.closest('.primary-parent-link');
      if ($clickTargetParent.hasClass('is-open')) {
        $clickTargetParent.removeClass('is-open');
      } else {
        $clickTargetParent.addClass('is-open');
      }
      $clickTarget.blur();
      return $clickTargetParent.blur();
    });
    return $('body').bind('touchmove', function() {
      return $('.primary-parent-link.is-open', '.primary-menu').removeClass('is-open');
    });
  },
  initPlaceholders: function() {
    return $('input, textarea').placeholder();
  },
  init: function() {
    this.initPlaceholders();
    this.initTabletFixes();
    return globalsearch.init();
  }
};

module.exports = header;
