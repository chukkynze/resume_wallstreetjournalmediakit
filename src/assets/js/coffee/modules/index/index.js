'use strict';
var $, cycle, index;

$ = require('jQuery');

cycle = require('cycle');

index = {
  initEvents: function() {
    return this.slideShow.on('cycle-before', function(event, opts) {
      return opts.timeout = 4000;
    });
  },
  initSlides: function() {
    return this.slideShow.cycle({
      speed: 300,
      timeout: 2000,
      slides: '> .slide',
      pauseOnHover: true,
      swipe: true,
      log: false,
      fx: 'fade'
    });
  },
  init: function() {
    this.slideShow = $('#js-landing-marquee');
    this.initEvents();
    return this.initSlides();
  }
};

module.exports = index;
