'use strict';
var $, CSSPlugin, Tweenlite, classifiedTemplate, rates, resultTemplate, selectInterface;

$ = require('jQuery');

selectInterface = require('../../components/select-interface');

resultTemplate = require('./rates-result.js');

classifiedTemplate = require('./rates-result-classified.js');

Tweenlite = require('TweenLite');

CSSPlugin = require('CSSPlugin');

rates = {
  $productSelect: $('#js-rates-product-select'),
  $resultsContainer: $('#js-rates-results'),
  $resultsColumn: $('#js-option-result-column'),
  ratesAPI: '/admin/rates_specs/get-rate-specs/',
  currentLocationData: null,
  currentProduct: null,
  currentLocation: null,
  ajaxCall: null,
  requestURL: null,
  isProcessing: false,
  noResults: false,
  getResult: function(url, selection, callback) {
    var requestURL;
    requestURL = url + selection;
    return $.ajax({
      url: requestURL,
      type: "GET",
      dataType: "json"
    }).done((function(_this) {
      return function(data) {
        return callback(data);
      };
    })(this)).fail((function(_this) {
      return function(data) {
        _this.$resultsColumn.html('<p>Something went wrong. Please try your selection again.</p>');
        return _this.showResults();
      };
    })(this));
  },
  disableSelect: function() {
    this.killResults();
    this.$productSelect.find('option:gt(0)').remove();
    return this.$productSelect.attr('disabled', true).trigger("chosen:updated");
  },
  disableWhileLoading: function() {
    var $selectContainer;
    this.$productSelect.attr('disabled', true).trigger("chosen:updated");
    $selectContainer = this.$productSelect.siblings('.chosen-container');
    return $selectContainer.addClass('is-loading');
  },
  enableAfterLoading: function() {
    var $selectContainer;
    $selectContainer = this.$productSelect.siblings('.chosen-container');
    $selectContainer.removeClass('is-loading');
    this.$productSelect.removeAttr('disabled').trigger("chosen:updated");
    $selectContainer = this.$productSelect.siblings('.chosen-container');
    return $selectContainer.removeClass('is-loading');
  },
  populateResults: function(ratesResult) {
    var compiledLinkResults, compiledResults;
    compiledResults = null;
    compiledLinkResults = null;
    if (ratesResult.classified_text || ratesResult.submit_ad_link) {
      compiledLinkResults = classifiedTemplate(ratesResult);
    } else {
      compiledLinkResults = null;
    }
    if (ratesResult.data[0].file.length) {
      compiledResults = resultTemplate(ratesResult.data);
    }
    if (compiledResults) {
      this.$resultsColumn.html(compiledResults);
    }
    if (compiledLinkResults) {
      this.$resultsColumn.append(compiledLinkResults);
    }
    return this.showResults();
  },
  showResults: function() {
    var resultHeight;
    resultHeight = this.$resultsContainer.find(this.$resultsColumn).height();
    this.$resultsContainer.height(resultHeight);
    return this.enableAfterLoading();
  },
  emptyResults: function() {
    return this.$resultsColumn.empty();
  },
  killResults: function() {
    this.$resultsContainer.height(0);
    return this.emptyResults();
  },
  initEvents: function() {
    return this.$productSelect.on('change', (function(_this) {
      return function(e) {
        var productLocations;
        _this.currentProduct = _this.$productSelect.val();
        _this.killResults();
        _this.disableWhileLoading();
        if (_this.currentProduct === "0") {
          _this.emptyResults();
          _this.enableAfterLoading();
          return;
        }
        return productLocations = _this.getResult(_this.ratesAPI, _this.currentProduct, $.proxy(_this.populateResults, _this));
      };
    })(this));
  },
  init: function() {
    selectInterface.init();
    return this.initEvents();
  }
};

module.exports = rates;
