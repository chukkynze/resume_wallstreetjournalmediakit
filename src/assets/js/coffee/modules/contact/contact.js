'use strict';
var $, CSSPlugin, Tweenlite, chosen, contact, resultTemplate;

$ = require('jQuery');

resultTemplate = require('./contact-result.js');

Tweenlite = require('TweenLite');

CSSPlugin = require('CSSPlugin');

chosen = require('chosen');

contact = {
  $productSelect: $('#js-contact-product-select'),
  $locationSelect: $('#js-contact-location-select'),
  $resultsContainer: $('#js-contact-results'),
  $resultsColumn: $('#js-option-result-column'),
  locationAPI: '/admin/contacts/get-locations/',
  contactsAPI: '/admin/contacts/get-all/',
  currentLocationData: null,
  currentProduct: null,
  currentLocation: null,
  ajaxCall: null,
  requestURL: null,
  isProcessing: false,
  noResults: false,
  getResult: function(url, selection, callback) {
    var ajaxRequest, requestURL;
    requestURL = url + selection;
    ajaxRequest = $.ajax({
      url: requestURL,
      type: "GET",
      dataType: "json"
    });
    ajaxRequest.done((function(_this) {
      return function(data) {
        return callback(data);
      };
    })(this));
    return ajaxRequest.fail((function(_this) {
      return function(data) {
        _this.$resultsColumn.html('<p>Something went wrong. Please try your selection again.</p>');
        return _this.showError();
      };
    })(this));
  },
  disableSelect: function() {
    this.killResults();
    this.$locationSelect.find('option:gt(0)').remove();
    return this.$locationSelect.attr('disabled', true).trigger("chosen:updated");
  },
  disableWhileLoading: function() {
    var $selectContainer;
    this.$locationSelect.attr('disabled', true).trigger("chosen:updated");
    $selectContainer = this.$locationSelect.siblings('.chosen-container');
    return $selectContainer.addClass('is-loading');
  },
  enableAfterLoading: function() {
    var $selectContainer;
    if (this.noLocationResults) {
      return;
    }
    this.$locationSelect.removeAttr('disabled').trigger("chosen:updated");
    $selectContainer = this.$locationSelect.siblings('.chosen-container');
    return $selectContainer.removeClass('is-loading');
  },
  populateLocSelect: function(locations) {
    var apiLocation, key, locationContacts, _results;
    if (locations[Object.keys(locations)[0]] === 'None') {
      this.disableSelect();
      this.noLocationResults = true;
      _results = [];
      for (key in locations) {
        apiLocation = key.replace('-', '/');
        _results.push(locationContacts = this.getResult(this.contactsAPI, apiLocation, $.proxy(this.populateResults, this)));
      }
      return _results;
    } else {
      this.$locationSelect.find('option:gt(0)').remove();
      this.$locationSelect.removeAttr('disabled');
      $.each(locations, (function(_this) {
        return function(key, value) {
          var locationOption;
          locationOption = '<option value="' + key + '">' + value + '</option>';
          return _this.$locationSelect.append(locationOption);
        };
      })(this));
      this.$locationSelect.trigger("chosen:updated");
      return this.noLocationResults = false;
    }
  },
  populateResults: function(contactResults) {
    var compiledResults;
    this.killResults();
    compiledResults = resultTemplate(contactResults);
    this.$resultsColumn.html(compiledResults);
    return this.showResults();
  },
  showResults: function() {
    var resultHeight;
    resultHeight = this.$resultsContainer.find(this.$resultsColumn).height();
    return TweenLite.to(this.$resultsContainer, 0.5, {
      height: resultHeight,
      ease: Cubic.easeInOut,
      onComplete: this.enableAfterLoading,
      onCompleteScope: this
    });
  },
  showError: function() {
    var resultHeight;
    this.disableSelect();
    resultHeight = this.$resultsContainer.find(this.$resultsColumn).height();
    return TweenLite.to(this.$resultsContainer, 0.5, {
      height: resultHeight,
      ease: Cubic.easeInOut
    });
  },
  emptyResults: function() {
    return this.$resultsColumn.empty();
  },
  killResults: function() {
    return TweenLite.to(this.$resultsContainer, 0.5, {
      height: 0,
      ease: Cubic.easeInOut,
      onComplete: this.emptyResults,
      onCompleteScope: this
    });
  },
  initEvents: function() {
    this.$productSelect.on('change', (function(_this) {
      return function(e) {
        var productLocations;
        _this.currentProduct = _this.$productSelect.val();
        _this.killResults();
        if (_this.currentProduct === "0") {
          _this.disableSelect();
          return;
        }
        return productLocations = _this.getResult(_this.locationAPI, _this.currentProduct, $.proxy(_this.populateLocSelect, _this));
      };
    })(this));
    return this.$locationSelect.on('change', (function(_this) {
      return function(e) {
        var apiLocation, locationContacts;
        _this.currentLocation = _this.$locationSelect.val();
        _this.killResults();
        if (_this.currentLocation === '0') {
          return;
        }
        apiLocation = _this.currentLocation.replace('-', '/');
        return locationContacts = _this.getResult(_this.contactsAPI, apiLocation, $.proxy(_this.populateResults, _this));
      };
    })(this));
  },
  initCustomSelects: function() {
    return $('select').chosen({
      disable_search: true,
      display_disabled_options: false
    });
  },
  init: function() {
    this.initCustomSelects();
    return this.initEvents();
  }
};

module.exports = contact;
