'use strict';

$ = require('jQuery')

#======================================================================
# Global Search
# this module handles global UI events
#======================================================================

globalSearch =

  $searchTrigger: $('#js-search-trigger')
  $searchSubmit: $('#js-submit-button')
  $searchElement: $('#js-search-block')
  $searchInput: $('#js-search-block').find('input[type="text"]')
  searchURL: "/search?search="

  initEvents: ()->

    @$searchTrigger.on 'click', (e)=>
      if @$searchTrigger.hasClass('active')
        @closeSearch()
      else
        @showSearch()


    #check body clicks and close search menu if clicking outside of it
    $(document).click (e) =>
      if $(e.target).closest(".global-header-outer").length == 0
        @closeSearch()

    #submit value of search when clicking enter (no submit button in design)
    $("#js-global-search").keydown (e) =>
      if e.keyCode is 13
        @submitSearch()

    @$searchSubmit.on 'click', ()=>
      @submitSearch()

    return

  submitSearch: ()->
      searchPath = @searchURL + $("#js-global-search").val()
      window.location.href = searchPath    

  clearSearch: ()->
    @$searchInput.val('')

  closeSearch: () ->
    @$searchTrigger.removeClass('active')
    @$searchElement.addClass('is-collapsed')

  showSearch: () ->
    @clearSearch()
    @$searchTrigger.addClass('active')
    @$searchElement.removeClass('is-collapsed')
    @$searchInput.focus()
  


  init: ()->
    @initEvents()
    @clearSearch()




module.exports = globalSearch