'use strict';

$ = require('jQuery')
Tweenlite = require('TweenLite')
CSSPlugin = require('CSSPlugin')
validate = require('validate')

#======================================================================
# PDF Modals
# this module handles form data collection when clicking some pdfs.
#======================================================================

pdfmodal =
  $modalElements: $('#js-modal-overlay')
  $firstInput: $('#js-modal-overlay').find('.input-text').first()
  $pdfForm: $('#js-pdf-request-form')
  $inputs: $('.input-text', @$pdfForm)
  $formSubmit: $('#js-modal-overlay').find('#js-form-submit')
  $modalClose: $('#js-modal-close-button')
  modalVisible: false
  validator: null

  initEvents: ()->
    $('body').on 'click', 'a.requires-form-input', (e)=>
      e.preventDefault()
      @$pdfForm.data('download', $(e.target).data('upload-id'))
      @showModal()

    $(document).on 'keyup', (e)=>
      if e.keyCode == 27 && @modalVisible == true
        @hideModal()

    @$pdfForm.on 'keyup blur', ()=>
      if @$pdfForm.valid()
        @$formSubmit.removeAttr('disabled')
      else
        @$formSubmit.attr('disabled', "disabled")


    @$modalClose.on 'click', (e)=>
      @hideModal()



  initValidation: ()->
    @validator = @$pdfForm.validate(
      onkeyup: false
      submitHandler: (form)->
        $form = $(form)
        $.post('download/' + $form.data('download'), $form.serialize(), (response)->
          if (response.success)
            window.open response.url, '_self'
        )
      errorPlacement: (error, element)->
        return true
      highlight: (element)->
        $(element).parent('.form-row').addClass('field-error')
      unhighlight: (element)->
        $(element).parent('.form-row').removeClass('field-error')
    )


  showModal: ()->
    unless @modalVisible
      TweenLite.to(@$modalElements, 0.1, {opacity: 1, ease: Cubic.easeInOut});
      @$modalElements.addClass('is-open')
      @$firstInput.focus()
      @$inputs.val('')
      @validator.resetForm()
      @modalVisible = true

  hideModal: ()->
    TweenLite.to(@$modalElements, 0.1, {
      opacity: 0, ease: Cubic.easeInOut, onCompleteScope: @, onComplete: ()->
        @$modalElements.removeClass('is-open')
    });

    @modalVisible = false


  init: ()->
    @initEvents()
    @initValidation()



module.exports = pdfmodal
