'use strict';

$ = require('jQuery')

#======================================================================
# Analytics
# Handles tracking events for analytics
#======================================================================

analytics =
  init: ()->
    # Links
    $('body').on 'click', 'a', () ->
      $link = $ this
      href = $link.attr 'href'

      if !href
        return

      if /^mailto:/.test href
        # Send email
        ga('send', 'event', 'Contact', 'Send Email', href.replace('mailto:', ''))
      else if /\.pdf$/.test href
        # Download
        ga('send', 'event', 'Download', 'Download', href)
      else if this.host != window.location.host
        # External link
        ga('send', 'event', 'External Link', 'Click', href)

    # Buttons
    $('body').on 'click', 'button', () ->
      $button = $ this

      if $button.attr('id') == 'js-submit-study-filters'
        # Submit Filter
        ga('send', 'event', 'Case Study Filter', 'Filter', null)
      else if $button.attr('id') == 'js-reset-study-filters'
        # Reset Filter
        ga('send', 'event', 'Case Study Filter', 'Reset', null)

    # Contact Product/Location
    $('#js-contact-product-select').on 'change', () ->
      ga('send', 'event', 'Contact', 'Product', $('option:selected', this).text())
    $('#js-contact-location-select').on 'change', () ->
      ga('send', 'event', 'Contact', 'Location', $('option:selected', this).text())

module.exports = analytics
