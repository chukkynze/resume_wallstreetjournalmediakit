'use strict';
$ = require('jQuery')
_ = require('lodash')
Tweenlite = require('TweenLite')
CSSPlugin = require('CSSPlugin')

#======================================================================
# Viewport Resize
# This handles taking hero images and making them full size
#======================================================================

viewportResize =

  heroBlock: $('.js-viewport-scale')
  viewportHeight: null
  initialLoad: false

  initEvents: ()->
    $(window).resize _.debounce(=>
      @scaleToViewport()
      return
    , 100)


  scaleToViewport: ()->
    @viewportHeight = $(window).height()

    if @initialLoad is false
      @heroBlock.css('height', @viewportHeight)
      @initialLoad = true
      @heroBlock.addClass('is-visible')

    else
      TweenLite.to(@heroBlock, 0.2, {
        ease: Cubic.easeOut,
        height: @viewportHeight
      });

  init: ()->
    @initEvents()
    @scaleToViewport()
    


module.exports = viewportResize