'use strict';

$ = require('jQuery')


#======================================================================
# css overrides
# adds special classes to elements that css can't style (based on comps)
#======================================================================

cssoverrides =



  # we need to makes sure the first x modules that are shallower than the right col have a narrower width.  this applies the narrow width to the first 2 if needed.  increase to 3 if there is a long right col and 3 shallow modules.
  
  setModuleWidth: ()->

    #set all widths smaller for product landing page
    if $('.product-listing').length

      $('.module').each (i)->
        $(this).addClass 'narrow-module'
      return


    #only do the first two modules on the detail pages
    if $('.col-right').length
      rightColOffset = $('.col-right').outerHeight(true)
      firstModuleHeight = null

      $('.module').each (i)->

        if i == 0
          $(this).addClass 'narrow-module'
          firstModuleHeight = $(this).outerHeight(true)

        if i == 1
          if firstModuleHeight < rightColOffset
              $(this).addClass 'narrow-module'



  init: ()->
    @setModuleWidth()


module.exports = cssoverrides
