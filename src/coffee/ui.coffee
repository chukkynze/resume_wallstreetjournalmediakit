'use strict';

$ = require('jQuery')
pdfmodal = require('./components/pdf-modal/pdf-modal')
viewportresize = require('./components/viewport-resize/viewport-resize')
analytics = require('./components/analytics/analytics')
modules = require('./modules/module-shared/modules-init')
header = require('./modules/header/header')
cssoverrides = require('./components/css-overrides/css-overrides')


#======================================================================
# UI
# this module handles global UI events
#======================================================================

ui =

  initEvents: ()->


  init: ()->
    @initEvents()
    pdfmodal.init()
    viewportresize.init()
    analytics.init()
    modules.init()
    section = $('body').data('path')
    header.init()
    cssoverrides.init()


    if section
      switch section
        when 'contact' then module = require('./modules/contact/contact')
        when 'index' then module = require('./modules/index/index')
        when 'rates' then module = require('./modules/rates-specs/rates')
        when 'gallery' then module = require('./modules/case-studies/casestudies')


    if module?.init?
      module.init()


module.exports = ui
