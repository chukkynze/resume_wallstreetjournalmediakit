'use strict';

$ = require('jQuery')
plaecholder = require('placeholder')
globalsearch = require('../../components/global-search/global-search')


#======================================================================
# HEADER
# initializes all components within header
#======================================================================

header =

  initTabletFixes: ()->

    #on tablets, clicking the link should open the submenu rather than a direct link into the landing page
    $('.primary-parent-link > a').on 'touchend', (e)->
      e.preventDefault()
      $clickTarget = $(e.currentTarget)
      $clickTargetParent = $clickTarget.closest('.primary-parent-link')

      if $clickTargetParent.hasClass('is-open')
        $clickTargetParent.removeClass('is-open')

      else
        $clickTargetParent.addClass('is-open')

      $clickTarget.blur()
      $clickTargetParent.blur()

    #hide menu when open and scrolling
    $('body').bind 'touchmove', ()->
      $('.primary-parent-link.is-open', '.primary-menu').removeClass('is-open')

  initPlaceholders: ()->
    $('input, textarea').placeholder()

  init: ()->
    @initPlaceholders()
    @initTabletFixes()
    globalsearch.init()

module.exports = header