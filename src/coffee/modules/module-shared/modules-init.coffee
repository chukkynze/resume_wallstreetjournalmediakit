'use strict';

$ = require('jQuery')

#======================================================================
# modules
# This initializes any module-level js throughout the site.
#======================================================================

modules =


  init: ()->

    $('.module').each ()->
      
      if $(this).attr "data-js-module"
        moduleName = $(this).attr "data-js-module"

        if moduleName
          switch moduleName
            when 'slideshow' then module = require('../slideshow/slideshow-module')

        if module?.init?
          module.init()


module.exports = modules