'use strict';

$ = require('jQuery')
cycle = require('cycle')

#======================================================================
# Case Studies
# this module handles all interactivity for the case studies parent/children
#======================================================================

index =

  initEvents: ()->

    #first slide is 2 second timeout, then after the first set it to 4 seconds.
    @slideShow.on 'cycle-before', (event, opts)->
      opts.timeout = 4000

  initSlides: ()->
    @slideShow.cycle(
      speed: 300
      timeout: 2000
      slides: '> .slide'
      pauseOnHover: true
      swipe: true
      log: false
      fx: 'fade'
    )


  init: ()->
    @slideShow = $('#js-landing-marquee')
    @initEvents()
    @initSlides()



module.exports = index