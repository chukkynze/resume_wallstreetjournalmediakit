'use strict';

$ = require('jQuery')
selectInterface = require('../../components/select-interface')
resultTemplate = require('./rates-result.js')
classifiedTemplate = require('./rates-result-classified.js')
Tweenlite = require('TweenLite')
CSSPlugin = require('CSSPlugin')


#======================================================================
# Contact
# this module handles all interactivity for the case studies parent/children
#======================================================================

rates =
  $productSelect: $('#js-rates-product-select')
  $resultsContainer: $('#js-rates-results')
  $resultsColumn: $('#js-option-result-column')
  ratesAPI: '/admin/rates_specs/get-rate-specs/'
  currentLocationData: null
  currentProduct: null
  currentLocation: null
  ajaxCall: null
  requestURL: null
  isProcessing: false
  noResults: false



#get results from endpoint
  getResult: (url, selection, callback)->
    requestURL = url + selection

    $.ajax(
      url: requestURL
      type: "GET"
      dataType: "json"
    )
    .done (data)=>
      callback(data)

    .fail (data)=>
      @$resultsColumn.html('<p>Something went wrong. Please try your selection again.</p>')
      @showResults()



#disabled state for second select when there aren't location results
  disableSelect: ()->
    @killResults()
    @$productSelect.find('option:gt(0)').remove();
    @$productSelect.attr('disabled', true).trigger("chosen:updated")



#this is a disabled state, but with a loading animation.   it's applied when getting results.
  disableWhileLoading: ()->
    @$productSelect.attr('disabled', true).trigger("chosen:updated")
    $selectContainer = @$productSelect.siblings('.chosen-container')
    $selectContainer.addClass('is-loading')



  enableAfterLoading: ()->
    $selectContainer = @$productSelect.siblings('.chosen-container')
    $selectContainer.removeClass('is-loading')
    @$productSelect.removeAttr('disabled').trigger("chosen:updated")
    $selectContainer = @$productSelect.siblings('.chosen-container')
    $selectContainer.removeClass('is-loading')




#show all results from choices
  populateResults: (ratesResult)->
    compiledResults = null
    compiledLinkResults = null

    # if a classified link is returned handle it as a link:
    if ratesResult.classified_text || ratesResult.submit_ad_link
      compiledLinkResults = classifiedTemplate(ratesResult)
    else
      compiledLinkResults = null

    # if actual pdf data exists, handle it accordingly
    if ratesResult.data[0].file.length
      compiledResults = resultTemplate(ratesResult.data)

    #add all actual links to pdfs in place of what was there before.
    if compiledResults
      @$resultsColumn.html(compiledResults)

    #if classified link exists, append it last
    if compiledLinkResults
      @$resultsColumn.append(compiledLinkResults)




    @showResults()



  showResults: ()->
    resultHeight = @$resultsContainer.find(@$resultsColumn).height()
    @$resultsContainer.height(resultHeight)
    @enableAfterLoading()



  emptyResults: ()->
    @$resultsColumn.empty()



  killResults: ()->
    # TweenLite.to(@$resultsContainer, 0.5,
    #   {height: 0, ease: Cubic.easeInOut, onComplete: @emptyResults, onCompleteScope: @});
    @$resultsContainer.height(0)
    @emptyResults()

  initEvents: ()->
    #events for product select
    @$productSelect.on 'change', (e)=>
      @currentProduct = @$productSelect.val()
      @killResults()
      @disableWhileLoading()

      #if they chose the default
      if @currentProduct == "0"
        @emptyResults()
        @enableAfterLoading()
        return

      productLocations = @getResult(@ratesAPI, @currentProduct, $.proxy(@populateResults, @))



  init: ()->
    selectInterface.init()
    @initEvents()


module.exports = rates