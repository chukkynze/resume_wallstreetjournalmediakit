'use strict';

$ = require('jQuery')
resultTemplate = require('./contact-result.js')
Tweenlite = require('TweenLite')
CSSPlugin = require('CSSPlugin')
chosen = require('chosen')


#======================================================================
# Contact
# this module handles all interactivity for the case studies parent/children
#======================================================================

contact =
  $productSelect: $('#js-contact-product-select')
  $locationSelect: $('#js-contact-location-select')
  $resultsContainer: $('#js-contact-results')
  $resultsColumn: $('#js-option-result-column')
  locationAPI: '/admin/contacts/get-locations/'
  contactsAPI: '/admin/contacts/get-all/'
  currentLocationData: null
  currentProduct: null
  currentLocation: null
  ajaxCall: null
  requestURL: null
  isProcessing: false
  noResults: false


#get results from endpoint
  getResult: (url, selection, callback)->
    requestURL = url + selection

    ajaxRequest = $.ajax(
      url: requestURL
      type: "GET"
      dataType: "json"
    )

    ajaxRequest.done (data)=>
      callback(data)

    ajaxRequest.fail (data)=>
      @$resultsColumn.html('<p>Something went wrong. Please try your selection again.</p>')
      @showError()



#disabled state for second select when there aren't location results
  disableSelect: ()->
    @killResults()
    @$locationSelect.find('option:gt(0)').remove();
    @$locationSelect.attr('disabled', true).trigger("chosen:updated")

#this is a disabled state, but with a loading animation.   it's applied when getting results.
  disableWhileLoading: ()->
    @$locationSelect.attr('disabled', true).trigger("chosen:updated")
    $selectContainer = @$locationSelect.siblings('.chosen-container')
    $selectContainer.addClass('is-loading')

  enableAfterLoading: ()->
    #since we disabled the select eariler, enable again unless there are no location results
    return if @noLocationResults

    @$locationSelect.removeAttr('disabled').trigger("chosen:updated")
    $selectContainer = @$locationSelect.siblings('.chosen-container')
    $selectContainer.removeClass('is-loading')


  populateLocSelect: (locations)->

    #if no locations, disable select and skip location site to get contacts
    if locations[Object.keys(locations)[0]] == 'None'
      @disableSelect()
      @noLocationResults = true
      #get only key to get location
      for key of locations
        apiLocation = key.replace('-', '/')
        locationContacts = @getResult(@contactsAPI, apiLocation, $.proxy(@populateResults, @))

    else
      @$locationSelect.find('option:gt(0)').remove();
      @$locationSelect.removeAttr('disabled')
      $.each locations, (key, value)=>
        locationOption = '<option value="' + key + '">' + value + '</option>'
        @$locationSelect.append(locationOption)

      @$locationSelect.trigger("chosen:updated")
      @noLocationResults = false


#show all results from choices
  populateResults: (contactResults)->
    @killResults()
    compiledResults = resultTemplate(contactResults)
    @$resultsColumn.html(compiledResults)

    @showResults()

  showResults: ()->
    resultHeight = @$resultsContainer.find(@$resultsColumn).height()
    TweenLite.to(@$resultsContainer, 0.5,
      {height: resultHeight, ease: Cubic.easeInOut, onComplete: @enableAfterLoading, onCompleteScope: @});

  showError: ()->
    @disableSelect()
    resultHeight = @$resultsContainer.find(@$resultsColumn).height()
    TweenLite.to(@$resultsContainer, 0.5, {height: resultHeight, ease: Cubic.easeInOut});

  emptyResults: ()->
    @$resultsColumn.empty()

  killResults: ()->
    TweenLite.to(@$resultsContainer, 0.5,
      {height: 0, ease: Cubic.easeInOut, onComplete: @emptyResults, onCompleteScope: @});


  initEvents: ()->

    #events for product select
    @$productSelect.on 'change', (e)=>
      @currentProduct = @$productSelect.val()
      @killResults()


      if @currentProduct == "0"
        @disableSelect()
        return

      productLocations = @getResult(@locationAPI, @currentProduct, $.proxy(@populateLocSelect, @))


    #events for location select
    @$locationSelect.on 'change', (e)=>
      @currentLocation = @$locationSelect.val()
      @killResults()

      #if trying default option
      if @currentLocation == '0'
        # @disableSelect()
        return

      apiLocation = @currentLocation.replace('-', '/')
      locationContacts = @getResult(@contactsAPI, apiLocation, $.proxy(@populateResults, @))


  initCustomSelects: ()->
    $('select').chosen({
      disable_search: true
      display_disabled_options: false
    })



  init: ()->
    @initCustomSelects()
    @initEvents()


module.exports = contact