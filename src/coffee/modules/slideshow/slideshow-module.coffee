'use strict';
$ = require('jQuery')
cycle = require('cycle')

#======================================================================
# Slideshow
# Initializes slideshows anywhere they appear
#======================================================================

slideshow =

  $slideContainer: $('.js-slideshow')


  initSlideshow: ()->

    @$slideContainer.each ()->

      $this = $(this)

      $prevButton = $this.siblings('.prev')
      $nextButton = $this.siblings('.next')

      $this.cycle (
        speed: 300
        timeOut: 0
        prev: $prevButton
        next: $nextButton
        slides: '> .slide'
        swipe: true
        log: false
        fx: 'fade'
      )

  init: ()->
    @initSlideshow()


module.exports = slideshow