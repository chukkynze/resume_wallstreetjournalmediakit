'use strict';
$ = require('jQuery')
chosen = require('chosen')
Tweenlite = require('TweenLite')
CSSPlugin = require('CSSPlugin')
cycle = require('cycle')

#======================================================================
# Case Studies
# this module handles all interactivity for the case studies parent/children
#======================================================================

casestudies =

  view: '.filter-columns'
  $detailSlides: $('#js-study-slideshow')
  filterForm: $('#js-studies-filters')
  $allSelects: $('.study-product-select', @view)
  $allStudies: $('.study-summary', '#js-filter-results')
  $selectedFilters: $('.selected-filter', '#selected-filters')
  $resultsContainer: $('#js-filter-results')
  $errorContainer: $('#js-filter-error-message')
  $customizedSelects: null
  $filterOptions: null
  $filterOptionCheck: null
  filteredItems: []



#handle events
  bindEvents: ()->
    _self = @

    $('#js-submit-study-filters').on 'click', ()=>
      TweenLite.to(@$resultsContainer, 0.2,
        {opacity: 0, ease: Cubic.easeInOut, onComplete: @submitFilters, onCompleteScope: @});


    $('#js-reset-study-filters').on 'click', ()=>
      # @$selectedFilters.addClass('is-invisible')
      TweenLite.to(@$resultsContainer, 0.2,
        {opacity: 0, ease: Cubic.easeInOut, onComplete: @resetSelects, onCompleteScope: @});

    $('#selected-filters').on 'click', '.selected-filter', (e)=>
      e.preventDefault()
      @activateOption($('.filter-columns .option[data-val="' + $(e.target).data('filter-id') + '"]'))
      TweenLite.to(@$resultsContainer, 0.2,
        {opacity: 0, ease: Cubic.easeInOut, onComplete: @submitFilters, onCompleteScope: @});

    #close if clicking outside the entire checkbox
    $(document).on 'click', (e)=>
      @$customizedSelects.removeClass('is-expanded') if @$customizedSelects.hasClass('is-expanded') unless $(e.target).closest(@$customizedSelects).length

    #activate option when clicking on option itself or child checkbox
    $('.filter-columns').on 'click', '.option', (e)=>
      if $(e.target).hasClass('.option')
        $option = $(e.target)
      else
        $option = $(e.target).closest('.option')

      @activateOption($option)


#convert standard select into custom element
  initSelects: ()->
    $wrapper = '<div class="custom-multi-select"></div>'
    _self = @

    #build fake selects based on actual select
    $('select', @view).each ()->
      $(this).wrap($wrapper)
      $(this).before('<div class="select-trigger"></div><ul class="select-options"></ul>')
      $trigger = $(this).siblings('.select-trigger')
      $options = $(this).siblings('.select-options')
      $select = $(this)
      $select.hide()
      $trigger.html($select.find('option:eq(0)').html())

      $select.find('option:eq(0)').remove()
      $select.find('option').each (option) ->
        choice = '<li class="option" data-val="' + $(this).val() + '"><input type="checkbox" class="option-checkbox" />' + $(this).text() + '</li>'
        $options.append(choice)

      #open options when clicking
      $trigger.on 'click', (e)->
        $selectWrapper = $(e.target).closest('.custom-multi-select')
        _self.showDropdown($selectWrapper)

    #cache new selectors for later
    @$customizedSelects = $('.custom-multi-select')
    @$filterOptions = $('.option', $('.filter-columns'))
    @$filterCheck = $('.option-checkbox', $('.filter-columns'))




#clicked an option or option's checkbox
  activateOption: ($option)->
    $optionCheck = $option.find('.option-checkbox')
    filterTag = $option.data('val')

    if $option.hasClass('is-selected')
      $option.removeClass('is-selected')
      $optionCheck.prop('checked', false)
      @removeFilter(filterTag)

    else
      $option.addClass('is-selected')
      $optionCheck.prop('checked', true)
      @addFilter(filterTag)


  resetOddElements: () ->
    @$allStudies.removeClass('odd')


#reset all filters
  resetSelects: ()->
    @hideFilterError()
    @$customizedSelects.find('option:selected').removeAttr('selected')
    @$filterOptions.removeClass('is-selected')
    @$filterCheck.prop('checked', false)
    @filteredItems = []
    @resetOddElements()
    @$allStudies.not(':odd').addClass('odd')
    @$allStudies.removeClass('is-invisible')
    @$selectedFilters.parents('#selected-filters').addClass('is-invisible')
    @revealResults()




#add selected element to filter array
  addFilter: (filter)->
    @filteredItems.push(filter)


#remove selected element from filter array
  removeFilter: (filter)->
    x = @filteredItems.indexOf(filter)
    @filteredItems.splice(x, 1) if x != -1



#submit selected filters
  submitFilters: ()->

    @$selectedFilters.parents('#selected-filters').removeClass('is-invisible')
    @$allStudies.addClass('is-invisible')
    @$selectedFilters.addClass('is-invisible')
    @resetOddElements()

    newSelector = ""
    for item, i in @filteredItems
      newSelector += ", " if i != 0
      newSelector += ".#{item}"
      @$selectedFilters.filter('[data-filter-id="' + item + '"]').removeClass('is-invisible')

    #nothing selected
    if !@filteredItems.length
      @resetSelects()
    #no results per current filter
    else if $(newSelector).length < 1 || $(newSelector) == undefined
      @displayFilterError()
    #normal filtering
    else
      @hideFilterError()
      $(newSelector).not(':odd').addClass('odd')
      $(newSelector).removeClass('is-invisible')

    @revealResults()




#show filtered results after fading
  revealResults: ()->
    TweenLite.to(@$resultsContainer, 0.1, {opacity: 1, ease: Cubic.easeInOut, delay: 1});




#showing opened select titles
  showDropdown: (clickedSelect)->
    $('.custom-multi-select', '.filter-columns').removeClass('is-expanded')

    if clickedSelect.hasClass('is-expanded')
      clickedSelect.removeClass('is-expanded')
    else
      clickedSelect.addClass('is-expanded')


  displayFilterError: ()->
    TweenLite.to(@$errorContainer, 0.1, {opacity: 1, ease: Cubic.easeInOut});


  hideFilterError: ()->
    TweenLite.to(@$errorContainer, 0.1, {opacity: 0, ease: Cubic.easeInOut});

  init: ()->
    @initSelects()
    @bindEvents()



module.exports = casestudies
